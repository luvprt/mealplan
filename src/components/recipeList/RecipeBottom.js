import React, { Component } from 'react';
import ReactResizeDetector from 'react-resize-detector';
import history from '../../history'

class RecipeBottom extends Component {
  constructor(props) {
    super(props)
    this.state = {
      recipeArr: props.recipeArr,
      lb: props.lb,
      ub: props.ub,
      recipeIndex: parseInt((9*window.innerWidth)/1299,0),
      seeAll: false,
      seeText: 'MORE SEE ALL',
      showMeal: 'mb-hide',
      seeTextClass:'',
      cookingDates: props.cookingDates,
      dinnerDates: props.dinnerDates,
      lunchDates: props.lunchDates,
      servingSize: props.servingSize,
      inputs: props.inputs,
      variety: props.variety,
      varietyIndex: props.varietyIndex,
      dayDates: props.dayDates,
      recipeCount: props.recipeCount,
      animateClass: props.animateClass
    }
  }

  // eslint-disable-next-line
  UNSAFE_componentWillReceiveProps(props) {
    this.setState({
      recipeArr: props.recipeArr,
      lb: props.lb,
      ub: props.ub,
      cookingDates: props.cookingDates,
      servingSize: props.servingSize,
      dinnerDates: props.dinnerDates,
      lunchDates: props.lunchDates,
      inputs: props.inputs,
      variety: props.variety,
      varietyIndex: props.varietyIndex,
      dayDates: props.dayDates,
      animateClass: props.animateClass,
      recipeCount: props.recipeCount,
    })
  }

  onResize() {
    let { showRecipeView } = this.state;
    if (window.innerWidth < 769 || window.outerWidth <769) {
      showRecipeView = true;
    } else {
      showRecipeView = false;
    }
    this.setState({ showRecipeView });
  }

  onPressDone() {
    const {
      cookingDates, servingSize, lunchDates, dinnerDates, variety, varietyIndex, 
      inputs, recipeArr, dayDates, lb, ub, recipeCount
    } = this.state
    window.analytics.track('Select Meals Completed', {
      numberOfRecipesSelected: recipeArr.length
    });
    sessionStorage.setItem('recipeArr', JSON.stringify(recipeArr))
    history.push('/setCookSchedule',{
      cookingDates, servingSize, recipeArr, recipeRange:`${lb}-${ub}`, 
      lunchDates, dinnerDates, variety, varietyIndex, inputs, dayDates, recipeCount
    }
    )
  }

  removeRecipe(data, key, id) {
    const { recipeArr, showRecipeView } = this.state
    const item = parseInt((9*window.innerWidth)/1299,0) + 1
    if(recipeArr.length <= item) {
      this.setState({
        seeTextClass : '',
        seeText:'MORE SEE ALL',
        seeAll: false,
        recipeIndex: parseInt((9*window.innerWidth)/1299,0)
      })
    }
    if(recipeArr.length === 1 && showRecipeView) {
      this.setState({ showMeal: "mb-hide"})
    }
    this.props.removeRecipe(data, key, null, null, id)
  }
  

  showRecipe() {
    const { recipeArr, recipeIndex, seeText } = this.state
    return recipeArr.map((value, index)=>{
      if(index < recipeIndex ||  seeText === 'COLLAPSE' || window.innerWidth < 769) {
        return(
          <div className="selected-meal-thumbnail" id='selected-meal-thumbnail' key={value.index} title={value.index}>

            <a onClick={()=>this.removeRecipe(`${value.index}`, value.key, value.id)} className="cross-icn">
              <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg"
                xmlnsXlink="http://www.w3.org/1999/xlink">
                <title>Close</title>
                <desc>Created with Sketch.</desc>
                <defs />
                <g id="Select-Meals" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                  <g id="Mobile:-Select-Meals-4b" transform="translate(-188.000000, -1423.000000)">
                    <g id="Bottom-Bar:-Apply-Filters-Button" transform="translate(0.000000, 1416.000000)">
                      <g id="Group-5-Copy-13" transform="translate(116.000000, 7.000000)">
                        <g id="Close" transform="translate(72.000000, 0.000000)">
                          <circle id="Oval" fill="#546E7A" cx="10" cy="10" r="10" />
                          <polygon id="Shape" fill="#FFFFFF" points="14.6666667 6.26666667 13.7333333 5.33333333 10 9.06666667 6.26666667 5.33333333 5.33333333 6.26666667 9.06666667 10 5.33333333 13.7333333 6.26666667 14.6666667 10 10.9333333 13.7333333 14.6666667 14.6666667 13.7333333 10.9333333 10" />
                        </g>
                      </g>
                    </g>
                  </g>
                </g>
              </svg>
            </a>
            <img src={value.image} alt=""/>
          </div>
        )}
      return null;
    }
    )
  }

  expandRecipe(){
    const { recipeArr, seeAll } = this.state;
    let { seeText, recipeIndex, seeTextClass } = this.state
    if(seeAll) {
      seeText= `MORE SEE ALL`
      recipeIndex = parseInt((9*window.innerWidth)/1299,0)
      seeTextClass = ''
    } else {
      seeText = 'COLLAPSE'
      recipeIndex= recipeArr.length
      seeTextClass = 'uncollapsed'
    }
    this.setState({ recipeIndex, seeAll: !seeAll, seeText, seeTextClass })
  }

  showMobileMealView() {
    let { showMeal } = this.state
    const { showRecipeView } = this.state
    if(showRecipeView) {
      if(showMeal === 'mb-hide') {
        showMeal = 'mb-only'
      } else {
        showMeal = 'mb-hide'
      }
      this.setState({ showMeal })
    }
  }

  render() {
    const { 
      recipeArr, lb, ub, recipeIndex, seeText, showMeal, seeTextClass, animateClass
    } = this.state
    let selectRecipe = ''
    let count = 0
    if(recipeArr && recipeArr.length > 0) {
      selectRecipe = 'selected'
      count = recipeArr.length
    }
    const recommendText = ub - count > 0 ? `We suggest ${lb-count > 0 ? lb-count : 0}-${ub-count} more...` : ub-count === 0 ? window.innerWidth > 768 ? "You're looking good!" : "Looking Good!" : `${count-ub} over recommended`
    const recipeText = count === 1 ? 'Recipe' : 'Recipes'
    return(
      <div className={`choose-recipe-btns select-meal-btns ${animateClass} ${selectRecipe} ${seeTextClass}`}>
        <ReactResizeDetector handleWidth handleHeight onResize={() => this.onResize()} />
        <div className="container">
          <div className="recipe-selected-status">
            <div className="recipe-icn" onClick={()=>recipeArr.length > 0 && this.showMobileMealView()}>
              <svg width="28px" height="28px" viewBox="0 0 28 28" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                <title>Shape</title>
                <desc>Created with Sketch.</desc>
                <defs />
                <g id="New-UX-Updated" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                  <g id="Desktop:-Select-Meals-2" transform="translate(-103.000000, -2057.000000)" fill="#FFFFFF" fillRule="nonzero">
                    <g id="Bottom-Bar:-Apply-Filters-Button" transform="translate(0.000000, 2020.000000)">
                      <g id="Indicators-&amp;-Recipes" transform="translate(93.000000, 6.000000)">
                        <g id="Indicators" transform="translate(0.000000, 11.000000)">
                          <g id="Meal-Tray" transform="translate(0.000000, 2.000000)">
                            <g id="Button" transform="translate(0.000000, 8.000000)">
                              <path d="M18.1418984,25.6571749 L22.0887092,21.5007562 L12.298387,11.205175 C10.1227599,13.4963458 10.1227599,17.2121547 12.298387,19.5180124 L18.1418984,25.6571749 Z M27.5975087,22.9988294 C29.7312969,24.0416058 32.7297573,23.3072562 34.9472235,20.9720245 C37.6109721,18.1668091 38.1269862,14.1425734 36.076876,11.9835856 C34.0407122,9.83928481 30.2194183,10.3680165 27.5417234,13.1732319 C25.3242572,15.5084636 24.6269409,18.6661668 25.6171301,20.9132766 L12.0055141,35.2477805 L13.9719464,37.3186463 L23.5809663,27.228683 L33.1760398,37.3333333 L35.1424721,35.2624675 L25.5473985,25.1578172 L27.5975087,22.9988294 Z"
                                id="Shape" />
                            </g>
                          </g>
                        </g>
                      </g>
                    </g>
                  </g>
                </g>
              </svg>
              <span className="count">{count}</span>
            </div>
            <h3 className="mb-hide">{count} {recipeText}</h3>
            <h3 className="mb-only">{count} {recipeText} Added</h3>
            <span className="othr">{count === 0 ? 'Select + to get started!': recommendText}</span>
         
           
          </div>

          <div className={`selected-meal-thumbnail-box ${showMeal}`}>
            <div className="select-bottom-dashed">
              <span>select + to add a meal</span>
            </div>
            {
              count > 0 &&
              this.showRecipe()
            }

          </div>
          {count > parseInt((9*window.innerWidth)/1299,0) && <a onClick={()=>this.expandRecipe()} className="selected-meal-see-more" style={{width:'5%'}}>{count-recipeIndex > 0 && seeText !== 'COLLAPSE' && `+${count-recipeIndex}`} {seeText}
            {
              seeText === 'COLLAPSE' && count > parseInt((9*window.innerWidth)/1299,0)  &&
            <svg
              className="svIcon"
              enableBackground="new 0 0 48 48"
              height="10px"
              id="Layer_3"
              version="1.1"
              viewBox="0 0 48 48"
              width="10px"
              xmlSpace="preserve"
              xmlns="http://www.w3.org/2000/svg"
              xmlnsXlink="http://www.w3.org/1999/xlink"
            >
              <polygon fill="#241F20" points="0,12.438 48,12.438 24,35.562 " />
            </svg>
            }
          </a>
          }
          <div className="recipe-done">
            <button 
              type="button" 
              className="next-btn" 
              disabled={count === 0} 
              onClick={()=>
                this.onPressDone()}
            >
              <span aria-label="Next">Next</span>
            </button>
          </div>
        </div>
      </div>
    )
  }

}

export default RecipeBottom