import React from 'react'
import LazyLoad from 'react-image-lazy-load';
import moment from 'moment'
// import { browserName } from 'react-device-detect';
import { getTimeStamp, differnceTimeStamp,sortByKey } from 'lib/services';
// import { getTimeStamp, differnceTimeStamp,sortByKey, totalValue } from 'lib/services';
import _ from 'lodash'

export const CookingDays = (props) => {
  let { cookingDate } = props
  const { cookArr, index } = props
  cookingDate = cookingDate.sort(sortByKey('cooking_date','asc'))
  return cookingDate.map((days)=>{
    const data = moment(days.cooking_date).format('ddd')
    const cookIndex = cookArr.findIndex(x=>x.data === data && x.key === index)
    // const listClass = browserName.indexOf('Edge') > -1 ? 'ie-design' : ''
    let check;
    // console.log(browserName)
    if(cookIndex > -1){
      check = true
    }
    else {
      check = false
    }
    return(
      <li key={index+data}>
        <input type="checkbox" defaultChecked={check} onChange={()=>props.selectCookDay(index, data, days.cooking_date)} name={index} />
        <span className="checkmark1 mb-hide alignt" tabIndex="0" role="button">
          {data}
        </span>
        <span className="checkmark1 mb-only alignt" tabIndex="0" role="button">
          { data !== 'Thu' && data!== 'Sat' ? data.split('')[0] : data.substring(0,2)}
        </span>
      </li>
    )
  })
}

export const PlusIcon = () => (
  <svg width="38px" height="38px" viewBox="0 0 38 38" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
    <title>Add Button</title>
    <desc>Created with Sketch.</desc>
    <defs />
    <g id="Adjust-Servings" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g id="ES:-Adjust-Servings-1" transform="translate(-287.000000, -456.000000)" fill="#FFFFFF" stroke="#00C853">
        <g id="Large-Card-v2-Copy-3" transform="translate(16.000000, 119.000000)">
          <g id="Edit-Servings" transform="translate(0.000000, 326.000000)">
            <g id="Add-Button" transform="translate(272.000000, 12.000000)">
              <path d="M17.75,18 L17.75,11 L17.75,18 L10.5,18 L17.75,18 Z M17.75,18 L17.75,26 L17.75,18 L25.5,18 L17.75,18 Z M18,36 C8.0588745,36 0,27.9411255 0,18 C0,8.0588745 8.0588745,0 18,0 C27.9411255,0 36,8.0588745 36,18 C36,27.9411255 27.9411255,36 18,36 Z" id="selection" />
            </g>
          </g>
        </g>
      </g>
    </g>
  </svg>
)

export const SubIcon = () => (
  <svg width="38px" height="38px" viewBox="0 0 38 38" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
    <title>Subtract Button</title>
    <desc>Created with Sketch.</desc>
    <defs />
    <g id="Adjust-Servings" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
      <g id="ES:-Adjust-Servings-1" transform="translate(-35.000000, -457.000000)" fill="#FFFFFF" stroke="#FF003D">
        <g id="Large-Card-v2-Copy-3" transform="translate(16.000000, 119.000000)">
          <g id="Edit-Servings" transform="translate(0.000000, 326.000000)">
            <g id="Subtract-Button" transform="translate(20.000000, 13.000000)">
              <path d="M18,36 C8.0588745,36 0,27.9411255 0,18 C0,8.0588745 8.0588745,0 18,0 C27.9411255,0 36,8.0588745 36,18 C36,27.9411255 27.9411255,36 18,36 Z M10.5,18 L25.5,18 L10.5,18 Z" id="selection" />
            </g>
          </g>
        </g>
      </g>
    </g>
  </svg>
)

export const ImageIcon = (props) => {
  const { totalHands, totalTime } = props
  return(
    <div>
      <div className="meal-post-data-box meal-post-data-data">
        <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">

          <title>Icon / Action / Hands On Time 2 Copy</title>
          <desc>Created with Sketch.</desc>
          <defs>
            <path d="M14.8177435,19 L9.60679846,19 C8.7475892,19 7.90429122,18.6623593 7.30761813,18.0771155 L1.5,12.3672363 L3.14681774,10.9941642 C3.6400675,10.5814923 4.36403086,10.4989579 4.94479267,10.7915798 L6.86210222,11.75198 L6.86210222,4.58649437 C6.86210222,3.55106294 7.75313404,2.7107128 8.85101254,2.7107128 C8.98625844,2.7107128 9.12150434,2.72571905 9.25675024,2.74822843 C9.32835101,1.77282201 10.1875603,1 11.2377049,1 C11.9218901,1 12.5185632,1.32263443 12.876567,1.81784077 C13.1072806,1.72780325 13.3618611,1.68278449 13.6243973,1.68278449 C14.7222758,1.68278449 15.6133076,2.52313464 15.6133076,3.55856607 L15.6133076,3.76865361 C15.7405979,3.74614423 15.8758438,3.73113797 16.0110897,3.73113797 C17.1089682,3.73113797 18,4.57148812 18,5.60691955 L18,15.9987495 C18,17.6569404 16.5759402,19 14.8177435,19 Z M3.79122469,12.4572739 L8.45323047,17.0341809 C8.75554484,17.3268028 9.16923819,17.4993747 9.59884282,17.4993747 L14.8177435,17.4993747 C15.692864,17.4993747 16.4088717,16.8240934 16.4088717,15.9987495 L16.4088717,5.60691955 C16.4088717,5.39683201 16.2338476,5.23176323 16.0110897,5.23176323 C15.7883317,5.23176323 15.6133076,5.39683201 15.6133076,5.60691955 L15.6133076,9.99624844 L14.0221794,9.99624844 L14.0221794,3.55856607 C14.0221794,3.34847853 13.8471553,3.18340975 13.6243973,3.18340975 C13.4016393,3.18340975 13.2266152,3.34847853 13.2266152,3.55856607 L13.2266152,9.99624844 L11.635487,9.99624844 L11.635487,2.87578158 C11.635487,2.66569404 11.4604629,2.50062526 11.2377049,2.50062526 C11.014947,2.50062526 10.8399229,2.66569404 10.8399229,2.87578158 L10.8399229,9.99624844 L9.2487946,9.99624844 L9.2487946,4.58649437 C9.2487946,4.37640684 9.07377049,4.21133806 8.85101254,4.21133806 C8.62825458,4.21133806 8.45323047,4.38390996 8.45323047,4.58649437 L8.45323047,14.2430179 L4.19696239,12.1196332 L3.79122469,12.4572739 Z" id="path-1" />
          </defs>
          <g id="Icon-/-Action-/-Hands-On-Time-2-Copy" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
            <mask id="mask-2" fill="white">
              <use xlinkHref="#path-1" />
            </mask>
            <use id="Mask" fill="#9B9B9B" fillRule="nonzero" xlinkHref="#path-1" />
            <g id="Swatch-/-Light-Grey" mask="url(#mask-2)" fill="#979797">
              <rect id="Rectangle-5" x="0" y="0" width="20" height="20" />
            </g>
          </g>
        </svg>
        {totalHands}
      </div>
      <div className="meal-post-data-box meal-post-data-data time-est">
        <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 100 100" enableBackground="new 0 0 100 100" xmlSpace="preserve">
          <g>
            <path d="M50,5C25.2,5,5,25.2,5,50s20.1,45,45,45s45-20.1,45-45S74.8,5,50,5z M50,89.7c-21.9,0-39.7-17.8-39.7-39.7   c0-21.9,17.8-39.7,39.7-39.7S89.7,28.1,89.7,50C89.7,71.9,71.9,89.7,50,89.7z" />
            <path d="M52.4,51.7V25.2c0-1.3-1.1-2.4-2.4-2.4s-2.4,1.1-2.4,2.4v27.3c0,0.3,0.1,0.6,0.1,0.8c0.1,0.4,0.3,0.8,0.6,1.1l19.3,19.3   c0.9,0.9,2.5,0.9,3.4,0c0.9-0.9,0.9-2.5,0-3.4L52.4,51.7z" />
          </g>
        </svg>
        {totalTime}
      </div>
    </div>
  )
}

export  const RecipeBottomIcon = (props) => {
  const { totalHands, totalTime, servingSize, equipmentIcon } = props
  return (
    <div className="sunday-cook cooking_dtls">
      <div className="dtl_ele ico-oven">
        <img src={equipmentIcon} alt="icon" />
      </div>
      <div className="dtl_ele"><b>{totalHands}</b>Hands On</div>
      <div className="dtl_ele"><b>{totalTime}</b>Total Time</div>
      <div className="dtl_ele"><b>{servingSize}</b>Servings</div>
    </div>
  )
}

export const EditRecipeIcon = props => {
  const { recipeClassName, deleteRecipeIndex } = props
  return (
    <div className={recipeClassName}>
      {
        !deleteRecipeIndex ?
          <RecipeIcons {...props} />
          :
          <DeleteRecipeConfirm {...props}/>

      }
    </div>
  )
}

export const RecipeIcons = props => {
  const { index } = props
  return (
    <div className="on-screen-icons">
      <div onClick={()=>props.showCookDay(index)}>
      Reschedule <img src="/images/Change Cook Day.svg" alt="" />
      </div>
      <div onClick={()=>props.editServing(index)}>
        Edit Servings <img src="/images/Edit Servings.svg" alt=""  />
      </div>
      <div onClick={()=>props.swapRecipe(index)}>
        Swap Recipe <img src="/images/Swap.svg" alt="" />
      </div>
      <div onClick={()=>props.deleteRecipe(index, props.id,props.recipeId)}>
        Delete Recipe <img src="/images/Delete.svg" alt="" />
      </div>
    </div>
  )
}

export  const DeleteRecipeConfirm = props => (
  <div>
    <span className="red-text"> Recipe will be deleted </span>
    <span className="undo" onClick={()=>props.cancelAction(props.index, props.recipeId)}> Undo </span>
  </div>
)


export const RecipeData = (props) => {
  // props also include > recipeIdArr if require can be used 
  const {
    recipeArr, cookingDate, lastStep, length, cookArr,
    location, editRecipe, editServingIndex, deleteRecipeIndex
  } = props

  const titleBRK = <div key='titleBRK' className="row__headings"><h2>Breakfasts</h2></div>
  
  const newArrSumBrk = {};
  const checkIdServingSums = recipeArr.map((item)=>{
    if(!newArrSumBrk[item.items.recipe.recipe_id]){
      newArrSumBrk[item.items.recipe.recipe_id] = item.servingSize
    }else{
      newArrSumBrk[item.items.recipe.recipe_id] +=  item.servingSize
    }
    return newArrSumBrk;
  })[0];

  const dataBRK = recipeArr.map((item, index)=>{

    let image = item.items.recipe.image
   
    let equipmentIcon;
    let sideName = null
    const editServingButton = editServingIndex && editServingIndex.length > 0 && editServingIndex.findIndex(x => x.index === index) > -1
    const deleteRecipeClass = deleteRecipeIndex && deleteRecipeIndex.findIndex(x => x.index === index) > -1 ? 'black-background-white':'black-background'
    const deleteRecipe = deleteRecipeIndex && deleteRecipeIndex.findIndex(x => x.index === index) > -1
    // let totalServing = totalValue(item.items.recipe.recipe_id,recipeIdArr,0)
    // if(totalServing === 1) {
    //   totalServing = item.servingSize
    // } else {
    //   totalServing *= item.servingSize
    // }

    const totalServing = _.find(checkIdServingSums,(ss,kk)=>{
      if(parseInt(kk,10)===parseInt(item.items.recipe.recipe_id,10)){
        return parseInt(ss,10);
      }
      return ''
    });
    const recipeName =
      item.items.recipe.name.length > 70 ?
        `${item.items.recipe.name.substr(0,31) }...`
        : item.items.recipe.name

    const setImage = image
    if(item.items.recipe.image === null) {
      image = '/images/small.png';
    } else if(image !== null) {
      image = image.split('mpn-recipe-images/')[0]
      const rId = item.items.recipe.recipe_id ? item.items.recipe.recipe_id : item.items.recipe.id
      const checkimage = setImage.includes('mpn-recipe-images/')
      if(checkimage){
        image +=`mpn-recipe-images/Images/${rId}-EH720.jpg`
      }else{
        image = setImage
      }      
    }
    if (item.items.recipe.recipe_equipment_type.length > 0) {
      equipmentIcon = `/images/${item.items.recipe.recipe_equipment_type[0].equipment_type.icon}`;
      if (equipmentIcon === '/images/') {
        equipmentIcon = '/images/none.svg';
      }
    } else {
      equipmentIcon = '/images/none.svg';
    }
    if (item.items.recipe.recipe_sides.length > 0) {
      if(item.items.recipe.recipe_sides[0].recipe){
        sideName = item.items.recipe.recipe_sides[0].recipe.name
      }
    }
    let totalPassive = differnceTimeStamp(item.totalTime, item.totalHands.split('m')[0])
    totalPassive = getTimeStamp(totalPassive)
    const cookDayClass = location !== 'dashboard' && !lastStep ? 'cookday-dsply': ''
    // earlier code 
    // if(item.cooking_date === length || length === 'test' && item.items.recipe.breakfast ===1 ){
    // new code     
    if((_.includes(item.cooking_date, length) || length === 'test') && item.items.recipe.breakfast === 1 ){
      const cookingDateIndex = cookingDate.findIndex(x=>x.cooking_date===length);
      let getParticularIndexServingSize;
      if(cookingDate[cookingDateIndex] && cookingDate[cookingDateIndex].recipe && cookingDate[cookingDateIndex].recipe.length){
        const recipeIndexCH = cookingDate[cookingDateIndex].recipe.findIndex(recipe => recipe.recipe_id === item.items.recipe_id);
        if(recipeIndexCH>-1){
          getParticularIndexServingSize = cookingDate[cookingDateIndex].recipe[recipeIndexCH].serving_size
        }
      }      
      return(
        <div className="meal-post small-device"
          // eslint-disable-next-line
          key={`${index}${item.items.recipe.name}`}
          onClick={()=>props.viewRecipe(item.totalHands, item.totalTime, totalPassive,
            item.items.recipe.name, sideName, item.items.recipe, totalServing )}
        >
          <a>
            <div className="headerSec">
              {
                editRecipe &&
              <EditRecipeIcon
                editServing={(servingIndex)=>props.editServing(servingIndex)}
                index={index}
                id={item.items.recipe.id}
                recipeId={item.items.recipe.recipe_id}
                deleteRecipe={(recipeIndex,id, recipeId)=>props.deleteRecipe(recipeIndex,id,recipeId)}
                recipeClassName={deleteRecipeClass}
                deleteRecipeIndex={deleteRecipe}
                cancelAction={(canclIndex, recipeId)=>props.cancelAction(canclIndex, recipeId)}
                showCookDay={(cookIndex)=>props.showCookDay(cookIndex)}
                swapRecipe={(swapIndex)=>props.swapRecipe(swapIndex)}
              />
              }
              <div className="image-plus-chips">
                <div className="meal-post-data small-device-icons">
                  {location !== 'dashboard' &&
                    <div className="meal-post-data-box meal-post-data-img">
                      <img
                        src={equipmentIcon}
                        className="equipment-icn"
                        alt="icon"
                      />
                    </div>
                  }
                  <div className="meal-post-data-box meal-post-data-data">
                    <p> {item.calories} Cals</p>
                  </div>
                  {location !=='dashboard' ?
                    <ImageIcon
                      totalHands={item.totalHands}
                      totalTime={item.totalTime}
                    />
                    :
                    null
                  }
                </div>
                <LazyLoad
                  loaderImage
                  originalSrc={image}
                  imageProps={{
                    src: '/images/small.png',
                    draggable: false,
                    name: 'd.',
                  }}
                  offsetLeft={window.innerWidth}
                  offsetHorizontal={360}
                  className="cook-recipe"
                />
              </div>

            </div>
            <div className="mb-rcp">
              <h5> {recipeName} </h5>
              <p className="aftr-hdng middle-text">{sideName}</p>
            </div>
          </a>
          <div className={`week-food-days no-space ${cookDayClass}`}>
            <ul>
              {!lastStep ?
                <CookingDays
                  index={index}
                  cookingDate={cookingDate}
                  cookArr={cookArr}
                  selectCookDay={
                    (indexV, data, cookingDays)=>props.selectCookDay(indexV, data, cookingDays)}
                />
                :
                location !== 'dashboard' || editServingButton
                  ?
                  props.setServingSize(index, item.cooking_date,length,getParticularIndexServingSize, item.items.recipe.id)
                  :
                  <RecipeBottomIcon
                    totalHands={item.totalHands}
                    totalTime={item.totalTime}
                    servingSize={item.servingSize}
                    equipmentIcon={equipmentIcon}
                  />
              }
              {
                cookingDate.length < 7 && !lastStep &&
                <li><span className="mb-hide dotted" onClick={()=>props.editCookDay(index)}>Other</span></li>
              }
            </ul>
          </div>
        </div>
      )
    }
    return null;    
  })

  const totalBRK = _.concat(titleBRK,dataBRK);

  const titleLD = <div key='titleLD' className="row__headings"><h2>Lunches/Dinners</h2></div>

  const dataLD = recipeArr.map((item, index)=>{
    let image = item.items.recipe.image
   
    let equipmentIcon;
    let sideName = null
    const editServingButton = editServingIndex && editServingIndex.length > 0 && editServingIndex.findIndex(x => x.index === index) > -1
    const deleteRecipeClass = deleteRecipeIndex && deleteRecipeIndex.findIndex(x => x.index === index) > -1 ? 'black-background-white':'black-background'
    const deleteRecipe = deleteRecipeIndex && deleteRecipeIndex.findIndex(x => x.index === index) > -1
    // let totalServing = totalValue(item.items.recipe.recipe_id,recipeIdArr,0)
    // if(totalServing === 1) {
    //   totalServing = item.servingSize
    // } else {
    //   totalServing *= item.servingSize
    // }
    const totalServing = _.find(checkIdServingSums,(ss,kk)=>{
      if(parseInt(kk,0)===parseInt(item.items.recipe.recipe_id,0)){
        return parseInt(ss,10);
      }
      return ''
    });

    const recipeName =
      item.items.recipe.name.length > 70 ?
        `${item.items.recipe.name.substr(0,31) }...`
        : item.items.recipe.name

    const setOTImage = image
    if(item.items.recipe.image === null) {
      image = '/images/small.png';
    } else if(image !== null) {      
      image = image.split('mpn-recipe-images/')[0]
      const rId =item.items.recipe.recipe_id ? item.items.recipe.recipe_id : item.items.recipe.id
      const checkimage = setOTImage.includes('mpn-recipe-images/')
      if(checkimage){
        image +=`mpn-recipe-images/Images/${rId}-EH720.jpg`
      }else{
        image = setOTImage
      }  
    }
    if (item.items.recipe.recipe_equipment_type.length > 0) {
      equipmentIcon = `/images/${item.items.recipe.recipe_equipment_type[0].equipment_type.icon}`;
      if (equipmentIcon === '/images/') {
        equipmentIcon = '/images/none.svg';
      }
    } else {
      equipmentIcon = '/images/none.svg';
    }
    if (item.items.recipe.recipe_sides.length > 0) {
      if(item.items.recipe.recipe_sides[0].recipe){
        sideName = item.items.recipe.recipe_sides[0].recipe.name
      }      
    }
    let totalPassive = differnceTimeStamp(item.totalTime, item.totalHands.split('m')[0])
    totalPassive = getTimeStamp(totalPassive)
    const cookDayClass = location !== 'dashboard' && !lastStep ? 'cookday-dsply': ''
    // earlier code
    // if(item.cooking_date === length || length === 'test' && item.items.recipe.breakfast ===0 ){
    // new code
    if((_.includes(item.cooking_date, length) || length === 'test') && item.items.recipe.breakfast === 0 ){
      const cookingDateIndex = cookingDate.findIndex(x=>x.cooking_date===length);
      let getParticularIndexServingSizeLD;
      if(cookingDate[cookingDateIndex] && cookingDate[cookingDateIndex].recipe && cookingDate[cookingDateIndex].recipe.length){
        const recipeIndexCH = cookingDate[cookingDateIndex].recipe.findIndex(recipe => recipe.recipe_id === item.items.recipe_id);
        if(recipeIndexCH>-1){
          getParticularIndexServingSizeLD = cookingDate[cookingDateIndex].recipe[recipeIndexCH].serving_size
        }        
      }
      return(
        <div className="meal-post small-device"
          // eslint-disable-next-line
          key={`${index}${item.items.recipe.name}`}
          onClick={()=>props.viewRecipe(item.totalHands, item.totalTime, totalPassive,
            item.items.recipe.name, sideName, item.items.recipe, totalServing )}
        >
          <a>
            <div className="headerSec">
              {
                editRecipe &&
              <EditRecipeIcon
                editServing={(servingIndex)=>props.editServing(servingIndex)}
                index={index}
                id={item.items.recipe.id}
                recipeId={item.items.recipe.recipe_id}
                deleteRecipe={(recipeIndex,id, recipeId)=>props.deleteRecipe(recipeIndex,id,recipeId)}
                recipeClassName={deleteRecipeClass}
                deleteRecipeIndex={deleteRecipe}
                cancelAction={(canclIndex, recipeId)=>props.cancelAction(canclIndex, recipeId)}
                showCookDay={(cookIndex)=>props.showCookDay(cookIndex)}
                swapRecipe={(swapIndex)=>props.swapRecipe(swapIndex)}
              />
              }
              <div className="image-plus-chips">
                <div className="meal-post-data small-device-icons">
                  {location !== 'dashboard' &&
                    <div className="meal-post-data-box meal-post-data-img">
                      <img
                        src={equipmentIcon}
                        className="equipment-icn"
                        alt="icon"
                      />
                    </div>
                  }
                  <div className="meal-post-data-box meal-post-data-data">
                    <p> {item.calories} Cals</p>
                  </div>
                  {location !=='dashboard' ?
                    <ImageIcon
                      totalHands={item.totalHands}
                      totalTime={item.totalTime}
                    />
                    :
                    null
                  }
                </div>
                <LazyLoad
                  loaderImage
                  originalSrc={image}
                  imageProps={{
                    src: '/images/small.png',
                    draggable: false,
                    name: 'd.',
                  }}
                  offsetLeft={window.innerWidth}
                  offsetHorizontal={360}
                  className="cook-recipe"
                />
              </div>

            </div>
            <div className="mb-rcp">
              <h5> {recipeName} </h5>
              <p className="aftr-hdng middle-text">{sideName}</p>
            </div>
          </a>
          <div className={`week-food-days no-space ${cookDayClass}`}>
            <ul>
              {!lastStep ?
                <CookingDays
                  index={index}
                  cookingDate={cookingDate}
                  cookArr={cookArr}
                  selectCookDay={
                    (indexV, data, cookingDays)=>props.selectCookDay(indexV, data, cookingDays)}
                />
                :
                location !== 'dashboard' || editServingButton
                  ?
                  props.setServingSize(index, item.cooking_date,length, getParticularIndexServingSizeLD, item.items.recipe.id)
                  :
                  <RecipeBottomIcon
                    totalHands={item.totalHands}
                    totalTime={item.totalTime}
                    servingSize={item.servingSize}
                    equipmentIcon={equipmentIcon}
                  />
              }
              {
                cookingDate.length < 7 && !lastStep &&
                <li><span className="mb-hide dotted" onClick={()=>props.editCookDay(index)}>Other</span></li>
              }
            </ul>
          </div>
        </div>
      )
    }
    return null;    
  })

  const totalLD = _.concat(titleLD,dataLD);
  // new code
  return lastStep ? _.concat(dataBRK,dataLD) : _.concat(totalBRK,totalLD)

}

