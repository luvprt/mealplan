import React, { Component } from 'react';
import InfiniteScroll from 'react-infinite-scroller';
// import LazyLoad from 'react-image-lazy-load';
import Loader from 'lib/loader';
import { compareRecipeTime, getTimeStamp } from 'lib/services';
import moment from 'moment';
import ReactResizeDetector from 'react-resize-detector';
import history from '../../history';
import MealDetail from '../dashboard_old/MealDetail';

let sideDishId = [];

class RecipeList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      recipeList: [],
      handsOnValueFilter: this.props.handsOnValueFilter || '',
      servingSize: props.servingSize || '',
      ...this.props,
    };
  }
  // unsafe
  // eslint-disable-next-line
  UNSAFE_componentWillReceiveProps(props) {
    this.setState({
      ...props,
    });
  }

  onClose() {
    this.setState({ display: '', mealDetail: false });
  }
  onResize() {
    let { useWindow } = this.state;
    
    if (window.innerWidth < 769 ) {
      useWindow = false
    } else {
      useWindow = true;
    }
    this.setState({ useWindow });
  }

  onSave(data) {
    this.setState({ save: true });
    this.pressBackBtn(data);
  }

  mealDetail(
    mealDate, id, weekId, type, size, rname, sname, totalHandsOn,
    totalTime, rHandsTime, totalRecipeTime, sideHandsOn, sidePassive,
    recipeImg, recipeVertImg, equipmentIcon, equipmentName,
  ) {
    const {fId, recipeSize, previousRId, dayDates} = this.state;
    history.push('/viewRecipe',
      { path:'viewRecipe', mealDate, id, rname, weekId, type, size, sname,
        totalHandsOn, totalTime, rHandsTime, totalRecipeTime, sideHandsOnTime: sideHandsOn,
        sidePassiveTime: sidePassive, fId, recipeImg, recipeVertImg, equipmentIcon,
        equipmentName, totalServing: size, pathFrom:'recipeList', recipeSize, previousRId, dayDates
      })
    this.setState({
   
      display: 'block',
      mealDate,
      id,
      rname,
      weekId,
      type,
      size,
      sname,
      totalHandsOn,
      totalTime,
      rHandsTime,
      totalRecipeTime,
      sideHandsOn,
      sidePassive,
      recipeImg,
      recipeVertImg,
      equipmentIcon,
      equipmentName,
    });

    // track recipe view
    window.analytics.track('Swap Recipe View detail Recipe Modal', {
      recipeName: rname,
      slot: type.split('_')[0],
      date: moment(mealDate).format('dddd DD-MM-YYYY'),
    });
  }

  recipeDetail(value) {
    if(value === 'loader') {
      this.setState({ loadData: true})
    }
    this.props.recipeDetail(this.state.offset);
  }

  pressBackBtn(data) {
    const { dayDates, recipeName, save, recipeSize } = this.state;
    // Canceled swap recipe
    if (!save) {
      window.analytics.track('Cancelled Swap Recipe', {
        recipeName,
        startDate: `${dayDates[0].fullDay} ${dayDates[0].date}-${dayDates[0].Month}-${dayDates[0].Year}`,
        endDate: `${dayDates[6].fullDay} ${dayDates[6].date}-${dayDates[6].Month}-${dayDates[6].Year}`,
      });
    }
    localStorage.setItem('method', 'back');
    history.push('/dashboard', { guestEmail: 'response.FirstName', type: '/login', dayDates, recipeSize, data });
  }

  recipeListData(recipeLis) {
    const {
      servingSize, weekId, type, mealDate,
    } = this.state;
    sideDishId = [];
    return (
      <div key={Math.random()}>
        {recipeLis.map((items) => {
          const recipeTotal = items.hands_on_time + items.passive_time;
          // eslint-disable-next-line
          let img = items.image,
            vertImg = items.vert_image,
            sname = null,
            handsOnTime = items.hands_on_time,
            sideTotal = 0,
            sideHandsOn = 0,
            totalTime = recipeTotal,
            sidePassive,
            equipmentIcon,
            equipmentName;
          if (items.image === null) {
            img = '/images/small.png';
          }
          if (vertImg === null) {
            vertImg = '/images/small.png';
          }
          if (items.recipe_equipment_type &&
              items.recipe_equipment_type.length > 0) {
            equipmentIcon = `/images/${items.recipe_equipment_type[0].equipment_type.icon}`;
            equipmentName = items.recipe_equipment_type[0].equipment_type.title;
            if (equipmentIcon === '/images/') {
              equipmentIcon = '/images/none.svg';
            }
          } else {
            equipmentIcon = '/images/none.svg';
            equipmentName = 'None';
          }
          if (items.recipe_sides && items.recipe_sides.length === 1) {
            handsOnTime += items.recipe_sides[0].recipe.hands_on_time;
            sideTotal = items.recipe_sides[0].recipe.hands_on_time + items.recipe_sides[0].recipe.passive_time;
            sideHandsOn = items.recipe_sides[0].recipe.hands_on_time;
            sidePassive = items.recipe_sides[0].recipe.passive_time;
            totalTime = compareRecipeTime(recipeTotal, sideTotal);
            totalTime = compareRecipeTime(totalTime, handsOnTime);
            sname = items.recipe_sides[0].recipe.name;
            sideDishId.push(items.recipe_sides[0].id);
          }
          handsOnTime = getTimeStamp(handsOnTime);
          totalTime = getTimeStamp(totalTime);

          return (
            <div className="meal-post" key={items.name}>
              <a
                onClick={() => this.mealDetail(
                  mealDate, items.id, weekId, type, servingSize,
                  items.name, sname, handsOnTime, totalTime,
                  items.hands_on_time, recipeTotal, sideHandsOn,
                  sidePassive, img, vertImg, equipmentIcon, equipmentName,
                )}
              >
                {/* <LazyLoad
                  loaderImage
                  originalSrc={img}
                  imageProps={{
                    src: '/images/small.png',
                    draggable: false,
                  }}
                  className="recipe_img"
                  threshold={100}
                  debounce={false}
                /> */}
                <img src={img} alt="recipeList" />
                <h5>{items.name}</h5>
                <div className="meal-post-data">
                  <div className="meal-post-data-box meal-post-data-img">
                    <img
                      src={equipmentIcon}
                      className="equipment-icn"
                      alt=""
                    />
                  </div>
                  <div className="meal-post-data-box">
                    <b>{handsOnTime}</b>
                      Hands On
                  </div>
                  <div className="meal-post-data-box">
                    <b>{totalTime}</b>
                      Total
                  </div>
                </div>
              </a>
            </div>
          );
        })
        }
      </div>
    );
  }

  render() {
    const {
      startDate, endDate, recipeList, mealDetail, display, loadData, hasMore, useWindow
    } = this.state;
    let showList = false;
    let check;
    if (recipeList && recipeList.length > 0) {
      showList = true;
    }
    return (
      <div className="col-lg-9 dash-main-cntnt">
        <ReactResizeDetector handleWidth handleHeight onResize={() => this.onResize()} />
        <div className="dash-cntnt-dtail swap-mode-dtail">
          <div className="dash-cntnt-dtail-hdr mb-hide">
            <h3>{startDate} - {endDate}</h3>
            <button className="btn back-to-plan" onClick={() => this.pressBackBtn()}>
              <svg
                height="512px"
                id="Layer_1"
                enableBackground="new 0 0 512 512"
                version="1.1"
                viewBox="0 0 512 512"
                width="512px"
                xmlSpace="preserve"
                xmlns="http://www.w3.org/2000/svg"
                xmlnsXlink="http://www.w3.org/1999/xlink"
              >
                <polygon points="352,115.4 331.3,96 160,256 331.3,416 352,396.7 201.5,256 " />
              </svg>
              Back to Weekly Plan
            </button>
          </div>
          <div className="swap-mode-dtail-cntnt mb-hide">
            <h4>Let’s Find A New Recipe to Swap In To Your Plan! </h4>
            <p>Select different filters on the left to narrow down the recipe library. New recipes are being added daily!</p>
          </div>
          <div className="recipe-list-cntnt-nw">
            <div className="cal-table-cntnt-row">
              <div className="cal-days-cntnt">
                { loadData &&
                  <center>
                    <div className="modal-backdrop fade in" />
                    <Loader />
                  </center>
                }
                { showList &&
                  <InfiniteScroll
                    loadMore={() => this.recipeDetail()}
                    hasMore={hasMore || false}
                    threshold={500}
                    useWindow={useWindow}
                    initialLoad={false}
                    useCapture={false}
                    isReverse={false}
                  >
                    {recipeList.map((recipeLis) => {
                      check = false;
                      if (recipeLis) {
                        if (recipeLis.length > 0) {
                          check = true;
                          showList = false;
                          return this.recipeListData(recipeLis)
                        }  showList = false; 
                      }
                      return true;
                      // return (
                      //   <div key='recipeList'>
                      //     { check &&
                      //      this.recipeListData(recipeLis)
                      //     }
                      //   </div>
                      // );
                    })
                    }
                    {hasMore &&
                      <div className="divider-small-scroll-box">
                        <div className="divider-small mb-hide">
                          <span className="scroll-view">Scroll to Load More</span>
                          <span className="divider-line" />
                        </div>
                        <button className="mb-only recipe-btn" onClick={() => this.recipeDetail('loader')}> Load More </button>
                      </div>
                    }
                  </InfiniteScroll>
                }
                {
                  ((!check && !showList && !loadData)) &&
                  <div className="new-recipe-no-result-cntnt">

                    <p>
                      Hey! We don&rsquo;t have any recipes that match your exact criteria...
                      but we probably will soon!
                    </p>
                    <p>
                      Uncheck some of your filters - OR - if you want to get our attention
                      on this sooner...
                      <span>Hit &quot; Request a Feature &quot; in the menu and tell us about it!</span>
                    </p>
                    <p>keeping on slicing, Ninja!</p>
                  </div>
                }
              </div>
            </div>
          </div>
          { mealDetail &&
          <div>
            <MealDetail
              display={display}
              onClose={() => this.onClose()}
              onSave={(data) => this.onSave(data)}
              recipeHandsTime={this.state.rHandsTime}
              swapMeal={() => this.swapMeal()}
              enableSwapButton={false}
              totalServing={this.state.totalServing}
              size={this.state.servingSize}
              sideDishArray={sideDishId}
              {...this.state}
            />
            <div className="modal-backdrop fade in" />
          </div>
          }
        </div>
      </div>
    );
  }
}

export default RecipeList;
