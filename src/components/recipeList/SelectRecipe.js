import React, { Component } from 'react';
import RoutingConstants from 'lib/RoutingConstants'
import { get, post } from 'lib/api';
import { compareRecipeTime, getTimeStamp, differnceTimeStamp, getSelectedWeekDates, getWeekDays } from 'lib/services';
import { onPressCancelBtn, onPressSaveBtn } from 'lib/common'
import LazyLoad from 'react-image-lazy-load';
import InfiniteScroll from 'react-infinite-scroller';
import Loader from 'lib/loader';
import MobileDesign from 'lib/MobileDesign';
import shuffleObjArrays from 'shuffle-obj-arrays'
import RecipeHeader from './RecipeHeader';
import RecipeMobileHeader from './RecipeMobileHeader';
import history from '../../history'
import DisplaySwapRecipe from './DisplaySwapRecipe'
import BottomBar from './BottomBar';
import ViewMeal from '../dashboard/ViewMeal';


let randomCount = 0

let interval;
const RecipeList = props => (
  <div className="recipe-header">
    {props.action === undefined && <div className="recommendation">
      {/* <p> <strong>We Recommend:</strong> <span>{props.recipeRange} Recipes</span>  */}
      <p>
        <span className="mb_range">We recommend: 
          <span>{props.recipeRange} Recipes (typical week)</span></span>
        <a onClick={()=>props.openChat()} className=""> Stuck? Chat Us!</a></p>
      <span>Scroll through recipes or use the filters in the top right to discover delicious, healthy recipes!  </span>
    </div>}
    <div className="category-name">
      <h4> {!props.viewAllRecipeData && !props.applyFilters ? 'Featured Dishes' :  props.categoryName[0] } </h4>
      {!props.viewAllRecipeData && !props.applyFilters && <a onClick={()=>props.viewAllRecipe()} className="clear-all-filtr">
          View all Recipes
      </a> }
    </div>
  </div>
)

class SelectRecipe extends Component {
  constructor(props) {
    super(props);
    const dataProps = this.props.location.state
    // const recipeRange = dataProps && dataProps.recipeRange ? dataProps.recipeRange : '1-2';
    const recipeRange = dataProps && dataProps.recipeRange ? dataProps.recipeRange : '3-5';
    const variety = dataProps && dataProps.variety ? dataProps.variety : '1';
    const varietyIndex = dataProps && dataProps.varietyIndex ? dataProps.varietyIndex : '1';
    const inputs = dataProps ? dataProps.inputs : {};
    const recipeArr = dataProps && dataProps.recipeArr !== undefined ? dataProps.recipeArr : [];
    // const cookingDates = dataProps && dataProps.cookingDates ? dataProps.cookingDates : [];
    const cookingDates = [];
    const dinnerDates = dataProps && dataProps.dinnerDates ? dataProps.dinnerDates : [];
    const lunchDates = dataProps && dataProps.lunchDates ? dataProps.lunchDates : [];
    const servingSize = dataProps && dataProps.servingSize ? dataProps.servingSize : 2;
    const loadData = sessionStorage.getItem('recipeApi') === null
    const recipeCount = dataProps && dataProps.recipeCount !== undefined ? dataProps.recipeCount === 1 ?  2 : dataProps.recipeCount : 0;
    const slideClass = dataProps && dataProps.slideClass !== undefined ? dataProps.slideClass : ''
    const weekEndPoints = getSelectedWeekDates('currentWeek');
    const dayDates = this.props.location.state && this.props.location.state.dayDates !== undefined ? this.props.location.state.dayDates: getWeekDays(weekEndPoints)
    this.state = {
      recipeRange,
      count: 0,
      recipeArr,
      bottomClass: recipeArr.length > 0 ?'' : 'mb-hide',
      recipeData: [],
      proteins: [],
      equipments: [],
      diet: [],
      loadData,
      opaque: false,
      showRecipe: true,
      applyFilters: false,
      recipeDivClass: '',
      cookingDates,
      servingSize,
      lunchDates,
      dinnerDates,
      inputs,
      variety,
      varietyIndex,
      showBottom: true,
      equipmentsList: [],
      viewAllRecipe: false,
      nextOffset: 0,
      slideClass,
      recipeCount,
      recipelocation: {},
      showViewRecipe: false,
      showFilter: false,
      randomRecipe: true,
      dayDates
    };
  }

  componentWillMount() {
    window.scroll(0,0)
    if(sessionStorage.getItem('recipeApi')) {
      this.recipeApiData('none')
    } else {
      this.getRecipesFilter()
    }
    randomCount = 0
  }

  
  onPressCancel() {
    onPressCancelBtn(this.state, this.props.location.state)
  }

  onPressSave() {
    onPressSaveBtn(this.state, this.props.location.state)
  }
  
  // eslint-disable-next-line
  onPressNext(index, length) {
    const elmnt = document.getElementById(`myDiv${index}`);    
    const prevButton = document.getElementById(`next${index}`)
    const count = (6/1183) * window.innerWidth
    let value = '0px';
    if(elmnt.style.transform.indexOf('(') > -1) {
      value = elmnt.style.transform.split('(')[1]
    }
    elmnt.style.transform = `translateX(${value.split('p')[0] - (count*180)}px)`
    elmnt.style.transition = 'all 0.5s ease'
    value = elmnt.style.transform.split('(')[1]
    const width = value.split('-')[1]
 
    elmnt.style.width = `${width.split('p')[0]}%`
    if(value.split('p')[0] <= 0) {
      prevButton.childNodes[0].style.display = 'block'
    }
    if(value.indexOf('-') > -1 ){
      const divWidth = value.split('-')[1]
      if(((length-count)*180) < divWidth.split('p')[0]) {
        prevButton.childNodes[1].style.display = 'none'
      }
    }
  }

  // eslint-disable-next-line
  onPressBack(index) {
    const elmnt = document.getElementById(`myDiv${index}`);    
    const prevButton = document.getElementById(`next${index}`)                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        
    let value = '0px';
    if(elmnt.style.transform.indexOf('(') > -1) {
      value = elmnt.style.transform.split('(')[1]
    }

    const left = value.split('-')[1]
    const count = (6/1183) * window.innerWidth
    const translate = (count*180) - left.split('p')[0]
    elmnt.style.transform = `translateX(${translate}px)`
    elmnt.style.transition = 'all 0.5s ease'
    
    if(elmnt.style.transform.indexOf('(') > -1) {
      value = elmnt.style.transform.split('(')[1]
    }
    if(parseFloat(value.split('p')[0],0).toFixed(2) <= 0.00) {
      prevButton.childNodes[0].style.display = 'none'
    }
    if(value.split('p')[0] >= 0) {
      elmnt.style.margin = '0px 0 0 0px'; 
    }
  }
 
  onPressPrevious(){
    const { 
      servingSize, dayDates
    } = this.state
    document.body.style.overflow = ''
    document.ontouchmove = () => (true)
    const dataProps = this.props.location.state
    if(sessionStorage.getItem('path') === 'dashboard' && (dataProps && dataProps.path === 'dashboard')) {
      this.onPressCancel()
    } else {
      sessionStorage.removeItem('recipeArr')
      sessionStorage.removeItem('filter')
      sessionStorage.removeItem('viewAll')
      sessionStorage.removeItem('bottom')
      // history.push('/selectMeals', { 
      //   cookingDates, servingSize, recipeRange, lunchDates, dinnerDates, 
      //   variety, varietyIndex, inputs, dayDates
      // })
      if (sessionStorage.getItem('plan') === 'create') {
        sessionStorage.setItem('method','back')
        history.push('/dashboard',{dayDates, defaultServingSize: servingSize})
      } else {
        history.push('/onboarding',{dayDates, startPosition: 4,})
      }


    }
  }

  getRecipes(filterData) {
    const url = RoutingConstants.getRecipes;
    const recipeDataList = []
    const categoryNameList = []
    let { showBottom, action, cookDate } = this.state
    const { recipeArr, bottomClass } = this.state
    let recipeIndex;
    const weekEndPoints = getSelectedWeekDates('currentWeek');
    const dayDates = this.props.location.state && this.props.location.state.dayDates !== undefined ? this.props.location.state.dayDates: getWeekDays(weekEndPoints)
    get(url).then((response) => {
      const tags = response.result.tags
      const recent = response.result.recent && response.result.recent.length > 0 ? response.result.recent : []
      // get the breakfast as a separate category
      const breakfast = response.result.breakfast && response.result.breakfast.length > 0 ? response.result.breakfast : []
      const proteins = response.result.protein
      const fish = proteins.Fish.length>0 ? proteins.Fish[0].recipe_protein_types: []
      const shellfish = proteins['Shell Fish'].length>0 ? proteins['Shell Fish'][0].recipe_protein_types: []
      const fishProtein = fish.concat(shellfish)

      // pushed the breakfast as a new category
      recipeDataList.push(
        tags.Featured && tags.Featured.length > 0 ? tags.Featured : [], breakfast,     
        recent,
        proteins.Chicken.length>0 ? proteins.Chicken[0].recipe_protein_types:[], 
        tags['Quick Prep'] && tags['Quick Prep'].length > 0 ? tags['Quick Prep']: [],
        tags['One-Pan'] && tags['One-Pan'].length > 0 ? tags['One-Pan']: [],
        tags['No Reheat'] && tags['No Reheat'].length > 0 ? tags['No Reheat']: [],
        proteins.Turkey.length>0 ? proteins.Turkey[0].recipe_protein_types:[], 
        proteins.Pork.length>0 ? proteins.Pork[0].recipe_protein_types: [], 
        proteins.Beef.length>0 ? proteins.Beef[0].recipe_protein_types: [], 
        fishProtein, 
        tags.Mexican && tags.Mexican.length > 0 ? tags.Mexican : [],
        tags.Asian && tags.Asian.length > 0 ? tags.Asian : [],
        tags.Italian && tags.Italian.length > 0 ? tags.Italian : [],
      )

      // placed the breakfast at the second place
      categoryNameList.push(
        'Featured Dishes', 'Breakfast', 'Recently Added', 'Winner Winner Chicken Dinner',
        'Quick Prep: Under 15 Mins Prep Time', 'One-Pan Meals For Easy Clean Up',
        'No Reheat Meals For Work',"Turkey Isn't Just For Thanksgiving",
        'Pork - Get Piggy With It',"Beef...It's What's For Dinner",
        'Down By The Sea','South Of The Border',
        'Asian Zing','Flavors of Italy'
      )
      if(this.props.location.state && 
          this.props.location.state.path === 'dashboard'){
        sessionStorage.setItem('path', 'dashboard')
        showBottom = false
        recipeIndex = this.props.location.state.index
        action = this.props.location.state.action
        cookDate = this.props.location.state.cookDate
      }

      const recipeApiData = { 
        recipeData: recipeDataList, categoryName: categoryNameList,
        proteins: filterData.proteins, 
        equipments: filterData.equipments,
        diet: filterData.diet,
        mealType: filterData.mealType
      }   
      sessionStorage.setItem('recipeApi', JSON.stringify(recipeApiData))

      this.setState({ 
        recipeData: recipeDataList, categoryName: categoryNameList, loadData: false, 
        proteins: filterData.proteins, 
        equipments: filterData.equipments,
        diet: filterData.diet,
        mealType: filterData.mealType,
        showBottom,
        recipeIndex,
        action,
        showRecipe : true,
        cookDate, dayDates, recipeArr,
        bottomClass,
        hasMore: false,
        viewAllRecipe: false
      })
    }).catch(()=>{
      this.setState({ loadData: false})
    })
  }
  

  getRecipesFilter() {
    const url = RoutingConstants.getFilterAttributes
    get(url).then(response => {
      const diet = ['Dairy-free','Gluten-free','Soy-free','Egg-free','Nut-free','Sugar-free']
      response.result.diet = diet
      this.getRecipes(response.result)
    }).catch(()=>{
      localStorage.removeItem('token');
      history.push('/login');
    })
  }

  recipeApiData(value) {
    const recipeApiData = JSON.parse(sessionStorage.getItem('recipeApi'))
    let { 
      showBottom, action, cookDate, recipeArr, showRecipe, recipeDivClass, 
      recipeCount, applyFilters, bottomClass, filterTags, equipmentsList, 
      viewAllRecipe, hasMore
    } = this.state
    let recipeIndex;
    const weekEndPoints = getSelectedWeekDates('currentWeek');
    const dayDates = this.props.location.state && this.props.location.state.dayDates !== undefined ? this.props.location.state.dayDates: getWeekDays(weekEndPoints)
    // const dayDates = this.props.location.state && this.props.location.state.dayDates !== undefined ? this.props.location.state.dayDates: []
    const dataProps = this.props.location.state
    const slideClass = dataProps && dataProps.slideClass !== undefined ? dataProps.slideClass : ''
    if(dataProps && 
      dataProps.path === 'dashboard'){
      sessionStorage.setItem('path', 'dashboard')
      showBottom = false
      recipeIndex = dataProps.index
      action = dataProps.action
      cookDate = dataProps.cookDate
    } 
   
    if(sessionStorage.getItem('recipeArr') && sessionStorage.getItem('recipeArr').length > 0) {
      recipeArr = JSON.parse(sessionStorage.getItem('recipeArr'))
    }
    if(sessionStorage.getItem('bottom')) {
      const bottom = JSON.parse(sessionStorage.getItem('bottom'))
      showRecipe = bottom.showRecipe
      showBottom = bottom.showBottom
      recipeCount = bottom.recipeCount === 1 ? 2 : bottom.recipeCount
      bottomClass = recipeArr.length > 0 ? '' : 'mb-hide'
    }
    if(sessionStorage.getItem('viewAll')) {
      const allRecipeDataList = JSON.parse(sessionStorage.getItem('viewAll'))
      recipeApiData.recipeData = allRecipeDataList.recipeData
      recipeApiData.categoryName = allRecipeDataList.categoryName
      viewAllRecipe = true
      hasMore = true
      recipeDivClass='filter-rslt'
      this.setState({ recipeDivClass })
    }
    if(sessionStorage.getItem('filter')) {
      const filterDataList = JSON.parse(sessionStorage.getItem('filter'))
      recipeApiData.recipeData = filterDataList.recipeData
      recipeApiData.categoryName = filterDataList.categoryName
      showRecipe = filterDataList.showRecipe
      showBottom = filterDataList.showBottom
      filterTags = filterDataList.filterTags
      recipeDivClass = filterDataList.recipeDivClass
      equipmentsList = filterDataList.equipmentsList
      applyFilters = true
      hasMore = false
      this.setState({ recipeDivClass, applyFilters})
    }
    if(value === 'emptyfilter') {
      showRecipe = true
      applyFilters = false
      viewAllRecipe = false
      recipeDivClass=''
      hasMore= false
    } else if( value === 'viewAll') {
      showRecipe = true
      applyFilters = false
    }
        
    this.setState({ 
      recipeData: recipeApiData.recipeData, 
      categoryName: recipeApiData.categoryName, 
      proteins: recipeApiData.proteins, 
      equipments: recipeApiData.equipments,
      diet: recipeApiData.diet,
      showBottom,
      recipeIndex,
      action,
      showRecipe,
      cookDate, dayDates, recipeArr,
      bottomClass,
      filterTags,
      equipmentsList,
      viewAllRecipe,
      hasMore,
      applyFilters,
      recipeCount,
      recipeDivClass,
      slideClass
    })
  }


  addMeal(event,key, image, items, id, totalHands, totalTime, calories) {
    const { count,  servingSize, showBottom, cookDate, recipeCount } = this.state
    let { bottomClass, recipeArr } = this.state
    const index =  recipeArr.findIndex(x => x.id === id)
    if(showBottom) {
      if(index > -1) {
        if ( document.getElementById(`${event}`)) {
          document.getElementById(`${event}`).style.display = 'block'
          if(document.getElementById(`${event}${key}`))
            document.getElementById(`${event}${key}`).style.display = 'none'
        }
        recipeArr.splice(index,1)
      } else {
        if ( document.getElementById(`${event}`)) {
          document.getElementById(event).style.display = 'none'
          document.getElementById(`${event}${key}`).style.display = 'block'
        }
        recipeArr.push({index: event, image, key, items, servingSize, id, totalHands, totalTime, calories})
        bottomClass = ''
      }
    } else{
      document.getElementById(`${event}${key}`).style.display = 'block'
      document.getElementById(event).style.display = 'none'
      recipeArr = [{ index: event, image, key, items, servingSize, id, totalHands, totalTime, calories, cooking_date: cookDate }]
    }
    if(sessionStorage.getItem('recipeArr')) {
      sessionStorage.setItem('recipeArr', JSON.stringify(recipeArr))
    }
    this.setState({ count: count+1, recipeArr, bottomClass, recipeCount:recipeCount + 1,  })
    if(items && items.recipe){
      const email = localStorage.getItem('email')
      window.analytics.track(`Recipe Selected`, {
        recipeName: items.recipe.name,
        email
      });
    }
  }

  recipeList(data, value) {
    const { recipeArr } = this.state
    return data.map((recipeItem)=>{
      if(recipeItem.recipe){
        let image = recipeItem.recipe.image
        let thumbImage = recipeItem.recipe.image
        let tickDisplay = 'none'
        let display = 'block'
        const id = recipeItem.recipe.id
        if(recipeArr.length > 0) {
          const recipeIndex = recipeArr.findIndex((
            x=>x.id && x.id === `add-meal-btn${value}${id}`
          ))
          if(recipeIndex > -1) {
            display = 'none'
            tickDisplay = 'block'
          } else {
            display ='block'
            tickDisplay = 'none'
          }
        }
        let equipmentIcon;
        let sideName = null
        let sideHands = 0
        let sidePassive = 0
        let totalHands = recipeItem.recipe.hands_on_time || 0
        const recipePassive = recipeItem.recipe.passive_time || 0
        const totalRecipeTime = totalHands + recipePassive
        let totalSideTime = 0
        let calories = 0
        let totalPassive = recipePassive;
        const recipeName = 
        recipeItem.recipe.name
      
        if(recipeItem.recipe.image === null) {
          image = '/images/small.png';
          thumbImage = '/images/small.png';
        }
        const setImage = image
        if(recipeItem.recipe.image !== null) {
          const checkImage = recipeItem.recipe.image.includes('mpn-recipe-images/')
          if(checkImage){
            image = recipeItem.recipe.image.split('mpn-recipe-images/')[0];
            thumbImage = `${image}mpn-recipe-images/Images/${recipeItem.recipe.id}-EV300.jpg` 
            image += `mpn-recipe-images/Images/${recipeItem.recipe.id}-EH720.jpg` 
          }else{
            image = recipeItem.recipe.image.split('Images/')[0];
            thumbImage = `${image}Images/${recipeItem.recipe.id}-EV300.jpg` 
            image = setImage
          }          
        }
        if (recipeItem.recipe.recipe_equipment_type.length > 0) {
          equipmentIcon = `/images/${recipeItem.recipe.recipe_equipment_type[0].equipment_type.icon}`;
          if (equipmentIcon === '/images/') {
            equipmentIcon = '/images/none.svg';
          }
        } else {
          equipmentIcon = '/images/none.svg';
        }
        const calorieIndex = recipeItem.recipe.recipe_nutrition_data && recipeItem.recipe.recipe_nutrition_data.findIndex(x=>x.nutrition_info_attribute.attribute === 'Calories')
        if(calorieIndex > -1) {
          calories = parseInt(recipeItem.recipe.recipe_nutrition_data[calorieIndex].value,0)
        }
        if (recipeItem.recipe.recipe_sides.length > 0) {
          if (recipeItem.recipe.recipe_sides[0].recipe) {
            sideName = recipeItem.recipe.recipe_sides[0].recipe.name
            sideHands = recipeItem.recipe.recipe_sides[0].recipe.hands_on_time || 0
            sidePassive = recipeItem.recipe.recipe_sides[0].recipe.passive_time || 0
            totalHands += sideHands
            totalSideTime = sideHands + sidePassive
            totalPassive += sidePassive
            const caloriesIndex = recipeItem.recipe.recipe_sides[0].recipe_nutrition_data && recipeItem.recipe.recipe_sides[0].recipe_nutrition_data.findIndex(x=>x.nutrition_info_attribute.attribute === 'Calories')
            if(caloriesIndex > -1) {
              calories += parseInt(recipeItem.recipe.recipe_sides[0].recipe_nutrition_data[caloriesIndex].value,0)
            }
          }
        } 
        let totalTime = compareRecipeTime(totalRecipeTime, totalSideTime);
        totalTime = compareRecipeTime(totalTime, totalHands);
        totalHands = getTimeStamp(totalHands)
        totalTime = getTimeStamp(totalTime)
        totalPassive = differnceTimeStamp(totalTime, totalHands.split('m')[0])
        totalPassive = getTimeStamp(totalPassive)
        const laterAddOn = {
          index:`add-meal-btn${value}${id}`,id:`add-meal-btn${value}${id}`, image:thumbImage, items:recipeItem,
          key:`add-meal-btn${value}${recipeItem.recipe.id}`
        }
        return(
          <div className="meal-post" key={`${value}${recipeItem.recipe.name}`} >
            <a 
              draggable={false} 
              className="add-meal-btn" 
              id={`add-meal-btn${value}${id}`} 
              onClick={()=>this.addMeal(
                `add-meal-btn${value}${id}`,id, thumbImage,recipeItem, 
                `add-meal-btn${value}${recipeItem.recipe.id}`, totalHands, totalTime, calories
              )} 
              style={{display, height:'auto'}}
            >
              <img src="/images/add.svg" alt=""/>
            </a>
            <a draggable={false} className="add-meal-btn tick-img" 
              onClick={()=>this.addMeal(
                `add-meal-btn${value}${id}`,id, thumbImage, recipeItem,
                `add-meal-btn${value}${recipeItem.recipe.id}`)
              } id={`add-meal-btn${value}${id}${id}`} style={{display:tickDisplay, height:'auto'}}>
              <img src="/images/tick.svg" alt="" />
            </a>
            <div  onClick={()=>this.viewRecipe(totalHands, totalTime, totalPassive, recipeItem.recipe.name, sideName, recipeItem.recipe, laterAddOn )}>
              <a>
                <LazyLoad
                  loaderImage
                  originalSrc={image}
                  offsetLeft={window.innerWidth}
                  offsetHorizontal={360}
                  height={120}
                  imageProps={{
                    src: '/images/small.png',
                    draggable: false,
                    name: 'd.',
                  }}
                  className='recipe_img'

                />
                <div className="select-meals-title-cntnt">
                  <h5>{recipeName}</h5>
                  <p className="aftr-hdng">{sideName}</p>
                </div>
                <div className="meal-post-data">
                  <div className="meal-post-data-box meal-post-data-img">
                    <img
                      src={equipmentIcon}
                      className="equipment-icn"
                      alt="icon"
                    />
                  </div>
                  <div className="meal-post-data-box">
                    <img src="/images/hands.svg" alt="" className="hands-svg"/>
                    {totalHands}
                  </div>
                  <div className="meal-post-data-box time-est">
                    <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px"
										 viewBox="0 0 100 100" style={{enableBackground:"new 0 0 100 100"}} xmlSpace="preserve">
                      <g>
                        <path d="M50,5C25.2,5,5,25.2,5,50s20.1,45,45,45s45-20.1,45-45S74.8,5,50,5z M50,89.7c-21.9,0-39.7-17.8-39.7-39.7   c0-21.9,17.8-39.7,39.7-39.7S89.7,28.1,89.7,50C89.7,71.9,71.9,89.7,50,89.7z" />
                        <path d="M52.4,51.7V25.2c0-1.3-1.1-2.4-2.4-2.4s-2.4,1.1-2.4,2.4v27.3c0,0.3,0.1,0.6,0.1,0.8c0.1,0.4,0.3,0.8,0.6,1.1l19.3,19.3   c0.9,0.9,2.5,0.9,3.4,0c0.9-0.9,0.9-2.5,0-3.4L52.4,51.7z" />
                      </g>
                    </svg>
                    {totalTime}</div>
                </div>
              </a>
            </div>
          </div>
        )
      }
      return '';
    })
  }



  // eslint-disable-next-line
  showButton(event,name, length) {
    const { applyFilters, viewAllRecipe } = this.state

    if(!applyFilters && !viewAllRecipe){
      const button = document.getElementById(event)
      const elem = document.getElementById(name)
      button.childNodes[1].style.display = 'block'

      let value = '0'
      if(elem.style.transform.indexOf('(') > -1) {
        value = elem.style.transform.split('(')[1]
      }
      
      if(value.split('p')[0] < '0' && parseFloat(value.split('p')[0],0).toFixed(3) < (-0.005)) {
        button.childNodes[0].style.display = 'block'
      }
      if(parseInt(window.innerWidth/180,0) > length) {
        button.childNodes[1].style.display = 'none'
      } 
      const count = (6/1299) * window.innerWidth
      if(value.indexOf('-') > -1 ){
        const divWidth = value.split('-')[1]
        if(((length-count)*180) <= divWidth.split('p')[0]) {
          button.childNodes[1].style.display = 'none'
        }
      }
    }
  }

  hideButton(event) {
    const { applyFilters } = this.state
    if(!applyFilters) {
      const button = document.getElementById(event)
      button.childNodes[1].style.display = 'none'
      button.childNodes[0].style.display = 'none'
    }
  }

  divOpaque() {
    let { opacity, opaque } = this.state
    let proteinelem = false ;
    let cookingelem = false ;
    let dietelem = false ;
    let mealelem = false ;

    if(document.getElementById('proteindrp')) {
      proteinelem = document.getElementById('proteindrp').getAttribute('aria-expanded')
    } 
    if(document.getElementById('mealtypedrp')) {
      mealelem = document.getElementById('mealtypedrp').getAttribute('aria-expanded')
    } 
    if(document.getElementById('cookingdrp')) {
      cookingelem = document.getElementById('cookingdrp').getAttribute('aria-expanded')
    }
    if(document.getElementById('dietdrp')) {
      dietelem = document.getElementById('dietdrp').getAttribute('aria-expanded')
    }
    if(proteinelem === 'true' || cookingelem === 'true' || dietelem === 'true' || mealelem === 'true') {  
      opaque = !opaque
      opacity = 0.5
    } else {
      opaque = false
      opacity = 1
    }
    this.setState({ opaque, opacity })
  }

  viewRecipe(totalHandsOn, totalTime, totalPassive, rname, sname, recipe, laterAddOn) {
    const {
      dayDates, servingSize, recipeArr, recipeData, recipeDivClass, recipeCount,
      categoryName, showRecipe, applyFilters, filterTags, showBottom, equipmentsList, 
      viewAllRecipe
    } = this.state
    const email = localStorage.getItem('email')
    sessionStorage.setItem('recipeArr', JSON.stringify(recipeArr))
    const bottom = { showBottom, showRecipe, recipeCount}
    sessionStorage.setItem('bottom', JSON.stringify(bottom))
    if (applyFilters) {
      const filterData = { recipeData,
        categoryName,
        showRecipe,
        showBottom,
        recipeDivClass,
        filterTags,
        equipmentsList,
      }

      sessionStorage.setItem('filter', JSON.stringify(filterData))
    }
    laterAddOn.servingSize = servingSize
    laterAddOn.totalHands = totalHandsOn
    laterAddOn.totalTime = totalTime
    if (viewAllRecipe) {
      const allRecipeData = { 
        recipeData,
        categoryName,
        showRecipe,
        showBottom,
        recipeDivClass,
      }
      sessionStorage.setItem('viewAll', JSON.stringify(allRecipeData))
    }
    window.analytics.track(`View Details`, {
      recipeName: rname,
      email
    });
    const recipelocation ={ state:{ 
      path:'viewRecipe', totalHandsOn, totalTime, totalPassive, rname, sname, 
      recipe, dayDates, totalServing: servingSize, laterAddOn, state:this.state, props:this.props.location.state
    }}
    document.getElementsByTagName('body')[0].className = 'modal-open'
    this.setState({ recipelocation, showViewRecipe: true })
  }

  viewAll() {
    const { recipeArr, showBottom, showRecipe, viewAllRecipe, applyFilters, recipeCount } = this.state
    sessionStorage.setItem('recipeArr', JSON.stringify(recipeArr))
    const bottom = { showBottom, showRecipe, recipeCount}
    sessionStorage.setItem('bottom', JSON.stringify(bottom))
    sessionStorage.removeItem('filter')
    const value = !viewAllRecipe || !applyFilters ? 'emptyfilter' : 'viewAll'
    if(!viewAllRecipe || !applyFilters) {
      sessionStorage.removeItem('viewAll')
    }
    this.recipeApiData(value)
  }

  recipeData() {
    const { 
      recipeData, categoryName, recipeDivClass, nextOffset, hasMore, viewAllRecipe, 
      applyFilters
    } = this.state
    return recipeData.map((item, index)=>{
      if(item.length>0){  
        if(randomCount === 0 && !hasMore) {
          shuffleObjArrays({item})
        }
        if(recipeData.length === index+1) {
          randomCount += 1
        }
        categoryName[index] = window.innerWidth < 768 && categoryName[index] === 'Featured Dishes' ?
          'Popular Right Now' : categoryName[index]
        const categoryClass = window.innerWidth < 768 && categoryName.length === 1 ?
          'mb-hide' : ''
        return(
          <div className="dishes-row-box" key={categoryName[index]} onMouseOver={()=>this.showButton(`next${index+1}`, `myDiv${index+1}`, item.length)} onMouseOut={()=>this.hideButton(`next${index+1}`, `myDiv${index+1}`)} onClick={()=>this.divOpaque('')} >
            <div className="container">
              <h4 className={categoryClass}>{categoryName[index]}</h4>
              {recipeDivClass !== '' && <div className="back-to-recipe view-all" style={{float:'right'}}>
                <button className="btn back-to-plan mb-only" onClick={() => this.viewAll()}>
                  <svg
                    height="512px"
                    id="Layer_1"
                    enableBackground="new 0 0 512 512"
                    version="1.1"
                    viewBox="0 0 512 512"
                    width="512px"
                    xmlSpace="preserve"
                    xmlns="http://www.w3.org/2000/svg"
                    xmlnsXlink="http://www.w3.org/1999/xlink"
                  >
                    <polygon points="352,115.4 331.3,96 160,256 331.3,416 352,396.7 201.5,256 " />
                  </svg>
                  { applyFilters && viewAllRecipe ? 'Back to View All' : ' Back to Featured Recipes'}
                </button>
              </div>}
              <div className="next-back-btns" id={`next${index+1}`}>
                <a className="back" onClick={()=>this.onPressBack(index+1)} style={{display:'none'}}>&lt;</a> 
                <a className="next" onClick={()=>this.onPressNext(index+1, item.length)} style={{display:'none'}}> &gt; </a>
              </div>
             
              <div className={`cal-days-cntnt dishes-row ${recipeDivClass}`} id={`myDiv${index+1}`}>
                <InfiniteScroll
                  loadMore={() => this.viewAllRecipe(nextOffset)}
                  hasMore={hasMore}
                  threshold={500}
                  useWindow
                  initialLoad={false}
                  useCapture={false}
                  isReverse={false}
                >
                  {this.recipeList(item, categoryName[index])}
                  {hasMore &&
                      <div className="divider-small-scroll-box">
                        <div className="divider-small">
                          <span className="scroll-view">Scroll to Load More</span>
                          <span className="divider-line" />
                        </div>
                      </div>
                  }
                </InfiniteScroll>
              </div>
            </div>
          </div> 
        )}
      return null
    }
    )
  }

  showRecipe() {
    const { showFilter } = this.state
    let { recipeCount } = this.state
    recipeCount = recipeCount === 1 ? 2 : recipeCount
    this.setState({ showFilter: !showFilter, recipeCount})
  }

  closeMeal(data, recipeCount) {
    recipeCount = recipeCount === 1 ? 2 : recipeCount
    this.setState({ recipeArr: data, recipeCount})
    const timeOut = window.innerWidth < 769 ? 700 :0
    document.getElementById('modal-dialog').style.overflowY = 'hidden'
    setTimeout(()=>{this.setState({showViewRecipe: false})}, timeOut)
  }

  // filter function
  async applyRecipeFilters(data, equipmentsList) {
    randomCount = 0
    const { viewAllRecipe, recipeData, categoryName, showBottom, recipeDivClass, showRecipe, offset, hasMore} = this.state
    this.setState({ applyFilters: true, loadData: true, hasMore: false, showFilter: false})
    document.ontouchmove = () => (true)
    document.body.style.overflow = ''
    const url = RoutingConstants.getFilterRecipe
    const recipeFilter = []
    const param = {}
    const recipeFilter1 = []
    const categoryFilter = []
    param.proteins = data.proteinsData
    param.equipment = data.equipmentsData
    param.tags = data.tagsData
    param.mealTypes = data.mealTypeData
    
    if (viewAllRecipe) {
      const allRecipeData = { 
        recipeData,
        categoryName,
        showRecipe,
        showBottom,
        recipeDivClass,
        offset,
        hasMore
      }
      sessionStorage.setItem('viewAll', JSON.stringify(allRecipeData))
    }

    await post(url, param).then((response)=>{      
      response.result.map(item => {
        recipeFilter1.push({id:'filter',recipe_id:item.id, recipe: item})
        return null
      })
      recipeFilter.push(recipeFilter1)
      categoryFilter.push('Results From Your Filters')
      this.setState({ 
        recipeData: recipeFilter, 
        categoryName: categoryFilter, 
        showRecipe: true,
        recipeDivClass: 'filter-rslt',
        loadData: false,
        opacity: 1,
        opaque: false,
        filterTags: data,
        equipmentsList
      })
    })
    return this.recipeData()
  }

  // view all Recipe function
  viewAllRecipe(offset) {
    let { recipeData } = this.state
    if(offset === 0) {
      this.setState({ loadData: true }) 
    }

    const url = `${RoutingConstants.getSwapRecipeList
    }?offset=${offset}&limit=18`;
    const recipeFilter = []
    const recipeFilter1 = []
    const categoryFilter = []
    get(url).then((response)=>{
      const hasMore = response.all_recipe_count === 18
      response.result.map(item => {
        recipeFilter1.push({id:'filter',recipe_id:item.id, recipe: item})
        return null
      })
      recipeFilter.push(recipeFilter1)
      const text = window.innerWidth > 768  ? `You're Viewing All Recipes` : 'Viewing All Recipes'
      categoryFilter.push(text)
      if(offset === 0) {
        recipeData = recipeFilter
      } else {
        Array.prototype.push.apply(recipeData[0],recipeFilter1)
      }
      this.setState({ 
        recipeData, 
        categoryName: categoryFilter, 
        showRecipe: true,
        recipeDivClass: 'filter-rslt',
        loadData: false,
        opacity: 1,
        opaque: false,
        viewAllRecipe: true,
        hasMore,
        nextOffset: response.next_offset,
        applyFilters: false
      })
    }).catch(()=>{
      this.setState({ hasMore: false, loadData: false})
    })
    return this.recipeData()
  }

  async removeFilter(data) {
    this.setState({ recipeDivClass: '', applyFilters: false, showRecipe: true, opacity:1 })
    randomCount = 0
    if(sessionStorage.getItem('recipeApi')) {
      await this.recipeApiData('emptyfilter')
    } else {
      await this.getRecipes(data)
    }
  }

  emptyRecipeArr() {
    const { recipeArr } = this.state
    if(document.getElementById(recipeArr[0].index)) {
      document.getElementById(recipeArr[0].index).style.display = 'block'
      if(document.getElementById(`${recipeArr[0].index}${recipeArr[0].key}`))
        document.getElementById(`${recipeArr[0].index}${recipeArr[0].key}`).style.display = 'none'
    }
    this.setState({recipeArr:[]})
    sessionStorage.removeItem('recipeArr')
  }

  // eslint-disable-next-line
  componentDidMount() {
    const { dayDates, cookingDates } = this.state
    const activeChat = this.props.location.state && this.props.location.state.path === 'dashboard'
    if(document.getElementById('PureChatWidget') === null && window.innerWidth < 768 && !activeChat) {
      interval = setInterval(()=>this.openChatBox(),1000)
    }
    if(cookingDates.length === 0 && dayDates.length > 0) {
      dayDates.map(item=>{  
        const itemDate = `${item.Year}-${item.Month}-${item.date}`;
        cookingDates.push({cooking_date:itemDate})
        return 0
      })
    }
  }

  // eslint-disable-next-line
  openChatBox() {
    if(document.getElementsByClassName('purechat-collapsed-image')[0]) {
      document.getElementById('PureChatWidget').classList.add('text')
      document.getElementsByClassName('purechat-collapsed-image')[0].removeAttribute('style')
    }
    if(document.getElementById('PureChatWidget') !== null) { 
      clearInterval(interval)
    }
  }

  render() { 
    const { 
      recipeRange, recipeData, applyFilters, loadData, proteins, equipments, diet, opacity, showRecipe, action,
      showBottom, recipeIndex, viewAllRecipe, categoryName, recipelocation, 
      showViewRecipe, slideClass, showFilter
    } = this.state
    const recipes = this.props.location.state && this.props.location.state.recipes
    const headingText = !showBottom? action === 'swap' ? 'Swap Recipe': action ==='add' && 'Add New Recipe' : 'Select Your Menu'
    // const subHeadingText = !showBottom ? 
    //   'Select another recipe for your plan.' : window.innerWidth > 768 ?`We recommend you choose ${recipeRange} recipes.`: 'Choose your recipes for the week!'
    const subHeadingText = !showBottom ? 
      'Select another recipe for your plan.' : window.innerWidth > 768 ? 'Most ninjas choose between 3-5 recipes for their weekly meal plan.': 'Start building your meal plan!'

    return (
      <div className={`wrapper ${slideClass}`} onClick={()=>this.divOpaque()}> 
        <style>
          {`
          @media(min-width:320px) and (max-width:767px){
            #PureChatWidget { display: none !important;}
            #PureChatWidget.text { display: block !important;}
            #PureChatWidget.purechat.purechat-animation-bounceInDown{
              animation-name: none!important;
            }
            #PureChatWidget.purechat.purechat-top-right {
              right: 56px !important;
              top: 60px !important;
              position:absolute!important;
            }
            #PureChatWidget.purechat.purechat-image-only.purechat-widget-collapsed.purechat-bottom-right .purechat-collapsed-image, #PureChatWidget.purechat.purechat-image-only.purechat-widget-collapsed.purechat-top-right .purechat-collapsed-image{ background-image: url("/images/chat-us.png") !important;
            opacity:0!important; height: 27px!important;
           width:144px !important; z-index: 9 !important; margin-bottom: -38.32px !important; margin-right: -34px !important;
          }
          #PureChatWidget.purechat.purechat-widget-expanded.purechat-top-right {
            top:90px!important;
          }
          }

          `}
        </style>
        {
          loadData &&
          <div className="loading">
            <Loader />
            <div className="modal-backdrop fade in" />
          </div>
        }
        {
          navigator.userAgent.match(/iPad/i) !== null &&
        <style>
          {`${MobileDesign.design}`}
        </style>
        }
        <header>  
          <div className="container">
            <a href="/" className="mb-hide">
              <img src="/images/logo.png" alt="" />
            </a>
            <div className="slect-meal-top">
              <div className="top-back-row-comon-hdr">
                <a onClick={()=>this.onPressPrevious()}>  
                  <svg enableBackground="new 0 0 256 256" height="256px" id="Layer_1" version="1.1" viewBox="0 0 256 256" width="256px" xmlSpace="preserve" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                    <path d="M91.474,33.861l-89.6,89.6c-2.5,2.5-2.5,6.551,0,9.051l89.6,89.6c2.5,2.5,6.551,2.5,9.051,0s2.5-6.742,0-9.242L21.849,134  H249.6c3.535,0,6.4-2.466,6.4-6s-2.865-6-6.4-6H21.849l78.676-78.881c1.25-1.25,1.875-2.993,1.875-4.629s-0.625-3.326-1.875-4.576  C98.024,31.413,93.974,31.361,91.474,33.861z" />
                  </svg>
                </a>
              </div>
              <h1 className="mb-only"> {headingText} </h1>
              <p className="after-hdng mb-only">
                {subHeadingText}
              </p>
            </div>
          </div>
        </header>
        <div className="filter-dropdown-mb mb-only">
          <RecipeMobileHeader 
            proteins={proteins}
            equipments={equipments}
            diet={diet}
            showFilter={!showFilter}
            divOpaque={(value)=>this.divOpaque(value)}
            showRecipe={()=>this.showRecipe()}
            applyFilters={applyFilters}
            viewAllRecipeData={viewAllRecipe}
            viewAllRecipe={()=>this.viewAllRecipe(0)}
            applyRecipeFilters={(data, equipmentsList)=>this.applyRecipeFilters(data, equipmentsList)} 
            getRecipes={(data)=>this.removeFilter(data)}
          />
        </div>
        <section className="select-meal-cntnt choose-recipe-cntnt">
          <div className="container">
            {
              <div className="top-back-row-comon-hdr mb-hide">
                <a onClick={()=>this.onPressPrevious()}>
                  <svg 
                    enableBackground="new 0 0 256 256" 
                    height="256px" 
                    id="Layer_1" 
                    version="1.1"
                    viewBox="0 0 256 256" 
                    width="256px" 
                    xmlSpace="preserve" 
                    xmlns="http://www.w3.org/2000/svg" 
                    xmlnsXlink="http://www.w3.org/1999/xlink"
                  >
                    <path d="M91.474,33.861l-89.6,89.6c-2.5,2.5-2.5,6.551,0,9.051l89.6,89.6c2.5,2.5,6.551,2.5,9.051,0s2.5-6.742,0-9.242L21.849,134  H249.6c3.535,0,6.4-2.466,6.4-6s-2.865-6-6.4-6H21.849l78.676-78.881c1.25-1.25,1.875-2.993,1.875-4.629s-0.625-3.326-1.875-4.576  C98.024,31.413,93.974,31.361,91.474,33.861z" />
                  </svg> 
                  <span className="mb-hide" style={{display:'inherit'}}>Back</span>
                </a>
              </div>}
            {showRecipe && <div className="choose-recipe-cntnt-box">
              <h1 className="mb-hide">{headingText}</h1>
              <p className="after-hdng mb-hide">{subHeadingText}</p>
              {
                action === 'swap' &&
                <DisplaySwapRecipe recipeArr={recipes[recipeIndex]}/>
              }
              <div className="mb-hide">
                <RecipeHeader 
                  proteins={proteins}
                  equipments={equipments}
                  diet={diet}
                  divOpaque={(value)=>this.divOpaque(value)}
                  applyRecipeFilters={(data, equipmentsList)=>this.applyRecipeFilters(data, equipmentsList)}
                  viewAllRecipe={()=>this.viewAllRecipe(0)}
                  getRecipes={(data)=>this.removeFilter(data)}
                  viewAllRecipeData={viewAllRecipe}
                  applyFilters={applyFilters}
                />
              </div>
            </div>
            }
          </div>
         
          {showRecipe &&
          <div>
            <div className="mb-only">
              <RecipeList  
                viewAllRecipe={()=>this.viewAllRecipe(0)} 
                viewAllRecipeData={viewAllRecipe} 
                applyFilters={applyFilters}
                categoryName={categoryName}
                recipeRange={recipeRange}
                action={action}
                openChat={()=>this.openChatBox()}
              />
            </div>
            <div style={{opacity, transition:'.5s'}} className="selct-rcipe">
              {recipeData.length > 0 && this.recipeData() }
            </div>
          </div>
          }
          <BottomBar 
            removeRecipe={(event, key, image, items ,id)=>
              this.addMeal(event, key,image, items, id)}
            onPressCancel={()=>this.onPressCancel()}
            onPressSave={()=>this.onPressSave()}
            location="select"
            pathFrom='chooseRecipe'
            emptyRecipeArr={()=>this.emptyRecipeArr()}
            {...this.state}
          />
        </section>
        {
          showViewRecipe &&
            <div id="schedule-popup" className="fullwidth_Modal modal fade in" role="dialog" style={{ display: 'block', overflowY:'hidden' }}>
              <div className="modal-dialog" id="modal-dialog">
                {/* <div className="modal-content edt-cook-popup" style={{background: 'transparent', boxShadow: 'none'}}> */}
                <ViewMeal location={recipelocation} closeMeal={(data,count)=>this.closeMeal(data, count)}/>
                {/* </div> */}
              </div>
            </div>
        }
      </div>
    );
  }
}

export default SelectRecipe;
