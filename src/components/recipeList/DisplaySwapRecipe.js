import React from 'react'

const DisplaySwapRecipe = props => {
  const { recipeArr } = props
  const recipeName = recipeArr.items.recipe.name
  let recipeImage = recipeArr.items.recipe.image

  const setrecipeImage = recipeImage
  if(recipeArr.items.recipe.image !== null) {
    recipeImage = recipeArr.items.recipe.image.split('mpn-recipe-images/')[0];
    const checkImage = setrecipeImage.includes('mpn-recipe-images/')
    if(checkImage){
      recipeImage += `mpn-recipe-images/Images/${recipeArr.items.recipe.recipe_id}-EH720.jpg` 
    }else{
      recipeImage = setrecipeImage
    }
  }
  const sideName = recipeArr.items.recipe.recipe_sides.length > 0 ? recipeArr.items.recipe.recipe_sides[0].recipe.name: null
  let equipmentIcon;
  if (recipeArr.items.recipe.recipe_equipment_type.length > 0) {
    equipmentIcon = `/images/${recipeArr.items.recipe.recipe_equipment_type[0].equipment_type.icon}`;
    if (equipmentIcon === '/images/') {
      equipmentIcon = '/images/none.svg';
    }
  } else {
    equipmentIcon = '/images/none.svg';
  }
  return(
  					
    <div className="outer-box-className">
      <div className="new-box-add">
        <h4> Swapping Out This Recipe: </h4>
        <div className="combined-section">
          <div className="new-box">
            <img src={recipeImage} alt="" /></div>
          <div className="inner-section">
            <h5>{recipeName} </h5>
            <p> {sideName}</p>
            <div className="swap-icons"> 
              <div className="meal-post-data small-device-icons  ">
                <div className="meal-post-data-box meal-post-data-img">
                  <img
                    src={equipmentIcon}
                 
                    alt="icon"
                  />
                </div>
              </div>
              <div className="meal-post-data-box mb-hide">
                <p> 497 Cals</p>
              </div>

              <div className="meal-post-data-box">
                <img src="/images/hands.svg" alt="" className="hands-svg" />
                {recipeArr.totalHands}
              </div>
              <div className="meal-post-data-box mb-only">
                <img src="/images/time.svg" alt="" className="hands-svg" />
                {recipeArr.totalTime}
              </div>

              <div className="meal-post-data-box mb-hide">
                <p> {recipeArr.servingSize} Servings </p>
              </div>
            </div>
          </div>
        </div>

      </div>
    </div>
  )}

export default DisplaySwapRecipe