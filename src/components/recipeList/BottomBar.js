import React from 'react'
import DashboardBottom from '../dashboard/DashboardBottom'
import RecipeBottom from './RecipeBottom'

const BottomBar = props => {
  const recipeArr = props.location === 'select' ? props.recipeArr : props.data ? props.data: []
  const { 
    showBottom, count, recipeRange, cookingDates, showRecipe,
    servingSize, lunchDates, dinnerDates, inputs, varietyIndex, variety, dayDates,
    action, recipeCount
  } = props
  const lb = recipeRange.split('-')[0]
  const ub = recipeRange.split('-')[1]
  // let { bottomClass } = props
  // bottomClass = (recipeArr.length > 0 || recipeCount >= 1) && showRecipe ? '' : 'mb-hide'
  return (
    showBottom && action!=='swap' && action!=='add' ? 
      <div >
        <RecipeBottom 
          count={count}
          recipeArr={recipeArr}
          removeRecipe={(event, key, image, items ,id)=>
            props.removeRecipe(event, key,image, items, id)}
          lb={lb}
          recipeCount={recipeCount}
          cookingDates={cookingDates}
          ub={ub}
          servingSize={servingSize}
          lunchDates={lunchDates}
          dinnerDates={dinnerDates}
          inputs={inputs}
          varietyIndex={varietyIndex}
          variety={variety}
          dayDates={dayDates}
          // animateClass={recipeArr.length > 0 && recipeCount === 1 ? 'test' : ''}
          animateClass=''
        />
      </div>:
      showRecipe &&
      <DashboardBottom 
        mbClass=""
        editRecipe
        cookDayArr=""
        onPressCancel={()=>props.onPressCancel()}
        onPressSave={()=>props.onPressSave()}
        showRecipe={recipeArr.length > 0}
        recipeArr={recipeArr}
        showBottom={false}
        path="recipe"
        recipeCount={recipeCount}
        removeRecipe={()=>props.emptyRecipeArr()}
        action={action}
        // animateClass={recipeArr.length > 0 && recipeCount === 1 ? 'test' : ''}
        animateClass=''
      />
  )
}

export default BottomBar;