import React, { Component } from 'react';

class RecipeHeader extends Component {
  constructor(props) {
    super(props)
    this.state = {
      proteinsList: props.proteins,
      equipmentsList:props.equipments,
      diet:props.diet,
      equipments:[],
      trackEquipments:[],
      viewAllRecipe: props.viewAllRecipeData,
      applyFilters: props.applyFilters,
      inputs:{
        proteinsData: [],
        equipmentsData: [],
        tagsData: [],
        mealTypeData:[]
      },
      mealTypeList:['Breakfast', 'Lunch/Dinner']
    }
  }

  componentDidMount() {
    this.setFilterData()
  }
  // eslint-disable-next-line
  UNSAFE_componentWillReceiveProps(props) {
    this.setState({
      proteinsList: props.proteins,
      equipmentsList:props.equipments,
      diet:props.diet,
      viewAllRecipe: props.viewAllRecipeData,
      applyFilters: props.applyFilters,
    })
  }

  setFilterData() {
    let { inputs, equipments } = this.state
    if(sessionStorage.getItem('filter')) {
      const filterDataList = JSON.parse(sessionStorage.getItem('filter'))
      inputs = filterDataList.filterTags;
      equipments = filterDataList.equipmentsList;
    }
    this.setState({ inputs, equipments })
  }

  updateCheck(event, name) {
    const { inputs, equipments } = this.state
    const isValueExists = inputs[name].includes(event.target.value);
    if (isValueExists) {
      const index = inputs[name].indexOf(event.target.value);
      if (index > -1) {
        inputs[name].splice(index, 1);
        if(name === 'equipmentsData') {
          equipments.splice(index, 1)
        }        
      }
       
    } else {
      inputs[name].push(event.target.value)
    }
    this.trackFilter()
    this.setState({ inputs, equipments })
    const proteinLength = inputs.proteinsData.length
    const equipmentLength = inputs.equipmentsData.length
    const dietLength = inputs.tagsData.length
    const mealTypeLength = inputs.mealTypeData.length
    if(proteinLength > 0 || equipmentLength > 0 || dietLength > 0 || mealTypeLength > 0) {
      this.props.applyRecipeFilters(inputs, equipments)
    }else {
      this.removeData();
    }
  }

  updateEquipment(event, name) {
    const { inputs, equipments, trackEquipments } = this.state
    const value = parseInt(event.target.value,0)
    const isValueExists = inputs[name].includes(value);
    if (isValueExists) {
      const index = inputs[name].indexOf(value);
      if (index > -1) {
        inputs[name].splice(index, 1);
        equipments.splice(index, 1)
        trackEquipments.splice(index, 1)
      }
       
    } else {
      inputs[name].push(value)
      equipments.push({name:event.target.name, id: event.target.value})
      trackEquipments.push(event.target.name)
    }
    this.trackFilter()
    this.setState({ inputs, equipments, trackEquipments })
    const proteinLength = inputs.proteinsData.length
    const equipmentLength = inputs.equipmentsData.length
    const dietLength = inputs.tagsData.length
    const mealTypeLength = inputs.mealTypeData.length
    if(proteinLength > 0 || equipmentLength > 0 || dietLength > 0 || mealTypeLength > 0) {
      this.props.applyRecipeFilters(inputs, equipments)
    }else {
      this.removeData();
    }
  }

  trackFilter() {
    const { inputs, trackEquipments } = this.state
    const appliedFiltersNumber = inputs.proteinsData.length + inputs.equipmentsData.length + inputs.tagsData.length + inputs.mealTypeData.length
    const appliedFiltersName = inputs.proteinsData.concat(inputs.tagsData.concat(trackEquipments))
    window.analytics.track('Select Meals: Filters Applied', {
      appliedFiltersNumber,
      appliedFiltersName
    });
  }

  removeCheck(event, name) {
    const { inputs, equipments, diet, proteinsList,mealTypeList, equipmentsList, viewAllRecipe } = this.state
    const isValueExists = inputs[name].includes(event);
  
    if (isValueExists) {
      const index = inputs[name].indexOf(event);
      if (index > -1) {
        inputs[name].splice(index, 1);
        if(name === 'equipmentsData') {
          equipments.splice(index, 1)
        }
      }  
      this.setState({ inputs, equipments })
    }
    const proteinLength = inputs.proteinsData.length
    const equipmentLength = inputs.equipmentsData.length
    const dietLength = inputs.tagsData.length
    const mealTypeLength = inputs.mealTypeData.length
    if(proteinLength > 0 || equipmentLength > 0 || dietLength > 0 || mealTypeLength>0) {
      this.props.applyRecipeFilters(inputs, equipments)
    } else{
      inputs.diet = diet
      inputs.proteins = proteinsList
      inputs.equipments = equipmentsList
      inputs.mealType = mealTypeList
      if(viewAllRecipe) {
        this.props.viewAllRecipe()
      } else {
        sessionStorage.removeItem('viewAll')
        this.props.getRecipes(inputs)
      }
    }
  }

  removeData(value) {
    const { inputs, diet, proteinsList, equipmentsList, mealTypeList, viewAllRecipe, applyFilters } = this.state
    inputs.proteinsData = []
    inputs.equipmentsData = []
    inputs.tagsData = []
    inputs.mealTypeData = []
    this.setState({ inputs, equipments:[] })
    inputs.diet = diet
    inputs.proteins = proteinsList
    inputs.equipments = equipmentsList
    inputs.mealType = mealTypeList
    sessionStorage.removeItem('filter')
    if(viewAllRecipe && applyFilters && value === null) {
      this.props.viewAllRecipe()
    } else {
      sessionStorage.removeItem('viewAll')
      this.props.getRecipes(inputs)
    }
    
  }


  render() {
    const { proteinsList, equipmentsList, diet,  equipments, viewAllRecipe, applyFilters, mealTypeList } = this.state
    const { proteinsData, tagsData, equipmentsData, mealTypeData } = this.state.inputs
    const mealTypeDataActive =  mealTypeData && mealTypeData.length > 0 ? "dropdown protein-filtr active" : "dropdown protein-filtr"
    const proteinActive =  proteinsData && proteinsData.length > 0 ? "dropdown protein-filtr active" : "dropdown protein-filtr"
    const equipmentActive =  equipmentsData && equipmentsData.length > 0 ? "dropdown protein-filtr active" : "dropdown protein-filtr"
    const tagsActive =  tagsData.length > 0 ? "dropdown protein-filtr active" : "dropdown protein-filtr"
    const filterText = applyFilters ? '(x)clear filters & return to normal view': '(x)return to normal view'
    return(
      <div className="choose-recipe-filters">
        <div className={proteinActive}>
          <button className="cstm-drp" type="button" id="proteindrp" data-toggle="dropdown" onClick={()=>this.props.divOpaque('drp')}>
            Protein
            <i className="fa fa-caret-down" />
          </button>
          <ul className="dropdown-menu drp-menu">
            {
              proteinsList.length > 0 &&
            proteinsList.map(data => {
              const index = proteinsData.includes(data.category)
              let checked = false
              if(index) {
                checked = true
              }
              return(
                <li key={data.category}>
                  <label className="container1 filtr-label">
                    <input type="checkbox" value={data.category} checked={checked} onChange={(event)=>this.updateCheck(event, 'proteinsData', data.category)} /> &nbsp;
                    <span className="checkmark1" tabIndex="0" role="button"/>
                    {data.category}
                  </label>
                </li>

              )})
            }
          </ul>
        </div>
        <div className={equipmentActive}>
          <button className="cstm-drp" type="button" id="cookingdrp" data-toggle="dropdown" onClick={()=>this.props.divOpaque('drp')}>Cook Method
            <i className="fa fa-caret-down" />
          </button>
          <ul className="dropdown-menu drp-menu">
            {
              equipmentsList.length > 0 &&
            equipmentsList.map(data => {
              // const index = equipmentsData.findIndex(x=>x.id === data.id.toString())
              const index = equipmentsData.indexOf(data.id)
              let checked = false
              if(index > -1) {
                checked = true
              }
              return(
                <li key={data.title}>
                  <label className="container1 filtr-label">
                    <input type="checkbox" name={data.title} checked={checked} value={data.id} onChange={(event)=>this.updateEquipment(event, 'equipmentsData')} /> 
                    <span className="checkmark1" tabIndex="0" role="button"/>
                &nbsp;{data.title}
                  </label>
                </li>

              )})
            }
          </ul>
        </div>
        <div className={tagsActive}>
          <button className="cstm-drp" type="button" id="dietdrp" data-toggle="dropdown" onClick={()=>this.props.divOpaque('drp')}>Diet or Restrictions
            <i className="fa fa-caret-down" />
          </button>
          <ul className="dropdown-menu drp-menu">
            {
              diet.length > 0 &&
            diet.map(data => {
              const index = tagsData.includes(data)
              let checked = false
              if(index) {
                checked = true
              }
              return(
                <li key={data}>
                  <label className="container1 filtr-label">
                    <input type="checkbox" value={data} checked={checked} onChange={(event)=>this.updateCheck(event, 'tagsData')} /> 
                    <span className="checkmark1" tabIndex="0" role="button"/>
                &nbsp;{data}
                  </label>
                </li>

              )}
            )
            }
          </ul>
        </div>
        <div className={mealTypeDataActive}>
          <button className="cstm-drp" type="button" id="mealtypedrp" data-toggle="dropdown" onClick={()=>this.props.divOpaque('drp')}>Meal Type
            <i className="fa fa-caret-down" />
          </button>
          <ul className="dropdown-menu drp-menu">
            { mealTypeList.length > 0 &&  mealTypeList.map(getData => {
              const index = mealTypeData.includes(getData)
             
              let checked = false
              if(index) {
                checked = true
              }
              return(
                <li key={getData}>
                  <label className="container1 filtr-label">
                    <input type="checkbox" value={getData} checked={checked} onChange={(event)=>this.updateCheck(event, 'mealTypeData')} /> &nbsp;
                    <span className="checkmark1" tabIndex="0" role="button"/>
                    {getData}
                  </label>
                </li>

              )})
            }
          </ul>
        </div>
        { ((proteinsData && proteinsData.length > 0) || (equipments && equipments.length > 0) || ( tagsData && tagsData.length > 0) || ( mealTypeData && mealTypeData.length > 0)) &&
          <a onClick={()=>this.removeData(null)} className="clear-all-filtr">(x)clear all filters</a>}
        {viewAllRecipe ? 
          <a onClick={()=>this.removeData('all')} className="clear-all-filtr">
            {filterText}
          </a> :
          !applyFilters &&
        <a onClick={()=>this.props.viewAllRecipe()} className="clear-all-filtr" style={{ color: '#13B75E'}}>
          Click to view all Recipes
        </a> 
        }
        <div className="choose-recipe-filters-selected">
          {proteinsData.length > 0 && 
            proteinsData.map((value)=>(
              <span className="filtr-selected" key={value}>
                {value} 
                <i className="fa fa-close" value={value} name={value} onClick={()=>this.removeCheck(value,'proteinsData')} />
              </span>
            ))
          }
          {equipments && equipments.length > 0 &&
            equipments.map((value)=>(
              <span className="filtr-selected" key={value}>
                {value.name} 
                <i className="fa fa-close" value={value.id} name={value.name} onClick={()=>this.removeCheck(parseInt(value.id,0),'equipmentsData')}/>
              </span>
            ))
          }
          {tagsData.length > 0 &&
            tagsData.map((value)=>(
              <span className="filtr-selected" key={value}>
                {value} 
                <i className="fa fa-close" value={value} name={value} onClick={()=>this.removeCheck(value,'tagsData')}/>
              </span>
            ))
          }
          {mealTypeData.length > 0 &&
            mealTypeData.map((value)=>(
              <span className="filtr-selected" key={value}>
                {value} 
                <i className="fa fa-close" value={value} name={value} onClick={()=>this.removeCheck(value,'mealTypeData')}/>
              </span>
            ))
          }
          
        </div>
      </div>
    )
  }

}

export default RecipeHeader