import React, { Component } from 'react';

class RecipeMobileHeader extends Component {
  constructor(props) {
    super(props)
    this.state = {
      proteinsList: props.proteins,
      equipmentsList:props.equipments,
      diet:props.diet,
      equipments:[],
      trackEquipments:[], 
      inputs:{
        proteinsData: [],
        equipmentsData: [],
        tagsData: [],
        mealTypeData:[]
      },
      showFilter: !props.showFilter,
      applyFilters: props.applyFilters,
      viewAllRecipe: props.viewAllRecipeData,
      mealTypeList:['Breakfast', 'Lunch/Dinner']
    }
  }

  componentDidMount() {
    this.setFilterData()
  }
  
  // eslint-disable-next-line
  UNSAFE_componentWillReceiveProps(props) {
    this.setState({
      proteinsList: props.proteins,
      equipmentsList:props.equipments,
      diet:props.diet,
      applyFilters: props.applyFilters,
      showFilter: !props.showFilter,
      viewAllRecipe: props.viewAllRecipeData,
    })
  }

  setFilterData() {
    let { inputs, equipments } = this.state
    if(sessionStorage.getItem('filter')) {
      const filterDataList = JSON.parse(sessionStorage.getItem('filter'))
      inputs = filterDataList.filterTags;
      equipments = filterDataList.equipmentsList;
    }
    this.setState({ inputs, equipments })
  }

  updateCheck(event, name, id) {
    const { inputs, equipments, trackEquipments } = this.state
    const isValueExists = inputs[name].includes(event);
    if (isValueExists) {
      const index = inputs[name].indexOf(event);
      if (index > -1) {
        inputs[name].splice(index, 1);
        if(name === 'equipmentsData') {
          equipments.splice(index, 1)
        }
      }
       
    } else {
      inputs[name].push(event)
      if(name === 'equipmentsData') {
        equipments.push({name: id, id: event})
        trackEquipments.push(id)
      }
    }
    this.trackFilter()
    this.setState({ inputs, equipments })
  }

  removeCheck(event, name) {
    const { inputs, equipments, diet, proteinsList, equipmentsList,mealTypeList, viewAllRecipe } = this.state
    const isValueExists = inputs[name].includes(event);
    if (isValueExists) {
      const index = inputs[name].indexOf(event);
      if (index > -1) {
        inputs[name].splice(index, 1);
        if(name === 'equipmentsData') {
          equipments.splice(index, 1)
        }
      }  
    }
    if(inputs[name].length > 0) {
      this.props.applyRecipeFilters(inputs, equipments)
    } else{
      inputs.diet = diet
      inputs.proteins = proteinsList
      inputs.equipments = equipmentsList
      inputs.mealType = mealTypeList
      if(viewAllRecipe) {
        this.props.viewAllRecipe()
      } else {
        sessionStorage.removeItem('viewAll')
        this.props.getRecipes(inputs)
      }
    }
    this.setState({ inputs, equipments })
  }

  applyFilters() {
    const { inputs, diet, proteinsList, equipmentsList, mealTypeList, viewAllRecipe } = this.state
    let { equipments } = this.state
    if( inputs.proteinsData.length === 0 && inputs.equipmentsData.length === 0 && inputs.tagsData.length === 0 && inputs.mealTypeData.length === 0) {
      inputs.diet = diet
      inputs.proteins = proteinsList
      inputs.equipments = equipmentsList
      inputs.mealType = mealTypeList
      inputs.proteinsData = []
      inputs.equipmentsData = []
      inputs.mealTypeData =[]
      inputs.tagsData = []
      equipments = []
      document.ontouchmove = () => (true)
      document.body.style.overflow = ''
      if(viewAllRecipe) {
        setTimeout(()=>{this.props.viewAllRecipe()},500)
      } else {
        sessionStorage.removeItem('viewAll')
        setTimeout(()=>{this.props.getRecipes(inputs)},500)
      }
    } else {
      setTimeout(()=>{this.props.applyRecipeFilters(inputs, equipments)},500)
    }
    this.setState({ inputs, equipments, filterAnimation: 'animateFilterDown' })
    
  }

  showFilter() {
    const { showFilter, inputs, applyFilters, diet, proteinsList, equipmentsList, mealTypeList } = this.state
    let { equipments, filterAnimation } = this.state
    const animtaionTime = showFilter ? 1000 : 0
    document.getElementsByTagName('body')[0].style.overflow = 'hidden'
    if(!showFilter) {
      filterAnimation = 'animateFilterUp'
      document.ontouchmove = (e) => { e.preventDefault(); }
      document.body.style.overflow = 'hidden'
    } else {
      filterAnimation = 'animateFilterDown'
      document.ontouchmove = () => (true)
      document.body.style.overflow = ''
      // document.getElementsByTagName('body')[0].className = ''
    }
    
    if(!applyFilters) {
      inputs.proteinsData = []
      inputs.equipmentsData = []
      inputs.tagsData = []
      inputs.mealTypeData = []
      equipments = []
    } else if( inputs.proteinsData.length === 0 && inputs.mealTypeData.length === 0 && inputs.equipmentsData.length === 0 &&  inputs.tagsData.length === 0) {
      inputs.diet = diet
      inputs.proteins = proteinsList
      inputs.equipments = equipmentsList
      inputs.mealType = mealTypeList
      inputs.proteinsData = []
      inputs.equipmentsData = []
      inputs.tagsData = []
      inputs.mealTypeData = []
      equipments = []
      setTimeout(()=>{this.props.getRecipes(inputs)},animtaionTime)
    } 
    setTimeout(()=>{this.props.showRecipe()},animtaionTime)
    this.setState({ inputs, equipments, filterAnimation, showFilter: !showFilter })

  }

  trackFilter() {
    const { inputs, trackEquipments } = this.state
    const appliedFiltersNumber = inputs.proteinsData.length + inputs.equipmentsData.length + inputs.tagsData.length + inputs.mealTypeData.length
    const appliedFiltersName = inputs.proteinsData.concat(inputs.tagsData.concat(trackEquipments))
    window.analytics.track('Select Meals: Filters Applied', {
      appliedFiltersNumber,
      appliedFiltersName
    });
  }


  render() {
    const { 
      proteinsList, equipmentsList, diet,  equipments, inputs, 
      showFilter, applyFilters , filterAnimation , mealTypeList
    } = this.state
    const { proteinsData, tagsData, equipmentsData, mealTypeData } = this.state.inputs
    const length = proteinsData.length + equipmentsData.length + tagsData.length + mealTypeData.length
    const applyActive = (proteinsData.length > 0 || mealTypeData.length > 0 || equipments.length > 0 || tagsData.length > 0) || applyFilters ? "active": ""
    
    return(
      <div className="container mb-only">
        <div className="filter-dropdown-main">
          <a className="filter-mb-btn active" onClick={()=>this.showFilter()}>
            <svg className="active-svg" width="20px" height="14px" viewBox="0 0 20 14" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
              <title>Icon / Action / Mobile Menu</title>
              <desc>Created with Sketch.</desc>
              <defs>
                <path d="M6.47085405,3 C4.91768433,3.00140263 3.56996301,4.05746255 3.21735201,5.54940919 L0.76818798,5.54940919 C0.34392948,5.5494092 1.55760097e-08,5.88863925 1.55760097e-08,6.30710073 C1.55760096e-08,6.72556221 0.34392948,7.06479226 0.76818798,7.06479227 L3.21735201,7.06479227 C3.57511899,8.55163916 4.92153614,9.60136494 6.47085405,9.60136494 C8.02017196,9.60136494 9.36658911,8.55163916 9.72435608,7.06479227 L19.231812,7.06479227 C19.6560705,7.06479227 20,6.72556221 20,6.30710073 C20,5.88863925 19.6560705,5.54940919 19.231812,5.54940919 L9.72435608,5.54940919 C9.37174508,4.05746255 8.02402377,3.00140263 6.47085405,3 Z M6.47085405,8.09881838 C5.47259874,8.09881838 4.66335292,7.30063 4.66335292,6.31601475 C4.66335292,5.33139949 5.47259874,4.53321112 6.47085405,4.53321112 C7.46910936,4.53321112 8.27835518,5.33139949 8.27835518,6.31601475 C8.27835518,7.30063 7.46910936,8.09881838 6.47085405,8.09881838 Z M13.5562585,10.3986351 C12.0030888,10.4000377 10.6553674,11.4560976 10.3027564,12.9480442 L0.76818798,12.9480442 C0.343929485,12.9480443 3.1152016e-08,13.2872743 3.11520159e-08,13.7057358 C3.11520159e-08,14.1241973 0.343929485,14.4634273 0.76818798,14.4634273 L10.2756439,14.4634273 C10.6334109,15.9502742 11.9798281,17 13.529146,17 C15.0784639,17 16.424881,15.9502742 16.782648,14.4634273 L19.231812,14.4634273 C19.6560705,14.4634273 20,14.1241973 20,13.7057358 C20,13.2872743 19.6560705,12.9480442 19.231812,12.9480442 L16.782648,12.9480442 C16.4323894,11.4657729 15.0992786,10.4123834 13.5562585,10.3986351 Z M13.5562585,15.4796254 C12.5580032,15.4796254 11.7487574,14.681437 11.7487574,13.6968218 C11.7487574,12.7122065 12.5580032,11.9140181 13.5562585,11.9140181 C14.5545138,11.9140181 15.3637596,12.7122065 15.3637596,13.6968218 C15.3637596,14.681437 14.5545138,15.4796254 13.5562585,15.4796254 Z" id="path-1" />
              </defs>
              <g id="Select-Meals" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                <g id="Mobile:-Select-Meals-4b" transform="translate(-324.000000, -16.000000)">
                  <g id="Logo-&amp;-Nav">
                    <g id="Icon-/-Action-/-Filters" transform="translate(324.000000, 13.000000)">
                      <mask id="mask-2" fill="white">
                        <use xlinkHref="#path-1" />
                      </mask>
                      <use id="Mask" fill="" fillRule="nonzero" xlinkHref="#path-1" />
                      <g id="Swatch-/-Dark-Gray" mask="url(#mask-2)" fill={length && applyFilters > 0 ? "#00C853": '#4A4A4A'} fillRule="evenodd">
                        <rect id="Rectangle-5" x="0" y="0" width="20" height="20" />
                      </g>
                    </g>
                  </g>
                </g>
              </g>
            </svg>
            {length > 0 && applyFilters && <span className="filtr-count">{length}</span>}
          </a>
	
          {showFilter && <div className={`filter-mb-cntnt ${filterAnimation}`}>
            <h5>Select Filters</h5>
            
            <div className="filter-mb-cntnt-row">
              <h6>Protein</h6>
              {
                proteinsList.length > 0 &&
                proteinsList.map(data => {
                  const index = proteinsData.includes(data.category)
                  let checked = false
                  if(index) {
                    checked = true
                  }
                  return(
                    <span className={checked ? "filtr-optn active": "filtr-optn"} key={data.category} onClick={()=>this.updateCheck(data.category, 'proteinsData')} >{data.category}</span>
                  )}
                )
              }
            </div>
            <div className="filter-mb-cntnt-row">
              <h6>Cook Method</h6>
              {
                equipmentsList.length > 0 &&
                equipmentsList.map(data => {
                  const index = inputs.equipmentsData.includes(data.id)
                  let checked = false
                  if(index) {
                    checked = true
                  }
                  return(
                    <span 
                      className={checked ? "filtr-optn active": "filtr-optn"} 
                      key={data.title} 
                      onClick={()=>this.updateCheck(parseInt(data.id,0), 'equipmentsData',data.title)} 
                    >
                      {data.title}
                    </span>
                  )}
                )
              }
            </div>
            <div className="filter-mb-cntnt-row">
              <h6>Diet & Restrictions</h6>
              {
                diet.length > 0 &&
                diet.map(data => {
                  const index = tagsData.includes(data)
                  let checked = false
                  if(index) {
                    checked = true
                  }
                  return(
                    <span 
                      className={checked ? "filtr-optn active": "filtr-optn"} 
                      key={data} 
                      onClick={()=>this.updateCheck(data, 'tagsData')} 
                    >
                      {data}
                    </span>
                  )})
              }
            </div>
            <div className="filter-mb-cntnt-row">
              <h6>Meal Type</h6>
              {
                mealTypeList.length > 0 &&
                mealTypeList.map(data => {
                  const index = mealTypeData.includes(data)
                  let checked = false
                  if(index) {
                    checked = true
                  }
                  return(
                    <span className={checked ? "filtr-optn active": "filtr-optn"} key={data} onClick={()=>this.updateCheck(data, 'mealTypeData')} >{data}</span>
                  )}
                )
              }
            </div>
            <div className={`filter-dropdown-mb-ftr ${filterAnimation === 'animateFilterUp' ? 'test' : filterAnimation}`}>
              <a onClick={()=>this.showFilter()}>Cancel</a>
              <button className={applyActive} onClick={()=>this.applyFilters()}>Apply</button>
            </div>
          </div>}
        </div>
      </div>
    )
  }

}

export default RecipeMobileHeader