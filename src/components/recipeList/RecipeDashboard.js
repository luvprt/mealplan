import React, { Component } from 'react';
import DashHeader from '../dashboard/DashHeader';
import RecipeFilter from './RecipeFilter';

class RecipeDashboard extends Component {
  constructor(props) {
    super(props);
    const {
      rname, size, totalTime, totalHandsTime, startDate, endDate,
      dayDates, weekId, type, fDate, fId, recipeImg, equipmentIcon,
      totalServing, mealDate, recipeSize, previousRId
    } = props.location.state;
    this.state = {
      recipeName: rname || null,
      servingSize: size || null,
      totalHandsTime,
      totalTime,
      startDate,
      endDate,
      dayDates,
      weekId,
      type,
      fDate,
      fId,
      recipeImg,
      equipmentIcon,
      totalServing,
      mealDate,
      recipeSize,
      previousRId
    };
  }

  render() {
    const {
      recipeName, servingSize, totalTime, totalHandsTime, startDate,
      endDate, dayDates, weekId, type, fDate, fId, recipeImg,
    } = this.state;
    return (
      <div className="wrapper dash-cntnt">
        <DashHeader
          dashClass="active"
        />
        <RecipeFilter
          recipeName={recipeName}
          servingSize={servingSize}
          totalTime={totalTime}
          totalHandsTime={totalHandsTime}
          startDate={startDate}
          endDate={endDate}
          dayDates={dayDates}
          weekId={weekId}
          type={type}
          fDate={fDate}
          fId={fId}
          recipeImg={recipeImg}
          {...this.state}
        />
      </div>
    );
  }
}

export default RecipeDashboard;
