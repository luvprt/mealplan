import React, { Component } from 'react';
import LazyLoad from 'react-image-lazy-load';
import { get } from 'lib/api';
import RoutingConstants from 'lib/RoutingConstants';
import RecipeList from './RecipeList';
import history from '../../history';

const MobileRecipeFilter = props => {
  const { recipeName } = props;
  return(
    <div className="mb-only">
      <button className="btn back-to-plan" onClick={() => props.pressBackBtn()}>
        <svg
          height="512px"
          id="Layer_1"
          enableBackground="new 0 0 512 512"
          version="1.1"
          viewBox="0 0 512 512"
          width="512px"
          xmlSpace="preserve"
          xmlns="http://www.w3.org/2000/svg"
          xmlnsXlink="http://www.w3.org/1999/xlink"
        >
          <polygon points="352,115.4 331.3,96 160,256 331.3,416 352,396.7 201.5,256 " />
        </svg>
              Back to Weekly Plan
      </button>
      <div className="swap-mode-dtail-cntnt">
        <h4>Let’s Find A New Recipe to Swap In To Your Plan! </h4>
        <div className="side-tag-lft">
          <div className="meal-breakdown-row-side-tag">Swap out</div>
          <div className="side-hdng-cntnt">{recipeName}</div>
        </div>
      </div>
    </div>
  )
}


class RecipeFilter extends Component {
  constructor(props) {
    super(props);
    this.state = {
      PROTEIN_LIST: [],
      equipmentList: [],
      handsOnCheck1: false,
      handsOnCheck2: false,
      totalTimeCheck: false,
      totalTimeValue: '',
      handsOnValueFilter: '',
      proteinFilter: [],
      cookingFilter: [],
      recipeList: [],
      counter: 0,
      loadData: true,
      tcount: 0,
      hcount: 0,
      pcount: 0,
      ccount: 0,
      clickAccordion: false,
      filterShow: "mb-hide",
      ...this.props,
    };
  }

  componentDidMount() {
    const url = RoutingConstants.fetchProteinExclusion;
    get(url).then((response) => {
      this.setState({ PROTEIN_LIST: response });
    }).catch(() => {
      // eslint-disable-next-line
      // console.log('protein exclusion error', err);
    });
    const link = RoutingConstants.getEquipmentList;
    get(link).then((response) => {
      this.setState({ equipmentList: response });
    }).catch(() => {
      // eslint-disable-next-line
      // console.log('equipment error', err);
    });

    this.recipeDetail(0);
  }

  pressBackBtn() {
    const { dayDates, recipeName, recipeSize } = this.state;
    // Canceled swap recipe
   
    window.analytics.track('Cancelled Swap Recipe', {
      recipeName,
      startDate: `${dayDates[0].fullDay} ${dayDates[0].date}-${dayDates[0].Month}-${dayDates[0].Year}`,
      endDate: `${dayDates[6].fullDay} ${dayDates[6].date}-${dayDates[6].Month}-${dayDates[6].Year}`,
    });

    localStorage.setItem('method', 'back');
    history.push('/dashboard', { guestEmail: 'response.FirstName', type: '/login', dayDates, recipeSize });
  }


  // hands on filter
  handsOnFilter(value) {
    const { totalTimeValue, proteinFilter, cookingFilter } = this.state;
    let {
      handsOnCheck1, handsOnCheck2, hcount
    } = this.state;
    let handsOnValueFilter;
    let name;
    if (value === '1' && handsOnCheck1 === false) {
      handsOnCheck1 = true;
      handsOnCheck2 = false;
      handsOnValueFilter = 10;
      hcount = 1;
    } else if (value === '0' && handsOnCheck2 === false) {
      handsOnCheck2 = true;
      handsOnCheck1 = false;
      handsOnValueFilter = 20;
      hcount = 1;
    } else if (handsOnValueFilter !== '') {
      handsOnCheck2 = false;
      handsOnCheck1 = false;
      handsOnValueFilter = '';
      name = '';
      hcount = 0;
    }
    this.setState({
      handsOnCheck1, handsOnCheck2, handsOnValueFilter, name, loadData: true, hcount
    });
    this.recipeDetail(0, handsOnValueFilter, totalTimeValue, proteinFilter, cookingFilter);
  }

  // total time filter
  totalTimeFilter() {
    let { totalTimeCheck, name, totalTimeValue, tcount } = this.state;
    const { handsOnValueFilter, proteinFilter, cookingFilter } = this.state;
    if (totalTimeCheck === true) {
      totalTimeCheck = false;
      totalTimeValue = '';
      name = '';
      tcount = 0;
    } else {
      totalTimeCheck = true;
      totalTimeValue = 30;
      tcount = 1;
    }
    this.setState({
      totalTimeCheck, totalTimeValue, name, loadData: true, tcount
    });
    this.recipeDetail(0, handsOnValueFilter, totalTimeValue, proteinFilter, cookingFilter);
  }

  // protein list filter
  proteinFilter(value) {
    const {
      proteinFilter, cookingFilter, handsOnValueFilter, totalTimeValue,
    } = this.state;
    let { pcount } = this.state;
    const isValueExists = proteinFilter.indexOf(value);

    if (isValueExists > -1) {
      proteinFilter.splice(isValueExists, 1);
      if(proteinFilter.length === 0) {
        pcount = 0
      }
    } else {
      proteinFilter.push(value);
      pcount = 1;
    }
    this.setState({ proteinFilter, loadData: true, pcount });
    // console.log('proteinFilter', proteinFilter);
    this.recipeDetail(0, handsOnValueFilter, totalTimeValue, proteinFilter, cookingFilter);
  }

  // cooking method filter
  cookingFilter(value) {
    const {
      proteinFilter, cookingFilter, handsOnValueFilter, totalTimeValue,
    } = this.state;
    const isValueExists = cookingFilter.indexOf(value);
    let { ccount } = this.state;
    if (isValueExists > -1) {
      cookingFilter.splice(isValueExists, 1);
      if(cookingFilter.length === 0) {
        ccount = 0
      }
    } else {
      cookingFilter.push(value);
      ccount = 1
    }
    this.setState({ cookingFilter, loadData: true, ccount });
    // console.log('cookingFilter', cookingFilter);
    this.recipeDetail(0, handsOnValueFilter, totalTimeValue, proteinFilter, cookingFilter);
  }

  recipeDetail(offset, handsOnValueFilter, totalTimeValue, proteinFilterValue, cookingFilterValue) {
    let {
      hasMore, isRecord, recipeList, counter,
    } = this.state;
    const { name } = this.state;
    handsOnValueFilter = handsOnValueFilter || '';
    totalTimeValue = totalTimeValue || '';

    if (proteinFilterValue) {
      proteinFilterValue = proteinFilterValue.toString();
    } else if (!proteinFilterValue) {
      proteinFilterValue = '';
    }
    if (offset === 0) {
      isRecord = true;
      recipeList = [];
      counter = 0;
    } else {
      counter += 18;
    }
    if (cookingFilterValue) {
      cookingFilterValue = cookingFilterValue.toString();
    } else {
      cookingFilterValue = '';
    }
    if (isRecord === true) {
      const url = `${RoutingConstants.getSwapRecipeList
      }?offset=${offset}&limit=18&exclusion_id=${proteinFilterValue}&equipment_id=${cookingFilterValue}&hands_on_time=${handsOnValueFilter}&total_time=${totalTimeValue}`;
      get(url).then((response) => {
        if (response.is_records_exists === 1) {
          hasMore = true;
          isRecord = true;
        } else if (response.status === 400) {
          isRecord = false;
          hasMore = false;
        } else {
          hasMore = false;
          isRecord = false;
        }
        if (response.result.length > 0) {
          recipeList.push(response.result);
          // console.log('recipeList', recipeList[0], response.result[0]);
        }
        this.setState({
          recipeList,
          offset: response.next_offset,
          counter,
          name,
          hasMore,
          isRecord,
          loadData: false,
        });
      }).catch((err) => {
        this.setState({
          recipeList,
          isRecord: false,
          hasMore: false,
        });
        // console.log('api', err);
        if (err.error === 'token_expired' || err.error === 'token_not_provided') {
          localStorage.removeItem('token');
          history.push('/login');
        }
      });
    }
  }

  clickAccordion() {
    const { clickAccordion } = this.state
    let { filterShow } = this.state;
    if(!clickAccordion) {
      filterShow = "mb-show"
    } else {
      filterShow = "mb-hide"
    }
    this.setState({
      clickAccordion: !clickAccordion,
      filterShow
    })
  }
  render() {
    const {
      PROTEIN_LIST, recipeName, servingSize, equipmentList, handsOnValueFilter, pcount,
      proteinFilter, equipmentIcon, totalTimeCheck, totalTimeValue, cookingFilter,
      recipeImg, totalTime, handsOnCheck1, handsOnCheck2, clickAccordion, filterShow,
      tcount, hcount, ccount
    } = this.state;
    const active = parseInt(tcount + hcount + pcount + ccount ,0)
    const { totalHandsTime } = this.state;
    let data = false;
    let list = false;
    if (PROTEIN_LIST.result) {
      data = true;
    }
    if (equipmentList.result) {
      list = true;
    }
    return (
      <section className="dashboard-main swap-page">
        <div className="container">
          <div className="col-lg-3 sidebar">
            <div className="sidebar-cntnt">
              <div className="sidebar-cntnt-bg" />
              <h4 className="text-center mb-hide">
                Current Recipe
              </h4>
              <div className="sidemenu-bloc">
                <MobileRecipeFilter 
                  recipeName={recipeName}
                  pressBackBtn={()=>this.pressBackBtn()}
                />
                <div className="meal-post mb-hide">
                  <a>
                    <LazyLoad
                      loaderImage
                      originalSrc={recipeImg}
                      imageProps={{
                        src: '/images/small.png',
                      }}
                      className="recipe_img1"
                    />

                    <h5>{recipeName}</h5>
                    <div className="meal-post-data">
                      <div className="meal-post-data-box meal-post-data-img">
                        <img src={equipmentIcon} alt="" className="equipment-icn-recipe" />
                      </div>
                      <div className="meal-post-data-box"><b>{servingSize}</b>Servings</div>
                      <div className="meal-post-data-box"><b>{totalHandsTime}</b>Hands On</div>
                      <div className="meal-post-data-box"><b>{totalTime}</b>Total</div>
                    </div>
                  </a>
                </div>
              </div>

              <div className="swap-sidemenu">
                <h3 className="recipe-filtr-side">
                  <svg  className="mb-hide" width="22px" height="22px" viewBox="0 0 22 22" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                    <title>0F15A50E-7CAD-4FCA-A7EC-E459A4D8985F</title>
                    <desc>Created with sketchtool.</desc>
                    <defs>
                      <path d="M21.9294491,0.244444444 C21.8316464,0.0977777778 21.6604917,0 21.4893371,0 L0.510662939,0 C0.315057586,0 0.143902902,0.0977777778 0.0705508947,0.268888889 C-0.0272517818,0.415555556 -0.0272517818,0.635555556 0.0950015639,0.831111111 L8.09037037,11.5133333 L8.09037037,21.4866667 C8.09037037,21.7066667 8.21262372,21.8777778 8.3837784,21.9511111 C8.48158108,22 8.57938376,22 8.60383442,22 C8.75053844,22 8.87279178,21.9511111 8.97059446,21.8533333 L12.7604482,18.0888889 C12.8582509,17.9911111 12.9071522,17.8688889 12.9071522,17.7222222 L12.9071522,11.66 L21.9049984,0.806666667 C22.0272518,0.635555556 22.0272518,0.44 21.9294491,0.244444444 Z M11.9780268,11.1955556 C11.9535761,11.2688889 11.9046748,11.3422222 11.9046748,11.4644444 L11.9046748,17.4777778 L9.14174915,20.2155556 L9.14174915,11.3422222 C9.14174915,11.22 9.09284781,11.1466667 9.04394647,11.0244444 L1.56204171,1.00222222 L20.3890569,1.00222222 L11.9780268,11.1955556 Z" id="path-1" />
                    </defs>
                    <g id="Swap-/-Add-Recipe" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                      <g id="Swap-Flow---Main-Screen" transform="translate(-33.000000, -435.000000)">
                        <g id="Sidebar-&amp;-Main-Menu">
                          <g id="Title:-Recipe-Filters" transform="translate(33.000000, 426.000000)">
                            <g id="Icon-/-Action-/-Filter" transform="translate(0.002800, 9.169231)">
                              <mask id="mask-2" fill="white">
                                <use xlinkHref="#path-1" />
                              </mask>
                              <use id="Mask" fill="#000000" fillRule="nonzero" xlinkHref="#path-1" />
                              <g id="Swatch-/-Dark-Gray" mask="url(#mask-2)" fill="#4A4A4A" fillRule="evenodd">
                                <rect id="Rectangle-5" x="0" y="0" width="22" height="22" />
                              </g>
                            </g>
                          </g>
                        </g>
                      </g>
                    </g>
                  </svg>
                    Recipe Filters
                  { active > 0 && 
                    <span className="mb-only active-filter">
                      {active} Active
                    </span>
                  }
                  {!clickAccordion ?
                    <svg
                      className="svIcon mb-only"
                      enableBackground="new 0 0 48 48"
                      height="10px"
                      id="Layer_3"
                      version="1.1"
                      viewBox="0 0 48 48"
                      width="10px"
                      xmlSpace="preserve"
                      xmlns="http://www.w3.org/2000/svg"
                      xmlnsXlink="http://www.w3.org/1999/xlink"
                      onClick={()=> this.clickAccordion()}
                    >
                      <polygon fill="#241F20" points="0,12.438 48,12.438 24,35.562 " />
                    </svg>
                    :
                    <svg
                      className="svIcon mb-only"
                      enableBackground="new 0 0 32 32"
                      height="13px"
                      id="svg2"
                      version="1.1"
                      viewBox="0 0 32 32"
                      width="13px"
                      xmlSpace="preserve"
                      xmlns="http://www.w3.org/2000/svg"
                      onClick={()=> this.clickAccordion()}
                    >
                      <g id="backbord"><rect fill="none" height="32" width="32" /></g>
                      <g id="arrow_x5F_up">
                        <polygon points="30,22 16.001,8 2.001,22  " />
                      </g>
                    </svg>
                  }
                </h3>
                <div className={filterShow}>
                  <div className="filterlist">
                    <ul>
                      <li><b>Protein Type</b></li>
                      { data &&
                    PROTEIN_LIST.result.map(item => (
                      <li key={item.id}>
                        <label className="container1" htmlFor={item.name}>
                          <input
                            type="checkbox"
                            name="proteinList"
                            value={item.id}
                            id={item.name}
                            onClick={event => this.proteinFilter(event.target.value)}
                          />
                          <span className="checkmark1" name="proteinList" />
                        </label>
                        {item.exclusion_types_data.charAt(0).toUpperCase() + item.exclusion_types_data.substring(1) }
                      </li>
                    ))
                      }

                    </ul>
                    <ul className="mb-only">
                      <li><b>Cooking Method</b></li>
                      { list &&
                    equipmentList.result.map(item => (
                      // return (
                      <li key={item.id}>
                        <label className="container1" htmlFor={item.id}>
                          <input
                            type="checkbox"
                            value={item.id}
                            id={item.id}
                            onClick={event => this.cookingFilter(event.target.value)}
                          />
                          <span className="checkmark1" />
                        </label>
                        {item.title.charAt(0).toUpperCase() + item.title.substring(1) }
                      </li>
                    ))
                      }
                    </ul>
                  </div>
                  <ul>
                    <li><b>Hands On Time</b></li>
                    <li>
                      <label className="container1" htmlFor="checkbox">
                        <input type="checkbox" id="checkbox" checked={handsOnCheck1} onClick={() => this.handsOnFilter('1')} />
                        <span className="checkmark1" />
                      </label>
                    Less than 10 mins
                    </li>
                    <li>
                      <label className="container1" htmlFor="checkbox1">
                        <input type="checkbox" id="checkbox1" checked={handsOnCheck2} onClick={() => this.handsOnFilter('0')} />
                        <span className="checkmark1" />
                      </label>
                    Less than 20 mins
                    </li>
                  </ul>
                  <ul>
                    <li>
                      <b>Total Time</b>
                    </li>
                    <li>
                      <label className="container1" htmlFor="totaltime">
                        <input type="checkbox" id="totaltime" checked={totalTimeCheck} onClick={() => this.totalTimeFilter()} />
                        <span className="checkmark1" />
                      </label>
                    Quick! Less than 30 mins
                    </li>
                  </ul>
                  <ul className="mb-hide">
                    <li><b>Cooking Method</b></li>
                    { list &&
                    equipmentList.result.map(item => (
                      // return (
                      <li key={item.id}>
                        <label className="container1" htmlFor={item.id}>
                          <input
                            type="checkbox"
                            value={item.id}
                            id={item.id}
                            onClick={event => this.cookingFilter(event.target.value)}
                          />
                          <span className="checkmark1" />
                        </label>
                        {item.title.charAt(0).toUpperCase() + item.title.substring(1) }
                      </li>
                    ))
                    }
                  </ul>
                </div>
              </div>
            </div>
          </div>
          <RecipeList
            recipeDetail={() => this.recipeDetail(
              this.state.offset, handsOnValueFilter,
              totalTimeValue, proteinFilter, cookingFilter,
            )}
            {... this.state}
          />

        </div>

        <div className={this.state.name} />
      </section>
    );
  }
}

export default RecipeFilter;
