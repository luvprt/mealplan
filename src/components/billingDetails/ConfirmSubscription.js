import React, { Component } from 'react';

class ConfirmSubscription extends Component {
  constructor(props) {
    super(props);
    this.state = {
      planName: this.props.value,
    };
    localStorage.removeItem('reminder')
  }
  // eslint-disable-next-line
  onClose() {
    window.location.reload();
    this.props.onClose();
  }

  render() {
    const { planName } = this.state;
    return (
      <div id="upgrade-popup" className="upgrade-popup subscription-confirmed-popup modal fade in" style={{ display: 'block' }}>
        <div className="modal-dialog">


          <div className="modal-content">

            <div className="popup-hdng">
              <button type="button" className="close" data-dismiss="modal" onClick={() => this.onClose()}>
                <svg
                  width="20px"
                  height="20px"
                  viewBox="0 0 20 20"
                  version="1.1"
                  xmlns="http://www.w3.org/2000/svg"
                  xmlnsXlink="http://www.w3.org/1999/xlink"
                >
                  <desc>Created with Sketch.</desc>
                  <defs />
                  <g id="Symbols" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                    <g id="Icon-/-Delete-(White)" fill="#FFFFFF" fillRule="nonzero">
                      <g id="noun_1054717">
                        <path d="M10,0.104166667 C4.54166667,0.104166667 0.104166667,4.54166667 0.104166667,10 C0.104166667,15.4583333 4.54166667,19.8958333 10,19.8958333 C15.4583333,19.8958333 19.8958333,15.4583333 19.8958333,10 C19.8958333,4.54166667 15.4583333,0.104166667 10,0.104166667 Z M10,18.1458333 C5.5,18.1458333 1.85416667,14.5 1.85416667,10 C1.85416667,5.5 5.5,1.85416667 10,1.85416667 C14.5,1.85416667 18.1458333,5.5 18.1458333,10 C18.1458333,14.5 14.5,18.1458333 10,18.1458333 Z" id="Shape" />
                        <path d="M14.7708333,6.70833333 L13.2916667,5.22916667 C13.125,5.0625 12.875,5.0625 12.7083333,5.22916667 L10,7.9375 L7.29166667,5.22916667 C7.125,5.0625 6.875,5.0625 6.70833333,5.22916667 L5.22916667,6.70833333 C5.0625,6.875 5.0625,7.125 5.22916667,7.29166667 L7.9375,10 L5.22916667,12.7083333 C5.0625,12.875 5.0625,13.125 5.22916667,13.2916667 L6.70833333,14.7708333 C6.875,14.9375 7.125,14.9375 7.29166667,14.7708333 L10,12.0625 L12.7083333,14.7708333 C12.875,14.9375 13.125,14.9375 13.2916667,14.7708333 L14.7708333,13.2916667 C14.9375,13.125 14.9375,12.875 14.7708333,12.7083333 L12.0625,10 L14.7708333,7.29166667 C14.9375,7.125 14.9375,6.875 14.7708333,6.70833333 Z" id="Shape" />
                      </g>
                    </g>
                  </g>
                </svg>
              </button>
              <h2>It’s Official!</h2>
              <p>You’re subscribed to the Meal Plan Ninja {planName} plan.</p>
            </div>
            <div className="upgrade-popup-cntnt">
              <p>You can manage your plan or change your payment details at any time in the
                <a className="gren-link" href> Billing Details </a>
                page in your User Profile.
              </p>
              <div className="back-to-planing-btn-box">
                <button className="back-to-planing-btn" onClick={() => this.onClose()}>Let&apos;s Get Back to It!</button>
              </div>
            </div>

          </div>
        </div>

      </div>

    );
  }
}

export default ConfirmSubscription;
