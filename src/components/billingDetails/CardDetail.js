import React, { Component } from 'react';
import RoutingConstants from 'lib/RoutingConstants';
import {  post } from 'lib/api';
import StripeCheckout from 'react-stripe-checkout';
import Loader from 'lib/loader';

class CardDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      trial: this.props.reminder,
      lastDigits: this.props.lastDigits || 'N/A',
      inputs: {}, 
    };
  }

  
  // eslint-disable-next-line
  UNSAFE_componentWillReceiveProps(props) {
    this.setState({
      trial: props.reminder,
      lastDigits: props.lastDigits || 'N/A',
    })
  }
  
  
  updateDetail(value) {
    this.setState({ loadData: true });
    const url = RoutingConstants.updateCard;
    const { inputs } = this.state;
    inputs.stripe_token = value.id;
    post(url, inputs).then(() => {
      this.setState({ loadData: false });
      window.location.reload();
      // track edit payment detail
      window.analytics.track('Edit Payment Information Successful', {
        status: 'success',
      });
    }).catch(() => {
      // track edit payment detail
      window.analytics.track('Edit Payment Information Unsuccessful', {
        status: 'failure',
      });
    })
  }
  
  // eslint-disable-next-line
  cancelTrial() {
    window.analytics.track('Clicks Cancel Plan');
  }

  render() {
    const { trial, lastDigits, loadData } = this.state;
    const key = RoutingConstants.key;
    const text = trial !== true ? '' : 'Free Trial';
    return (
      <div className="free-trial-biling-dtail-box free-trial-biling-paymnt-info">
        {
          loadData &&
        <div className="loading">
          <Loader />
          <div className="modal-backdrop fade in" />
        </div>
        }
        <div className="row">
          <div className="col-md-12">
            <h5>Payment Information:</h5>
            <p>Your Meal Plan Ninja subscription renews each billing period using the following payment information.</p>
            <div className="free-trial-biling-inner-dtail">
              <p>Card Ending in: <span>{lastDigits}</span></p>
              { trial !== true &&
                <StripeCheckout
                  panelLabel='Update Card Details'
                  ComponentClass="div"
                  allowRememberMe={false}
                  image="https://stripe.com/img/documentation/checkout/marketplace.png"
                  token={(value) => this.updateDetail(value)}
                  stripeKey={key}
                >
                  <button className="back-to-planing-btn edit-paymnt-dtail-btn">
                  Edit Payment Details
                  </button>
                </StripeCheckout>  
              }
              <button className="cancl-free-trail" onClick={() => this.cancelTrial()}>
                To Cancel {text} Subscription, email us at
                <span className="pwd-forgot-msg"> support@mealplanninja.com </span>
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default CardDetail;
