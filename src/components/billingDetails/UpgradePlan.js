import React, { Component } from 'react';
import StripeCheckout from 'react-stripe-checkout';
import ReactHtmlParser from 'react-html-parser';
import RoutingConstants from 'lib/RoutingConstants';
import {  post } from 'lib/api';
import Loader from 'lib/loader';
import  moment  from 'moment';

let planId = 0;

class UpgradePlan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      reminder: props.reminder,
      display: props.display || 'block',
      inputs: {},
      zIndex: props.zIndex,
      page: props.page,
      yOff: '45% Off',
      qOff: '10% Off',
      mPlanId: '',
      qPlanId: '',
      yPlanId: '',
    };
  }
  componentDidMount() {
    this.subscriptionDetail();
    if (this.props.zIndex === 1) {
      localStorage.setItem('reminder','true') 
    } 
  }

  onPay(value) {
    // console.log(value, 'token');
    planId = value.id;
    this.setState({ loadData: true })
    const url = RoutingConstants.subscribePlan;
    const { inputs, planName } = this.state;
    inputs.stripe_token = value.id;
    post(url, inputs).then(() => {
      this.setState({ loadData: false })
      this.onClosedForm(planName)
      // new subscription
      window.analytics.track('New subscription', {
        planName,
        date: moment().format('dddd DD-MM-YYYY'),
      });
    })
  }

 

  onClose() {
    if(planId === 0) {
      this.props.onClose();
      this.setState({ display: 'block' }); 
    }
  }

  onClosedForm(value) {
    // eslint-disable-next-line
   this.props.onClosedForm(value);
    return (
      <div className="modal-backdrop fade in" />
    )
  }

  // eslint-disable-next-line
  onOpenForm(id, planName){
    const { inputs } = this.state;
    inputs.plan_id = id;
    this.setState({ display: 'none', inputs, planName });
    // track click on subscrption plan
    window.analytics.track('Clicks On Plan', {
      planName,
    });
  }

  subscriptionDetail() {
    const { subscriptionPlan } = this.props || [];
    let {
      mplanName, mprice, qplanName, qprice, yplanName, yprice, currency,
      mPlanId, qPlanId, yPlanId, yOff, qOff,
    } = this.state;
    const { page } = this.state;
    if (subscriptionPlan && subscriptionPlan.length > 0) {
      subscriptionPlan.map((plan) => {
        if (plan.interval_count === 1 && plan.interval === 'month' && plan.active && plan.trial_period_days === null) {
          mplanName = plan.nickname.split(' ');
          mplanName = mplanName[0];
          mprice = (plan.amount) / 100;
          mPlanId = plan.id;
        }
        if(plan.interval_count > 1 && plan.interval === 'month' && plan.active) {
          qplanName = plan.nickname.split(' ');
          qplanName = qplanName[0];
          qprice = (plan.amount) / 100;
          qPlanId = plan.id;
          if(plan.metadata.Percentage){
            qOff = plan.metadata.Percentage;
          }
        }
        if(plan.interval === 'year' && plan.active) {
          yplanName = plan.nickname.split(' ');
          yplanName = yplanName[0];
          yprice = (plan.amount) / 100;
          yPlanId = plan.id;
          if(plan.metadata.Percentage){
            yOff = plan.metadata.Percentage;
          }
        }
        currency = plan.currency.toUpperCase();
        return plan;
      })
      // track billing popup viewed
      window.analytics.track('Billing Popup Viewed', {
        page,
      });
    }
    this.setState({
      mplanName, mprice, qplanName, qprice, yplanName, 
      yprice, currency, mPlanId, qPlanId, yPlanId, yOff, qOff
    });
  }
  
  render() {
    const { reminder, display, yOff, qOff } = this.state;
    const key = RoutingConstants.key;
    let planClass = 'col-md-4';
    const {
      mplanName, mprice, qplanName, qprice, yplanName, yprice, currency, mPlanId, 
      qPlanId, yPlanId, loadData, zIndex
    } = this.state;
    if(mPlanId === '' || qPlanId === '' || yPlanId === '') {
      planClass = 'col-md-6'
    }
    return (
      <div>
        {
          loadData &&
        <div className="loading">
          <Loader />
          <div className="modal-backdrop fade in" />
        </div>
        }
        <div id="upgrade-popup" className="upgrade-popup modal fade in" style={{ display, zIndex, marginTop:'-54px' }}>
          <div className="modal-dialog">
            <div className="modal-content">
              <div className="popup-hdng">
                {reminder ?
                  <div>
                    <h2>Hey Ninja! Upgrade Your Status!</h2>
                    <p>Your free trial period has expired. In order to keep using your Meal Plan Ninja acocunt you’ll need to upgrade to a paid plan from one of the options below</p>
                  </div>
                  :
                  <div>
                    <button type="button" className="close" onClick={() => this.onClose()}>
                      <svg
                        width="20px"
                        height="20px"
                        viewBox="0 0 20 20"
                        version="1.1"
                        xmlns="http://www.w3.org/2000/svg"
                        xmlnsXlink="http://www.w3.org/1999/xlink"
                      >
                        <desc>Created with Sketch.</desc>
                        <defs />
                        <g id="Symbols" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                          <g id="Icon-/-Delete-(White)" fill="#FFFFFF" fillRule="nonzero">
                            <g id="noun_1054717">
                              <path d="M10,0.104166667 C4.54166667,0.104166667 0.104166667,4.54166667 0.104166667,10 C0.104166667,15.4583333 4.54166667,19.8958333 10,19.8958333 C15.4583333,19.8958333 19.8958333,15.4583333 19.8958333,10 C19.8958333,4.54166667 15.4583333,0.104166667 10,0.104166667 Z M10,18.1458333 C5.5,18.1458333 1.85416667,14.5 1.85416667,10 C1.85416667,5.5 5.5,1.85416667 10,1.85416667 C14.5,1.85416667 18.1458333,5.5 18.1458333,10 C18.1458333,14.5 14.5,18.1458333 10,18.1458333 Z" id="Shape" />
                              <path d="M14.7708333,6.70833333 L13.2916667,5.22916667 C13.125,5.0625 12.875,5.0625 12.7083333,5.22916667 L10,7.9375 L7.29166667,5.22916667 C7.125,5.0625 6.875,5.0625 6.70833333,5.22916667 L5.22916667,6.70833333 C5.0625,6.875 5.0625,7.125 5.22916667,7.29166667 L7.9375,10 L5.22916667,12.7083333 C5.0625,12.875 5.0625,13.125 5.22916667,13.2916667 L6.70833333,14.7708333 C6.875,14.9375 7.125,14.9375 7.29166667,14.7708333 L10,12.0625 L12.7083333,14.7708333 C12.875,14.9375 13.125,14.9375 13.2916667,14.7708333 L14.7708333,13.2916667 C14.9375,13.125 14.9375,12.875 14.7708333,12.7083333 L12.0625,10 L14.7708333,7.29166667 C14.9375,7.125 14.9375,6.875 14.7708333,6.70833333 Z" id="Shape" />
                            </g>
                          </g>
                        </g>
                      </svg>
                    </button>
                    <h2>Let’s Upgrade You to Full Ninja Status!</h2>
                  </div>
                }
              </div>
              <div className="upgrade-popup-cntnt">
                <h5>Select a Plan to Continue Your Membership</h5>
                <p>All Meal Plan Ninja subscriptions come with the same amazing benefits. The only difference is you save more by subscribing longer!</p>

                <div className="popup-plans">
                  { mPlanId !== '' && 
                    <div className={planClass}>
                      <div className="plan-green-box">
                        <StripeCheckout
                          panelLabel={`Pay $${mprice}`}
                          label={ReactHtmlParser(`${mplanName}<span>$${mprice}</span>`)}
                          image="https://stripe.com/img/documentation/checkout/marketplace.png"
                          token={(value) => this.onPay(value)}
                          opened={() => this.onOpenForm(`${mPlanId}`, `${mplanName}`)}
                          closed={() => this.onClose()}
                          stripeKey={key}
                          zipCode
                          billingAddress
                          currency={currency}
                        />  
                      </div>
                      <p>Renews every 30 days</p>
                    </div>
                  }
                  {qPlanId !== '' &&
                    <div className={planClass}>
                      <div className="plan-green-box">
                        <StripeCheckout
                          panelLabel={`Pay $${qprice}`}
                          label={ReactHtmlParser(`${qplanName}<span>$${qprice}</span>`)}
                          image="https://stripe.com/img/documentation/checkout/marketplace.png"
                          stripeKey={key}
                          zipCode
                          billingAddress
                          token={(value) => this.onPay(value)}
                          opened={() => this.onOpenForm(`${qPlanId}`, `${qplanName}`)}
                          closed={() => this.onClose()}
                          currency={currency}
                          triggerEvent="onClick"
                        />
                      </div>
                      <p><strong>{qOff}</strong></p>
                      <p>Renews every 90 days</p>
                    </div>
                  }
                  {yPlanId !== '' &&
                    <div className={planClass}>
                      <div className="plan-green-box">
                        <StripeCheckout
                          panelLabel={`Pay $${yprice}`}
                          label={ReactHtmlParser(`${yplanName}<span>$${yprice}</span>`)}
                          image="https://stripe.com/img/documentation/checkout/marketplace.png"
                          stripeKey={key}
                          zipCode
                          billingAddress
                          token={(value) => this.onPay(value)}
                          opened={() => this.onOpenForm(`${yPlanId}`, `${yplanName}`)}
                          closed={() => this.onClose()}
                          currency={currency}
                        />
                      </div>
                      <p><strong>Best Deal, {yOff}</strong></p>
                      <p>Renews once per year</p>
                    </div>
                  }
                </div>
                {reminder &&
                <div className="popup-ftr">
                  <p> Need a little more time to decide? Email our team to </p>
                  <p> ask for an extension on your free trial at support@mealplanninja.com </p>
                </div>
                }
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default UpgradePlan;
