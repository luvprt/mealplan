import React, { Component } from 'react';
import moment from 'moment';
import RoutingConstants from 'lib/RoutingConstants';
import { post } from 'lib/api';
// eslint-disable-next-line
import UpgradePlan from './UpgradePlan';
import ConfirmSubscription from './ConfirmSubscription';

class SubscriptionDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      trial: this.props.reminder,
      confirmPop: false,
      subscriptionPlan: this.props.subscriptionPlan || [],
      inputs: {},
      ...this.props,
    };
  }

  // eslint-disable-next-line
  UNSAFE_componentWillReceiveProps(props) {
    this.setState({ subscriptionPlan: props.subscriptionPlan, trial: props.reminder, ...props });
    if(props.planId === undefined) {
      this.getDetail(props.subscriptionPlan, props.reminder); 
    } else{
      this.getPlan (props.planId, props.reminder)
    }
  }

  onClose() {
    this.setState({ show: false, confirmPop: false });
  }

  getDetail(subscriptionPlan, trial) {
    let { renewPlanDate, interval, mprice } = this.state;
    const { renewDate } = this.state;
    let planIndex;
    let detailText = '';
    if (subscriptionPlan && subscriptionPlan.length > 0) {

      subscriptionPlan.map((plan, pIndex) => {
        if(plan.interval_count === 1 && plan.interval === 'month' && plan.active && plan.trial_period_days === null ) {
          planIndex = pIndex;
        }
        return plan;
      })
      mprice = (subscriptionPlan[planIndex].amount) / 100;
    }
    if (!this.state.trial && subscriptionPlan.length > 0) {
      const count = subscriptionPlan[planIndex].interval_count;
      const validity = subscriptionPlan[planIndex].interval;
      detailText = subscriptionPlan[planIndex].nickname.split(' ');
      renewPlanDate = moment(renewDate).add(count, validity).format('MMMM DD, YYYY');
      detailText = `${detailText[0]} Plan`;
      interval = `${count} ${validity}`;
      if(subscriptionPlan[planIndex].trial_period_days !== null) {
        trial = true
        detailText = ' Free Trial';
      }
    } else if (this.state.trial) { detailText = 'Free Trial'; }
    this.setState({
      detailText, renewPlanDate, interval, mprice, trial
    });
  }
  
  getPlan(planId) {
    let { renewPlanDate, interval, mprice, trial } = this.state;
    const { renewDate } = this.state;
    const { inputs } = this.state;
    let detailText = ''
    inputs.planId = planId
    const url = RoutingConstants.getPlanById;
    post(url, inputs ).then((response) => {
      const count = response.result.interval_count;
      const validity = response.result.interval;
      detailText = response.result.nickname.split(' ');
      renewPlanDate = moment(renewDate).add(count, validity).format('MMMM DD, YYYY');
      detailText = `${detailText[0]} Plan`;
      interval = `${count} ${validity}`;
      mprice = (response.result.amount) / 100;
      if(response.result.trial_period_days !== null) {
        trial = true
        detailText = ' Free Trial';
      }
      this.setState({detailText, renewPlanDate, interval, mprice, trial})
    }).catch(()=>{
    })
  }

  upgradePlan() {
    this.setState({ show: true });
    // track subscribe click
    window.analytics.track('Trial User Subscribe Clicks');
  }

  confirmScreen(value) {
    this.setState({
      show: false,
      confirmPop: true,
      value,
    });
  }

  render() {
    const {
      trial, show, confirmPop, value, subscriptionPlan, mprice, detailText,
      renewPlanDate, interval, trialEndDate,
    } = this.state;
    let { activeMember } = this.state;
    activeMember = moment(activeMember).format('MMMM DD, YYYY');
    const trialEnd = moment(trialEndDate).format('MMMM DD');

    return (
      <div className="free-trial-biling-dtail-box">
        <div className="row">
          <div className="col-md-6">
            <h5>Your Subscription Details:</h5>
            <p>Below is more information on your Meal Plan Ninja subscription.</p>
            <div className="free-trial-biling-inner-dtail">
              <p>You’ve Been a Ninja Since: <span>{activeMember}</span></p>
              <p>Subscription Details: <span>{detailText}</span></p>
              { !trial ?
                <div>
                  <p className="subscribe-today-msg">Renew Every {interval} at ${mprice}</p>
                  <p className="subscribe-today-msg subscribe-today-msg-two">
                    Next Renewal Date: {renewPlanDate}
                  </p>
                </div>
                :
                <p className="subscribe-today-msg">Subscribe today for just ${mprice} per month!</p>
              }
            </div>
          </div>
          {trial &&
          <div className="col-md-6">
            <h5 className="text-center">Free Trial Ends: <span className="green-date">{trialEnd}</span></h5>
            <p className="fre-traial-end-dtail">Subscribe today to make sure you don’t lose access to your account!</p>
            <div className="back-to-planing-btn-box">
              <button className="back-to-planing-btn" onClick={() => this.upgradePlan()}>
                Subscribe Today
              </button>
            </div>
            { show &&
              <div>
                <UpgradePlan
                  reminder={false}
                  onClose={() => this.onClose()}
                  onClosedForm={data => this.confirmScreen(data)}
                  subscriptionPlan={subscriptionPlan}
                  zIndex={1000000}
                  page="Users Billing Details Page"
                />
                <div className="modal-backdrop fade in" />
              </div>
            }
            {confirmPop &&
              <div>
                <ConfirmSubscription
                  value={value}
                  onClose={() => this.onClose()}
                />
                <div className="modal-backdrop fade in" />
              </div>
            }
          </div>
          }
        </div>
      </div>
    );
  }
}

export default SubscriptionDetail;
