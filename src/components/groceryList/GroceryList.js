/*
Component Name: GroceryList component
Default screen: Dashboard
*/


import React, { Component } from 'react';
import { fraction } from 'mathjs';
import Loader from 'lib/loader';
import moment from 'moment';
import ReactResizeDetector from 'react-resize-detector';
import { post } from 'lib/api';
import RoutingConstants from 'lib/RoutingConstants';


class GroceryList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...this.props,
      groceryList: [],
      displayPrint: true,
      defaultValue: 'show',
      showCustomItem: false,
      customItems: [],
      startDate: '',
      endDate: '',
      customItemText: '',
      focusIngredient: null,
      loader: false
    };
  }

  // unsafe
  // eslint-disable-next-line
  UNSAFE_componentWillReceiveProps(props) {
    const dayDates = props.dayDates;
    const startDate = `${dayDates[0].Year}-${dayDates[0].Month}-${dayDates[0].date}`;
    const endDate = `${dayDates[6].Year}-${dayDates[6].Month}-${dayDates[6].date}`;
    const  groceryList  = [];
    let loader ;
    let { previous } = this.state;
    if(props.groceryList) {
      for (const x in props.groceryList) {
        if (props.groceryList) {
          groceryList.push(props.groceryList[x]);
        }
      }
    }
    const weekdate = parseInt(props.dayDates[0].date, 0)
    const currentDate = parseInt(moment().startOf('week').format('DD'),0)
    const currentMonth = parseInt(moment().startOf('week').format('MM'),0)
    const weekMonth = parseInt(props.dayDates[0].Month,0)
    const weekYear = parseInt(props.dayDates[0].Year,0)
    const currentYear = parseInt(moment().startOf('week').format('YYYY'),0)

    if(currentMonth > weekMonth && currentYear === weekYear) {
      previous = props.previous
    } else if(currentMonth === weekMonth) {
      if(weekdate < currentDate ) {
        previous = true
      } else {
        previous = false
      }
    } else {
      previous = false
    }
    this.setState({
      ...props,
      loader,
      groceryList,
      previous,
      startDate,
      endDate,
      customItems: props.customItems
    });
  }

  componentDidUpdate(){
    if (this.addItem) {
      this.addItem.focus();
    }

  }

  onResize() {
    let { displayPrint } = this.state;
    if (window.innerWidth < 769 ) {
      displayPrint = false;

    } else {
      displayPrint = true;
    }
    this.setState({ displayPrint });
  }

  buildNextWeekPlan(daydates) {
    this.props.buildNextWeekPlan(daydates);
  }

  async clickIngredient(itemKey, event) {
    const {groceryList} = this.state;
    const list = groceryList[itemKey];
    const ingredient = event.target.value;
    let status = event.target.checked;
    if (event.target.checked === true) {
      status = 0;
    }else {
      status = 1;
    }
    const url = RoutingConstants.changeGroceryItemStatus;
    const filterList = await list.filter((item)=>item.ingredient_name === ingredient);
    await Promise.all(filterList.map((filterItem)=>{
      post(url, {user_grocery_list_id: filterItem.id, is_visible: status})
      const findIndex = list.findIndex((item)=>item.id === filterItem.id);
      filterItem.is_visible = status;
      list[findIndex] = filterItem;
      return filterItem;
    }));
    groceryList[itemKey] = list;
    this.setState({
      groceryList
    });
  }
  async clickCustomIngredient(itemKey, event) {
    const {customItems} = this.state;
    const list = customItems[itemKey];
    // const ingredient = event.target.value;
    let status = event.target.checked;
    if (event.target.checked === true) {
      status = 0;
    }else {
      status = 1;
    }
    list.is_visible = status;
    customItems[itemKey] = list;
    const url = RoutingConstants.changeCustomGroceryItemStatus;
    await post(url, {user_grocery_list_id: list.id, is_visible: status});
    this.setState({
      customItems
    });
  }

  saveCustomItem(){
    const { dayDates } = this.state
    this.setState({loader: true},()=>{
      const {customItems, startDate, endDate, customItemText} = this.state;
      const itemText = customItemText.trim();
      if (itemText && itemText.length > 0) {
        const url = RoutingConstants.addCustomGroceryItem;
        post(url, {ingredient_name: itemText, is_visible: 1, week_start_date: startDate,week_end_date: endDate}).then((responseJson)=>{
          if (responseJson.success) {
            customItems.push(responseJson.result);
            this.setState({customItems, showCustomItem: false, customItemText: '', loader:false});
          }
          window.analytics.track('Grocery List Custom Item Saved', {
            startDate: `${dayDates[0].fullDay} ${dayDates[0].date}-${dayDates[0].Month}-${dayDates[0].Year}`,
            endDate: `${dayDates[6].fullDay} ${dayDates[6].date}-${dayDates[6].Month}-${dayDates[6].Year}`,
            itemName: itemText
          });
        }).catch((err)=>{
          // eslint-disable-next-line
          console.log(err);
          this.setState({loader: false});
        })
      }
    });



  }


  scrollToDomRef() {
    const domNode = this.addItem;
    domNode.scrollIntoView();
  }


  handleClickAddItem(){
    this.setState({showCustomItem: true, focusIngredient: false})

  }

  handleKeyPress(e){
    if (e.keyCode === 13) {
      this.saveCustomItem();
    }
  }
  groceryData() {
    const { defaultValue } = this.state;
    let { groceryList } = this.state;
    let listCheck = false;
    let float;
    if (groceryList && groceryList.length > 0) {
      // console.log(groceryList)
      listCheck = true;
    } else{
      listCheck = false;
      groceryList = [];
    }


    return (
      <div>
        <div className="row grocery-list-cntnt">
          { listCheck &&
          groceryList.map((items, key) => {
            const img = groceryList[key][0].grocery_category.icon_image;
            let imgCheck = false;
            let lastChild;
            let liStyle;
            let groceryCategory = ''
            let groceryCount = 0
            const ingredientCount = 0
            if (img === null) {
              imgCheck = true;
            }
            if (key % 2 !== 0 ) {
              float = 'right';
            } else {
              float = 'left';
            }
            for(let i=0; i<groceryList[key].length;i+=1){
              if(groceryList[key][i].is_visible === 1) {
                groceryCount +=1
              }
            }
            groceryCategory = groceryCount > 0  || defaultValue === 'show' ?
              '':'show-grocery-category'
            return (
              // eslint-disable-next-line
              <div className={`col-md-6 ${groceryCategory}`} style={{ float }} key={`${items.name}${key}`}>

                <div className="grocery-inner-list">

                  <ul>
                    <li className={`bordr-top-hdng ${groceryCategory}`}>
                      <div className="grocery-list-icn no-print">
                        {!imgCheck ?
                          <img src={`/images/${img}`} alt="" className={groceryCategory} />
                          :
                          <svg xmlns="http://www.w3.org/2000/svg" width="40" height="40" viewBox="0 0 40 40">
                            <path fill="#4A4A4A" fillRule="nonzero" d="M38.971 18.417c.424.079.931-.238 1.016-.634.085-.396-.254-.87-.677-.95-1.608-.237-2.962-.079-4.062.238.338-1.03.508-2.296.254-3.8-.085-.396-.508-.713-1.016-.634-.423.08-.762.476-.677.95.423 2.138-.338 3.721-1.016 4.592a3.625 3.625 0 0 0-.592.475l-.423-.396c.508-.633 1.354-2.137.846-4.512-.084-.396-.508-.713-1.015-.633-.423.079-.762.475-.677.95.254 1.583-.085 2.612-.423 3.087l-.085-.08a.858.858 0 0 0-1.185 0l-1.946 1.9a13.066 13.066 0 0 0-6.094-1.503c-1.692 0-3.385.396-4.739 1.108-.338-1.188-.677-2.375-1.015-3.404.253.079.507.079.761.079.508 0 .847-.317.847-.792 0-4.275-1.693-7.995-4.317-9.737V1.79c0-.474-.338-.791-.846-.791H7.574c-.508 0-.846.317-.846.792V4.72c-2.624 1.742-4.232 5.462-4.232 9.737 0 .475.339.792.847.792.761 0 1.438-.158 2.115-.396-.169 1.663-.253 3.563-.507 5.304-.085.475-.17 1.109-.339 1.663-.085 1.504-1.946 4.908-1.946 4.908s0 .08-.085.08c-.085.158-1.354 2.85-1.523 3.72-.254 1.98.338 3.958 1.608 5.542C5.12 39 8.759 39 9.097 39c1.524 0 3.047-.475 4.401-1.267.085 0 .17.08.254.08.17 0 4.74.791 9.394.791 4.485 0 7.532-.712 9.055-2.137 3.216-3.009 3.47-7.838 1.016-12.034l2.03-1.9c.17-.158.255-.316.255-.554 0-.237-.085-.396-.254-.554l-.17-.158c.593-.317 1.608-.713 3.301-.396.423.079.93-.238 1.016-.633.084-.396-.254-.871-.677-.95-2.54-.396-4.147.316-4.824.791l-.254-.237c.169-.159.338-.317.508-.554.846-.555 2.538-1.188 4.823-.871zm-12.706 5.18a.758.758 0 0 0 0 1.118c2.87 2.795 3.526 6.868 1.476 8.944a.758.758 0 0 0 0 1.118c.164.16.41.24.574.24.246 0 .41-.08.574-.24 2.706-2.636 2.05-7.746-1.476-11.18-.246-.319-.82-.319-1.148 0zM4.189 13.509c.254-3.483 1.693-6.491 3.808-7.679.254-.158.423-.396.423-.712V2.583h2.709v2.534c0 .316.169.554.423.712 2.115 1.188 3.639 4.196 3.808 7.68-1.777-.476-2.877-2.217-2.962-2.376l-.085-.079c-.423-.475-1.1-.158-1.1-.158a.705.705 0 0 0-.338.633c.084 2.454-.847 4.83-1.016 5.225-.677-1.504-1.015-3.325-1.015-5.225 0-.079 0-.158-.085-.237-.085-.238-.339-.475-.592-.475h-.254a1.03 1.03 0 0 0-.762.396c-.085.079-1.27 1.9-2.962 2.295zm9.225 7.838s0 .079 0 0c-2.116 4.116-1.016 12.033-.593 14.883-1.27.871-2.877 1.346-4.57 1.108-1.692-.158-3.131-1.029-4.231-2.216-1.016-1.267-1.524-2.771-1.27-4.354.085-.713.339-1.346.677-1.98 0 0 2.962-4.75 3.132-8.391.423-2.217.507-4.592.592-6.73.254 1.98.93 3.8 1.862 5.305.17.237.423.396.761.396.254 0 .508-.159.678-.317 0 0 0-.08.084-.08 1.693-2.77 1.862-4.907 1.862-5.145v-.158c.17.158.338.316.508.396.169.079.254.158.423.316 0 0 .085 0 .085.08.507 1.504 1.015 3.245 1.523 5.066-.17.158-.339.238-.508.396-.17.158-.338.396-.508.633 0 0 0 .08-.084.08-.085.237-.254.474-.423.712zm17.602 14.012c-.846.792-2.877 1.663-7.87 1.663-3.555 0-7.194-.475-8.548-.713-.169-.95-.592-4.512-.592-4.75-.085-1.108-.846-8.866 2.116-10.925.084-.079.169-.079.169-.158 1.354-.95 2.962-1.504 4.824-1.504 2.962 0 5.924 1.267 8.293 3.404 4.316 4.038 4.993 9.817 1.608 12.983zm1.27-12.191c-.508-.634-2.2-2.613-3.555-3.246l1.1-1.03 3.47 3.247-1.015 1.029z" />
                          </svg>}
                      </div>
                      <b className={groceryCategory}>{groceryList[key][0].grocery_category.name}</b>
                    </li>
                    {this.renderList(items,lastChild,liStyle, ingredientCount, key)}
                  </ul>
                </div>
              </div>
            );
          })
          }
          {listCheck &&
          <div className="col-md-6 custom-grocery-main" key="others">
            <div className="grocery-inner-list">
              <ul>
                <li className="bordr-top-hdng">
                  <div className="grocery-list-icn no-print" />
                  <b>Add Your Own Item</b>
                </li>
              </ul>
              <div>

                {this.renderCustomList()}
                {this.state.showCustomItem ?
                  <div className="custom-grocery-div">
                    <li>
                      <input
                        className="save-item-input"
                        placeholder="Enter Grocery Name Here.."
                        type="text"
                        value={this.state.customItemText}
                        onChange={(e)=>this.setState({customItemText: e.target.value})}
                        onKeyUp={(e)=> this.handleKeyPress(e)}
                        onBlur={()=> {this.addItem.value = '';}}
                        // onFocus={()=> this.scrollToDomRef()}
                        ref={(addItem)=> {this.addItem = addItem}}
                      />
                    </li>
                    <button className="save-item-btn" name="Save" value="Save" onClick={(event)=> this.saveCustomItem(event)}>Save </button>
                  </div>
                  : <li><span onClick={()=> this.handleClickAddItem()} className="add-more-item no-print">+ click to add a custom item</span></li>
                }

              </div>
            </div>
          </div>
          }
        </div>
      </div>
    );
  }

  showList(event, value) {
    const { reminder } = this.state;
    if(!reminder){
      if(value !== 'buy') {
        document.getElementById('buy').className = ""
        event.target.className = "grocery-list-btn";
      } else if(value !== 'show') {
        document.getElementById('show').className = ""
        event.target.className = "grocery-list-btn";
      }
      this.setState({ defaultValue: value })
    }
  }

  updateIngredient(itemId) {
    const ingredientName = this[`editIngredient_${itemId}`].value.trim();
    this.setState({loader: true},async ()=>{

      if (ingredientName && ingredientName.length > 0) {
        const url = RoutingConstants.changeCustomGroceryItemStatus;
        await post(url, {user_grocery_list_id: itemId, ingredient_name:ingredientName});
        this.setState({focusIngredient: null, loader:false})
      }
    });

  }

  handleEditKeyPress(e, id){
    if (e.keyCode === 13) {
      this.setState({loader: true},async()=>{
        const ingredientName = this[`editIngredient_${id}`].value.trim();
        if (ingredientName && ingredientName.length > 0) {
          const url = RoutingConstants.changeCustomGroceryItemStatus;
          await post(url, {user_grocery_list_id: id, ingredient_name:ingredientName});
          this.setState({loader: false, focusIngredient: null});
        }
      })
    }
  }

  renderCustomList(){

    const {customItems, defaultValue} = this.state;
    if (customItems && customItems.length > 0) {
      return customItems.map((item, key)=>{
        let liClass = '';
        if (item.is_visible !== 1) {
          liClass = 'ingredient-strike';
        }
        if (defaultValue === 'show' || !liClass){
          return(
            // eslint-disable-next-line
            <div className="custom-grocery-div" key={`list${key}`}>
              <li key={item.id} className={liClass}>
                <label className="container1">
                  <input
                    checked={!!liClass}
                    type="checkbox"
                    onClick={(event) => this.clickCustomIngredient(key,event)}
                  />
                  <span className="checkmark1" />
                </label>
                <input
                  disabled={!!liClass}
                  className="save-item-input"
                  placeholder="Enter Grocery Name Here.."
                  type="text"
                  ref={(editIngredient) => { this[`editIngredient_${item.id}`] = editIngredient }}
                  defaultValue={item.ingredient_name}
                  onKeyUp={(e)=> this.handleEditKeyPress(e, item.id)}
                  onFocus={() => this.setState({focusIngredient: item.id, showCustomItem: false})}
                />
              </li>
              {(this.state.focusIngredient && this.state.focusIngredient === item.id)?
                <button className="save-item-btn no-print" name="Save" value="Save" onClick={()=> this.updateIngredient(item.id)}>Update</button>
                : null}
            </div>
          )
        }
        return null;
      })
    }
    return null;
  }
  renderList(items,lastChild,liStyle, ingredientCount, itemKey){
    const{defaultValue} = this.state;
    let hideList = ''
    // let dotList = ''

    // const newArrSumBrk = {};
    // const checkIdServingSums = recipeArr.map((item)=>{
    //   if(!newArrSumBrk[item.items.recipe.recipe_id]){
    //     newArrSumBrk[item.items.recipe.recipe_id] = item.servingSize
    //   }else{
    //     newArrSumBrk[item.items.recipe.recipe_id] +=  item.servingSize
    //   }
    //   return newArrSumBrk;
    // })[0];


    return items.map((list, listKey) => {
      if (defaultValue === 'buy' && list.is_visible !== 1) {
        hideList = 'hide-list'
        
        // return null;
      }
    
      // console.log(list)
      let quantity = list.ingredient_quantity;
      let unit = '';
      let listStyle = false;
      let a = 0;
      if (list.is_visible !== 1) {
        listStyle = 'ingredient-strike';
      }

      if(quantity !== '0.00') {
        quantity = parseFloat(quantity);
        unit = list.unit_of_measurement;
        if(listKey + 1 < items.length && items[listKey].ingredient_name === items[listKey+1].ingredient_name && items[listKey+1].ingredient_quantity !=='0.00'){
          lastChild = 'multiple-units'
          liStyle = 'listStyle'
          ingredientCount += 1
        } else if(listKey > 0 && items[listKey].ingredient_name === items[listKey-1].ingredient_name && items[listKey-1].ingredient_quantity !=='0.00'){
          lastChild = ''
          liStyle = 'listStyle'
          ingredientCount = 0
        } else{
          lastChild = ''
          liStyle = ''
          ingredientCount = 0
        }

        const quantityValue = quantity.toString().split('.');
        quantityValue[0] = parseInt(quantityValue[0], 0);
        if (quantityValue[0] === 0) {
          quantityValue[0] = '';
        }
        if (quantityValue.length > 1) {
          quantityValue[1] = `${0}.${quantityValue[1]}`;
          a = fraction(parseFloat(quantityValue[1]));
          if (a.d > 0) {
            a.d = Math.round(a.d / a.n);
            a.n = 1;
          }
        } else {
          a=0
        }
        if (a !== 0 && a.d > 1) {
          return (
            // eslint-disable-next-line
            <div key={listKey}>
              {ingredientCount !== 1 ?
                <li key={list.id} className={`${lastChild} ${liStyle} ${listStyle} ${hideList}`} id={`${lastChild} ${liStyle}`} value={list.ingredient_name}>
                  <label className="container1" htmlFor={list.id}>
                    <input
                      checked={!!listStyle}
                      type="checkbox"
                      value={list.ingredient_name}
                      id={list.id}
                      onClick={(event) => this.clickIngredient(itemKey,event)}
                    />
                    <span className="checkmark1" />
                  </label>
                  <span className="bullet-style">.</span>
                  {quantityValue[0]}
                  <sup>{a.n}</sup>&frasl;<sub>{a.d}</sub>&nbsp;
                  {unit} {list.ingredient_name}
                </li>

                :<div>
                  <li className={`multiple-units ${listStyle} ${hideList}`} value={list.ingredient_name} >
                    <label className="container1" htmlFor={list.id}>
                      <input
                        checked={!!listStyle}
                        type="checkbox"
                        value={list.ingredient_name}
                        id={list.id}
                        onClick={(event) => this.clickIngredient(itemKey,event)}
                      />
                      <span className="checkmark1" />
                    </label>
                    {list.ingredient_name} (multiple units)
                  </li>
                  <li key={list.id} className={`${lastChild} ${liStyle} ${listStyle} ${hideList}`} id={`${lastChild} ${liStyle}`} value={list.ingredient_name}>
                    <label className="container1" htmlFor={list.id}>
                      <input
                        checked={!!listStyle}
                        type="checkbox"
                        value={list.ingredient_name}
                        id={list.id}
                        onClick={(event) => this.clickIngredient(itemKey,event)}
                      />
                      <span className="checkmark1" />
                    </label>
                    <span className="bullet-style">.</span>
                    {quantityValue[0]}
                    <sup>{a.n}</sup>&frasl;<sub>{a.d}</sub>&nbsp;
                    {unit} {list.ingredient_name}
                  </li>
                </div>
              }
            </div>

          );
        } else if (quantityValue[0] === '') {
          return (
            // eslint-disable-next-line
            <div key={`groecryList ${listKey}`}>
              {ingredientCount !== 1 ?
                <li key={list.id} className={`${lastChild} ${liStyle} ${listStyle} ${hideList}`} value={list.ingredient_name}>
                  <label className="container1" htmlFor={list.id}>
                    <input
                      checked={!!listStyle}
                      type="checkbox"
                      value={list.ingredient_name}
                      id={list.id}
                      onClick={(event) => this.clickIngredient(itemKey,event)}
                    />
                    <span className="checkmark1" />
                  </label>
                  <span className="bullet-style">.</span>
                  {a.n}  {unit} {list.ingredient_name}
                </li>:
                <div>
                  <li className={`multiple-units ${listStyle} ${hideList}`} value={list.ingredient_name}>
                    <label className="container1" htmlFor={list.id}>
                      <input
                        checked={!!listStyle}
                        type="checkbox"
                        value={list.ingredient_name}
                        id={list.id}
                        onClick={(event) => this.clickIngredient(itemKey,event)}
                      />
                      <span className="checkmark1" />
                    </label>
                    {list.ingredient_name} (multiple units)
                  </li>
                  <li key={list.id} className={`${lastChild} ${liStyle} ${listStyle} ${hideList}`} value={list.ingredient_name}>
                    <label className="container1" htmlFor={list.id}>
                      <input
                        checked={!!listStyle}
                        type="checkbox"
                        value={list.ingredient_name}
                        id={list.id}
                        onClick={(event) => this.clickIngredient(itemKey,event)}
                      />
                      <span className="checkmark1" />
                    </label>
                    <span className="bullet-style">.</span>
                    {a.n}  {unit} {list.ingredient_name}
                  </li>
                </div>}
            </div>
          );
        }
        return (<div value={list.ingredient_name} key={`${quantityValue[0]} ${unit} ${list.ingredient_name}`}>{ingredientCount !== 1 ?
          <li key={list.id} className={`${lastChild} ${liStyle} ${listStyle} ${hideList}`} value={list.ingredient_name}>
            <label className="container1" htmlFor={list.id}>
              <input
                checked={!!listStyle}
                type="checkbox"
                value={list.ingredient_name}
                id={list.id}
                onChange={(event) => this.clickIngredient(itemKey,event)}
              />
              <span className="checkmark1" />
            </label>
            <span className="bullet-style">.</span>
            {quantityValue[0]} {unit} {list.ingredient_name}
          </li>
          :
          <div>
            <li className={`multiple-units ${listStyle} ${hideList}`} value={list.ingredient_name}>
              <label className="container1" htmlFor={list.id}>
                <input
                  checked={!!listStyle}
                  type="checkbox"
                  value={list.ingredient_name}
                  id={list.id}
                  onChange={(event) => this.clickIngredient(itemKey,event)}
                />
                <span className="checkmark1" />
              </label>
              {list.ingredient_name} (multiple units)
            </li>
            <li key={list.id} className={`${lastChild} ${liStyle} ${listStyle} ${hideList}`} value={list.ingredient_name}>
              <label className="container1" htmlFor={list.id}>
                <input
                  checked={!!listStyle}
                  type="checkbox"
                  value={list.ingredient_name}
                  id={list.id}
                  onChange={(event) => this.clickIngredient(itemKey,event)}
                />
                <span className="checkmark1" />
              </label>
              <span className="bullet-style">.</span>
              {quantityValue[0]} {unit} {list.ingredient_name}
            </li>
          </div>}</div>
        );}
      return null;
    })

  }

  render() {
    const { error, previousWeekEvents, previous, loader, displayPrint, groceryList } = this.state;
    // eslint-disable-next-line
    const currentList = previousWeekEvents !== 'none' ? true : false;
    const groceryText = !error ? 'Here\'s Your Grocery List for the Week ' :
      'This week’s Grocery List is empty!';
    const grocerySubText = !error ? ` Your grocery list is based on the cook schedule in the sidebar.
    Print this list or view it from your mobile phone!` : 
      `Time to build your meal plan for the week! Go to the Weekly Plan tab to create a new meal plan. Looking for a list you already built? Try switching dates above to see if the list you're looking for is scheduled for a different week. `
    const groceryClass = error ? 'grocery_top_txt' : 'mb-hide grocery_top_txt'
    const grocerySubClass = error ? 'grocery_sub_txt' : 'grocery_sub_txt err_free';
    const previousWeekClass =  previous && groceryList && groceryList.length > 0 ? 'no-print mb-hide' : 'no-print lst_wk_err'
    return (
      <div className="grocery-week">
        <style type="text/css">
          {"@media print{@page {size: A4 portrait}}"}
        </style>
        <ReactResizeDetector handleWidth handleHeight onResize={() => this.onResize()} />
        {
          loader &&
          <div className="loading">
            <Loader />
            <div className="modal-backdrop fade in" />
          </div>
        }

        <div>
          <div className="swap-mode-dtail-cntnt grocry-cntnt">
            {currentList && previous === false ?
              <div className="no-print">
                <h4 className={groceryClass}>{ groceryText }</h4>
                {displayPrint ?
                  <p className={grocerySubClass}>
                    {grocerySubText}
                  </p>
                  :
                  error &&
                  <p className="grocery_sub_txt">
                    <span>Time to build your meal plan for the week! Go to the Weekly Plan tab to 
                     create a new meal plan. </span>
                    <span>
                    Looking for a list you already built? Try switching dates above to see if the 
                    list you're looking for is scheduled for a different week. </span>
                  </p>
                }
                { groceryList && groceryList.length === 0 && error &&
                  <div className="build-new-plan">
                    <img src="/images/GroceryListIcon.png" alt="grocerylist"/>
                  </div>
                }
              </div>
              :
              <div className={previousWeekClass}>
                <h4>
                  <b className="grocery-list-heading">Hang On Ninja! </b>
                  This is last week’s Grocery List!
                </h4>
                {error === true && previous === false ?
                  <p>
                    Make sure you don’t dwell too long in the past… old groceries won’t last!
                    Set the stale kale chips aside and let’s get planning or cooking for another week.
                    Advance to the next week, or create a new plan to get yourself started!
                  </p> :
                  <div>
                    <p>Aaaand its empty! Advance to go to the current week.</p>
                    {error && previous &&
                    <div className="build-new-plan">
                      <img src="/images/GroceryListIcon.png" alt="grocerylist"/>
                    </div>}
                  </div>
                }

              </div>
            }
            {groceryList && groceryList.length >0 &&
            <div className="display-grocery-list">
              <span>
                <button onClick={(event) => this.showList(event, 'buy')} id="buy">
                  To Buy
                </button>
                <button onClick={(event) => this.showList(event, 'show')} id="show" className="grocery-list-btn">
                  Show All
                </button>
              </span>
            </div>
            }
            <div  id="grocery-inner-list">
              {!error && this.groceryData()}
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default GroceryList;
