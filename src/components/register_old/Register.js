import React, { Component } from 'react';
import { getGuestData, updateGuestData, removeTag } from 'lib/api';
import moment from 'moment';
import Basic from './Basic';
import MealPlan from './MealPlan';
import FoodPrefrences from './FoodPrefrences';
import FinalStep from './FinalStep';

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentStep: 'basic',
    };
  }

  // dummy purpose
  componentDidMount() {
    localStorage.setItem('token', '');
  }

  onClickFinish(data) {
    const guestEmail = data;
    const guestData = sessionStorage.getItem('guestAccount') ?
      JSON.parse(sessionStorage.getItem('guestAccount')) : null;
    if (guestData) {
      const inputData = {};
      inputData.email = guestEmail;
      getGuestData(inputData).then((responseJson) => {
        if (responseJson && responseJson.result_code === 1) {
          inputData.id = responseJson.id;
          inputData.tags = '';
          inputData['p[6]'] = 6;
          inputData['field[%PEOPLE_COOKING_FOR%, 0]'] = guestData.mealPlan.PEOPLE_COOKING_FOR;
          inputData['field[%COOKING_STYLE%, 0]'] = guestData.mealPlan.COOKING_STYLE;
          // inputData['field[%DIET_TYPE%, 0]'] = guestData.foodPrefrences.DIET_TYPE;
          inputData['field[%FREE_TRIAL_SIGN_UP_DATE%, 0]'] = moment();
          updateGuestData(inputData).then(() => {
            const inputs = {};
            inputs.email = guestEmail;
            inputs.tags = 'Incomplete Trial Sign Up';
            removeTag(inputs).then((responseJson2) => {
              if (responseJson2 && responseJson2.result_code === 1);
            });
          });
          if (guestEmail && guestEmail != null) {
            this.props.history.push('/dashboard', { guestEmail, exist: 'no' });
          } else {
            this.props.history.push('/login');
          }
        }
      }).catch(() => {
      });
    }
  }

  onClickNext(currentState, nextState, data) {
    let guestAccount = {};
    const guestAccountData = sessionStorage.getItem('guestAccount');
    if (guestAccountData && guestAccountData != null) {
      guestAccount = JSON.parse(guestAccountData);
    }
    guestAccount[`${currentState}`] = data;
    sessionStorage.setItem('guestAccount', JSON.stringify(guestAccount));
    this.setState({ currentStep: nextState });
  }

  renderContent() {
    switch (this.state.currentStep) {
    case 'basic':
      return (
        <Basic onClickNext={data => this.onClickNext('basic', 'mealPlan', data)} {...this.props} />
      );

    case 'mealPlan':
      return (
        <MealPlan
          onClickNext={data => this.onClickNext('mealPlan', 'foodPrefrences', data)}
          onClickBack={() => { this.setState({ currentStep: 'basic' }); }}
          {...this.props}
        />
      );

    case 'foodPrefrences':
      return (
        <FoodPrefrences
          onClickNext={() => { this.setState({ currentStep: 'final' }); }}
          onClickBack={() => { this.setState({ currentStep: 'mealPlan' }); }}
          {...this.props}
        />
      );

    case 'final':
      return (
        <FinalStep
          onClickFinish={data => this.onClickFinish(data)}
          onClickBack={() => { this.setState({ currentStep: 'foodPrefrences' }); }}
          {...this.props}
        />
      );
    default:
    }
    return this.state.currentStep;
  }
  render() {
    const { currentStep } = this.state;
    const { history } = this.props;

    return (
      <div className="wrapper transition-item detail-page">
        <header>
          <div className="container">
            <img src="/images/logo.png" alt="" onClick={() => { history.push('/'); }} />
          </div>
        </header>
        <section className="main-cntnt">
          <div className="container">
            <div className="main-cntnt-box">
              <h2>Ready to Become A Ninja?</h2>
              <p className="hdng-para">
                We&apos;ll help you create a customized weekly meal plan that fits your preferences AND
                saves you time planning, shopping and cooking delicious meals throughout the week.
                <p>Sign up below!</p>
              </p>
              <div className="main-cntnt-inner">
                <div className="col-xs-12 col-md-6 intro-sectn">
                  <div className="timeline">
                    <div className={`${(currentStep === 'basic') ? 'timeline-row active' : 'timeline-row'}`}>
                      <div className="timeline-content">
                        <span />
                        <p>The Basics</p>
                      </div>
                    </div>
                    <div className={`${(currentStep === 'mealPlan') ? 'timeline-row active' : 'timeline-row'}`}>
                      <div className="timeline-content">
                        <span />
                        <p>Meal Plan Style</p>
                      </div>
                    </div>
                    <div className={`${(currentStep === 'foodPrefrences') ? 'timeline-row active' : 'timeline-row'}`}>
                      <div className="timeline-content">
                        <span />
                        <p>Food Preferences</p>
                      </div>
                    </div>
                    <div className={`${(currentStep === 'final') ? 'timeline-row active' : 'timeline-row'} last-row-timeline`}>
                      <div className="timeline-content">
                        <span />
                        <p>Build Your First Plan</p>
                      </div>
                    </div>
                  </div>
                </div>
                <div className="col-xs-12 col-md-6 login-sectn">
                  {this.renderContent()}
                </div>
              </div>
            </div>
          </div>
        </section>

        <section className="copyright">
          <div className="container">
            <p>© Copyright - Meal Plan Ninja, Inc. 2018</p>
          </div>
        </section>
      </div>
    );
  }
}

export default Register;
