import React, { Component } from 'react';
import moment from 'moment';

export default class FinalStep extends Component {
  constructor(props) {
    super(props);
    this.state = {
      guestEmail: localStorage.email,
    };
  }

  onPressFinsh() {
    let { guestEmail } = this.state;
    const guestAccountData = sessionStorage.getItem('guestAccount');
    if (guestAccountData && guestAccountData != null) {
      guestEmail = localStorage.email;
      //  sessionStorage.removeItem('guestAccount');
      /* temp run need to remove */
      /* End temp run */
      if (guestEmail && guestEmail != null) {
        localStorage.removeItem('type');
        this.props.history.push('/dashboard', { guestEmail, exist: 'no' });

        // track first click build
        window.analytics.track('Clicks Build Plan: First Signup', {
          date: moment().format('dddd DD-MM-YYYY'),
        });
      } else {
        this.props.history.push('/login');
      }
    }
    this.props.onClickFinish(guestEmail);
  }

  render() {
    return (
      <div className="build-plan-first">
        <h4>Congrats! You&apos;re a Meal Plan Ninja!</h4>
        <div className="row">
          <div className="col-md-12 form-row">
            <p>
              Time to build your first plan. Don&apos;t worry, our tutorial will be there to
              guide you through the process the first time. If you ever get stuck, just contact
              the Support Ninjas!
            </p>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12 form-row">
            <p className="free-trail-txt">Now let{'\''}s start your free trial!</p>
          </div>
        </div>
        <div className="row">
          <div className="form-row text-center col-md-12">
            <button className="login-btn" onClick={() => this.onPressFinsh()} >Build My Plan</button>
          </div>
        </div>
      </div>
    );
  }
}
