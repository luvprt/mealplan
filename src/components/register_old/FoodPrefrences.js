import React, { Component } from 'react';
import RoutingConstants from 'lib/RoutingConstants';
import { post, get } from 'lib/api';
import moment from 'moment';

export default class FoodPrefrences extends Component {
  constructor(props) {
    super(props);
    this.state = {
      DIET_TYPE: '',
      PROTEIN_EXCLUSION_LIST: [],
      PROTEIN_LIST: [],
      errorMessage: {},
      isLoading: false,
      input: {}
    };
  }

  componentDidMount() {
    this.loadData();
  }

  async onClickFinish() {
    const guestData = await sessionStorage.getItem('guestAccount') ?
      JSON.parse(sessionStorage.getItem('guestAccount')) : null;
    const { PROTEIN_EXCLUSION_LIST } = this.state;
    if (guestData) {
      const inputData = {};
      const url = RoutingConstants.mealPlanStyles;

      inputData.userExclusionsIds = PROTEIN_EXCLUSION_LIST;
      inputData.PEOPLE_COOKING_FOR = guestData.mealPlan.PEOPLE_COOKING_FOR;
      inputData.COOKING_STYLE = guestData.mealPlan.COOKING_STYLE;
      inputData.WHY_WE_USE = guestData.foodPrefrences.why_we_use;
      inputData.FEEL_ABOUT_MEAL_PLAN = guestData.foodPrefrences.feel_about_meal_plan;
      inputData.BEST_DESCRIBE_YOU = guestData.mealPlan.BEST_DESCRIBE_YOU;
      post(url, inputData).then(() => {
        this.setState({ isLoading: false });
        this.props.onClickNext();
      });
    }
  }

  onClickNext() {
    const { input, errorMessage } = this.state;
    console.log(this.state);
    if (input.feel_about_meal_plan === '') {
      errorMessage.feel_about_meal_plan = "Please select your choice"
    } else {
      errorMessage.feel_about_meal_plan = false
    }
    if (input.why_we_use === '') {
      errorMessage.why_we_use = "Please select your choice"
    } else {
      errorMessage.why_we_use = false;
    }
    if (!errorMessage.feel_about_meal_plan &&
    !errorMessage.why_we_use) {
      this.setState({ isLoading: true }, () => {
        let guestAccount = {};
        const guestAccountData = sessionStorage.getItem('guestAccount');
        if (guestAccountData && guestAccountData != null) {
          guestAccount = JSON.parse(guestAccountData);
        }
        guestAccount.foodPrefrences = this.state.input;
        sessionStorage.setItem('guestAccount', JSON.stringify(guestAccount));

        // track food preferences
        // window.analytics.track('Completes Step 3: Food Preferences', {
        //   date: moment().format('dddd DD-MM-YYYY'),
        //   DIET_TYPE,
        //   no_of_proteins_excluded: PROTEIN_EXCLUSION_LIST.length,
        // });
        this.onClickFinish();
      });
    } else {
      this.setState({ isLoading: false });
      // window.analytics.track('Error In Step 3: Food Preferences', {
      //   date: moment().format('dddd DD-MM-YYYY'),
      //   error: 'Diet Type not selected',
      // });
    }
  }

  onClickBack() {
    // track back button
    window.analytics.track('Clicked Back Button: Step 3 ', {
      date: moment().format('dddd DD-MM-YYYY'),
    });

    this.props.onClickBack();
  }

  loadData() {
    const url = RoutingConstants.fetchProteinExclusion;
    const { input } = this.state;
    input.feel_about_meal_plan = ""
    input.why_we_use = ""
    get(url).then((response) => {
      this.setState({ PROTEIN_LIST: response, input });
    }).catch(() => {
      // console.log('protein exclusion error',err)
    });
  }

  updateValue(event) {
    const { input, errorMessage } = this.state;
    input[event.target.name] = event.target.value;
    errorMessage[event.target.name] = false;
    this.setState ({ input, errorMessage })
  }

  updateList(ref, value) {
    let isValueExists = false;
    const { PROTEIN_EXCLUSION_LIST } = this.state;
    if (PROTEIN_EXCLUSION_LIST) {
      isValueExists = PROTEIN_EXCLUSION_LIST.includes(value);
      if (isValueExists) {
        const index = PROTEIN_EXCLUSION_LIST.indexOf(value);
        if (index > -1) {
          PROTEIN_EXCLUSION_LIST.splice(index, 1);
          this.setState({ PROTEIN_EXCLUSION_LIST });
        }
      } else {
        PROTEIN_EXCLUSION_LIST.push(value);
        this.setState({ PROTEIN_EXCLUSION_LIST });
      }
    }
  }

  render() {
    const { PROTEIN_LIST, errorMessage } = this.state;

    let data = false;
    if (PROTEIN_LIST.result) {
      data = true;
    }
    return (
      <div>
        <h4>Food Preferences</h4>
        {/* <div className="row">
          <div className="col-md-12 form-row select-row-small">
            <label htmlFor="message" className={this.state.errorMessage ? 'error-label' : null}>
              Preferred Diet Type:
              <span style={{ color: '#4C4C4C' }}>
                <strong>Low Carb </strong>is the only diet type available with early access,
                but your answer to this question helps us prioritize which diet type we launch next!&quot;
              </span>
            </label>
            <div className="btn-group bootstrap-select custm-selct">

              <select
                className="selectpicker show-tick form-control dietType"
                onChange={e => this.setState({ DIET_TYPE: e.target.value })}
                value={this.state.DIET_TYPE}
              >
                <option value="">Select</option>
                <option value="low_carbs">Low carb</option>
                <option value="balanced">Balanced</option>
                <option value="keto">Keto</option>
                <option value="Paleo">Paleo</option>
                <option value="Vegetarian">Vegetarian</option>
                <option value="Whole30">Whole30</option>
              </select>
            </div>
          </div>
        </div> */}
        <div className="row">
          <div className="col-md-12 form-row select-row-small label-row-full">
            <label htmlFor="taste">Protein Exclusion List (Things You DO NOT Eat):
              <span>
                Select proteins you want excluded from your plan. You can change these settings later.
              </span>
            </label>
            <div className="row select-checks-box">
              { data && PROTEIN_LIST.result.map((item) => {
                const text = item.exclusion_types_data;
                let value = 4;
                if (text.match(/Mixed.*/)) {
                  value = 8;
                }
                return (
                  <div className={`col-md-${value}`} key={item.id}>
                    <label className="container1" htmlFor={item.id}>
                      <input
                        type="checkbox"
                        id={item.id}
                        onChange={e => this.updateList('PROTEIN_EXCLUSION_LIST', e.target.value)}
                        value={item.id}
                      />
                      { item.exclusion_types_data !== '' &&
                      item.exclusion_types_data
                      }
                      <span className="checkmark1" />
                    </label>
                  </div>
                );
              })
              }
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12 form-row">
            <p
              className={errorMessage.feel_about_meal_plan ?
                'inner-check-row-hdng error-label' : 'nner-check-row-hdng'}
            >
            When you think about meal planning or prepping for the week, how do you feel?
            </p>
            {errorMessage.feel_about_meal_plan ? <span className="error-msg new-form">{errorMessage.feel_about_meal_plan}</span> : null}
            <div className="check-row">
              <label className="rbutton" htmlFor="difficult">
                <input
                  type="radio"
                  name="feel_about_meal_plan"
                  id="difficult"

                  onChange={(event) => this.updateValue(event)}
                  value="It's difficult (I really don't look forward to it)"
                />
                <span className="checkmark" />
              1- It&apos;s difficult (I really don&apos;t look forward to it)
              </label>
            </div>

            <div className="check-row">
              <label className="rbutton" htmlFor="inconvenient">
                <input
                  type="radio"
                  name="feel_about_meal_plan"
                  id="inconvenient"
                  onChange={(event) => this.updateValue(event)}
                  value="It's inconvenient (I don't always have time or want to)"
                />
                <span className="checkmark" />
              2- It&apos;s inconvenient (I don&apos;t always have time or want to)
              </label>
            </div>
            <div className="check-row">
              <label className="rbutton" htmlFor="Neutral">
                <input
                  type="radio"
                  name="feel_about_meal_plan"
                  id="Neutral"
                  onChange={(event) => this.updateValue(event)}
                  value="Neutral (It's ok...)"
                />
                <span className="checkmark" />
              3- Neutral (It&apos;s ok...)
              </label>
            </div>
            <div className="check-row">
              <label className="rbutton" htmlFor="good">
                <input
                  type="radio"
                  name="feel_about_meal_plan"
                  id="good"
                  onChange={(event) => this.updateValue(event)}
                  value="Good (It's not too much of a burden / It's easy!)"
                />
                <span className="checkmark" />
              4- Good (It&apos;s not too much of a burden / It&apos;s easy!)
              </label>
            </div>

            <div className="check-row">
              <label className="rbutton" htmlFor="easy">
                <input
                  type="radio"
                  name="feel_about_meal_plan"
                  id="easy"
                  onChange={(event) => this.updateValue(event)}
                  value="It's easy! (I kind of look forward to it!)"
                />
                <span className="checkmark" />
              5- It&apos;s easy! (I kind of look forward to it!)
              </label>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12 form-row">
            <p
              className={errorMessage.why_we_use ?
                'inner-check-row-hdng error-label' : 'nner-check-row-hdng'}
            >
            What do you think is the most difficult part about your meal planning ?
            Basically, why use Meal Plan Ninja ?
            </p>
            {errorMessage.why_we_use ? <span className="error-msg new-form">{errorMessage.why_we_use}</span> : null}
            <span className="inner-hint"> Pick one: Select the item that is most painful for you</span>
            <div className="check-row">
              <label className="rbutton" htmlFor="Reseaching">
                <input
                  type="radio"
                  name="why_we_use"
                  id="Reseaching"

                  onChange={(event) => this.updateValue(event)}
                  value="Reseaching new recipes each week"
                />
                <span className="checkmark" />
              Reseaching new recipes each week
              </label>
            </div>

            <div className="check-row">
              <label className="rbutton" htmlFor="planning">
                <input
                  type="radio"
                  name="why_we_use"
                  id="planning"
                  onChange={(event) => this.updateValue(event)}
                  value="Actually selecting / planning out the recipes that i want for the week"
                />
                <span className="checkmark" />
              Actually selecting / planning out the recipes that i want for the week
              </label>
            </div>
            <div className="check-row">
              <label className="rbutton" htmlFor="Creating">
                <input
                  type="radio"
                  name="why_we_use"
                  id="Creating"
                  onChange={(event) => this.updateValue(event)}
                  value="Creating a grocery list that calculates the ingredient totals
                for all my meals for the week."
                />
                <span className="checkmark" />
              Creating a grocery list that calculates the ingredient totals
              for all my meals for the week.
              </label>
            </div>
            <div className="check-row">
              <label className="rbutton" htmlFor="selecting">
                <input
                  type="radio"
                  name="why_we_use"
                  id="selecting"
                  onChange={(event) => this.updateValue(event)}
                  value="Selecting the meals that go well together for meal prep / batch cooking
                so I don't have to spend hours cooking in the kitchen"
                />
                <span className="checkmark" />
              Selecting the meals that go well together for meal prep / batch cooking
              so I don&apos;t have to spend hours cooking in the kitchen
              </label>
            </div>

            <div className="check-row">
              <label className="rbutton" htmlFor="poke">
                <input
                  type="radio"
                  name="why_we_use"
                  id="poke"
                  onChange={(event) => this.updateValue(event)}
                  value="Not Applicable (I'm just poking around)"
                />
                <span className="checkmark" />
              Not Applicable (I&apos;m just poking around)
              </label>
            </div>
            <div className="check-row">
              <label className="rbutton" htmlFor="other">
                <input
                  type="radio"
                  name="why_we_use"
                  id="other"
                  onChange={(event) => this.updateValue(event)}
                  value="other"
                />
                <span className="checkmark" />
              Other (please specify below)
              </label>
            </div>
            <div className="check-row">

              <textarea
                className='form-control'
                id="exampleFormControlTextarea1"
                rows="3"
                name="why_we_use"
                onChange={event => this.updateValue(event)}
              />
            </div>
          </div>
        </div>
        <div className="row">
          <div className="form-row text-right col-md-12">
            <button className="back-btn" onClick={() => this.onClickBack()}>Back</button>
            {
              this.state.isLoading ?
                <button className="next-btn">Loading...</button>
                :
                <button className="next-btn" onClick={() => this.onClickNext()}>Next</button>
            }

          </div>
        </div>
      </div>
    );
  }
}
