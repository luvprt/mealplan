import React, { Component } from 'react';
import moment from 'moment';

export default class MealPlan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      PEOPLE_COOKING_FOR: 2,
      COOKING_STYLE: null,
      BEST_DESCRIBE_YOU: null,
      errorMessage: {},
    };
  }

  componentDidMount() {
    this.loadData();
  }

  onClickNext() {
    const { PEOPLE_COOKING_FOR, COOKING_STYLE, BEST_DESCRIBE_YOU } = this.state;
    if (PEOPLE_COOKING_FOR && COOKING_STYLE && BEST_DESCRIBE_YOU) {
      this.props.onClickNext({ PEOPLE_COOKING_FOR, COOKING_STYLE, BEST_DESCRIBE_YOU });

      // track meal preferences
      window.analytics.track('Completes Step 2: Meal Plan Style', {
        date: moment().format('dddd DD-MM-YYYY'),
        PEOPLE_COOKING_FOR,
        COOKING_STYLE,
        BEST_DESCRIBE_YOU
      });
    } else {
      const errorMessage = this.state.errorMessage;
      if (!PEOPLE_COOKING_FOR) {
        errorMessage.PEOPLE_COOKING_FOR = 'Please Select people cooking for value from dropdown';
      } else {
        errorMessage.PEOPLE_COOKING_FOR = false;
      } if (!COOKING_STYLE) {
        errorMessage.COOKING_STYLE = 'Please Select cooking style from radio button';
        window.analytics.track('Error in Step 2: Meal Plan Style', {
          date: moment().format('dddd DD-MM-YYYY'),
          error: 'cooking style not selected',
        });
      } else {
        errorMessage.COOKING_STYLE = false;
      }
      if (!BEST_DESCRIBE_YOU) {
        errorMessage.BEST_DESCRIBE_YOU = 'Please Select the best describe option.';
      } else {
        errorMessage.BEST_DESCRIBE_YOU = false;
      }

      this.setState({ errorMessage });
    }
  }

  async loadData() {
    const data = await sessionStorage.getItem('guestAccount') ?
      JSON.parse(sessionStorage.getItem('guestAccount')) : null;
    if (data && data.mealPlan) {
      this.setState(data.mealPlan);
    }
  }

  render() {
    const { errorMessage } = this.state;
    return (
      <div>
        <h4>Meal Plan Style</h4>
        <div className="row">
          <div className="col-md-12 form-row select-row-small mealplan-top-row">
            <label htmlFor="cooking_for" className={errorMessage.PEOPLE_COOKING_FOR ? 'error-label' : null}>
            How many people will you be planning and cooking for on a weekly basis?
            </label>
            <div className="btn-group bootstrap-select custm-selct mealplan-btn">

              <select className="selectpicker show-tick form-control" onChange={(e) => { this.setState({ PEOPLE_COOKING_FOR: e.target.value }); }} value={this.state.PEOPLE_COOKING_FOR}>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
              </select>
            </div>
          </div>
        </div>
        <div className="row">
          <div className="col-md-12 form-row">
            <p
              className={errorMessage.BEST_DESCRIBE_YOU ?
                'inner-check-row-hdng error-label' : 'nner-check-row-hdng'}
            >
              Which of the following best describes you.
            </p>
            <div className="check-row">
              <label className="rbutton">
                <input type="radio" name="best-describe" onChange={(e) => { this.setState({ BEST_DESCRIBE_YOU: e.target.value }); }} value="I am trying meal planning for the first time" checked={(this.state.BEST_DESCRIBE_YOU === 'I am trying meal planning for the first time')} />
                <span className="checkmark" />
                I am trying meal planning for the first time
              </label>
            </div>
            <div className="check-row">
              <label className="rbutton">
                <input type="radio" name="best-describe" onChange={(e) => { this.setState({ BEST_DESCRIBE_YOU: e.target.value }); }} value="I actively meal plant atleast 2 weeks per month." checked={(this.state.BEST_DESCRIBE_YOU === 'I actively meal plant atleast 2 weeks per month.')} />
                <span className="checkmark" />
                I actively meal plant atleast 2 weeks per month.
              </label>
            </div>
            <div className="check-row">
              <label className="rbutton">
                <input type="radio" name="best-describe" onChange={(e) => { this.setState({ BEST_DESCRIBE_YOU: e.target.value }); }} value="I meal plan every once a while" checked={(this.state.BEST_DESCRIBE_YOU === 'I meal plan every once a while')} />
                <span className="checkmark" />
                I meal plan every once a while
              </label>
            </div>

          </div>
        </div>
        <div className="row">
          <div className="col-md-12 form-row">
            <p
              className={errorMessage.COOKING_STYLE ?
                'inner-check-row-hdng error-label' : 'nner-check-row-hdng'}
            >
              Select your preferred cooking style (pick 1):
              <span className="hint-msg">
                Tailored meal plans for each cooking style are coming soon!
                We&quot;ll save your preferences so we can improve your plan when we update our algorithms
              </span>
            </p>
            <div className="check-row">
              <label className="rbutton" htmlFor="Batch">
                <input type="radio" name="cooking-style" id="Batch" onChange={(e) => { this.setState({ COOKING_STYLE: e.target.value }); }} value="Batch Cook" checked={(this.state.COOKING_STYLE === 'Batch Cook')} />
                <span className="checkmark" />
                <b>Batch Cook:</b> Make multiple meals in a larger cook session (60-90 mins), two+ times per week. Cook twice, eat all week.
              </label>
            </div>

            <div className="check-row">
              <label className="rbutton" htmlFor="Weeknight">
                <input type="radio" name="cooking-style" id="Weeknight" onChange={(e) => { this.setState({ COOKING_STYLE: e.target.value }); }} value="Batch & weeknight" checked={(this.state.COOKING_STYLE === 'Batch & weeknight')} />
                <span className="checkmark" />
                <b>Batch & Weeknight:</b> Batch cook on the weekend for the first half of the week then cook nightly meals for the last half.
              </label>
            </div>
            <div className="check-row">
              <label className="rbutton" htmlFor="fresh">
                <input type="radio" name="cooking-style" id="fresh" onChange={(e) => { this.setState({ COOKING_STYLE: e.target.value }); }} value="Cook fresh" checked={(this.state.COOKING_STYLE === 'Cook fresh')} />
                <span className="checkmark" />
                <b>Cook fresh:</b> Cook quick, fresh dinners each night of the week.
              </label>
            </div>
          </div>
        </div>

        <div className="row">
          <div className="form-row text-right col-md-12">
            <button className="next-btn" onClick={() => this.onClickNext()}>Next</button>
          </div>
        </div>
      </div>
    );
  }
}
