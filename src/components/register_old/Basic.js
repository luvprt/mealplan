/*
Component Name: Basic
Description: First step of registeration.
*/

import React, { Component } from 'react';
import RoutingConstants from 'lib/RoutingConstants';
import { validateInputs } from 'lib/services';
import { registerGuestUser } from 'lib/api';
import ReactHtmlParser from 'react-html-parser';

export default class Basic extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputs: {},
      errorMessage: {},
      isLoading: false,
      accept: false,
    };
  }

  onClickNext() {
    const { email, password, gender,
    } = this.state.inputs;
    const { errorMessage, inputs, accept } = this.state;
    window.analytics.track('Clicked next on step 1', {
      first_name: inputs.first_name,
      last_name: inputs.last_name,
      email,
      gender,
    }, {
      integrations: {
        'All': true,
        'Facebook Pixel': false,
      }
    });
    const dateStr = `${this.state.year}-${this.state.month}-${this.state.day}`;
    if (!validateInputs('alphabetics', inputs.first_name)) {
      errorMessage.first_name = 'Enter your first name';
    } else {
      errorMessage.first_name = false;
    } if (!validateInputs('hypenAlphabetics', inputs.last_name)) {
      errorMessage.last_name = 'Enter your last name';
    } else {
      errorMessage.last_name = false;
    } if (!validateInputs('email', email)) {
      errorMessage.email = 'Please enter a valid email address';
    } else {
      errorMessage.email = false;
    } if (!validateInputs('password', password)) {
      errorMessage.password = 'Create a secure password. It must contain 8+ characters with 1 number or special character.';
    } else {
      errorMessage.password = false;
    } if(!accept) {
      errorMessage.terms_service_consent = 'In order to sign up with Meal Plan Ninja, you must accept the terms of service and privacy policy.';
    } else {
      errorMessage.terms_service_consent = false;
    }

    if (!errorMessage.first_name &&
        !errorMessage.last_name &&
        !errorMessage.email &&
        !errorMessage.password &&
        !errorMessage.terms_service_consent
    ) {
      this.setState({ isLoading: true }, () => {
        const Inputs = this.state.inputs;

        const url = RoutingConstants.register;
        Inputs.date_of_birth = dateStr;

        registerGuestUser(url, Inputs).then((responseJson) => {
          if (responseJson.result) {
            sessionStorage.removeItem('guestAccount');
            localStorage.removeItem('token');
            localStorage.setItem('token', responseJson.result.token);
            localStorage.setItem('firstName', responseJson.result.first_name);
            localStorage.setItem('lastName', responseJson.result.last_name);
            localStorage.setItem('email', responseJson.result.email);
            localStorage.removeItem('isTourCompleted');
            window.analytics.identify(Inputs.email, {
              first_name: Inputs.first_name,
              last_name: Inputs.last_name,
              email: Inputs.email,
            });
          }
          this.props.onClickNext(Inputs);
        }).catch((err) => {
          if (err.error && err.error.email[0] === 'The email has already been taken.') {
            errorMessage.email =
              ` <div>
              This email already exists. Please go to the
              <a href=/login>
                login
              </a>
              page.
              </div>`;
            errorMessage.email = ReactHtmlParser(errorMessage.email);
            window.analytics.track('Error in Step 1', {
              email: Inputs.email,
              error: 'User Already Exist',
            });
          }
          this.setState({ isLoading: false, errorMessage });
        });
      });
    } else {
      this.setState({ errorMessage });

      // track error
      window.analytics.track('Error in Step 1', {
        error: errorMessage,
      });
    }
  }

  updateInputs(ref, text) {
    const {inputs, errorMessage} = this.state;
    let { accept } = this.state;
    errorMessage[`${ref}`] = ''
    inputs[`${ref}`] = text.trim();
    if( ref === "terms_service_consent" ) {
      accept = !accept
      inputs[`${ref}`] = accept;
    }
    this.setState({ inputs, accept });
  }

  render() {
    const { errorMessage } = this.state;
    return (
      <div>
        <h4>Let{'\''}s Start with Some Basics</h4>
        {errorMessage.error ? <span className="error-msg" style={{ position: 'static' }}>{errorMessage.error}</span> : null}
        <div className="row">
          <div className="col-md-6 form-row">
            <label htmlFor="first_name" className={errorMessage.first_name ? 'error-label' : null}>First:</label>
            <input type="text" onChange={e => this.updateInputs('first_name', e.target.value)}/>
            {errorMessage.first_name ? <span className="error-msg">{errorMessage.first_name}</span> : null}
          </div>
          <div className="col-md-6 form-row">
            <label htmlFor="last_name" className={errorMessage.last_name ? 'error-label' : null}>Last:</label>
            <input type="text" onChange={e => this.updateInputs('last_name', e.target.value)}/>
            {errorMessage.last_name ? <span className="error-msg">{errorMessage.last_name}</span> : null}
          </div>
        </div>
        <div className="row">
          <div className="col-md-12 form-row email-label">
            <label htmlFor="email" className={errorMessage.email ? 'error-label' : null}>Email:</label>
            <input type="email" onChange={e => this.updateInputs('email', e.target.value)}/>
            {errorMessage.email ? <span className="error-msg">{errorMessage.email}</span> : null}
          </div>
        </div>
        <div className="row">
          <div className="col-md-12 form-row pass-row">
            <label htmlFor="password" className={errorMessage.password ? 'error-label' : null}>Create Password:</label>
            <input
              className="pwdInput"
              type="password"
              onChange={e => this.updateInputs('password', e.target.value)}
            />
            {errorMessage.password ?
              <span className="error-msg">{errorMessage.password}</span>
              :
              <span className="hint-msg">A strong password has: 8+ characters with at least 1 number or special character</span>
            }
          </div>
        </div>
        <div className="row">
          <div className="col-md-12 form-row pass-row">

            <label className={errorMessage.terms_service_consent ? 'container1 terms-condition error-label' : 'container1 terms-condition'}>

              <input
                type="checkbox"
                name="terms_service_consent"
                onChange={e => this.updateInputs('terms_service_consent', e.target.value)}
                style={{width:'auto'}}
                tabIndex="-1"
              />
              <span className="checkmark1" tabIndex="0" role="button"/>


            </label>
            <span className="terms-condition" style={{display:'block'}}>
            By creating an account, you agree to our
              <a href="https://mealplanninja.com/terms-of-service/" target='_blank' rel="noopener noreferrer">Terms of Service</a>
              and
              <a href="https://mealplanninja.com/privacy-policy/" target='_blank' rel="noopener noreferrer">  Privacy Policy  </a>
            </span>

            {errorMessage.terms_service_consent &&
              <span className="error-msg">{errorMessage.terms_service_consent}</span>
            }
          </div>
        </div>
        <div className="row">
          <div className="form-row text-right col-md-12">
            {this.state.isLoading ?
              <button type="button" className="next-btn" disabled>Loading...</button> :
              <button type="button" className="next-btn" onClick={() => this.onClickNext()}>Next</button>
            }
          </div>
        </div>
      </div>
    );
  }
}
