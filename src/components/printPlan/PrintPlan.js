/* eslint-disable arrow-body-style */
/* eslint-disable react/no-array-index-key */
/* eslint-disable radix */
import React, { Component } from 'react';
import Loader from 'lib/loader';
import ViewMainDish from './ViewMainDish'
import ViewsSideDish from './ViewsSideDish'
import Login from '../login/Login';
// eslint-disable-next-line import/no-named-as-default
import DashHeader from '../dashboard/DashHeader';
import WeekCalenderFullArea from '../dashboard/WeekCalanderFullArea';
import { getSelectedWeekDates, getWeekDays, getTimeStamp, compareRecipeTime } from '../../lib/services';
import RoutingConstants from '../../lib/RoutingConstants'
// eslint-disable-next-line import/first
import { post } from 'lib/api'// import BottomBar from '../recipeList/BottomBar';
import history from '../../history'
// eslint-disable-next-line import/first
import _ from 'lodash'


class PrintPlan extends Component {
  constructor(props) {
    super(props)
    const weekStartEnd = getSelectedWeekDates('currentWeek');
    const dayDates = getWeekDays(weekStartEnd);
    const dataProps = this.props.location.state !== undefined ? this.props.location.state : {}
    const planType = this.props.location.state ? this.props.location.state.exist : 'yes';
    const slideClass = window.innerWidth < 768 ? dataProps !== undefined && dataProps && dataProps.slideClass === undefined ? 'sliderighttrue' : dataProps.slideClass : ''
    this.state = {
      dayDates,
      printPlanClass: 'active',
      reminder: false,
      inputs: {
        delete: [],
        update: [],
        add: [],
        deleted_cooking_ids: []
      },
      subscriptionPlan: [],
      planType,
      error: false,
      cookArr: [],
      recipeArr: [],
      cookingDate: [],
      servingArr: [],
      cookDayArr: [],
      lastStep: false,
      editRecipe: false,
      deleteRecipeIndex: [],
      bottomText: false,
      mbClass: 'mb-hide',
      showCook: false,
      calories: '0',
      recipeIdArr: [],
      groceryList: [],
      customItems: [],
      showFeedbackPopup: false,
      redirectDash: false,
      slideRepeat: false,
      slideClass
    };}

  componentDidMount() {
    const dataProps = this.props.location.state
    document.getElementsByTagName('body')[0].className = ''
    if (dataProps === undefined || (dataProps && dataProps.bottomBtnState === undefined) || sessionStorage.getItem('method') !== 'back') {
      let { dayDates } = this.state
      window.scrollTo(0, 0)
      dayDates = this.props.location.state && this.props.location.state.dayDates !== undefined ? this.props.location.state.dayDates : dayDates
      if(!dayDates){
        const weekStartEnd = getSelectedWeekDates('currentWeek');
        dayDates = getWeekDays(weekStartEnd);
      }      
      this.fetchPlan(dayDates)
      sessionStorage.removeItem('method')
    } else if (dataProps && sessionStorage.getItem('method') === 'back') {
      const slideClass = window.innerWidth < 768 ? 'dashslideleft' : ''
      this.useHistoryData(dataProps, slideClass)
    }}
    
  // eslint-disable-next-line react/sort-comp
  fetchPlan (dayDates) {
    const { servingArr, deleteRecipeIndex, cookDayArr, recipeArr, cookingDate } = this.state
    const { cookArr } = this.state
    const url = RoutingConstants.getWeeklySchedule;
    const { inputs } = this.state
    const startDate = `${dayDates[0].Year}-${dayDates[0].Month}-${dayDates[0].date}`;
    const endDate = `${dayDates[6].Year}-${dayDates[6].Month}-${dayDates[6].date}`;
    inputs.week_start_date = startDate;
    inputs.week_end_date = endDate;
    inputs.user_weekly_schedule_id = ''
    post(url, inputs).then(response => {const data = response.result.userWeeklyScheduleData
      if (response.result.userWeeklyScheduleId)
        inputs.user_weekly_schedule_id = response.result.userWeeklyScheduleId
      if (data && data.length > 0) {
        data.map((item) => {
          cookingDate.push({ cooking_date: item.cooking_date, id: item.id })
          item.cooking_date_recipe.map(items => {
            let sideHands = 0
            let sidePassive = 0
            let calories = 0
            let totalHands = items.recipe.hands_on_time || 0
            const recipePassive = items.recipe.passive_time || 0
            const totalRecipeTime = totalHands + recipePassive
            let totalSideTime = 0
            const recipeItm = {}
            recipeItm.recipe = items.recipe
            recipeItm.recipe.recipe_id = items.recipe.id
            recipeItm.recipe.id = items.id
            recipeItm.recipe.recipe_sides = []
            const calorieIndex = items.recipe.recipe_nutrition_data.findIndex(x => x.nutrition_info_attribute.attribute === 'Calories')
            if (calorieIndex > -1) {
              calories = parseInt(items.recipe.recipe_nutrition_data[calorieIndex].value, 0)
            }
            if (items.recipe_sides !== null) {
              recipeItm.recipe.recipe_sides.push({ recipe: items.recipe_sides })
            }
            if (items.recipe_sides !== null) {
              sideHands = items.recipe_sides.hands_on_time || 0
              sidePassive = items.recipe_sides.passive_time || 0
              totalHands += sideHands
              totalSideTime = sideHands + sidePassive
              const caloriesIndex = items.recipe_sides.recipe_nutrition_data.findIndex(x => x.nutrition_info_attribute.attribute === 'Calories')
              if (caloriesIndex > -1) {
                calories += parseInt(items.recipe_sides.recipe_nutrition_data[caloriesIndex].value, 0)
              }
            }

            let totalTime = compareRecipeTime(totalRecipeTime, totalSideTime);
            totalTime = compareRecipeTime(totalTime, totalHands);
            totalHands = getTimeStamp(totalHands)
            totalTime = getTimeStamp(totalTime)

            recipeItm.image = items.recipe.image
            recipeArr.push({
              cooking_date: item.cooking_date,
              servingSize: items.recipe_serving_size,
              items: recipeItm,
              totalHands,
              totalTime,
              calories
            })
            return recipeArr
          })
          return item
        })
      }

      this.setState({
        cookArr, recipeArr, cookingDate, lastStep: true, servingArr,
        deleteRecipeIndex, cookDayArr, inputs, dayDates, error: false,
        recipeIdArr: [], defaultServingSize: response.result.serving_size, loader: false
      })
    }).catch((err) => {
      this.catchError(err, dayDates)
    })

  }

  catchError(err, dayDates) {
    let defaultServingSize = 2
    if (err.error === 'token_expired') {
      localStorage.removeItem('token');
      history.push('/login', { location: this.state.location });
    }
    if (err.result && err.result.serving_size) {
      defaultServingSize = err.result.serving_size
    }
    this.setState({ error: true, dayDates, defaultServingSize, loader: false })
  }

  getWeekData(dayDates, selectedWeek) {
    const { inputs, slideRepeat } = this.state
    inputs.delete = []
    inputs.update = []
    inputs.add = []
    inputs.deleted_cooking_ids = []
    let slide;

    if (selectedWeek === 'nextWeek' && window.innerWidth < 768) {
      slide = `slideleft${slideRepeat}`
      // slide= 'slidetest'
    } else if (selectedWeek === 'previousWeek' && window.innerWidth < 768) {
      slide = `slideright${slideRepeat}`
      // slide= '`slideleft${slideRepeat}`'
    }
    this.setState({ recipeArr: [], cookingDate: [], inputs, loader: true, slideClass: slide, slideRepeat: !slideRepeat })
    this.fetchPlan(dayDates)
  }

  render() {
    const recipeData = this.state.recipeArr;
    // console.log(recipeData);

    const newArrSumBrk = {};
    const checkIdServingSums = recipeData.map((item)=>{
      if(!newArrSumBrk[item.items.recipe.recipe_id]){
        newArrSumBrk[item.items.recipe.recipe_id] = item.servingSize
      }else{
        newArrSumBrk[item.items.recipe.recipe_id] +=  item.servingSize
      }  
      return newArrSumBrk;
    })[0];

    // get the unique records
    const recipeDataNew = _.uniqBy(recipeData, (e) => e.items.recipe.recipe_id);

    // get the total serving size for the unique records to calculate ingridients
    const dataFilter = _.groupBy(recipeData, (e) => e.items.recipe.recipe_id);

    _.map(dataFilter, (e, i) => {
      dataFilter[i] = _.map(e, (f) => { return f.servingSize });
    });

    const receipeAppearedInAWeek = []
    _.map(dataFilter, (e, i) => { receipeAppearedInAWeek[i] = e.length; });


    const totalServingSizeArr = [];
    _.map(dataFilter, (a, b) => totalServingSizeArr.push({ id: b, getServingSize: _.sum(a) }));

    const showRecipeData = recipeDataNew.map((res, index) => {

      // DONT REMOVE THE BELOW CODE COMMENTED : IMPORTANT
      // match id to get the total serving
      // let getServing = res.servingSize;
      // _.map(totalServingSizeArr, (sv) => {
      //   // eslint-disable-next-line radix
      //   if (parseInt(sv.id) === parseInt(res.items.recipe.recipe_id)) {
      //     getServing = sv.getServingSize
      //   }
      // })
      // check for the appearence of the recipe appearing in week plan
      // let multipleTimes = 1;
      // _.map(receipeAppearedInAWeek, (times, timesIndex) => {
      //   if (parseInt(timesIndex) === parseInt(res.items.recipe.recipe_id)) {
      //     multipleTimes = times
      //   }
      // })

      // const totalServing = getServing * multipleTimes;

      const totalServing = _.find(checkIdServingSums,(ss,kk)=>{
        if(parseInt(kk,10)===parseInt(res.items.recipe.recipe_id,10)){
          return parseInt(ss,10);
        }
        return ''
      });

      const mainDish = <ViewMainDish
        // type={this.state.viewRecipeData.type}
        response={res.items.recipe}
        recipeImg={res.items.image}
        servingSize={totalServing}
        {...this.state}
        // eslint-disable-next-line react/no-array-index-key
        key={index}
      />

      let setsideDish = '';
      if (res.items.recipe.recipe_sides.length > 0) {
        setsideDish = res.items.recipe.recipe_sides.map((sideRes, sideIndex) => {
          return <ViewsSideDish
            response={sideRes}
            sid={sideRes.id}
            length={res.items.recipe.recipe_sides.length}
            sideDishValue={res.items.recipe.recipe_sides[0]}
            servingSize={totalServing}
            {... this.state}
            key={sideIndex}
          />
        });
      }
      const fullHtml = _.concat(mainDish, setsideDish);
      return <div key={index} className='break-before'>{fullHtml}</div>;
    })


    const {
      dayDates, reminder, subscriptionPlan, planType, editRecipe, printPlanClass, error,
      loader, slideClass
    } = this.state;
    if (!this.props.location.state && localStorage.type === 'checkUser') {
      localStorage.removeItem('token');
      return <Login location='/dashboard' />;
    }
    return (
      <div className={`wrapper printPlan ${slideClass}`} >
        <style>
          {`
          @media(min-width:320px) and (max-width:767px){
          #PureChatWidget.purechat.purechat-image-only.purechat-widget-collapsed.purechat-bottom-right .purechat-collapsed-image, #PureChatWidget.purechat.purechat-image-only.purechat-widget-collapsed.purechat-top-right .purechat-collapsed-image{
            left:5px !important; right: auto !important;
          }
          #PureChatWidget.purechat.purechat-widget-collapsed{
            left: 20px important;
            right: auto !important;
            top: 15px !important;
          }
        }
          `}
        </style>
        {
          loader &&
                    <div className="loading">
                      <Loader />
                      <div className="modal-backdrop fade in" />
                    </div>
        }
        <div className="mb-hide">
          <DashHeader
            dashClass={this.state.dashClass}
            groceryClass={this.state.groceryClass}
            printPlanClass={this.state.printPlanClass}
            dayDates={dayDates}
            subscriptionPlan={subscriptionPlan}
            previous={this.state.previous}
            planExist={planType}
            reminder={reminder}
          />
        </div>
        <section className="printPlanOuterContainer">
          <div className="printPlanInnerContainer">
            <div className="container">

              <div className="cal-days-cntnt dishes-row dashboard-page no-print">
                <WeekCalenderFullArea
                  dayDates={dayDates}
                  getWeekData={(dayDate, selectedWeek) => this.getWeekData(dayDate, selectedWeek)}
                  editRecipe={() => this.editRecipe()}
                  editRecipeButton={editRecipe}
                  onPressCancel={() => this.onPressCancel()}
                  error={error}
                  printPlanClass={printPlanClass}
                />
              </div>
              {showRecipeData}            
            </div>
          </div>
        </section>

      </div>
    );
  }
}
export default PrintPlan;
