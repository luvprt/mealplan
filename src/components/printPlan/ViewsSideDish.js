import React, { Component } from 'react';
import ReactHtmlParser from 'react-html-parser';
import { getTimeStamp, totalValue, getFormatTime } from 'lib/services';
import { fraction } from 'mathjs';
import ReactResizeDetector from 'react-resize-detector';

export default class ViewsSideDish extends Component {
  constructor(props) {
    super(props);
    if (props.sideDishValue && props.sideDishValue.recipe) {
      this.state = {
        handsOnTime: props.sideDishValue.recipe.hands_on_time,
        passiveTime: props.sideDishValue.recipe.passive_time,
        recipeIngredients: props.sideDishValue.recipe.recipe_ingredients,
        servingSize: props.servingSize,
        sideImg: props.sideDishValue.recipe.image,
        recipeInstruction: props.sideDishValue.recipe.recipe_instructions,
        sid: props.sideDishValue.recipe.id,
        sideDishArray: props.sideDishArray,
        dataValue: '',
        showInstructions: true,
        showIngredients: true,
        sideDishValue: props.sideDishValue,
        ...this.props,
        sideDishName: props.sideDishValue.recipe.name
      };
    }
  }

  onResize() {
    let { activeAccordio, showIngredients, showInstructions } = this.state;
    const { dataValue } = this.state
    if (window.innerWidth < 768) {
      activeAccordio = true
      if (dataValue === '' || dataValue === 'nutrition') {
        showIngredients = false;
        showInstructions = false;
      }
    } else {
      activeAccordio = false;
      showIngredients = true;
      showInstructions = true;
    }
    this.setState({ activeAccordio, showIngredients, showInstructions });
  }

  render() {
    const {
      passiveTime, recipeIngredients, recipeInstruction, activeAccordio,
      showIngredients, showInstructions, sid, sideDishArray, servingSize, sideDishValue,
      clickIngredientAccordion, clickInstructionAccordion,sideDishName
    } = this.state;
    let { handsOnTime, sideImg } = this.state;
    let totaltime = getTimeStamp(handsOnTime + passiveTime);
    if (sideImg && sideImg !== null) {
      sideImg = sideImg.split('mpn-recipe-images/')[0]
      sideImg += `mpn-recipe-images/Images/${sideDishValue.recipe.id}-EH720.jpg`
    }
    let check = false;
    let instructions = false;
    let size = totalValue(sid, sideDishArray, 0);
    size *= servingSize;
    if (recipeIngredients) {
      check = true;
    }
    if (recipeInstruction) {
      instructions = true;
    }
    if (size === 0) {
      size = servingSize
    }
    totaltime = getFormatTime(totaltime);
    handsOnTime = getTimeStamp(handsOnTime);
    handsOnTime = getFormatTime(handsOnTime);
    return (
      <div className="printPlanSide">
        <ReactResizeDetector handleWidth handleHeight onResize={() => this.onResize()} />
        <div className="printPlanSideInner">
          <div className="printPlanSideSpan">Side Dish </div>
          <h2 className="printPlanSideTitle">{sideDishName}</h2>
          <div className="printPlanSideTime">
            <span>
              Hands On:
              <b> {handsOnTime}</b>
            </span>
            <span>
              Total Time:
              <b> {totaltime}</b>
            </span>
          </div>

          <div className="printPlanSideIngredients">
            <h4>
              Ingredients
              {activeAccordio ?
                !clickIngredientAccordion ?
                  <svg
                    className="svIcon"
                    enableBackground="new 0 0 48 48"
                    height="10px"
                    id="Layer_3"
                    version="1.1"
                    viewBox="0 0 48 48"
                    width="10px"
                    xmlSpace="preserve"
                    xmlns="http://www.w3.org/2000/svg"
                    xmlnsXlink="http://www.w3.org/1999/xlink"
                  >
                    <polygon fill="#241F20" points="0,12.438 48,12.438 24,35.562 " />
                  </svg>
                  :
                  <svg
                    className="svIcon"
                    enableBackground="new 0 0 32 32"
                    height="13px"
                    id="svg2"
                    version="1.1"
                    viewBox="0 0 32 32"
                    width="13px"
                    xmlSpace="preserve"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <g id="backbord"><rect fill="none" height="32" width="32" /></g>
                    <g id="arrow_x5F_up">
                      <polygon points="30,22 16.001,8 2.001,22  " />
                    </g>
                  </svg>
                :
                null
              }
            </h4>
            <ul className={`showlist${showIngredients}`}>
              {check &&
                recipeIngredients.map((value) => {
                  let ingredientName = '';
                  let quantity = value.quantity;
                  quantity *= (size / 4);
                  let a = 0;

                  quantity = quantity.toString().split('.');
                  quantity[0] = parseInt(quantity[0], 0);

                  if (quantity[0] === 0) {
                    quantity[0] = '';
                  }
                  if (quantity.length > 1 && quantity[1] !== '00') {
                    quantity[1] = `${0}.${quantity[1]}`;
                    a = fraction(parseFloat(quantity[1]));
                    if (a.d > 0) {
                      a.d = Math.round(a.d / a.n);
                      a.n = 1;
                    }
                  }
                  if (a !== 0 && a.d > 1) {
                    a = ReactHtmlParser(`${quantity[0]}<sup>${a.n}</sup>&frasl;<sub>${a.d}</sub>`);
                  } else if (a !== 0) {
                    a = a.n;
                  } else {
                    a = quantity[0];
                  }
                  if (value.ingredient) {
                    ingredientName = value.ingredient.name;
                  }
                  return (
                    
                    <li key={value.id}>
                      {a} {value.unit} {ingredientName}
                    </li>
                   
                  );
                })
              }
            </ul>
          </div>
          {instructions &&
            <div className="printPlanSideDirections">
              <h4 className="printPlanSideInstructions">
                Directions {activeAccordio ?
                  !clickInstructionAccordion ?
                    <svg
                      className="svIcon"
                      enableBackground="new 0 0 48 48"
                      height="10px"
                      id="Layer_3"
                      version="1.1"
                      viewBox="0 0 48 48"
                      width="10px"
                      xmlSpace="preserve"
                      xmlns="http://www.w3.org/2000/svg"
                      xmlnsXlink="http://www.w3.org/1999/xlink"
                    >
                      <polygon fill="#241F20" points="0,12.438 48,12.438 24,35.562 " />
                    </svg>
                    :
                    <svg
                      className="svIcon"
                      enableBackground="new 0 0 32 32"
                      height="13px"
                      id="svg2"
                      version="1.1"
                      viewBox="0 0 32 32"
                      width="13px"
                      xmlSpace="preserve"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <g id="backbord"><rect fill="none" height="32" width="32" /></g>
                      <g id="arrow_x5F_up">
                        <polygon points="30,22 16.001,8 2.001,22  " />
                      </g>
                    </svg>
                  :
                  null
                }
              </h4>
              <ol className={`instructions showlist${showInstructions}`}>
                {
                  recipeInstruction.map(value => (
                    <li key={value.id}>
                      {ReactHtmlParser(value.instruction)}
                    </li>
                  ))
                }
              </ol>
            </div>
          }
        </div>
      </div>
    );

  }
}
