/* eslint-disable import/first */
import React, { Component } from 'react';
import ReactHtmlParser from 'react-html-parser';
import { fraction } from 'mathjs';
import { getTimeStamp, getFormatTime } from '../../lib/services';
import ReactResizeDetector from 'react-resize-detector';

export default class ViewMainDish extends Component {
  constructor(props) {
    super(props);
    if (props.response) {
      const { name } = props.response;
      this.state = {
        name,
        handsOnTime: props.response.hands_on_time,
        passiveTime: props.response.passive_time,
        recipeIngredients: props.response.recipe_ingredients,
        recipeImg: props.response.image,
        response: props.response,
        showIngredients: true,
        showInstructions: true,
        dataValue: '',
        servingSize: props.servingSize,
        recipeInstruction: props.response.recipe_instructions,
      };
    }
  }

  onSave() {
    const { enableSwapButton } = this.state;
    if (enableSwapButton) {
      this.props.onSave();
    } else {
      this.props.swapSave();
    }
  }

  onResize() {
    let { activeAccordio, showIngredients, showInstructions } = this.state;
    const { dataValue } = this.state
    if (window.innerWidth < 768) {
      activeAccordio = true;
      if (dataValue === '') {
        showIngredients = false;
        showInstructions = false;
      }
    } else {
      activeAccordio = false;
      showIngredients = true;
      showInstructions = true;
    }
    this.setState({ activeAccordio, showIngredients, showInstructions });
  }

  render() {
   
    const {
      name, passiveTime, recipeIngredients, recipeInstruction, showInstructions, showIngredients,
      activeAccordio, response, clickIngredientAccordion, clickInstructionAccordion, servingSize
    } = this.state;
    let { handsOnTime, recipeImg } = this.state;
    let totaltime = getTimeStamp(handsOnTime + passiveTime);
  
    totaltime = getFormatTime(totaltime);
    handsOnTime = getTimeStamp(handsOnTime);
    handsOnTime = getFormatTime(handsOnTime);
    if (recipeImg && recipeImg !== null) {
      recipeImg = recipeImg.split('mpn-recipe-images/')[0]
      const rId = response.recipe_id ? response.recipe_id : response.id
      recipeImg += `mpn-recipe-images/Images/${rId}-EH720.jpg`
    }
    let check = false;
    let instructions = false;
    if (recipeIngredients) {
      check = true;
    }
    if (recipeInstruction) {
      instructions = true;
    }
    return (
      <div className="printPlanMain">
        <ReactResizeDetector handleWidth handleHeight onResize={() => this.onResize()} />
        <div className="printPlanMainInner">
          <div className="printPlanSpan">Main Dish </div>
          <h2 className="printPlanTitle">{name}</h2>
          <div className="printPlanTime">
            <span>
              Hands On:
              <b> {handsOnTime}</b>
            </span>
            <span>
              Total Time:
              <b> {totaltime}</b>
            </span>
          </div>
          <div className="printPlanIngredients">
            <h4>
              Ingredients
              {activeAccordio ?
                !clickIngredientAccordion ?
                  <svg
                    className="svIcon"
                    enableBackground="new 0 0 48 48"
                    height="10px"
                    id="Layer_3"
                    version="1.1"
                    viewBox="0 0 48 48"
                    width="10px"
                    xmlSpace="preserve"
                    xmlns="http://www.w3.org/2000/svg"
                    xmlnsXlink="http://www.w3.org/1999/xlink"
                  >
                    <polygon fill="#241F20" points="0,12.438 48,12.438 24,35.562 " />
                  </svg>
                  :
                  <svg
                    className="svIcon"
                    enableBackground="new 0 0 32 32"
                    height="13px"
                    id="svg2"
                    version="1.1"
                    viewBox="0 0 32 32"
                    width="13px"
                    xmlSpace="preserve"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <g id="backbord"><rect fill="none" height="32" width="32" /></g>
                    <g id="arrow_x5F_up">
                      <polygon points="30,22 16.001,8 2.001,22  " />
                    </g>
                  </svg>
                :
                null
              }
            </h4>
            <ul className={`showlist${showIngredients}`}>
              {check &&
                recipeIngredients.map((value) => {
                  let quantity = parseFloat(value.quantity);
                  quantity *= (servingSize / 4);
                  let a = 0;
                  quantity = quantity.toString().split('.');
                  quantity[0] = parseInt(quantity[0], 0);
                  if (quantity[0] === 0) {
                    quantity[0] = '';
                  }
                  if (quantity.length > 1 && quantity[1] !== '00') {
                    quantity[1] = `${0}.${quantity[1]}`;
                    a = fraction(parseFloat(quantity[1]));
                    if (a.d > 0) {
                      a.d = Math.round(a.d / a.n);
                      a.n = 1;
                    }
                  }
                  if (a !== 0 && a.d > 1) {
                    a = ReactHtmlParser(`${quantity[0]}<sup>${a.n}</sup>&frasl;<sub>${a.d}</sub>`);
                  } else if (a !== 0) {
                    a = a.n;
                  } else {
                    a = quantity[0];
                  }
                  return (                    
                    <li key={value.id}>
                      {a} {value.unit} {value.ingredient.name}
                    </li>
                  );
                })
              }
            </ul>
          </div>
          {instructions &&
            <div className="printPlanDirections">
              <h4 className="printPlanInstructions">
                Directions {activeAccordio ?
                  !clickInstructionAccordion ?
                    <svg
                      className="svIcon"
                      enableBackground="new 0 0 48 48"
                      height="10px"
                      id="Layer_3"
                      version="1.1"
                      viewBox="0 0 48 48"
                      width="10px"
                      xmlSpace="preserve"
                      xmlns="http://www.w3.org/2000/svg"
                      xmlnsXlink="http://www.w3.org/1999/xlink"
                    >
                      <polygon fill="#241F20" points="0,12.438 48,12.438 24,35.562 " />
                    </svg>
                    :
                    <svg
                      className="svIcon"
                      enableBackground="new 0 0 32 32"
                      height="13px"
                      id="svg2"
                      version="1.1"
                      viewBox="0 0 32 32"
                      width="13px"
                      xmlSpace="preserve"
                      xmlns="http://www.w3.org/2000/svg"
                    >
                      <g id="backbord"><rect fill="none" height="32" width="32" /></g>
                      <g id="arrow_x5F_up">
                        <polygon points="30,22 16.001,8 2.001,22  " />
                      </g>
                    </svg>
                  :
                  null
                }
              </h4>
              <ol className={`instructions showlist${showInstructions}`}>
                {
                  recipeInstruction.map(value => (
                    <li key={value.id}>
                      {ReactHtmlParser(value.instruction)}
                    </li>
                  ))
                }
              </ol>
            </div>
          }
        </div>
      </div>
    );
  }
}
