/*
Component Name: WeekCalanderFullArea component
Contains : RecipeForWeekCalander, BuildPlanScreen
Includes : Services for custom calander
Default screen: Dashboard
*/
import React, { Component } from 'react';
import { getSelectedWeekDates, getWeekDays } from 'lib/services';
import moment from 'moment';

class WeekCalanderFullArea extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentWeek: true,
      weekStartEnd: {},
      dayDates: props.dayDates || {},
      alertMessage: false,
      previousWeekEvents: 'auto',
      editButton: true,
      startDate: false,
      week: 'current',
      error: this.props.error,
      ...props
    };
  }

  componentDidMount() {
    this.weekData();
  }

  // unsafe
  // eslint-disable-next-line
  UNSAFE_componentWillReceiveProps(props) {
    let {
      editButton, displayCooking, startDate, week, alertMessage, previousWeekEvents,
    } = this.state;
    const { currentWeek, selectedWeek, groceryClass } = this.state;
    if (props.error === true) {
      editButton = false;
      displayCooking = false;
      week = '';
      if (selectedWeek === 'previousWeek' && startDate === false) {
        startDate = false;
        week = 'previous';
        alertMessage = true;
      } else if (selectedWeek === 'nextWeek') {
        week = 'next';
        startDate = true;
        alertMessage = false;
      }
      if (currentWeek.length > 0) {
        if (currentWeek[0].date === props.dayDates[0].date) {
          editButton = false;
          startDate = false;
          week = 'current';
          alertMessage = false;
          previousWeekEvents = 'auto';
        }
      } else if (currentWeek === true) {
        editButton = false;
        alertMessage = false;
      } 
    } else if (props.error === false) {
      displayCooking = true;
      if (selectedWeek === 'previousWeek' && startDate === false) {
        editButton = false;
        week = 'previous';
        alertMessage = true;
        previousWeekEvents = 'none';
      } else if (selectedWeek === 'nextWeek' && startDate === false) {
        editButton = true;
        startDate = true;
        week = 'next';
        alertMessage = false;
        previousWeekEvents = 'auto';
      } else {
        editButton = true;
      }
      if (currentWeek.length > 0) {
        if (currentWeek[0].date === props.dayDates[0].date) {
          editButton = true;
          startDate = false;
          week = 'current';
          alertMessage = false;
          previousWeekEvents = 'auto';
        } else if (currentWeek === true) {
          editButton = true;
          alertMessage = false;
        }
      }
    }
    if (groceryClass === 'active') {
      alertMessage = false;
      editButton = false;
    }
    const weekdate = parseInt(props.dayDates[0].date, 0)
    const currentDate = parseInt(moment().startOf('week').format('DD'),0)
    const currentMonth = parseInt(moment().startOf('week').format('MM'),0)
    const weekMonth = parseInt(props.dayDates[0].Month,0)
    const weekYear = parseInt(props.dayDates[0].Year,0)
    const currentYear = parseInt(moment().startOf('week').format('YYYY'),0)

    if(currentMonth > weekMonth && currentYear === weekYear) {
      if(weekdate !== currentDate && groceryClass !== 'active') {
        alertMessage = true
        editButton = false
      } else{
        alertMessage = false
        if(groceryClass !== 'active') {
          editButton = true
        }
        previousWeekEvents = 'auto'
      }
    } else if(currentMonth === weekMonth) {
      if(weekdate < currentDate && groceryClass !== 'active') {
        alertMessage = true
        editButton = false
        previousWeekEvents = 'none'
      } else if(weekdate < currentDate && groceryClass === 'active') {
        previousWeekEvents = 'none'
      } else {
        alertMessage = false
        previousWeekEvents = 'auto'
        if(groceryClass !== 'active') {
          editButton = true
        }
      }
    } else {
      alertMessage = false
    }
    const startWeekDate = `${props.dayDates[0].fullMonth} ${props.dayDates[0].date}`;
    const endDate = `${props.dayDates[6].fullMonth} ${props.dayDates[6].date}`;
    this.setState({
      editButton,
      displayCooking,
      startDate,
      week,
      alertMessage,
      previousWeekEvents,
      currentWeek,
      dayDates: props.dayDates || this.state.weekDates,
      startWeekDate,
      endDate,
      ...props
    });
  }

  weekData() {
    let weekStartEnd = {};
    let dayDates = {};
    let startWeekDate;
    let endDate;
    if (this.state.currentWeek === true) {
      weekStartEnd = getSelectedWeekDates('currentWeek');
      dayDates = this.props.dayDates || getWeekDays(weekStartEnd);
      startWeekDate = `${dayDates[0].fullMonth} ${dayDates[0].date}`;
      endDate = `${dayDates[6].fullMonth} ${dayDates[6].date}`;
      this.setState({
        weekStartEnd,
        dayDates: this.props.dayDates || dayDates,
        weekDates: dayDates,
        startWeekDate,
        endDate,
      });
    }
  }

  /* condition based selection and view for weekly plan calander */
  async loadWeek(selectedWeek, weekStartEndPoints) {
    window.scroll(0,0)
    const {reminder, groceryClass, editRecipeButton} = this.state;
    if (!reminder && !editRecipeButton) {
      let { dayDates } = this.state;
      const currentWeekEnd = getSelectedWeekDates('currentWeek');
      const currentWeek = getWeekDays(currentWeekEnd);
      let switchType = '';
      const { startDate } = this.state;
      weekStartEndPoints.weekStart = `${dayDates[0].Year}-${dayDates[0].Month}-${dayDates[0].date}`
      weekStartEndPoints.weekEnd = `${dayDates[6].Year}-${dayDates[6].Month}-${dayDates[6].date}`
      const weekStartEnd = getSelectedWeekDates(selectedWeek, weekStartEndPoints);
     
      dayDates = await getWeekDays(weekStartEnd);
      // console.log(weekStartEnd, weekStartEndPoints)

      this.setState({
        weekStartEnd,
        dayDates,
        selectedWeek,
      });
      if (selectedWeek === 'previousWeek' && startDate === false) {
        this.setState({
          previousWeekEvents: 'none',
          editButton: false,
          currentWeek: false,
          week: 'previous',
        });
        switchType = 'Backward';
      } else if (selectedWeek === 'nextWeek') {
        this.setState({
          alertMessage: false,
          previousWeekEvents: 'auto',
          startDate: true,
          editButton: true,
          currentWeek,
          week: 'next',
        });
    
        switchType = 'Forward';
      }
      if (currentWeek[0].date === dayDates[0].date) {
        this.setState({
          startDate: false,
          editButton: true,
          currentWeek,
          previousWeekEvents: 'auto',
          week: 'current',
        });
    
      }
      let path;
      // track week type
      if (groceryClass !== 'active') {
        path = 'Dashboard';
      } else {
        path = 'Grocery List:';
      }

      window.analytics.track(`${path} Switch Weeks ${switchType}`, {
        startDate: `${dayDates[0].fullDay} ${dayDates[0].date}-${dayDates[0].Month}-${dayDates[0].Year}`,
        endDate: `${dayDates[6].fullDay} ${dayDates[6].date}-${dayDates[6].Month}-${dayDates[6].Year}`,
      });
      this.props.getWeekData(dayDates, selectedWeek);
    }
  }

  editRecipe(value) {
    const { dayDates } = this.state
    const action = value === 'Edit' ? 'Edit Mode Activated' : 'Changes Cancelled' ;
    window.analytics.track(action, {
      startDate: `${dayDates[0].fullDay} ${dayDates[0].date}-${dayDates[0].Month}-${dayDates[0].Year}`,
      endDate: `${dayDates[6].fullDay} ${dayDates[6].date}-${dayDates[6].Month}-${dayDates[6].Year}`
    });
    if(value === 'Edit') {
      this.props.editRecipe()
    } else{
      this.props.onPressCancel()
    }
  }

  render() {
    const { 
      weekStartEnd, endDate, startWeekDate, editRecipeButton, error, 
      dayDates, previousWeekEvents, groceryClass, printPlanClass
    } = this.state;
    const topHeading = groceryClass !== 'active' ?  printPlanClass  === 'active' ? 'Print Plan' : 'Weekly Plan' : 'Grocery List'
    const editText = !editRecipeButton? 'Edit' : 'Cancel'
    const startWeekMobileDate = `${dayDates[0].halfMonth} ${dayDates[0].date}`
    const endWeekMobileDate = `${dayDates[6].halfMonth} ${dayDates[6].date}`
    return (
      <div className="dash-cntnt-dtail-hdr cook-day-top no-print">
        <div className="mobile-view-small mob-dash">
          <h5> {topHeading} </h5>
          <div className="calndr-cntrls one-line">
            <a onClick={()=>this.loadWeek('previousWeek', weekStartEnd)} style={{pointerEvents: previousWeekEvents}}>
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
                <path d="M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 
                9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z" />
              </svg>
            </a>
            <h3 className="mb-hide">{startWeekDate} - {endDate}</h3>
            <h3 className="mb-only">{startWeekMobileDate} - {endWeekMobileDate}</h3>
            <a onClick={()=>this.loadWeek('nextWeek', weekStartEnd)}>
              <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
                <path d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z" />
              </svg>
            </a>
          </div>
          {this.state.alertMessage ?
            <div className="alert-msg">
              <span>
                Heads Up!
              </span>
              <p>This is a previous week’s plan.</p>
            </div> :
            <div className="edit-btn-week">
              {!editRecipeButton && !error && groceryClass !== 'active' && printPlanClass !== 'active' &&
          <button 
            className="btn edit-schedule-btn outer-border mb-hide" 
            data-toggle="modal" 
            data-target="#schedule-popup"
            onClick={()=>this.editRecipe('Edit')}
          >
            Edit Plan
          </button>
              }
            </div>
          }
        </div>
        {!error && groceryClass !== 'active' && !this.state.alertMessage &&
          <span className="only-mobile-view mb-only" onClick={()=>this.editRecipe(editText)}> 
            {editText}
          </span>
        }
        
        {groceryClass === 'active' && !error && 
          <div className="grocery-btn no-print" id="ty">
            <button
              className="print-btn"
              onClick={() => window.print()}
            >
              Print List
            </button>
          </div>
        }
      </div>
    );
  }
}
export default WeekCalanderFullArea;
