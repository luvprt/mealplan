import React, { Component } from 'react';
import ReactHtmlParser from 'react-html-parser';
import moment from 'moment';
import { getTimeStamp, totalValue, getFormatTime } from 'lib/services';
import LazyLoad from 'react-image-lazy-load';
import { fraction } from 'mathjs';
import ReactResizeDetector from 'react-resize-detector';

export default class ViewsSideDish extends Component {
  constructor(props) {
    super(props);
    this.state = {
      slide: "",
      left: true,
      dataValue: '',
      showInstructions: true,
      showIngredients: true,
      sideDishValue: props.sideDishValue,
      recipeNutrition:{},
      message: false,
      showNutrition: false, 
      ...this.props,
    };
  }

  // unsafe
  // eslint-disable-next-line
    UNSAFE_componentWillReceiveProps(props) {
    if (props.sideDishValue && props.sideDishValue.recipe) {
      this.setState({
        name: props.sideDishValue.recipe.name,
        handsOnTime: props.sideDishValue.recipe.hands_on_time,
        passiveTime: props.sideDishValue.recipe.passive_time,
        recipeIngredients: props.sideDishValue.recipe.recipe_ingredients,
        servingSize: props.servingSize,
        sideDishValue: props.sideDishValue ,
        sideImg: props.sideDishValue.recipe.image,
        mealDate: props.fDate,
        sideNutritionName: props.sideNutritionName,
        nutritionName: props.nutritionName,
        recipeNutrition: props.sideDishValue.recipe_nutrition_data,
        saveBtn: props.saveBtn,
      });
      if (props.sideDishValue.recipe.recipe_instructions) {
        this.setState({
          recipeInstruction: props.sideDishValue.recipe.recipe_instructions,
          sid: props.sideDishValue.recipe.id,
          sideDishArray: props.sideDishArray,
        });
      }
    }
  }

  

  onResize() {
    let { activeAccordio, showIngredients, showInstructions } = this.state;
    const { dataValue } = this.state
    if (window.innerWidth < 768 ) {
      activeAccordio = true
      if(dataValue === '' || dataValue === 'nutrition') {
        showIngredients = false;
        showInstructions = false;
      }
    } else {
      activeAccordio = false;
      showIngredients = true;
      showInstructions = true;
    }
    this.setState({ activeAccordio, showIngredients, showInstructions });
  }

  save() {
    this.setState ({ message: true });
    this.props.save();
    setTimeout(() =>  {
      this.setState({message: false});
    }, 2000)
  }

  clickAccordion(value){
    let { showIngredients, showInstructions, showNutrition, clickIngredientAccordion, clickInstructionAccordion, clickNutritionAccordion } = this.state;
    const { activeAccordio } = this.state
    if(activeAccordio){
      if(value === 'ingredients') {
        showIngredients = !showIngredients
        clickIngredientAccordion = !clickIngredientAccordion
      } else if(value === 'instructions') {
        showInstructions = !showInstructions
        clickInstructionAccordion = !clickInstructionAccordion
      } else if(value === 'nutrition') {
        showNutrition = !showNutrition
        clickNutritionAccordion = !clickNutritionAccordion
      }
    }
    else {
      showIngredients = true
      showInstructions = true
      if(value === 'nutrition') {
        showNutrition = !showNutrition
        clickNutritionAccordion = !clickNutritionAccordion
      }
    }
    this.setState({ 
    // clickAccordion: !clickAccordion,
      showNutrition,
      showIngredients,
      showInstructions,
      clickIngredientAccordion,
      clickInstructionAccordion ,
      clickNutritionAccordion ,
      dataValue: value
    });
  }


  saveAnalytics() {
    const { recipeNutrition, name, mealDate, type } = this.state
    window.analytics.track('Click Recipe Nutrition in Side Dish Tab', {
      sideRecipeName: name,
      link: recipeNutrition,
      slot: type,
      date: moment(mealDate).format('dddd DD-MM-YYYY'),
    });
  }
  changeSide() {
    const { left } = this.state;
    this.setState({ slide:`slideleft${left}`, left: !left})
    this.props.changeSide();
  }

  // eslint-disable-next-line
  nutritionData() {
    const { sideNutritionName, nutritionName, showNutrition, recipeNutrition} = this.state
    return(
      <div className={`showlist${showNutrition}`}>
        <div className="popup-main-icn-box-rght-inner-box2 mb-only">
          <div className="meal-sumry-icn-inner">
            <b>{sideNutritionName.Calories}{nutritionName.unitCalories}</b>
            <span>Calories</span>
          </div>
          <div className="meal-sumry-icn-inner">
            <b>{sideNutritionName.Protein}{nutritionName.unitProtein}</b>
            <span>Protein</span>
          </div>
          <div className="meal-sumry-icn-inner">
            <b>{sideNutritionName['Net Carbohydrates']}{nutritionName['unitNet Carbohydrates']}</b>
            <span>Net Carbs</span>
          </div>
          <div className="meal-sumry-icn-inner">
            <b>{sideNutritionName['Total Fat']}{nutritionName['unitTotal Fat']}</b>
            <span>Total Fat</span>
          </div>
        </div>
        <div className="dtail-nutri-info-cntnt">
          <ul>
            {
              recipeNutrition.length >0 &&
              recipeNutrition.map((value, index) => {
                if (value.nutrition_info_attribute.attribute === 'Trans Fat' || 
                    value.nutrition_info_attribute.attribute === 'Saturated Fat'||
                    value.nutrition_info_attribute.attribute === 'Sugars' ||
                    value.nutrition_info_attribute.attribute === 'Dietary Fiber'
                ) {
                  return(
                    // eslint-disable-next-line
                    <ul className="inner-child" key={`${value.value}${index}`}>
                      <li key={value.nutrition_info_attribute.attribute}>
                        {value.nutrition_info_attribute.attribute}
                        <span className="pull-right">
                          {value.value}&nbsp;
                          {value.nutrition_info_attribute.measurement_unit}
                        </span>
                      </li>
                    </ul>
                  )
                }
                if(index > 0){
                  return(
                    <li key={value.nutrition_info_attribute.attribute}>
                      {value.nutrition_info_attribute.attribute}
                      <span className="pull-right">
                        {value.value}&nbsp;
                        {value.nutrition_info_attribute.measurement_unit}
                      </span>
                    </li>
                  ) }
                return <li key={value.value} />
              })
            }
            <li className="note-txt">*Nutritional estimates based on a standard of 1 serving.</li>
          </ul>  
        </div>
      </div>
    )
  }

  

  render() {
    const {
      passiveTime, recipeIngredients, recipeInstruction, activeAccordio,
      showIngredients, showInstructions, sideNutritionName, nutritionName,
      sid, sideDishArray, servingSize, slide, sideDishValue, 
      clickIngredientAccordion, clickInstructionAccordion, recipeNutrition,
      clickNutritionAccordion, saveBtn, message
    } = this.state;
    let { handsOnTime, sideImg } = this.state;
    const { sname } = this.state.viewRecipeData
    let totaltime = getTimeStamp(handsOnTime + passiveTime);
    const carouselList = sideDishValue && sideDishValue.length > 1;
    const setImage = sideImg
    if(sideImg && sideImg !== null) {
      sideImg = sideImg.split('mpn-recipe-images/')[0]
      const checkImage = setImage.includes('mpn-recipe-images/')
      if(checkImage){
        sideImg += `mpn-recipe-images/Images/${sideDishValue.recipe.id}-EH720.jpg`
      }else{
        sideImg = setImage
      }
    }
    let check = false;
    let instructions = false;
    let size = totalValue(sid, sideDishArray, 0);
    size *= servingSize;
    if (recipeIngredients) {
      check = true;
    }
    if (recipeInstruction) {
      instructions = true;
    }
    if(size === 0) {
      size = servingSize
    }
    totaltime = getFormatTime(totaltime);
    handsOnTime = getTimeStamp(handsOnTime);
    handsOnTime = getFormatTime(handsOnTime);
    return (
      <div id="side-dish" className={slide}>
        <ReactResizeDetector handleWidth handleHeight onResize={() => this.onResize()} />
        <div className="tab-lft-cntnt">
          <div>
            <LazyLoad
              loaderImage
              originalSrc={sideImg}
              imageProps={{
                src: '/images/small.png',
              }}
              className="img-side1"
            />
          </div>
          <div className="icn-box-iner mb-hide">
            {recipeNutrition && recipeNutrition.length > 0 &&
              <div className="row">
                <h4> Nutritional facts</h4>
                <div className="popup-main-icn-box-rght-inner-box2 mb-hide">
                  <div className="meal-sumry-icn-inner">
                    <b>{sideNutritionName.Calories}{nutritionName.unitCalories}</b>
                    <span>Calories</span>
                  </div>
                  <div className="meal-sumry-icn-inner">
                    <b>{sideNutritionName.Protein}{nutritionName.unitProtein}</b>
                    <span>Protein</span>
                  </div>
                  <div className="meal-sumry-icn-inner">
                    <b>{sideNutritionName['Net Carbohydrates']}{nutritionName['unitNet Carbohydrates']}</b>
                    <span>Net Carbs</span>
                  </div>
                  <div className="meal-sumry-icn-inner">
                    <b>{sideNutritionName['Total Fat']}{nutritionName['unitTotal Fat']}</b>
                    <span>Total Fat</span>
                  </div>
                </div>
              </div>
            }
          </div>
          <div className="custm-dropdwn-side mb-hide">
            {recipeNutrition && recipeNutrition.length > 0 &&
              <h4 onClick={()=> this.clickAccordion('nutrition')}>
                Detailed Nutrition Info 
                {!clickNutritionAccordion ?
                  <svg
                    className="svIcon"
                    enableBackground="new 0 0 48 48"
                    height="10px"
                    id="Layer_3"
                    version="1.1"
                    viewBox="0 0 48 48"
                    width="10px"
                    xmlSpace="preserve"
                    xmlns="http://www.w3.org/2000/svg"
                    xmlnsXlink="http://www.w3.org/1999/xlink"
                  >
                    <polygon fill="#241F20" points="0,12.438 48,12.438 24,35.562 " />
                  </svg>
                  :
                  <svg
                    className="svIcon"
                    enableBackground="new 0 0 32 32"
                    height="13px"
                    id="svg2"
                    version="1.1"
                    viewBox="0 0 32 32"
                    width="13px"
                    xmlSpace="preserve"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <g id="backbord"><rect fill="none" height="32" width="32" /></g>
                    <g id="arrow_x5F_up">
                      <polygon points="30,22 16.001,8 2.001,22  " />
                    </g>
                  </svg>
                }
              </h4>}
            { recipeNutrition &&
              this.nutritionData()
            }
            
          </div>
        </div><div className="tab-right-cntnt">
          <div className="meal-summary">Side Dish </div>
          <h2>{sname}</h2>
          <div className="after-hdng-cntnt">
            <span>
              Hands On:
              <b> {handsOnTime}</b>
            </span>
            <span>
              Total Time:
              <b> {totaltime}</b>
            </span>
          </div>
          {carouselList &&
            <div className="change-dish-box mb-hide">
              <button className="swap-meal-btn" onClick={() => this.changeSide()}>
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  xmlnsXlink="http://www.w3.org/1999/xlink"
                  version="1.1"
                  x="0px"
                  y="0px"
                  viewBox="17 -18 100 100"
                  enableBackground="new 17 -18 100 100"
                  xmlSpace="preserve"
                >
                  <path d="M48.7,10.7C55,10.7,60.3,5.5,60.3-1s-5.2-11.7-11.7-11.7S37-7.5,37-1S42.2,10.7,48.7,10.7z M85.4,76.7  c6.5,0,11.7-5.2,11.7-11.7s-5.2-11.7-11.7-11.7c-6.3,0-11.7,5.2-11.7,11.7S78.9,76.7,85.4,76.7z M100.3,52L117,35.3H83.7L100.3,52z   M17,28.7h33.3L33.7,12L17,28.7z M67,72V58.7c-14.5,0-26.7-12.2-26.7-26.7v-7.3H27V32C27,53.3,45.7,72,67,72z M93.7,39.3H107V32  c0-21.3-18.7-40-40-40V5.3c14.5,0,26.7,12.2,26.7,26.7V39.3z" />
                </svg>
                <span> Change Side</span>
              </button>
            </div>}
          {
            saveBtn &&
              <div className="confirm-btn mb-hide">
                <button onClick={() => this.save()} className="recipe-btn"> 
                    Save Changes
                </button>
                {
                  message &&
                  <p> changes save succesfully</p>
                }
              </div>
          }
          <div className="popup-ingredients">
            <h4 onClick={()=> this.clickAccordion('ingredients')}>
              Ingredients
              { activeAccordio ?
                !clickIngredientAccordion ?
                  <svg
                    className="svIcon"
                    enableBackground="new 0 0 48 48"
                    height="10px"
                    id="Layer_3"
                    version="1.1"
                    viewBox="0 0 48 48"
                    width="10px"
                    xmlSpace="preserve"
                    xmlns="http://www.w3.org/2000/svg"
                    xmlnsXlink="http://www.w3.org/1999/xlink"
                  >
                    <polygon fill="#241F20" points="0,12.438 48,12.438 24,35.562 " />
                  </svg>
                  :
                  <svg
                    className="svIcon"
                    enableBackground="new 0 0 32 32"
                    height="13px"
                    id="svg2"
                    version="1.1"
                    viewBox="0 0 32 32"
                    width="13px"
                    xmlSpace="preserve"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <g id="backbord"><rect fill="none" height="32" width="32" /></g>
                    <g id="arrow_x5F_up">
                      <polygon points="30,22 16.001,8 2.001,22  " />
                    </g>
                  </svg>
                :
                null
              }
            </h4>
            <div key='ingredient' className={`showlist${showIngredients}`}>
              { check &&
              recipeIngredients.map((value) => {
                let ingredientName = '';
                let quantity = value.quantity;
                quantity *= (size / 4);
                let a = 0;

                quantity = quantity.toString().split('.');
                quantity[0] = parseInt(quantity[0], 0);

                if (quantity[0] === 0) {
                  quantity[0] = '';
                }
                if (quantity.length > 1 && quantity[1] !== '00') {
                  quantity[1] = `${0}.${quantity[1]}`;
                  a = fraction(parseFloat(quantity[1]));
                  if (a.d > 0) {
                    a.d = Math.round(a.d / a.n);
                    a.n = 1;
                  }
                }
                if (a !== 0 && a.d > 1) {
                  a = ReactHtmlParser(`${quantity[0]}<sup>${a.n}</sup>&frasl;<sub>${a.d}</sub>`);
                } else if (a !== 0) {
                  a = a.n;
                } else {
                  a = quantity[0];
                }
                if (value.ingredient) {
                  ingredientName = value.ingredient.name;
                }
                return (
                  <ul key={ingredientName}>
                    <li key={value.id}>
                      {a} {value.unit} {ingredientName}
                    </li>
                  </ul>
                );
              })
              }
            </div>
          </div>
          {instructions &&
          <div className="popup-ingredients popup-instructions">
            <h4 className="recipe-instruction" onClick={()=> this.clickAccordion('instructions')}>
              Directions
              { activeAccordio ?
                !clickInstructionAccordion ?
                  <svg
                    className="svIcon"
                    enableBackground="new 0 0 48 48"
                    height="10px"
                    id="Layer_3"
                    version="1.1"
                    viewBox="0 0 48 48"
                    width="10px"
                    xmlSpace="preserve"
                    xmlns="http://www.w3.org/2000/svg"
                    xmlnsXlink="http://www.w3.org/1999/xlink"
                  >
                    <polygon fill="#241F20" points="0,12.438 48,12.438 24,35.562 " />
                  </svg>
                  :
                  <svg
                    className="svIcon"
                    enableBackground="new 0 0 32 32"
                    height="13px"
                    id="svg2"
                    version="1.1"
                    viewBox="0 0 32 32"
                    width="13px"
                    xmlSpace="preserve"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <g id="backbord"><rect fill="none" height="32" width="32" /></g>
                    <g id="arrow_x5F_up">
                      <polygon points="30,22 16.001,8 2.001,22  " />
                    </g>
                  </svg>
                :
                null
              }
            </h4>
            <ol className={`instructions showlist${showInstructions}`}>
              {
                recipeInstruction.map(value => (
                  <li key={value.id}>
                    {ReactHtmlParser(value.instruction) }
                  </li>
                ))
              }
            </ol>
          </div>
          }

          {activeAccordio &&
          <div className="popup-ingredients popup-instructions print-hide">
            <h4 onClick={()=> this.clickAccordion('nutrition')}>
              Nutrition Facts
              {!clickNutritionAccordion ?
                <svg
                  className="svIcon"
                  enableBackground="new 0 0 48 48"
                  height="10px"
                  id="Layer_3"
                  version="1.1"
                  viewBox="0 0 48 48"
                  width="10px"
                  xmlSpace="preserve"
                  xmlns="http://www.w3.org/2000/svg"
                  xmlnsXlink="http://www.w3.org/1999/xlink"
                >
                  <polygon fill="#241F20" points="0,12.438 48,12.438 24,35.562 " />
                </svg>
                :
                <svg
                  className="svIcon"
                  enableBackground="new 0 0 32 32"
                  height="13px"
                  id="svg2"
                  version="1.1"
                  viewBox="0 0 32 32"
                  width="13px"
                  xmlSpace="preserve"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <g id="backbord"><rect fill="none" height="32" width="32" /></g>
                  <g id="arrow_x5F_up">
                    <polygon points="30,22 16.001,8 2.001,22  " />
                  </g>
                </svg>
              }
            </h4>
            {recipeNutrition && this.nutritionData()}
          </div>
          }
          {
            saveBtn &&
              <div className="mb-only confirm-btn">
                <button onClick={() => this.save()} className="recipe-btn"> 
                    Save Changes
                </button>
                {
                  message &&
                  <p> Changes saved succesfully!</p>
                  
                }
              </div>
          }
        </div>
      </div>
    );

  }
}
