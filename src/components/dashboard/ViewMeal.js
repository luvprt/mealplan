import React, { Component } from 'react';
import Loader from 'lib/loader';
import moment from 'moment';
import { onPressCancelBtn, onPressSaveBtn } from 'lib/common';
import ViewMainDish from './ViewMainDish'
import ViewsSideDish from './ViewsSideDish'
// import BottomBar from '../recipeList/BottomBar';
import history from '../../history'

const BackBtn = props => {
  const btnText = props.path !== 'viewRecipe' ? ' Back To Weekly Plan' : 'Back To View All Recipes '
  
  return(
    <div className="back-to-recipe" style={{float:'left'}}>
      <button className="btn back-to-plan" onClick={() => props.pressBackBtn()}>
        <svg
          height="512px"
          id="Layer_1"
          enableBackground="new 0 0 512 512"
          version="1.1"
          viewBox="0 0 512 512"
          width="512px"
          xmlSpace="preserve"
          xmlns="http://www.w3.org/2000/svg"
          xmlnsXlink="http://www.w3.org/1999/xlink"
        >
          <polygon points="352,115.4 331.3,96 160,256 331.3,416 352,396.7 201.5,256 " />
        </svg>
        {btnText}
      </button>
    </div>
  )}

const AddRecipe = props => {
  const { laterAddOn, data, bottomBtnState } = props
  let image = "/images/add-menu.svg"
  
  let btnText = bottomBtnState.action === 'add' || bottomBtnState.action === 'swap' ? "Select Recipe" : "Add To Menu"
  let addmenuClass = ""
  if(data && data.length > 0) {
    const index = data.findIndex(x=>x.items.recipe.id === laterAddOn.items.recipe.id && x.index === laterAddOn.index)
    if(index > -1) {
      image = "/images/item-add.svg"
      btnText = bottomBtnState.action === 'add' || bottomBtnState.action === 'swap' ? "Recipe Selected" : "Added To Menu"
      addmenuClass="recipe-add"
    }
  }
  return(
    <div>
      <button onClick={()=>props.addRecipe()} className={addmenuClass}>
        <img src={image} alt=""/>
        <span>{btnText}</span>
      </button>
      {props.error && <span className="error-msg"> Recipe Removed </span>}
    </div>
  )
}

class ViewMeal extends Component {
  constructor(props){
    super(props)
    const viewRecipeData = this.props.location && this.props.location.state
    const slideClass= window.innerWidth < 768 ? 'sliderighttrue' : ''
    this.state = {
      sid: 0,
      recipe: {},
      loadData: false,
      nutritionName: [],
      recipeNutritionName: [],
      sideNutritionName: [],
      recipeNutritionValue: [],
      message: false,
      loading: false,
      viewRecipeData,
      nutritionData: [],
      totalServing: viewRecipeData.totalServing,
      laterAddOn: viewRecipeData.laterAddOn,
      bottomBtnState:viewRecipeData.state,
      bottomBtnProps:viewRecipeData.props,
      data: [],
      error: false,
      slideClass
    }
  }
  
  componentDidMount() {
    this.getMealData();
  }
  
  onPressCancel() {
    const {bottomBtnProps, bottomBtnState} = this.state
    onPressCancelBtn(bottomBtnState, bottomBtnProps)
  }

  async onPressSave() {
    const {bottomBtnProps, bottomBtnState, data} = this.state
    data[0].cooking_date = bottomBtnState.cookDate
    bottomBtnState.recipeArr = data
    await onPressSaveBtn(bottomBtnState,bottomBtnProps)
  }

  
  getMealData() {
    const { path } = this.state.viewRecipeData
    const { bottomBtnState } = this.state
    if(path !== 'viewRecipe') {
      window.scrollTo(0,0)
    }
    const nutritionName = [];
    const recipeNutritionName = []
    const sideNutritionName = []
    const recipe = this.props.location.state && this.props.location.state.recipe
    let sideDishValue = null;
    const data = JSON.parse(sessionStorage.getItem('recipeArr'))
    let nutritionData = recipe.recipe_nutrition_data.length > 0 ? recipe.recipe_nutrition_data: []
    if(nutritionData.length > 0) {
      nutritionData.map((item) => {
           
        const attribute = item.nutrition_info_attribute.attribute
        let unit = '';
        if(item.nutrition_info_attribute.measurement_unit != null){
          unit = item.nutrition_info_attribute.measurement_unit
        }
        recipeNutritionName[`${attribute}`] = parseInt(item.value,0)
        nutritionName[`${attribute}`] = parseInt(item.value,0)
        nutritionName[`unit${attribute}`] =  unit
       
        return recipeNutritionName
      })
    }
    const value = recipe.recipe_sides.length > 0 && recipe.recipe_sides[0]
    if(value) {
      nutritionData = path === 'viewRecipe' ? value.recipe_nutrition_data : value.recipe.recipe_nutrition_data
      if(nutritionData && nutritionData.length > 0) {
    
        sideDishValue = recipe.recipe_sides[0]
        sideDishValue.recipe_nutrition_data =  path === 'viewRecipe' ? value.recipe_nutrition_data : value.recipe.recipe_nutrition_data
    
        nutritionData.map((item) => {
      
          const attribute = item.nutrition_info_attribute.attribute
          let unit = '';
          if(item.nutrition_info_attribute.measurement_unit != null){
            unit = item.nutrition_info_attribute.measurement_unit
          }
          nutritionName[`${attribute}`] = nutritionName[`${attribute}`]+parseInt(item.value,0)
          sideNutritionName[`${attribute}`] = parseInt(item.value,0)
          nutritionName[`unit${attribute}`] =  unit
      
          return nutritionData;
        })
      }
    }
    bottomBtnState.recipeCount = bottomBtnState.recipeCount === 1 ? 2 : bottomBtnState.recipeCount
    this.setState({ recipeNutritionName, nutritionName, nutritionData, sideNutritionName, sideDishValue, data, bottomBtnState})
  }

  emptyRecipeArr() {
    this.setState({ data: [], error: true})
    sessionStorage.setItem('recipeArr', JSON.stringify([]))
  }
  

  clickServing() {
    const { totalServing, mealDate, rname } = this.state.viewRecipeData;
    let { type } = this.state.viewRecipeData;
    type = type.split('_')[0];
    // serving size event track
    window.analytics.track('Clicked Serving Size', {
      servingSize: totalServing,
      date: moment(mealDate).format('dddd DD-MM-YYYY'),
      recipeName: rname,
      slot: type,
    });
  }

 

  // eslint-disable-next-line
  pressBackBtn() {
    const { path, dayDates } = this.state.viewRecipeData
    const { bottomBtnState, data } = this.state
    if (path !== 'viewRecipe') {
      history.push('/dashboard',{dayDates, bottomBtnState})
      sessionStorage.setItem('method', 'back');
    } else {
      // window.history.back()
      if(window.innerWidth < 769){
        this.setState({ slideClass: 'slidetestleft'})
      }
      document.getElementsByTagName('body')[0].className = ""
      this.props.closeMeal(data, bottomBtnState.recipeCount)
    }
  }

  addRecipe() {
    const { laterAddOn } = this.state.viewRecipeData
    const { action } = this.state.bottomBtnState
    let {data} = this.state
    const {bottomBtnState} = this.state
    data = data === null ? [] : data
    let index = -1
    if(action!== 'swap' && action !=='add' ) {
      index = data.findIndex(x=>x.index === laterAddOn.index)
      if(data.length > 0) {
        if(index === -1) {
          data.push(laterAddOn)
        } else {
          data.splice(index,1)
          this.setState({ error: true})
        }
      } else { 
        data.push(laterAddOn)
      }
    } else {
      index = data.findIndex(x=>x.index === laterAddOn.index)
      if(index === -1) {
        data[0] = laterAddOn
      } else {
        data.splice(index,1)
        this.setState({ error: true})
      }
    }
    if(laterAddOn.items && laterAddOn.items.recipe){
      const email = localStorage.getItem('email')
      window.analytics.track(`Recipe Selected`, {
        recipeName: laterAddOn.items.recipe.name,
        email
      });
    }
    if(laterAddOn.items && laterAddOn.items.recipe){
      const email = localStorage.getItem('email')
      window.analytics.track(`Recipe Selected`, {
        recipeName: laterAddOn.items.recipe.name,
        email
      });
    }
    bottomBtnState.recipeCount +=1 
    this.setState({ data, bottomBtnState })
    sessionStorage.setItem('recipeArr', JSON.stringify(data))
  }

  removeRecipe(event){
    const { data, laterAddOn } = this.state
    let { error } = this.state
    const index = data.findIndex(x=>x.index === event) 
    if(index > -1) {
      data.splice(index,1)
      if(event === laterAddOn.index ) {
        error = true
      } else { error = false }
    }
    this.setState({ data, error })
    sessionStorage.setItem('recipeArr', JSON.stringify(data))
  }

  render() {
    const {
      sid, sideDishValue, loadData, nutritionName, nutritionData, saveBtn, error
    } = this.state;
    const { totalTime, totalHandsOn, path, totalPassive, rname, sname, 
      recipeImg, totalServing, recipe, dayDates} = this.state.viewRecipeData
       
    const startWeekMobileDate = path!== 'viewRecipe' ? `${dayDates[0].halfMonth} ${dayDates[0].date}`: ''
    const endWeekMobileDate = path!== 'viewRecipe' ? `${dayDates[6].halfMonth} ${dayDates[6].date}`: ''
    const { slideClass } = this.state
    if(error) {
      setTimeout(()=>{this.setState({ error: false})}, 2000)
    }
    return(
      <div className={`wrapper ${slideClass}`} style={{right: slideClass ==='slidetest' && `-500px`}}>
        <header>
          <div className="container">
            <a href="/" className="mb-hide">
              <img src="/images/logo.png" alt="" />
            </a>
            <div className="top-back-row-comon-hdr mb-only">
              <a onClick={()=>this.pressBackBtn()}>  
                <svg enableBackground="new 0 0 256 256" height="256px" id="Layer_1" version="1.1" viewBox="0 0 256 256" width="256px" xmlSpace="preserve" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                  <path d="M91.474,33.861l-89.6,89.6c-2.5,2.5-2.5,6.551,0,9.051l89.6,89.6c2.5,2.5,6.551,2.5,9.051,0s2.5-6.742,0-9.242L21.849,134  H249.6c3.535,0,6.4-2.466,6.4-6s-2.865-6-6.4-6H21.849l78.676-78.881c1.25-1.25,1.875-2.993,1.875-4.629s-0.625-3.326-1.875-4.576  C98.024,31.413,93.974,31.361,91.474,33.861z" />
                </svg>
              </a>
            </div>
            <div className=" dash-cntnt-dtail-hdr cook-day-top mb-only">
              {path !== 'viewRecipe' ?
                <div className="mobile-view-small mob-dash">
                  <h5 className='view-heading'> Weekly Plan </h5>
                  <div className="calndr-cntrls one-line view-dtail">
                    <h3 className="mb-only">{startWeekMobileDate} - {endWeekMobileDate}</h3>
                  </div>
                </div>
                :
                <div className="mobile-view-small">
                  <h5 className='view-heading'> View Details </h5>
                  <h3 className="mb-only">Go back to continue selecting menu</h3>
                </div>
              }
            </div>
          </div>
        </header>
        <div className="col-lg-9 main-cntnt-all" draggable={false} style={{ background: '#fff'}}>
      
          <style type="text/css">
            {"@media print{@page {size: portrait; margin:0;  } }"}
          </style>
          {
            loadData ?
              <div className="loading" style={{marginTop:'20%'}}>
                <Loader />
                <div className="modal-backdrop fade in" />
              </div>
              :
              <div className="dash-cntnt-dtail swap-mode-dtail view-recipe" >
                <div>
                  <div className="tab-content">
                    <div className="recipe-top-btn mb-hide">
                      <BackBtn 
                        pressBackBtn={()=> this.pressBackBtn()}
                        path={path}
                      />
                    </div>
                    <div id="meal-dtail" className="tab-pane fade in active">
                      <div className="recipe-top-cntnt">
                        <div className="add-menu">
                          {
                            path === 'viewRecipe' && <AddRecipe {... this.state} addRecipe={()=>this.addRecipe()}/>
                          }
                          <h1>{rname}</h1>
                        </div>
                        {sname != null && 
                    <div className="side-tag-lft">
                      <div className="meal-breakdown-row-side-tag">Side</div>
                      <div className="side-hdng-cntnt">{sname}</div>
                    </div>
                        }
                  
                        <div className="meal-sumry-icn-box">
                          <div className="popup-main-icn-box small-box">
                            <div className="row">
                              <div className="meal-summary mb-hide">  Meal Summary </div>
                              <div className="meal-summary mb-border mb-only"> Meal Summary (Main + Side)</div>
                              <div className="meal-sumry-icn-inner chef-cap-small">
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  xmlnsXlink="http://www.w3.org/1999/xlink"
                                  version="1.1"
                                  x="0px"
                                  y="0px"
                                  viewBox="0 0 100 100"
                                  style={{ enableBackground: 'new 0 0 100 100' }}
                                  xmlSpace="preserve"
                                >
                                  <path d="M79,40.9c0-7.2-5.9-13.1-13.1-13.1c-1.8,0-3.5,0.4-5.2,1.1c-2.4-3.5-6.4-5.6-10.7-5.6s-8.3,2.1-10.7,5.6  c-1.6-0.7-3.4-1.1-5.2-1.1c-7.2,0-13.1,5.9-13.1,13.1c0,6.6,5,12.1,11.4,12.9v18.9c0,2.2,1.8,4,4,4h27.2c2.2,0,4-1.8,4-4V53.9  C74,53,79,47.5,79,40.9z M63.6,73.3H36.4c-0.3,0-0.6-0.3-0.6-0.6v-3.4h28.4v3.4C64.2,73,63.9,73.3,63.6,73.3z M65.9,50.6  c-2.1,0-4-0.6-5.7-1.9c-0.8-0.6-1.8-0.4-2.4,0.4c-0.6,0.8-0.4,1.8,0.4,2.4c1.8,1.3,3.9,2.1,6,2.4v12.1h-8v-6.8  c0-0.9-0.8-1.7-1.7-1.7s-1.7,0.8-1.7,1.7v6.8h-5.7v-6.8c0-0.9-0.8-1.7-1.7-1.7s-1.7,0.8-1.7,1.7v6.8h-8V53.8  c2.2-0.3,4.3-1.1,6.2-2.5c0.8-0.6,0.9-1.6,0.3-2.4c-0.6-0.8-1.6-0.9-2.4-0.3c-1.7,1.3-3.7,1.9-5.8,1.9c-5.3,0-9.7-4.3-9.7-9.7  s4.3-9.7,9.7-9.7c1.7,0,3.5,0.5,4.9,1.4c0.4,0.2,0.9,0.3,1.3,0.2c0.5-0.1,0.8-0.4,1-0.9c1.7-3.2,5-5.2,8.6-5.2s6.9,2,8.6,5.2  c0.2,0.4,0.6,0.7,1,0.9c0.5,0.1,0.9,0.1,1.3-0.2c1.5-0.9,3.2-1.4,4.9-1.4c5.3,0,9.7,4.3,9.7,9.7S71.2,50.6,65.9,50.6z" />
                                </svg>
                                <b>{totalHandsOn}</b>
                                <span>Hands On</span>
                                <span className="mb-only">:</span>
                              </div>
                              <div className="meal-sumry-icn-inner passive-only">
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  xmlnsXlink="http://www.w3.org/1999/xlink"
                                  version="1.1"
                                  x="0px"
                                  y="0px"
                                  viewBox="0 0 44 44"
                                  style={{ enableBackground: 'new 0 0 44 44' }}
                                  xmlSpace="preserve"
                                >
                                  <g>
                                    <path d="M35.879,27c-0.834,0-1.618,0.324-2.207,0.914l-5.793,5.793C27.69,33.896,27.438,34,27.171,34H23V18h15c0.552,0,1-0.448,1-1   c0-6.91-7.092-12.562-16-12.961V1c0-0.552-0.448-1-1-1s-1,0.448-1,1v3.039C12.092,4.438,5,10.09,5,17c0,0.552,0.448,1,1,1h15v16H8   c-1.654,0-3,1.346-3,3c0,1.453,1.038,2.666,2.412,2.941L6.382,42H6c-0.552,0-1,0.447-1,1s0.448,1,1,1h1   c0.379,0,0.725-0.214,0.895-0.553L9.618,40H21v1c0,0.552-0.449,1-1,1h-1c-0.552,0-1,0.447-1,1s0.448,1,1,1h6c0.552,0,1-0.447,1-1   s-0.448-1-1-1h-1c-0.551,0-1-0.448-1-1v-1h5.343c0.336,0,0.667-0.034,0.99-0.098l1.773,3.545C31.275,43.786,31.621,44,32,44h1   c0.552,0,1-0.447,1-1s-0.448-1-1-1h-0.382l-1.443-2.886c0.248-0.171,0.486-0.361,0.703-0.578l6.208-6.207   C38.667,31.747,39,30.943,39,30.121C39,28.4,37.6,27,35.879,27z M36.938,16h-7.965c-0.157-3.989-1.24-7.417-2.866-9.577   C31.966,7.648,36.374,11.422,36.938,16z M7.062,16c0.565-4.578,4.973-8.352,10.831-9.577c-1.626,2.16-2.709,5.588-2.866,9.577   H7.062z M17.021,16C17.274,10.058,19.759,6,22,6s4.726,4.058,4.979,10H17.021z M36.672,30.914l-6.208,6.207   C29.906,37.68,29.133,38,28.343,38H8c-0.551,0-1-0.448-1-1s0.449-1,1-1h19.171c0.801,0,1.555-0.312,2.122-0.879l5.793-5.793   C35.298,29.116,35.579,29,35.879,29C36.497,29,37,29.503,37,30.121C37,30.416,36.88,30.705,36.672,30.914z" />
                                  </g>
                                </svg>
                                <b className="mb-hide">{totalPassive}</b>
                                <span className="mb-hide">Passive Time</span>
                                {path !== 'viewRecipe' && <div><span className="mb-only">Total Servings:</span>
                                  <span className="mb-only">{totalServing}</span></div>}
                              </div>
                              <div className="meal-sumry-icn-inner popup-time-icn">
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  xmlnsXlink="http://www.w3.org/1999/xlink"
                                  version="1.1"
                                  x="0px"
                                  y="0px"
                                  viewBox="0 0 100 100"
                                  style={{ enableBackground: 'new 0 0 100 100' }}
                                  xmlSpace="preserve"
                                >
                                  <g>
                                    <path d="M50,5C25.2,5,5,25.2,5,50s20.1,45,45,45s45-20.1,45-45S74.8,5,50,5z M50,89.7c-21.9,0-39.7-17.8-39.7-39.7   c0-21.9,17.8-39.7,39.7-39.7S89.7,28.1,89.7,50C89.7,71.9,71.9,89.7,50,89.7z" /><path d="M52.4,51.7V25.2c0-1.3-1.1-2.4-2.4-2.4s-2.4,1.1-2.4,2.4v27.3c0,0.3,0.1,0.6,0.1,0.8c0.1,0.4,0.3,0.8,0.6,1.1l19.3,19.3   c0.9,0.9,2.5,0.9,3.4,0c0.9-0.9,0.9-2.5,0-3.4L52.4,51.7z" />
                                  </g>
                                </svg>
                                <b>{totalTime}</b>
                                <span>Total Time</span>
                                <span className="mb-only">:</span>
                              </div>
                              {
                                path !== 'viewRecipe' && <div className="ttl-serving"><span className="mb-hide">Total Servings:</span>
                                  <span className="mb-hide">{totalServing}</span></div>
                              }
                            </div>
                          </div>
                       
                          <div className="popup-main-icn-box popup-main-icn-box-rght small-box">
                            {nutritionData && nutritionData.length > 0 && 
                            <div className="row">
                              <div className="meal-summary mb-hide mb-border"> Nutritional summary (Main + Side)</div>
                              <div className="popup-main-icn-box-rght-inner-box">
                                <div className="meal-sumry-icn-inner">
                                  <b>{nutritionName.Calories}{ nutritionName.unitCalories}</b>
                                  <span>Calories</span>
                                </div>
                                <div className="meal-sumry-icn-inner">
                                  <b>{nutritionName.Protein}{nutritionName.unitProtein}</b>
                                  <span>Protein</span>
                                </div>
                                <div className="meal-sumry-icn-inner">
                                  <b>{nutritionName['Net Carbohydrates']}{nutritionName['unitNet Carbohydrates']}</b>
                                  <span>Net Carbs</span>
                                </div>
                                <div className="meal-sumry-icn-inner">
                                  <b>{nutritionName['Total Fat']}{nutritionName['unitTotal Fat']}</b>
                                  <span>Total Fat</span>
                                </div>
                              </div>
                            </div>}
                          </div>

                        </div>
                      </div>
                
                    </div>
              
               
                  </div>
                </div>
                <ViewMainDish
                // type={this.state.viewRecipeData.type}
                  response={recipe}
                  recipeImg={recipeImg}
                  servingSize={totalServing}
                  { ...this.state}
                />
                {sname !=null &&
                <ViewsSideDish 
                  response={recipe}
                  sid={sid}
                  length={this.state.length} 
                  sideDishValue={sideDishValue}
                  saveBtn={saveBtn}
                  servingSize={totalServing}
                  {... this.state}
                />
                }
              </div>
          }
        </div>
        {/* {
          path === 'viewRecipe' && ((data && data.length > 0) || (this.state.bottomBtnState.recipeCount > 0 && window.innerWidth < 769 ))&&
            <footer>
              <BottomBar {...this.state.bottomBtnState} location="view"
                onPressCancel={()=>this.onPressCancel()}
                onPressSave={()=>this.onPressSave()}
                data={this.state.data}
                pathFrom='viewMeal'
                emptyRecipeArr={()=>this.emptyRecipeArr()}
                removeRecipe={(event, key, image, items ,id)=>
                  this.removeRecipe(event, key,image, items, id)}
              />
            </footer>
        } */}
      </div> 
    )
  }
}
export default ViewMeal;
