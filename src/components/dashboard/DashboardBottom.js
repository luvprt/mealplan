import React from 'react'
import history from '../../history';

const DashboardBottom = props => {
  const { editRecipe, mbClass } = props 
  const showBottom = mbClass !== ''
  return(
    <div>
      {editRecipe && <DashboardDesktopBottom {...props}/>}
      {showBottom && !editRecipe && <MobileFooter {...props}/>}
    </div>
  )
}
const SwapRecipe = props => {
  const { action } = props
  const value = props.recipeArr[0]
  return(
    <div className="recipe-swap">
      <div className="selected-meal-thumbnail" id='selected-meal-thumbnail' key={value.index} title={value.index}>

        <a onClick={()=>props.removeRecipe(`${value.index}`, value.key, value.id)} className="cross-icn">
          <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg"
            xmlnsXlink="http://www.w3.org/1999/xlink">
            <title>Close</title>
            <desc>Created with Sketch.</desc>
            <defs />
            <g id="Select-Meals" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
              <g id="Mobile:-Select-Meals-4b" transform="translate(-188.000000, -1423.000000)">
                <g id="Bottom-Bar:-Apply-Filters-Button" transform="translate(0.000000, 1416.000000)">
                  <g id="Group-5-Copy-13" transform="translate(116.000000, 7.000000)">
                    <g id="Close" transform="translate(72.000000, 0.000000)">
                      <circle id="Oval" fill="#546E7A" cx="10" cy="10" r="10" />
                      <polygon id="Shape" fill="#FFFFFF" points="14.6666667 6.26666667 13.7333333 5.33333333 10 9.06666667 6.26666667 5.33333333 5.33333333 6.26666667 9.06666667 10 5.33333333 13.7333333 6.26666667 14.6666667 10 10.9333333 13.7333333 14.6666667 14.6666667 13.7333333 10.9333333 10" />
                    </g>
                  </g>
                </g>
              </g>
            </g>
          </svg>
        </a>
        <img src={value.image} alt=""/>
      </div>
      <div>
        <p className="swap-tag">{action === 'swap' ? 'Swapping In:': 'Adding:'}</p>
        <div className="swap-recipe">{value.items.recipe.name} </div>
        {value.items.recipe.recipe_sides.recipe ? <div className="swap-side mb-hide">{value.items.recipe.recipe_sides.length > 0 && value.items.recipe.recipe_sides[0].recipe.name} </div> : ''}
      </div>
    </div>
  )
}

const DashboardDesktopBottom = props => {
  const { bottomText,  cookDayArr, showRecipe, path, animateClass } = props
  const bottomSubText = cookDayArr && cookDayArr.length > 0 ? 'Cook days & times have changed.' : ' Grocery list will regenerate on save.'
  const buttonText = path === 'recipe' ? 'Done' : 'Save'
  
  return (
    <div className={`choose-recipe-btns select-meal-btns selected bottom-space dash-bottom ${animateClass}`}>
      <div className="container">
        {bottomText && 
        <div className="recipe-selected-status main-cook-set">
          <p className="mb-hide cook-days dash-msg">
             Heads Up! Changes Made 
          </p>
          
          <div style={{ float: 'left', width:'100%'}}>
            <span className='set-cookday dash-msg'>
              {bottomSubText}
            </span>
          </div>
        </div>
        }
        { 
          showRecipe &&
          <SwapRecipe {...props} />
        }
        <div className="dash-bottom" style={{float:'right'}}>
          <button
            className="next-btn button-small-device"
            style={{height:'52px'}}
          >
            <span aria-label="Next" onClick={()=>props.onPressSave()}>{buttonText}</span>
          </button>
          <button
            className="next-btn outer-boder button-small-device cancl-btn mb-hide"
          >
            <span aria-label="Next"className="mb-hide" onClick={()=>props.onPressCancel()}>Cancel</span>
          </button>
        </div>
      </div>
    </div>
  )
}

const MobileFooter = (props) => {
  const { groceryClass, dashClass } = props
  let planIcon = "images/Recipeslist.svg"
  let groceryIcon = "images/Groceries.svg"
  let accountIcon = "/images/Account.svg"
  if(groceryClass === 'active'){
    planIcon = "images/Recipeslist.svg"
    groceryIcon = "images/Groceries_active.svg"
    accountIcon = "/images/Account.svg"
  } else if(dashClass === 'active'){
    planIcon = "images/Recipeslist_active.svg"
    groceryIcon = "images/Groceries.svg"
    accountIcon = "/images/Account.svg"
  } else {
    planIcon = "images/Recipeslist.svg"
    groceryIcon = "images/Groceries.svg"
    accountIcon = "/images/Account_active.svg"
  }
  return(
    <div className="mobile-footer">
      <div className="mobile-view-footer mb-only">
        <ul>
          <li>
            <div className="1st-space">
              <img src={planIcon} alt=""  onClick={()=>{sessionStorage.setItem('method','back');
                history.push('/dashboard', {
                  type: '/login', dayDates: props.dayDates,
                })}} />
              <span> Plan </span>
            </div>
          </li>
          <li>
            <img src={groceryIcon} alt="" onClick={()=>{sessionStorage.setItem('method','back'); 
              history.push('/groceryList', {
                type: '/login', dayDates: props.dayDates,
              })}}/>
            <span> Grocery list </span>
          </li>
          <li>
            <img src={accountIcon} alt="" onClick={()=>history.push('/profileSettings',{
              exist: 'yes', reminder:false
            })}/>
            <span> Account </span>
          </li>
        </ul>
      </div>
    </div>
  )}
export default DashboardBottom