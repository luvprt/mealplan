import React, { Component } from 'react';
import ReactHtmlParser from 'react-html-parser';
import LazyLoad from 'react-image-lazy-load';
import { fraction } from 'mathjs';
import { getTimeStamp, getFormatTime } from 'lib/services';
import moment from "moment";
import ReactResizeDetector from 'react-resize-detector';

export default class ViewMainDish extends Component {
  constructor(props) {
    super(props);
    this.state = {
      response: this.props.response || null,
      showIngredients: true,
      showInstructions: true,
      showNutrition: false, 
      dataValue: '',
      recipeNutritionName: props.recipeNutritionName,
      nutritionName: props.nutritionName,
      servingSize: props.servingSize
    };
  }

  // change UNSAFE
  // eslint-disable-next-line
  UNSAFE_componentWillReceiveProps(props) {
    if (props.response) {
      const { name } = props.response;
      this.setState({
        name,
        handsOnTime: props.response.hands_on_time,
        passiveTime: props.response.passive_time,
        recipeIngredients: props.response.recipe_ingredients,
        enableSwapButton: props.enableSwapButton,
        recipeNutrition: props.response.recipe_nutrition_data,
        mealDate: props.fDate,
        recipeImg: props.response.image,
        recipeNutritionName: props.recipeNutritionName,
        nutritionName: props.nutritionName,
        response: props.response
      });
      if (props.response.recipe_instructions) {
        this.setState({
          recipeInstruction: props.response.recipe_instructions,
        });
      }
    }
  }

  onSave() {
    const { enableSwapButton } = this.state;
    if (enableSwapButton) {
      this.props.onSave();
    } else {
      this.props.swapSave();
    }
  }

  onResize() {
    let { activeAccordio, showIngredients, showInstructions} = this.state;
    const { dataValue } = this.state
    if (window.innerWidth < 768 ) {
      activeAccordio = true;
      if(dataValue === '') {
        showIngredients = false;
        showInstructions = false;
      }
    } else {
      activeAccordio = false;
      showIngredients = true;
      showInstructions = true;
    }
    this.setState({ activeAccordio, showIngredients, showInstructions });
  }
  
  saveAnalytics() {
    const { recipeNutrition, name, mealDate, type } = this.state
    window.analytics.track('Click Recipe Nutrition in Main Dish Tab', {
      recipeName: name,
      link: recipeNutrition,
      slot: type,
      date: moment(mealDate).format('dddd DD-MM-YYYY'),
    });
  }

  clickAccordion(value){
    const { clickAccordion } = this.state
    let { showIngredients, showInstructions, showNutrition, clickIngredientAccordion, clickInstructionAccordion, clickNutritionAccordion } = this.state;
    if(value === 'ingredients') {
      showIngredients = !showIngredients
      clickIngredientAccordion = !clickIngredientAccordion
    } else if(value === 'instructions') {
      showInstructions = !showInstructions
      clickInstructionAccordion = !clickInstructionAccordion
    } else if(value === 'nutrition') {
      showNutrition = !showNutrition
      clickNutritionAccordion = !clickNutritionAccordion
    }
    this.setState({ 
      clickAccordion: !clickAccordion,
      showNutrition,
      showIngredients,
      showInstructions,
      clickIngredientAccordion,
      clickInstructionAccordion,
      clickNutritionAccordion,
      dataValue: value
    });
  }
  
  nutritionData() {
    const {recipeNutritionName, nutritionName, recipeNutrition, showNutrition } = this.state
    return(
      <div className={`showlist${showNutrition}`}>
        <div className="showlist" />
        <div className="popup-main-icn-box-rght-inner-box2 mb-only">
          <div className="meal-sumry-icn-inner">
            <b>{recipeNutritionName.Calories}{nutritionName.unitCalories}</b>
            <span>Calories</span>
          </div>
          <div className="meal-sumry-icn-inner">
            <b>{recipeNutritionName.Protein}{nutritionName.unitProtein}</b>
            <span>Protein</span>
          </div>
          <div className="meal-sumry-icn-inner">
            <b>{recipeNutritionName['Net Carbohydrates']}{nutritionName['unitNet Carbohydrates']}</b>
            <span>Net Carbs</span>
          </div>
          <div className="meal-sumry-icn-inner">
            <b>{recipeNutritionName['Total Fat']}{nutritionName['unitTotal Fat']}</b>
            <span>Total Fat</span>
          </div>
        </div>
        <div className="dtail-nutri-info-cntnt">
          <ul>
            {
              recipeNutrition.length >0 &&
              recipeNutrition.map((value, index) => {
                if (value.nutrition_info_attribute.attribute === 'Trans Fat' || 
                    value.nutrition_info_attribute.attribute === 'Saturated Fat'||
                    value.nutrition_info_attribute.attribute === 'Sugars' ||
                    value.nutrition_info_attribute.attribute === 'Dietary Fiber'
                ) {
                  return(
                    // eslint-disable-next-line
                    <ul className="inner-child" key={`${value.value}${index}`}>
                      <li key={value.nutrition_info_attribute.attribute}>{value.nutrition_info_attribute.attribute}
                        <span className="pull-right">
                          {value.value}&nbsp;
                          {value.nutrition_info_attribute.measurement_unit}
                        </span>
                      </li>
                    </ul>
                  )
                }
                if (index > 0) {
                  return(
                    <li key={value.nutrition_info_attribute.attribute}>
                      {value.nutrition_info_attribute.attribute}
                      <span className="pull-right">
                        {value.value}&nbsp;
                        {value.nutrition_info_attribute.measurement_unit}
                      </span>
                    </li>
                  ) }
                return <li key={value.value} />
              })
            }
            <li className="note-txt">*Nutritional estimates based on a standard of 1 serving.</li>
          </ul>  
        </div>
      </div>
    )
  }

  render() {
    const {
      name, passiveTime, recipeIngredients, recipeInstruction, showInstructions, showIngredients,
      activeAccordio, recipeNutrition,response,
      recipeNutritionName, nutritionName, clickIngredientAccordion, clickInstructionAccordion,
      clickNutritionAccordion,servingSize
    } = this.state;
    let { handsOnTime, recipeImg } = this.state;
    let totaltime = getTimeStamp(handsOnTime + passiveTime);
    totaltime = getFormatTime(totaltime);
    handsOnTime = getTimeStamp(handsOnTime);
    handsOnTime = getFormatTime(handsOnTime);
    const setImage = recipeImg
    if(recipeImg && recipeImg !== null) {
      recipeImg = recipeImg.split('mpn-recipe-images/')[0]
      const rId = response.recipe_id ? response.recipe_id :response.id
      const checkImage = setImage.includes('mpn-recipe-images/')
      if(checkImage){
        recipeImg += `mpn-recipe-images/Images/${rId}-EH720.jpg`
      }else{
        recipeImg = setImage
      }
    }
    let check = false;
    let instructions = false;
    if (recipeIngredients) {
      check = true;
    }
    if (recipeInstruction) {
      instructions = true;
    }
    return (
      <div id="main-dish" className="main-dish-cntnt">
        <ReactResizeDetector handleWidth handleHeight onResize={() => this.onResize()} />
        <div className="tab-lft-cntnt">
          <div>
            <LazyLoad
              loaderImage
              originalSrc={recipeImg}
              imageProps={{
                src: '/images/small.png',
              }}
              className="img-side1"
            />
          </div>
          <div className="icn-box-iner mb-hide">
            {recipeNutrition && recipeNutrition.length > 0 &&
            <div className="row">
              <h4> Nutritional facts</h4>
              <div className="popup-main-icn-box-rght-inner-box2 mb-hide">
                <div className="meal-sumry-icn-inner">
                  <b>{recipeNutritionName.Calories}{nutritionName.unitCalories}</b>
                  <span>Calories</span>
                </div>
                <div className="meal-sumry-icn-inner">
                  <b>{recipeNutritionName.Protein}{nutritionName.unitProtein}</b>
                  <span>Protein</span>
                </div>
                <div className="meal-sumry-icn-inner">
                  <b>{recipeNutritionName['Net Carbohydrates']}{nutritionName['unitNet Carbohydrates']}</b>
                  <span>Net Carbs</span>
                </div>
                <div className="meal-sumry-icn-inner">
                  <b>{recipeNutritionName['Total Fat']}{nutritionName['unitTotal Fat']}</b>
                  <span>Total Fat</span>
                </div>
              </div>
            </div>}
          </div>
          <div className="custm-dropdwn-side mb-hide">
            {recipeNutrition && recipeNutrition.length > 0 &&
            <h4 onClick={()=> this.clickAccordion('nutrition')}>
              Detailed Nutrition Info 
              {!clickNutritionAccordion ?
                <svg
                  className="svIcon"
                  enableBackground="new 0 0 48 48"
                  height="10px"
                  id="Layer_3"
                  version="1.1"
                  viewBox="0 0 48 48"
                  width="10px"
                  xmlSpace="preserve"
                  xmlns="http://www.w3.org/2000/svg"
                  xmlnsXlink="http://www.w3.org/1999/xlink"
              
                >
                  <polygon fill="#241F20" points="0,12.438 48,12.438 24,35.562 " />
                </svg>
                :
                <svg
                  className="svIcon"
                  enableBackground="new 0 0 32 32"
                  height="13px"
                  id="svg2"
                  version="1.1"
                  viewBox="0 0 32 32"
                  width="13px"
                  xmlSpace="preserve"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <g id="backbord"><rect fill="none" height="32" width="32" /></g>
                  <g id="arrow_x5F_up">
                    <polygon points="30,22 16.001,8 2.001,22  " />
                  </g>
                </svg>
              }
            </h4>}
            { recipeNutrition &&
              this.nutritionData()
            }
          </div>

        </div>
        <div className="tab-right-cntnt">
          <div className="meal-summary">Main Dish </div>
          <h2>{name}</h2>
          <div className="after-hdng-cntnt">
            <span>
              Hands On:
              <b> {handsOnTime}</b>
            </span>
            <span>
              Total Time:
              <b> {totaltime}</b>
            </span>
          </div>
          <div className="popup-ingredients">
            <h4 onClick={()=> this.clickAccordion('ingredients')}>
              Ingredients
              { activeAccordio ?
                !clickIngredientAccordion ?
                  <svg
                    className="svIcon"
                    enableBackground="new 0 0 48 48"
                    height="10px"
                    id="Layer_3"
                    version="1.1"
                    viewBox="0 0 48 48"
                    width="10px"
                    xmlSpace="preserve"
                    xmlns="http://www.w3.org/2000/svg"
                    xmlnsXlink="http://www.w3.org/1999/xlink"
                  >
                    <polygon fill="#241F20" points="0,12.438 48,12.438 24,35.562 " />
                  </svg>
                  :
                  <svg
                    className="svIcon"
                    enableBackground="new 0 0 32 32"
                    height="13px"
                    id="svg2"
                    version="1.1"
                    viewBox="0 0 32 32"
                    width="13px"
                    xmlSpace="preserve"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <g id="backbord"><rect fill="none" height="32" width="32" /></g>
                    <g id="arrow_x5F_up">
                      <polygon points="30,22 16.001,8 2.001,22  " />
                    </g>
                  </svg>
                :
                null
              }
            </h4>
            <div key='ingredients' className={`showlist${showIngredients}`}>

              { check  &&
              recipeIngredients.map((value) => {
                let quantity = value.quantity;
                quantity *= (servingSize / 4);
                let a = 0;
                quantity = quantity.toString().split('.');
                quantity[0] = parseInt(quantity[0], 0);
                if (quantity[0] === 0) {
                  quantity[0] = '';
                }
                if (quantity.length > 1 && quantity[1] !== '00') {
                  quantity[1] = `${0}.${quantity[1]}`;
                  a = fraction(parseFloat(quantity[1]));
                  if (a.d > 0) {
                    a.d = Math.round(a.d / a.n);
                    a.n = 1;
                  }
                }
                if (a !== 0 && a.d > 1) {
                  a = ReactHtmlParser(`${quantity[0]}<sup>${a.n}</sup>&frasl;<sub>${a.d}</sub>`);
                } else if (a !== 0) {
                  a = a.n;
                } else {
                  a = quantity[0];
                }
                return (
                  <ul key={value.ingredient.name}>
                    <li key={value.id}>
                      {a} {value.unit} {value.ingredient.name }
                    </li>
                  </ul>
                );
              })
              }
            </div>
          </div>
          {instructions &&
          <div className="popup-ingredients popup-instructions">
            <h4 className="recipe-instruction"  onClick={()=> this.clickAccordion('instructions')}>
              Directions
              { activeAccordio ?
                !clickInstructionAccordion ?
                  <svg
                    className="svIcon"
                    enableBackground="new 0 0 48 48"
                    height="10px"
                    id="Layer_3"
                    version="1.1"
                    viewBox="0 0 48 48"
                    width="10px"
                    xmlSpace="preserve"
                    xmlns="http://www.w3.org/2000/svg"
                    xmlnsXlink="http://www.w3.org/1999/xlink"
                  >
                    <polygon fill="#241F20" points="0,12.438 48,12.438 24,35.562 " />
                  </svg>
                  :
                  <svg
                    className="svIcon"
                    enableBackground="new 0 0 32 32"
                    height="13px"
                    id="svg2"
                    version="1.1"
                    viewBox="0 0 32 32"
                    width="13px"
                    xmlSpace="preserve"
                    xmlns="http://www.w3.org/2000/svg"
                  >
                    <g id="backbord"><rect fill="none" height="32" width="32" /></g>
                    <g id="arrow_x5F_up">
                      <polygon points="30,22 16.001,8 2.001,22  " />
                    </g>
                  </svg>
                :
                null
              }
            </h4>
            <ol className={`instructions showlist${showInstructions}`}>
              { 
                recipeInstruction.map(value => (
                  <li key={value.id}>
                    {ReactHtmlParser(value.instruction) }
                  </li>
                ))
              }
            </ol>
          </div>
          }
          {activeAccordio &&
          <div className="popup-ingredients popup-instructions print-hide">
            <h4 onClick={()=> this.clickAccordion('nutrition')}>
              Nutrition Facts
              {!clickNutritionAccordion ?
                <svg
                  className="svIcon"
                  enableBackground="new 0 0 48 48"
                  height="10px"
                  id="Layer_3"
                  version="1.1"
                  viewBox="0 0 48 48"
                  width="10px"
                  xmlSpace="preserve"
                  xmlns="http://www.w3.org/2000/svg"
                  xmlnsXlink="http://www.w3.org/1999/xlink"
                >
                  <polygon fill="#241F20" points="0,12.438 48,12.438 24,35.562 " />
                </svg>
                :
                <svg
                  className="svIcon"
                  enableBackground="new 0 0 32 32"
                  height="13px"
                  id="svg2"
                  version="1.1"
                  viewBox="0 0 32 32"
                  width="13px"
                  xmlSpace="preserve"
                  xmlns="http://www.w3.org/2000/svg"
                >
                  <g id="backbord"><rect fill="none" height="32" width="32" /></g>
                  <g id="arrow_x5F_up">
                    <polygon points="30,22 16.001,8 2.001,22  " />
                  </g>
                </svg>
              }
            </h4>
            { recipeNutrition && this.nutritionData()}
          </div>
          }
        </div>
      </div>
    );
  }
}
