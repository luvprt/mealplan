/*
Component Name: Dashboard component
Contains : DashHeader, DashboardMain, BuildPlanScreen
Imcludes : Services for custom calander
Default screen: Dashboard
*/
import React, { Component } from 'react';
import { getSelectedWeekDates, getWeekDays, formatTimeStamp, getTimeStamp, compareRecipeTime, sortByKey } from 'lib/services';
import moment from 'moment'
import _ from 'lodash'
import { post, put, get } from 'lib/api'
import RoutingConstants from 'lib/RoutingConstants'
import Loader from 'lib/loader';
import DashHeader from './DashHeader';
import Login from '../login/Login';
import {RecipeData, PlusIcon, SubIcon} from '../recipeList/RecipeData'
import WeekCalenderFullArea from './WeekCalanderFullArea'
import DashboardBottom from './DashboardBottom';
import EditCookDay from '../plans/EditCookDay';
import BuildNewPlan from '../plans/BuildNewPlan';
import history from '../../history';
import GroceryList from '../groceryList/GroceryList';
import FeedbackPopup from '../UserFeedback/FeedbackPopup';

const AddMoreRecipe = (props) =>(
  <div className="meal-post small-device dashed-border bottom-margin">
    <div className="outer-dashed-circle">
      <div className="main-circle">
        <div className="inner-circle ">
          <div className="plus-icon" onClick={()=>props.addNewRecipe(props.cookDate)}> + </div>
        </div> </div>
      <div className="new text">
        <p> Add New Recipe </p> </div>
    </div>
  </div>
)


class Dashboard extends Component {
  constructor(props) {
    super(props);
    const weekStartEnd = getSelectedWeekDates('currentWeek');
    const dayDates = getWeekDays(weekStartEnd);
    const dataProps = this.props.location.state !== undefined ? this.props.location.state : {}
    const planType = this.props.location.state ? this.props.location.state.exist : 'yes';
    const slideClass = window.innerWidth < 768 ? dataProps !== undefined && dataProps && dataProps.slideClass === undefined ? 'sliderighttrue' : dataProps.slideClass :''
    this.state = {
      dayDates,
      dashClass: 'active',
      reminder: false,
      inputs: {
        delete: [],
        update: [],
        add: [],
        deleted_cooking_ids:[]
      },
      subscriptionPlan: [],
      planType,
      error: false,
      cookArr: [],
      recipeArr: [],
      cookingDate: [],
      servingArr: [],
      cookDayArr:[],
      lastStep: false,
      editRecipe: false,
      deleteRecipeIndex: [],
      bottomText: false,
      mbClass: 'mb-hide',
      showCook: false,
      calories:'0',
      recipeIdArr: [],
      groceryList: [],
      customItems: [],
      loader: true,
      showFeedbackPopup: false,
      redirectDash: false,
      slideRepeat: false,
      slideClass
    };
    // profile detail
    const url = RoutingConstants.fetchProfileDetail;
    get(url).then((response) => {
      if (response.result && response.result.length > 0) {
        const data = response.result[0];
        const mealPlan = data.user_settings;
        localStorage.setItem('default_serving_size',mealPlan.default_serving_size);       
      }
    });   
    
  }

  componentDidMount() {
    const dataProps = this.props.location.state
    document.getElementsByTagName('body')[0].className = ''
    if(dataProps === undefined ||(dataProps && dataProps.bottomBtnState === undefined) || sessionStorage.getItem('method') !=='back'){
      let { dayDates } = this.state
      window.scrollTo(0,0)
      dayDates = this.props.location.state && this.props.location.state.dayDates !== undefined ? this.props.location.state.dayDates : dayDates
      if(sessionStorage.getItem('path')!=='dashboard' && sessionStorage.getItem('method') !=='back'){
        const weekStartEnd = getSelectedWeekDates('currentWeek');
        dayDates = getWeekDays(weekStartEnd);
      }
      if(localStorage.getItem('analytics') && localStorage.getItem('feedback') !=='submit') {
        this.showFeedback()
      }
      if (this.props.location.pathname === '/groceryList') {

        this.groceryPlan(dayDates)
      }
      if (this.props.location.pathname === '/dashboard') {
        this.fetchPlan(dayDates)
        sessionStorage.removeItem('method')
        window.analytics.track('Views Dashboard', {
          startDate: `${dayDates[0].fullDay} ${dayDates[0].date}-${dayDates[0].Month}-${dayDates[0].Year}`
        });
      }
    } else if(dataProps && sessionStorage.getItem('method') ==='back') {
      const slideClass = window.innerWidth < 768 ? 'dashslideleft' :''
      this.useHistoryData(dataProps, slideClass)
    }
    sessionStorage.removeItem('plan')
    sessionStorage.removeItem('recipeArr')
    sessionStorage.removeItem('method')
    sessionStorage.removeItem('path')
    sessionStorage.removeItem('filter')
    sessionStorage.removeItem('viewAll')
    sessionStorage.removeItem('bottom')
  }

  onPressCancel() {
    const { inputs, dayDates, cookingDate } = this.state
    let { recipeArr } = this.state
    window.scrollTo(0,0)
    recipeArr = JSON.parse(localStorage.getItem('editRecipe'))
    if(recipeArr &&  recipeArr.length === 0) {
      cookingDate.splice(0,1)
    }
    sessionStorage.removeItem('path') 
    inputs.delete = []
    inputs.update = []
    inputs.add = []
    inputs.deleted_cooking_ids=[]
    this.setState({ 
      deleteRecipeIndex: [],  servingArr:[],
      bottomText: false, mbClass: 'mb-hide', recipeArr, editRecipe: false, inputs, cookingDate})
    localStorage.removeItem('editRecipe')
    window.analytics.track('Changes Cancelled', {
      startDate: `${dayDates[0].fullDay} ${dayDates[0].date}-${dayDates[0].Month}-${dayDates[0].Year}`,
      endDate: `${dayDates[6].fullDay} ${dayDates[6].date}-${dayDates[6].Month}-${dayDates[6].Year}`
    });
  }

  onPressSave() {
    window.scrollTo(0,0)
    this.setState({ loader: true})
    const { deleteRecipeIndex, recipeArr, inputs, cookingDate } = this.state
    sessionStorage.removeItem('path')
    if(deleteRecipeIndex.length > 0) {
      deleteRecipeIndex.map(key => {
        recipeArr.splice(key.index, 1)
        return null
      })
    }
    cookingDate.map((recipes, index)=>{
      const rIndex = recipeArr.findIndex(x=>x.cooking_date===recipes.cooking_date)
      if(rIndex === -1){
        const cIndex = inputs.deleted_cooking_ids.indexOf(recipes.id)
        if(cIndex === -1 && recipes.id !== undefined) {
          inputs.deleted_cooking_ids.push(recipes.id)
          cookingDate.splice(index,1)
        }
      }
      return null
    })
    this.editPlan(inputs, cookingDate)
  }

  getWeekData(dayDates, selectedWeek) {
    const { inputs, groceryClass, slideRepeat } = this.state
    inputs.delete = []
    inputs.update = []
    inputs.add = []
    inputs.deleted_cooking_ids=[]
    let slide;

    if (selectedWeek === 'nextWeek' && window.innerWidth < 768) {
      slide= `slideleft${slideRepeat}`
      // slide= 'slidetest'
    } else if (selectedWeek === 'previousWeek' && window.innerWidth < 768) {
      slide= `slideright${slideRepeat}`
      // slide= '`slideleft${slideRepeat}`'
    }
    this.setState({ recipeArr: [], cookingDate: [], inputs, loader: true, slideClass: slide, slideRepeat: !slideRepeat})
    if(groceryClass !== 'active')
      this.fetchPlan(dayDates)
    else {
      this.groceryPlan(dayDates)
    }
  }

  setServingSize(index) {
    const { recipeArr, dayDates } = this.state
    const servingSize = parseInt(recipeArr[index].servingSize,0)
    window.analytics.track('Change Serving Sizes', {
      startDate: `${dayDates[0].fullDay} ${dayDates[0].date}-${dayDates[0].Month}-${dayDates[0].Year}`,
      endDate: `${dayDates[6].fullDay} ${dayDates[6].date}-${dayDates[6].Month}-${dayDates[6].Year}`
    });
    return(
      <ul className="set-serving">		
        <li>
          <a className="mb-hide red-circle" onClick={()=>this.removeServing(index, servingSize)}>
            <SubIcon />
          </a></li>
        <li> <h6> {servingSize} Servings </h6></li>
        <li><a className="mb-hide Green-circle" onClick={()=>this.addServing(index, servingSize)}>
        

          <PlusIcon />
        </a></li>
      </ul>
     
    )
  }

  useHistoryData(dataProps, slideClass) {
    this.setState({ ...dataProps.bottomBtnState, slideClass})
  }

  groceryPlan(dayDates) {
    this.setState({ groceryClass: 'active', dashClass: '' });
    const inputs = {}
    const url = RoutingConstants.groceryList;
    const startDate = `${dayDates[0].Year}-${dayDates[0].Month}-${dayDates[0].date}`;
    const endDate = `${dayDates[6].Year}-${dayDates[6].Month}-${dayDates[6].date}`;
    inputs.week_start_date = startDate;
    inputs.week_end_date = endDate;
    post(url, inputs).then((response) => {
      this.setState({
        groceryList: response.result.groceries,
        customItems: response.result.custom,
        dayDates, 
        loader: false,
        error: false
      });
    }).catch((err) => {
      this.setState({ groceryList: [], recipeList: [], dayDates, loader: false, error: true });
      if (err.error === 'token_expired') {
        localStorage.removeItem('token');
        history.push('/login');
      }
    });
  }


  editPlan(inputs, cookingDate) {
    const url = RoutingConstants.editWeeklySchedule
    const { recipeArr, dayDates, defaultServingSize } = this.state

    post(url,inputs).then(()=>{
      inputs.delete = []
      inputs.update = []
      inputs.add = []
      inputs.deleted_cooking_ids=[]
      this.setState({ 
        recipeArr, deleteRecipeIndex: [] , editRecipe: false, 
        bottomText: false, mbClass: '', servingArr:[], cookingDate, inputs, loader: false 
      })
      window.analytics.track('Plan Changes Saved', {
        startDate: `${dayDates[0].fullDay} ${dayDates[0].date}-${dayDates[0].Month}-${dayDates[0].Year}`,
        endDate: `${dayDates[6].fullDay} ${dayDates[6].date}-${dayDates[6].Month}-${dayDates[6].Year}`
      });
    }).catch((err)=>{
      err.result = {serving_size: defaultServingSize}
      this.catchError(err, dayDates)
    })
  }

  catchError(err, dayDates) {
    let defaultServingSize = 2
    if (err.error === 'token_expired') {
      localStorage.removeItem('token');
      history.push('/login', {location: this.state.location});
    }
    if(err.result && err.result.serving_size) {
      defaultServingSize = err.result.serving_size
    }
    this.setState({error: true, dayDates, defaultServingSize, loader: false })
  }

  fetchPlan(dayDates) {
    const  { servingArr, deleteRecipeIndex, cookDayArr, recipeArr, cookingDate} = this.state
    const { cookArr } = this.state
    if(sessionStorage.getItem('path') === 'dashboard' && this.props.location.state &&
    this.props.location.state.editRecipe !== undefined
    ) {
      this.swapRecipeData(dayDates)
    } else{
      const url = RoutingConstants.getWeeklySchedule;
      const { inputs } = this.state
      const startDate = `${dayDates[0].Year}-${dayDates[0].Month}-${dayDates[0].date}`;
      const endDate = `${dayDates[6].Year}-${dayDates[6].Month}-${dayDates[6].date}`;
      inputs.week_start_date = startDate;
      inputs.week_end_date = endDate;
      inputs.user_weekly_schedule_id = ''
      post(url, inputs).then(response =>{
        const data = response.result.userWeeklyScheduleData
        if(response.result.userWeeklyScheduleId)
          inputs.user_weekly_schedule_id = response.result.userWeeklyScheduleId
        if(data && data.length > 0) { 
          data.map((item) => {
            cookingDate.push({cooking_date: item.cooking_date, id:item.id})
            item.cooking_date_recipe.map(items => {
              let sideHands = 0
              let sidePassive = 0
              let calories = 0
              let totalHands = items.recipe.hands_on_time || 0
              const recipePassive = items.recipe.passive_time || 0
              const totalRecipeTime = totalHands + recipePassive
              let totalSideTime = 0
              const recipeItm  = {}
              recipeItm.recipe =  items.recipe
              recipeItm.recipe.recipe_id =  items.recipe.id
              recipeItm.recipe.id =  items.id
              recipeItm.recipe.recipe_sides = []
              const calorieIndex = items.recipe.recipe_nutrition_data.findIndex(x=>x.nutrition_info_attribute.attribute === 'Calories')
              if(calorieIndex > -1) {
                calories = parseInt(items.recipe.recipe_nutrition_data[calorieIndex].value,0)
              }
              if(items.recipe_sides!==null) {
                recipeItm.recipe.recipe_sides.push({recipe: items.recipe_sides})
              }
              if (items.recipe_sides!== null ) {
                sideHands = items.recipe_sides.hands_on_time || 0
                sidePassive = items.recipe_sides.passive_time || 0
                totalHands += sideHands
                totalSideTime = sideHands + sidePassive
                const caloriesIndex = items.recipe_sides.recipe_nutrition_data.findIndex(x=>x.nutrition_info_attribute.attribute === 'Calories')
                if(caloriesIndex > -1) {
                  calories += parseInt(items.recipe_sides.recipe_nutrition_data[caloriesIndex].value,0)
                }
              } 
              
              let totalTime = compareRecipeTime(totalRecipeTime, totalSideTime);
              totalTime = compareRecipeTime(totalTime, totalHands);
              totalHands = getTimeStamp(totalHands)
              totalTime = getTimeStamp(totalTime)
            
              recipeItm.image = items.recipe.image
              recipeArr.push({ 
                cooking_date:item.cooking_date, 
                servingSize:items.recipe_serving_size,
                items:recipeItm,
                totalHands,
                totalTime,
                calories
              })
              return recipeArr
            })
            return item
          })
        }
        
        this.setState({ 
          cookArr, recipeArr, cookingDate, lastStep: true, servingArr,
          deleteRecipeIndex, cookDayArr, inputs, dayDates, error: false,
          recipeIdArr:[], defaultServingSize: response.result.serving_size, loader: false
        })
      }).catch((err)=>{
        this.catchError(err, dayDates)
      })
    }
  }

  swapRecipeData(dayDates) {
    let  { editRecipe, servingArr, deleteRecipeIndex, cookDayArr, recipeArr, cookingDate} = this.state
    let  { inputs, defaultServingSize } = this.state
    editRecipe = true
    servingArr = this.props.location.state.servingArr
    deleteRecipeIndex = this.props.location.state.deleteRecipeIndex
    cookDayArr = this.props.location.state.cookDayArr || []
    inputs = this.props.location.state && this.props.location.state.inputs
    recipeArr = this.props.location.state.recipeArr
    defaultServingSize = this.props.location.state.defaultServingSize
    const recipeIndex = this.props.location.state && this.props.location.state.recipeIndex
    cookingDate = this.props.location.state.cookingDate
    if(this.props.location.state.action === 'add' &&
    this.props.location.state.recipeArr.length > 0) {
      if(this.props.location.state.cookingDate.length === 0) {
        cookingDate.push({ cooking_date: recipeArr[0].cooking_date})
      }
      const length = recipeArr.length-1
      const cookDate = recipeArr[length].cooking_date
      const recipeId = recipeArr[length] && recipeArr[length].items.recipe_id
      recipeArr[length].items.recipe.recipe_id = recipeId
      inputs.add.push({cooking_date: cookDate, recipe:[{
        recipe_id:recipeId,
        side_dish_id:recipeArr[length] && recipeArr[length].items.recipe.recipe_sides.length > 0 ?  recipeArr[length].items.recipe.recipe_sides[0].recipe ? recipeArr[length].items.recipe.recipe_sides[0].recipe.id : null : null,
        serving_size:recipeArr[length].servingSize,
      }]})
      window.analytics.track('Add Recipe to Plan', {
        startDate: `${dayDates[0].fullDay} ${dayDates[0].date}-${dayDates[0].Month}-${dayDates[0].Year}`,
        endDate: `${dayDates[6].fullDay} ${dayDates[6].date}-${dayDates[6].Month}-${dayDates[6].Year}`,
        recipeName: recipeArr[length].items.recipe.name
      });
    } 
   
    const previousRecipeData = this.props.location.state && this.props.location.state.previousData
    const sideDishId = recipeArr[recipeIndex] && recipeArr[recipeIndex].items.recipe.recipe_sides.length > 0 && recipeArr[recipeIndex].items.recipe.recipe_sides[0].recipe.id
    if(previousRecipeData){
      const cIndex = inputs.add.findIndex(x=>x.cooking_date === previousRecipeData.cooking_date)
      if(cIndex > -1) {
        inputs.delete.push(previousRecipeData.items.recipe.id) 
        const index = inputs.add[cIndex].recipe.findIndex(x=>x.recipe_id === previousRecipeData.items.recipe.recipe_id)
        if(index > -1) {
          inputs.add[cIndex].recipe[index].recipe_id=recipeArr[recipeIndex].items.recipe_id
          inputs.add[cIndex].recipe[index].side_dish_id=sideDishId
        } 
      }else {
        const updateIndex = inputs.update.findIndex(x=>x.id === previousRecipeData.items.recipe.id)
        if(updateIndex > -1) {
          inputs.update[updateIndex].recipe_id=recipeArr[recipeIndex].items.recipe_id 
          inputs.update[updateIndex].side_dish_id=sideDishId
        } else {
          inputs.update.push({
            id:previousRecipeData.items.recipe.id,
            recipe_id:recipeArr[recipeIndex].items.recipe_id,
            side_dish_id:sideDishId,
            serving_size: recipeArr[recipeIndex].servingSize
          })
        }
      }}
    if(recipeArr[recipeIndex] && recipeArr[recipeIndex].items.recipe.recipe_sides.length > 0 && recipeArr[recipeIndex].items.recipe.recipe_sides[0].recipe_nutrition_data){
      recipeArr[recipeIndex].items.recipe.recipe_sides[0].recipe.recipe_nutrition_data =  recipeArr[recipeIndex].items.recipe.recipe_sides[0].recipe_nutrition_data
      
    }
    recipeArr = recipeArr.sort(sortByKey('cooking_date','asc'))
    this.setState({ 
      recipeArr, cookingDate, lastStep: true, editRecipe, servingArr, 
      editRecipeButton: true, defaultServingSize, deleteRecipeIndex, cookDayArr, loader: false,
      inputs, recipeIdArr: [], error: false, groceryClass:'',dayDates
    })
  }

  addServing(index, servingSize) {
    const { recipeArr } = this.state
    let { inputs } = this.state
    if(servingSize < 100) {
      recipeArr[index].servingSize =  servingSize +1
      inputs = this.updateRecipe(index,servingSize+1)
    }
    this.setState({ recipeArr, bottomText: true, mbClass:'', inputs })
  }
  
  updateRecipe(index,servingSize) {
    const { recipeArr, inputs } = this.state
    const id = recipeArr[index].items.recipe.id
    const recipeId = recipeArr[index].items.recipe.recipe_id
    let sideDishId = null;
    if(recipeArr[index].items.recipe.recipe_sides.length > 0) {
      sideDishId = recipeArr[index].items.recipe.recipe_sides[0].recipe.id
    }

    let servingIndex = -1 ;
    let changeCookIndex = -1;
    if(inputs.add && inputs.add.length > 0){
      inputs.add.map((item)=>{
        changeCookIndex = item.recipe.findIndex(x=>x.recipe_id===recipeArr[index].items.recipe.recipe_id)
        if(changeCookIndex > -1) {
          item.recipe[changeCookIndex].serving_size = servingSize
        }
        return null
      })}

    servingIndex = inputs.update.findIndex(x=>x.id === id)

    if(servingIndex > -1){
      inputs.update[servingIndex].serving_size = servingSize
    }
    else if(changeCookIndex === -1) {
      inputs.update.push({ 
        id, recipe_id: recipeId, serving_size: servingSize, side_dish_id: sideDishId
      })
    }
    return inputs;
  }

  removeServing(index, servingSize) {
    const { recipeArr } = this.state
    let { inputs } = this.state
    if(servingSize > 1) { 
      recipeArr[index].servingSize =  servingSize - 1
      inputs = this.updateRecipe(index, servingSize-1)
    }
    this.setState({ recipeArr, bottomText:true, mbClass:'', inputs })
  }

  recipeTime(days) {
    const { recipeArr, recipeIdArr } = this.state
    let totalCookTime = 0
    let totalCookHands = 0
    
    recipeArr.map((item) => {
      if(item.cooking_date === days.cooking_date ){
        totalCookTime +=  formatTimeStamp(item.totalTime) 
        totalCookHands +=  formatTimeStamp(item.totalHands )
        if(recipeArr.length > recipeIdArr.length)
          recipeIdArr.push(item.items.recipe.recipe_id)
      }
      return totalCookTime
    })
    
    totalCookTime = getTimeStamp(totalCookTime)
    totalCookHands = getTimeStamp(totalCookHands)
   
    return `${totalCookTime},${totalCookHands}, ${recipeIdArr}`
  }

  viewPlan() {
    const {recipeArr, lastStep, cookArr, cookingDate, editRecipe, servingArr, dayDates } = this.state
    let weekDate = moment().format('YYYY-MM-DD')
    const weekIndex = dayDates.findIndex(x=>x.date === moment().format('DD') && x.Month=== moment().format('MM') && x.Year === moment().format('YYYY'));
    if(weekIndex === -1) {
      weekDate = `${dayDates[0].Year}-${dayDates[0].Month}-${dayDates[0].date}`
    }

    const newCookingDate=[];
    dayDates.map((inDD)=>{
      newCookingDate.push({cooking_date:`${inDD.Year}-${inDD.Month}-${inDD.date}`,id:''})      
      return null;
    })   
    const setCookingDate = _.sortBy(_.unionBy(cookingDate, newCookingDate, "cooking_date"),"cooking_date",['asc']);

    // let count = 0
    return(
      <div>
        {/* {cookingDate.length > 0 ? */}
        {setCookingDate.length > 0 ?
          setCookingDate.map((days)=>{
            
            const cooking = moment(days.cooking_date).format('dddd M/D')
            
            const cookindex = recipeArr ? recipeArr.findIndex(x=>x.cooking_date === days.cooking_date): -1
           
            if(cookindex >= -1) {
              // count +=1
              let totalCookTime =this.recipeTime(days)
              totalCookTime = totalCookTime.split(',')
              // const totalHands = totalCookTime[1]
              const recipeIdArr = totalCookTime[2]
              totalCookTime = totalCookTime[0]
              return (
                <div key={cooking}>
                  <div className="dash-cntnt-dtail-hdr cook-day-top">
                    <div className="heade_title topBorderNone">
                      <span>
                        <h4 className="mb-hide">{cooking}</h4>
                        <h4 className="mb-only"> {cooking} </h4>
                      </span>
                      {/* do not remove below code */}
                      {/* <span className="icns-rgt">
                        <img src="/images/chef.png" alt="chef"/>
                        <div className="1h">
                          <span> {totalHands} </span>
                          <b className="mb-hide"> Hands On </b>
                        </div>
                      </span>
                      <span className="icns-rgt">
                        <img src="/images/time.svg" alt="chef"/>
                        <div className="1h">
                          <span> {totalCookTime}</span>
                          <b className="mb-hide"> Total Time </b>
                        </div>
                      </span> */}
                    </div>
                  </div>
                  {days.id === '' ?
                    editRecipe === true ?
                      <RecipeData
                        length={days.cooking_date}
                        recipeArr={recipeArr}
                        lastStep={lastStep}
                        // cookingDate={cookingDate}
                        cookingDate={setCookingDate}
                        cookArr={cookArr}
                        setServingSize={(index)=>this.setServingSize(index)}
                        location="dashboard"
                        recipeIdArr={recipeIdArr}
                        editRecipe={editRecipe}
                        editServing={(index)=>this.editServing(index)}
                        editServingIndex={servingArr}
                        deleteRecipe={(index, id, recipeId)=>this.deleteRecipe(index, id, recipeId)}
                        cancelAction={(index, recipeId)=>this.cancelAction(index, recipeId)}
                        showCookDay={(index)=>this.showCookDay(index)}
                        swapRecipe={(index)=>this.swapRecipe(index)}
                        viewRecipe={(totalHandsOn, totalTime, totalPassive, 
                          recipeName, sideName, recipe, servingSize)=>this.viewRecipe(
                          totalHandsOn, totalTime, totalPassive, recipeName, sideName, 
                          recipe, servingSize 
                        )}
                        {...this.state}
                      />                        
                      : <div className="noMeals"><span>No Meals Planned</span></div>
                    :
                    cookindex > -1 ?  
                      <RecipeData
                        length={days.cooking_date}
                        recipeArr={recipeArr}
                        lastStep={lastStep}
                        // cookingDate={cookingDate}
                        cookingDate={setCookingDate}
                        cookArr={cookArr}
                        setServingSize={(index)=>this.setServingSize(index)}
                        location="dashboard"
                        recipeIdArr={recipeIdArr}
                        editRecipe={editRecipe}
                        editServing={(index)=>this.editServing(index)}
                        editServingIndex={servingArr}
                        deleteRecipe={(index, id, recipeId)=>this.deleteRecipe(index, id, recipeId)}
                        cancelAction={(index, recipeId)=>this.cancelAction(index, recipeId)}
                        showCookDay={(index)=>this.showCookDay(index)}
                        swapRecipe={(index)=>this.swapRecipe(index)}
                        viewRecipe={(totalHandsOn, totalTime, totalPassive, 
                          recipeName, sideName, recipe, servingSize)=>this.viewRecipe(
                          totalHandsOn, totalTime, totalPassive, recipeName, sideName, 
                          recipe, servingSize 
                        )}
                        {...this.state}
                      />
                      : <div className="noMeals"><span>No Meals Planned</span></div>
                  }
                  {
                    editRecipe &&
                    <AddMoreRecipe addNewRecipe={(cookDate)=>this.addNewRecipe(cookDate)} cookDate={days.cooking_date}/>
                  }
                </div>
              )
            }
            return null;
          })
          :
          editRecipe &&
            <AddMoreRecipe addNewRecipe={(cookDate)=>this.addNewRecipe(cookDate)} cookDate={weekDate}/>
        }
      </div>
    )
  }
  
  editRecipe() {
    const { editRecipe, recipeArr } = this.state
    localStorage.setItem('editRecipe',JSON.stringify(recipeArr));
    this.setState({ editRecipe : !editRecipe, editRecipeButton: true, error: false })
  }

  editServing(index) {
    const { servingArr, recipeArr } = this.state
    const servingIndex = servingArr.findIndex(x=>x.index === index)
    if(servingIndex === -1) {
      servingArr.push({ index, servingSize: recipeArr[index].servingSize})
    }
    this.setState({ servingArr })
  }

  addNewRecipe(cookDate) {
    const {
      cookingDate, lastStep, cookArr, recipeArr, servingArr, deleteRecipeIndex,
      cookDayArr, inputs,  dayDates, defaultServingSize
    } = this.state
    const editWeek = []
    editWeek.push({servingArr, deleteRecipeIndex, cookDayArr })
    localStorage.setItem('editRecipes',JSON.stringify(recipeArr));
    history.push('/chooseRecipe',{
      inputs, path:'dashboard', cookingDates: cookingDate, defaultServingSize,
      lastStep, cookArr, recipes: recipeArr, editWeek, action:'add', cookDate, dayDates
    })
  }

  deleteRecipe(index, id, recipeId) {
    const { recipeArr, deleteRecipeIndex, servingArr, inputs } = this.state
    const servingIndex = servingArr.findIndex(x=>x.index === index)
    if(servingIndex > -1) {
      servingArr.splice(servingIndex, 1)
    }
    const cIndex = inputs.add.findIndex(x=>x.cooking_date === recipeArr[index].cooking_date)
    let addIndex;
    if(cIndex > -1) {
      addIndex = inputs.add[cIndex].recipe.findIndex(x=>x.recipe_id === recipeId)
    }
    const updateIndex = inputs.update.findIndex(x=>x.id === id)
    if(addIndex > -1) {
      inputs.add[cIndex].recipe.splice(addIndex,1)
      if(inputs.add[cIndex].recipe.length === 0) {
        inputs.add.splice(cIndex,1)
      }
    } else if(updateIndex > -1) {
      inputs.update.splice(updateIndex,1)
    }
    deleteRecipeIndex.push({index})
    if(cIndex === -1) {inputs.delete.push(id)}
    recipeArr[index].tHands = recipeArr[index].totalHands
    recipeArr[index].tTime = recipeArr[index].totalTime
    recipeArr[index].totalTime = '0m'
    recipeArr[index].totalHands = '0m'
    this.setState({ deleteRecipeIndex, servingArr, bottomText: true, mbClass: '', recipeArr})
  }
  
  cancelAction(index, recipeId) {
    let {  mbClass, bottomText } = this.state
    const { servingArr, recipeArr, deleteRecipeIndex, inputs } = this.state
    
    const servingIndex = servingArr.findIndex(x=>x.index === index)
    if(servingIndex > -1) {
      servingArr.splice(servingIndex, 1)
    }
    if(servingArr.length > 0) {
      mbClass= ''
      bottomText = true
    } else{
      mbClass = 'mb-hide'
      bottomText = false
    }
    if(inputs.delete.includes(recipeId)) {
      const deleteIndex = inputs.delete.indexOf(recipeId)
      inputs.delete.splice(deleteIndex, 1)
    }
    recipeArr[index].totalHands = recipeArr[index].tHands
    recipeArr[index].totalTime = recipeArr[index].tTime
    const deleteIndex = deleteRecipeIndex.findIndex(x=>x.index === index)
    if(deleteIndex > -1) {
      deleteRecipeIndex.splice(deleteIndex, 1)
    }
    this.setState({ deleteRecipeIndex, servingArr, bottomText, mbClass, recipeArr })
  }

  showCookDay(index) {
    const { showCook } = this.state
    this.setState({ showCook: !showCook, cookIndex: index})
  }

  changeCookDay(event, data, cookDate) {
    const { cookDayArr, inputs, recipeArr, dayDates } = this.state
    let { cookingDate } = this.state
    const cookIndex = cookDayArr.findIndex(x=>x.index === event)
    if(cookIndex === -1) {
      cookDayArr.push({ index: event})
    }  
    if(cookingDate.findIndex(x=> x.cooking_date === cookDate) === -1) {
      cookingDate.push({ cooking_date: cookDate})
      cookingDate = cookingDate.sort(sortByKey('cooking_date','asc'))
    }
    recipeArr[event].c_date = recipeArr[event].cooking_date
    recipeArr[event].cooking_date = cookDate
    const sideDishId= recipeArr[event].items.recipe.recipe_sides.length > 0 ? recipeArr[event].items.recipe.recipe_sides[0].recipe.id:null
    let changeCookIndex;
    if(inputs.add.length>0) {
      inputs.add.map((item, index)=>{
        changeCookIndex = item.recipe.findIndex(x=>x.recipe_id===recipeArr[event].items.recipe.recipe_id)
        if(changeCookIndex > -1) {
          item.recipe.splice(changeCookIndex, 1)
        }
        if(item.recipe.length ===0) {
          inputs.add.splice(index,1)
        }
        return null
      })
    }
    if(recipeArr[event].items.recipe.id !== null) {
      inputs.delete.push(recipeArr[event].items.recipe.id)
    }
    inputs.add.push({
      cooking_date: cookDate,
      recipe: [{
        recipe_id: recipeArr[event].items.recipe.recipe_id,
        side_dish_id: sideDishId,
        serving_size: recipeArr[event].servingSize
      }]
    })
    const updateIndex = inputs.update.findIndex(x=>x.id=== recipeArr[event].items.recipe.id)
    if(updateIndex>-1){
      inputs.update.splice(updateIndex,1)
    }
    window.analytics.track('Change Cook Day', {
      startDate: `${dayDates[0].fullDay} ${dayDates[0].date}-${dayDates[0].Month}-${dayDates[0].Year}`,
      endDate: `${dayDates[6].fullDay} ${dayDates[6].date}-${dayDates[6].Month}-${dayDates[6].Year}`
    });
    this.setState({ showCook: false, recipeArr, cookingDate, bottomText: true, inputs})
  }

  swapRecipe(index) {
    const {
      cookingDate, lastStep, cookArr, recipeArr, servingArr, dayDates,
      deleteRecipeIndex, cookDayArr, inputs,defaultServingSize
    } = this.state
    const editWeek = []
    const cookDate = recipeArr[index].cooking_date
    editWeek.push({servingArr, deleteRecipeIndex, cookDayArr })
    const swapRecipeName = recipeArr[index].items.recipe.name
    window.analytics.track(' Swap Recipe', {
      startDate: `${dayDates[0].fullDay} ${dayDates[0].date}-${dayDates[0].Month}-${dayDates[0].Year}`,
      endDate: `${dayDates[6].fullDay} ${dayDates[6].date}-${dayDates[6].Month}-${dayDates[6].Year}`,
      swapRecipeName
    });
    localStorage.setItem('editRecipes',JSON.stringify(recipeArr));
    history.push('/chooseRecipe',{
      index, inputs, path:'dashboard', cookingDates: cookingDate, defaultServingSize,
      lastStep, cookArr, recipes: recipeArr, editWeek, action:'swap', cookDate, dayDates
    })
  }
  
  createPlan(weekDate) {
    const { defaultServingSize, dayDates } = this.state
    const cDate = []
    sessionStorage.setItem('plan','create');
    // history.push('/selectMeals',{dayDates: weekDate, servingSize: defaultServingSize})
    // loop for adding cooking
    dayDates.map(item=>{  
      const itemDate = `${item.Year}-${item.Month}-${item.date}`;
      cDate.push({cooking_date:itemDate})
      return cDate
    })
    history.push('/chooseRecipe',{dayDates: weekDate, servingSize: defaultServingSize, cookingDates: cDate})

  }

  viewRecipe(totalHandsOn, totalTime, totalPassive, rname, sname, recipe, totalServing) {
    const { editRecipe, defaultServingSize, dayDates } = this.state
    if(!editRecipe) {
      window.scroll(0,0)
      history.push('/viewRecipe',{ 
        path:'dashboard', totalHandsOn, totalTime, totalPassive, rname, sname, 
        recipe, dayDates, totalServing,defaultServingSize, state:this.state
      })
    }
  }

  showFeedback() {
    this.setState({ showFeedbackPopup: true})
  }

  submitFeedback(inputs) {
    this.setState({ loader: true })
    const url = RoutingConstants.userFeedback
    localStorage.setItem('feedback','submit')
    post(url, inputs).then(()=>{
      this.setState({ loader: false})
    }).catch(()=>{
      this.setState({ loader: false})
    })
  }

  updateFeedback(inputs) {
    this.setState({ loader: true })
    const url = RoutingConstants.userFeedback
    put(url, inputs).then(()=>{
      localStorage.setItem('feedback','submit')
      this.setState({ loader: false})
    }).catch(()=>{
      this.setState({ loader: false})
    })
  }

  removeFeedbackPopup() {
    this.setState({ showFeedbackPopup: false})
    localStorage.removeItem('analytics')
  }

  /* authentication behaviour end */
  render() {
    const { 
      dayDates, reminder, subscriptionPlan, planType, editRecipe, groceryClass, 
      dashClass, bottomText, mbClass, showCook, cookIndex, cookDayArr, error, 
      loader, showFeedbackPopup, slideClass
    } = this.state;
    const editRecipeClass = editRecipe ? 'cal-days-cntnt dishes-row dashboard-page  middle-cntnt btm-margn': 'cal-days-cntnt dishes-row dashboard-page middle-cntnt'
    if (!this.props.location.state && localStorage.type ==='checkUser') {
      localStorage.removeItem('token');
      return <Login location='/dashboard' />;
    }
    return (
      <div className={`wrapper ${slideClass}`} >
        <style>
          {`
          @media(min-width:320px) and (max-width:767px){
          #PureChatWidget.purechat.purechat-image-only.purechat-widget-collapsed.purechat-bottom-right .purechat-collapsed-image, #PureChatWidget.purechat.purechat-image-only.purechat-widget-collapsed.purechat-top-right .purechat-collapsed-image{
            left:5px !important; right: auto !important;
          }
          #PureChatWidget.purechat.purechat-widget-collapsed{
            left: 20px important;
            right: auto !important;
            top: 15px !important;
          }
        }
          `}
        </style>
        {
          loader &&
              <div className="loading">
                <Loader />
                <div className="modal-backdrop fade in" />
              </div>
        }
        <div className="mb-hide">
          <DashHeader
            dashClass={this.state.dashClass}
            groceryClass={this.state.groceryClass}
            dayDates={dayDates}
            subscriptionPlan={subscriptionPlan}
            previous={this.state.previous}
            planExist={planType}
            reminder={reminder}
          />
        </div>
        <section className="select-meal-cntnt choose-recipe-cntnt">
          <div className="dishes-row-box nw-dash-cntnt">
            <div className="container">
            
              <div className="cal-days-cntnt dishes-row dashboard-page no-print">
                <WeekCalenderFullArea 
                  dayDates={dayDates}
                  getWeekData={(dayDate, selectedWeek)=>this.getWeekData(dayDate, selectedWeek)}
                  editRecipe={()=>this.editRecipe()}
                  editRecipeButton={editRecipe}
                  onPressCancel={()=>this.onPressCancel()}
                  error={error}
                  groceryClass={groceryClass}
                />
              </div>
              {showFeedbackPopup &&
                <div>
                  <FeedbackPopup 
                    submitFeedback={(data)=>this.submitFeedback(data)}
                    updateFeedback={(data)=>this.updateFeedback(data)}
                    removeFeedbackPopup={()=>this.removeFeedbackPopup()}
                  />
                  <div className="modal-backdrop fade in"/>
                </div>
              }
              <div className={editRecipeClass}>
                { groceryClass !=='active' ?
                  error  ? 
                    <BuildNewPlan 
                      dayDates={dayDates}
                      createPlan={(weekDate)=>this.createPlan(weekDate)}
                    />
                    :
                    this.viewPlan()
                  :
                  <GroceryList 
                    dayDates={dayDates} 
                    {...this.state} 
                    buildNextWeekPlan={(weekDate)=>this.createPlan(weekDate)}
                  />
                }
              </div>
              {
                showCook &&
                <div>
                  <EditCookDay 
                    cancelCookDay={()=>this.showCookDay()}
                    saveCookDay={(event, data, cookDate)=>this.changeCookDay(event, data, cookDate)}
                    indexValue={cookIndex}
                    location="dashboard"
                    dayDates={dayDates}
                  />
                  <div className="modal-backdrop fade in" />
                </div>
              }
              <DashboardBottom 
                editRecipe={editRecipe}
                bottomText={bottomText}
                mbClass={mbClass}
                onPressCancel={()=>this.onPressCancel()}
                onPressSave={()=>this.onPressSave()}
                cookDayArr={cookDayArr}
                path="dashboard"
                dayDates={dayDates}
                groceryClass={groceryClass}
                dashClass={dashClass}
              />
            </div>
          </div>
        </section>
            
      </div>
    );
  }
}

export default Dashboard;
