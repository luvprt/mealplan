/*
Component Name: WeekCalanderFullArea component
Contains : RecipeForWeekCalander, BuildPlanScreen
Includes : Services for custom calander
Default screen: Dashboard
*/
import React, { Component } from 'react';
import { getSelectedWeekDates, getWeekDays } from 'lib/services';
import moment from 'moment';
import BuildPlanScreen from '../plans/BuildPlanScreen';
import RecipeForWeekCalander from './RecipeForWeekCalander';
import GroceryList from '../groceryList/GroceryList';


class WeekCalanderFullArea extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentWeek: true,
      weekStartEnd: {},
      dayDates: this.props.dayDates || {},
      color: this.props.color || null,
      cookingDates: this.props.cookingDates,
      dinnerDates: this.props.dinnerDates,
      lunchDates: this.props.lunchDates,
      showBuilPlanComponent: false,
      mealData: this.props.mealData || null,
      length: this.props.length || null,
      alertMessage: false,
      previousWeekEvents: 'auto',
      editButton: true,
      startDate: false,
      count: 0,
      week: 'current',
      error: this.props.error,
      ...this.props,
    };
  }

  componentDidMount() {
    this.weekData();
  }

  // unsafe
  // eslint-disable-next-line
  UNSAFE_componentWillReceiveProps(props) {
    let {
      editButton, error, displayCooking, startDate, week, alertMessage, previousWeekEvents,
    } = this.state;
    const { currentWeek, selectedWeek, groceryClass } = this.state;
    if (props.error === true) {
      editButton = false;
      displayCooking = false;
      week = '';
      if (selectedWeek === 'previousWeek' && startDate === false) {
        error = false;
        startDate = false;
        week = 'previous';
        alertMessage = true;
      } else if (selectedWeek === 'nextWeek') {
        error = true;
        week = 'next';
        startDate = true;
        alertMessage = false;
      }
      if (currentWeek.length > 0) {
        if (currentWeek[0].date === props.dayDates[0].date) {
          editButton = false;
          startDate = false;
          error = true;
          week = 'current';
          alertMessage = false;
          previousWeekEvents = 'auto';
        }
      } else if (currentWeek === true) {
        error = true;
        editButton = false;
        alertMessage = false;
      } else {
        error = true;
      }
    } else if (props.error === false) {
      displayCooking = true;
      if (selectedWeek === 'previousWeek' && startDate === false) {
        editButton = false;
        week = 'previous';
        alertMessage = true;
        previousWeekEvents = 'none';
      } else if (selectedWeek === 'nextWeek' && startDate === false) {
        editButton = true;
        startDate = true;
        week = 'next';
        alertMessage = false;
        previousWeekEvents = 'auto';
      } else {
        editButton = true;
      }
      if (currentWeek.length > 0) {
        // console.log('currentWeek', currentWeek.length, dayDates[0]);
        if (currentWeek[0].date === props.dayDates[0].date) {
          editButton = true;
          startDate = false;
          week = 'current';
          alertMessage = false;
          previousWeekEvents = 'auto';
        } else if (currentWeek === true) {
          editButton = true;
          alertMessage = false;
        }
      }
    }
    if (groceryClass === 'active') {
      alertMessage = false;
      editButton = false;
    }
    const weekdate = parseInt(props.dayDates[0].date, 0)
    const currentDate = parseInt(moment().startOf('week').format('DD'),0)
    const currentMonth = parseInt(moment().startOf('week').format('MM'),0)
    const weekMonth = parseInt(props.dayDates[0].Month,0)
    if(currentMonth > weekMonth) {
      if(weekdate !== currentDate && groceryClass !== 'active') {
        alertMessage = true
        editButton = false
      } else{
        alertMessage = false
        if(groceryClass !== 'active') {
          editButton = true
        }
        previousWeekEvents = 'auto'
      }
    } else if(currentMonth === weekMonth) {
      if(weekdate < currentDate && groceryClass !== 'active') {
        alertMessage = true
        editButton = false
        previousWeekEvents = 'none'
      } else if(weekdate < currentDate && groceryClass === 'active') {
        previousWeekEvents = 'none'
      } else {
        alertMessage = false
        previousWeekEvents = 'auto'
        if(groceryClass !== 'active') {
          editButton = true
        }
      }
    }
    const startWeekDate = `${props.dayDates[0].fullMonth} ${props.dayDates[0].date}`;
    const endDate = `${props.dayDates[6].fullMonth} ${props.dayDates[6].date}`;
    this.setState({
      length: props.length,
      mealData: props.mealData,
      cookingDates: props.cookingDates,
      dinnerDates: props.dinnerDates,
      lunchDates: props.lunchDates,
      editButton,
      displayCooking,
      startDate,
      week,
      alertMessage,
      previousWeekEvents,
      batchDates: props.cookDate,
      error,
      currentWeek,
      dayDates: props.dayDates || this.state.weekDates,
      startWeekDate,
      endDate,
      ...props,
    });

  }

  onClickCloseBtn() {
    const {
      dayDates, mealData, cookingDates, dinnerDates, lunchDates,
    } = this.state;
    this.setState({
      showBuilPlanComponent: false,
    });
    this.props.getWeekData(dayDates, mealData, cookingDates, dinnerDates, lunchDates);
  }

  onClickSaveBtn(mealData) {
    const {
      dayDates, cookingDates, dinnerDates, lunchDates,
    } = this.state;
    this.setState({
      showBuilPlanComponent: false,
      mealData,
    });
    this.props.getWeekData(dayDates, mealData, cookingDates, dinnerDates, lunchDates);
  }

  getWeeklySchedule(value, mealData, cookingDates, dinnerDates) {
    if (value) {
      this.setState({
        showBuilPlanComponent: true,
        mealData,
        cookingDates,
        dinnerDates,
      });
    }
  }

  weekData() {
    let weekStartEnd = {};
    let dayDates = {};
    let startWeekDate;
    let endDate;
    if (this.state.currentWeek === true) {
      weekStartEnd = getSelectedWeekDates('currentWeek');
      dayDates = getWeekDays(weekStartEnd);
      startWeekDate = `${dayDates[0].fullMonth} ${dayDates[0].date}`;
      endDate = `${dayDates[6].fullMonth} ${dayDates[6].date}`;
      this.setState({
        weekStartEnd,
        dayDates: this.props.dayDates || dayDates,
        weekDates: dayDates,
        startWeekDate,
        endDate,
      });
    }
  }

  /* condition based selection and view for weekly plan calander */
  async loadWeek(selectedWeek, weekStartEndPoints) {
    const {
      mealData, cookingDates, dinnerDates, lunchDates, reminder, pathname, inputs
    } = this.state;
    if (!reminder) {
      let { dayDates, alertMessage } = this.state;
      const currentWeekEnd = getSelectedWeekDates('currentWeek');
      const currentWeek = getWeekDays(currentWeekEnd);
      let switchType = '';
      const { startDate } = this.state;
      weekStartEndPoints.weekStart = inputs.week_start_date
      weekStartEndPoints.weekEnd = inputs.week_end_date
      const weekStartEnd = getSelectedWeekDates(selectedWeek, weekStartEndPoints);
      dayDates = await getWeekDays(weekStartEnd);

      this.setState({
        weekStartEnd,
        dayDates,
        cookingDates: [],
        dinnerDates: [],
        lunchDates: [],
        selectedWeek,
      });
      if (selectedWeek === 'previousWeek' && startDate === false) {
        this.setState({
          alertMessage: true,
          previousWeekEvents: 'none',
          count: 1,
          editButton: false,
          currentWeek: false,
          week: 'previous',
        });
        alertMessage = true;
        switchType = 'Backward';
      } else if (selectedWeek === 'nextWeek') {
        this.setState({
          alertMessage: false,
          previousWeekEvents: 'auto',
          startDate: true,
          editButton: true,
          count: 0,
          currentWeek,
          week: 'next',
        });
        alertMessage = false;
        switchType = 'Forward';
      }
      if (currentWeek[0].date === dayDates[0].date) {
        this.setState({
          startDate: false,
          editButton: true,
          currentWeek,
          count: 0,
          alertMessage: false,
          previousWeekEvents: 'auto',
          week: 'current',
        });
        alertMessage = false;
      }
      let path;
      // track week type
      if (pathname === '/dashboard') {
        path = 'Dashboard';
      } else {
        path = 'Grocery List:';
      }
      window.analytics.track(`${path} Switch Weeks ${switchType}`, {
        startDate: `${dayDates[0].fullDay} ${dayDates[0].date}-${dayDates[0].Month}-${dayDates[0].Year}`,
        endDate: `${dayDates[6].fullDay} ${dayDates[6].date}-${dayDates[6].Month}-${dayDates[6].Year}`,
      });
      this.props.getWeekData(dayDates, mealData, cookingDates, dinnerDates, lunchDates, selectedWeek, alertMessage);
    }
  }

  /* on edit plan component load */
  clickEditSchedule() {
    const { reminder } = this.state;
    if (!reminder && localStorage.getItem('test') !== 'test') {
      this.setState({
        showBuilPlanComponent: true,
      });
    }
  }

  swapRecipe() {
    const {
      mealData, dayDates, cookingDates, dinnerDates, lunchDates,
    } = this.state;
    this.props.getWeekData(dayDates, mealData, cookingDates, dinnerDates, lunchDates);
  }

  buildNextWeekPlan() {
    this.setState({
      show: 'display',
      showBuilPlanComponent: true,
      cookingDates: [],
      dinnerDates: [],
      lunchDates: [],
      alertMessage: false,
      newWeek: true,
    });
  }

  moveToDash(guestEmail, dayDates, mealData) {
    const { cookingDates, dinnerDates, lunchDates, } = this.state;
    this.setState({
      showBuilPlanComponent: false,
      dayDates,
      mealData,
      cookingDates: [],
      dinnerDates: [],
      lunchDates: [],
      editButton: true,
    });
    this.props.getWeekData(dayDates, mealData, cookingDates, dinnerDates, lunchDates);
  }

  render() {
    /* for rendring selected week endpoints and selected week dates */
    const {
      weekStartEnd, dayDates, mealData, cookingDates, color, length, dinnerDates, lunchDates,
      startWeekDate, endDate, groceryClass, loadData, reminder, planType, newWeek, error
    } = this.state;
    let c = 0;
    const dinnerDate = this.state.dinnerDates;
    const mainClass = groceryClass !== 'active' ? '' : 'swap-mode-dtail';
    return (
      <div className="col-lg-9 dash-main-cntnt" draggable={false}>
        <div className={`dash-cntnt-dtail ${mainClass}`}>
          <div className="dash-cntnt-dtail-hdr no-print">
            <h3>{startWeekDate} - {endDate}</h3>
            <div className="calndr-cntrls">
              <button
                onClick={() => this.loadWeek('previousWeek', weekStartEnd)}
                style={{ pointerEvents: this.state.previousWeekEvents }}
              >
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
                  <path d="M34.52 239.03L228.87 44.69c9.37-9.37 24.57-9.37 33.94 0l22.67 22.67c9.36 9.36 9.37 24.52.04 33.9L131.49 256l154.02 154.75c9.34 9.38 9.32 24.54-.04 33.9l-22.67 22.67c-9.37 9.37-24.57 9.37-33.94 0L34.52 272.97c-9.37-9.37-9.37-24.57 0-33.94z" />
                </svg>
              </button>
              <button onClick={() => { this.loadWeek('nextWeek', weekStartEnd); }}>
                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 320 512">
                  <path d="M285.476 272.971L91.132 467.314c-9.373 9.373-24.569 9.373-33.941 0l-22.667-22.667c-9.357-9.357-9.375-24.522-.04-33.901L188.505 256 34.484 101.255c-9.335-9.379-9.317-24.544.04-33.901l22.667-22.667c9.373-9.373 24.569-9.373 33.941 0L285.475 239.03c9.373 9.372 9.373 24.568.001 33.941z" />
                </svg>
              </button>
            </div>

            {this.state.editButton &&
              reminder === false &&
              <button
                className="btn edit-schedule-btn"
                data-toggle="modal"
                data-target="#schedule-popup"
                onClick={() => this.setState({ showBuilPlanComponent: true, show: 'show' })}
              >
                Edit Schedule
              </button>
            }
            {this.state.alertMessage ?
              <div className="alert-previous-week">
                Alert - You are viewing a previous week`s meal plan!
                Any changes made will not be reflected in current schedule
                or grocery list.
              </div> : null
            }
            {
              this.state.showBuilPlanComponent ?
                <div>
                  <BuildPlanScreen
                    showBuilPlanComponent={this.state.show}
                    onClickCloseBtn={() => { this.onClickCloseBtn(); }}
                    onClickSaveBtn={() => this.onClickSaveBtn(mealData)}
                    startDate={`${dayDates[0].Year}-${dayDates[0].Month}-${dayDates[0].date}`}
                    endDate={`${dayDates[6].Year}-${dayDates[6].Month}-${dayDates[6].date}`}
                    dayDates={dayDates}
                    cookingDates={cookingDates}
                    dinnerDate={dinnerDate}
                    lunchDates={lunchDates}
                    planExist={planType}
                    newWeek={newWeek}
                    newUser={this.state.state}
                    onClickBuild={(data) => { this.moveToDash(data, dayDates, mealData); }}
                  />
                  <div className="modal-backdrop in" />
                </div> : null
            }

          </div>
          { this.state.groceryClass !== 'active' &&
            <div className="days-cal">
              {
                dayDates.map((days) => {
                  const dates = `${days.Year}-${days.Month}-${days.date}`;
                  let data = '';
                  let classValue = 'days-hdng-normal';
                  let colors = 'black';
                  let svgClass = 'visible';
                  if (this.state.batchDates) {
                    const badgeIndex = this.state.batchDates.findIndex(x => x.date === dates);
                    if (badgeIndex > -1) {
                      classValue = 'days-hdng-bold';
                      colors = color[c];
                      c += 1;
                      data = this.state.batchDates[badgeIndex].data;
                      if (data === 'Single') {
                        svgClass = 'hide';
                      }
                    }
                  }
                  return (
                    <span key={days.day} className={`days-hdng ${classValue} ${colors}`}>
                      <svg
                        className={`static-cal-icn ${svgClass}`}
                        xmlns="http://www.w3.org/2000/svg"
                        xmlnsXlink="http://www.w3.org/1999/xlink"
                        version="1.1"
                        x="0px"
                        y="0px"
                        viewBox="0 0 100 100"
                        enableBackground="new 0 0 100 100"
                        xmlSpace="preserve"
                      >
                        <path d="M94.353,31.47l-21.04-11.91c-0.248-0.141-0.546-0.169-0.816-0.076s-0.488,0.298-0.599,0.562  c-5.473,13.128-12.894,25.32-22.039,36.276c-9.093-10.913-16.557-23.165-22.022-36.275c-0.11-0.264-0.328-0.469-0.599-0.562  c-0.271-0.092-0.568-0.064-0.816,0.076L5.373,31.472c-0.467,0.264-0.642,0.848-0.398,1.325c6.13,12.009,13.917,23.099,23.144,32.961  c0.353,0.375,0.932,0.422,1.337,0.111l9.234-7.051c0.489-0.375,1.117-0.529,1.727-0.421c0.6,0.105,1.108,0.442,1.434,0.948  c0.745,1.161,1.51,2.305,2.284,3.441c-4.004,4.262-8.285,8.298-12.751,11.993c-0.214,0.178-0.345,0.436-0.36,0.714  c-0.017,0.277,0.084,0.549,0.277,0.749l6.341,6.601c0.187,0.195,0.443,0.306,0.713,0.308c0.003,0,0.006,0,0.008,0  c0.267,0,0.523-0.106,0.711-0.297c3.808-3.849,7.439-7.963,10.793-12.231c3.346,4.261,6.979,8.376,10.8,12.235  c0.188,0.189,0.444,0.296,0.711,0.296c0.003,0,0.005,0,0.008,0c0.27-0.002,0.527-0.113,0.714-0.308l6.335-6.605  c0.192-0.201,0.293-0.473,0.276-0.75c-0.016-0.277-0.146-0.535-0.36-0.712c-4.502-3.73-8.762-7.741-12.763-11.991  c0.771-1.108,1.543-2.254,2.297-3.438c0.325-0.507,0.835-0.844,1.437-0.95c0.606-0.109,1.237,0.044,1.725,0.415l9.229,7.051  c0.181,0.138,0.395,0.205,0.607,0.205c0.268,0,0.534-0.107,0.729-0.316c9.235-9.862,17.021-20.952,23.142-32.96  C94.994,32.317,94.818,31.733,94.353,31.47z M40.762,56.427c-0.241-0.042-0.483-0.063-0.726-0.063c-0.918,0-1.823,0.301-2.561,0.864  l-8.513,6.5c-8.613-9.323-15.931-19.741-21.769-30.99l19.233-10.884c5.118,12.006,11.897,23.281,20.048,33.473l-3.136,2.696  C42.712,57.194,41.798,56.61,40.762,56.427z M38.368,80.715l-4.884-5.084c4.124-3.467,8.083-7.222,11.812-11.167  c1.081,1.539,2.186,3.044,3.309,4.517c-0.101,0.13-0.201,0.263-0.302,0.391C45.204,73.316,41.866,77.128,38.368,80.715z   M66.247,75.629l-4.881,5.089c-3.51-3.597-6.853-7.41-9.945-11.35c-2.414-3.069-4.742-6.323-6.951-9.679l3.265-2.806  C53.317,63.669,59.509,69.96,66.247,75.629z M51.179,57.867c0.274-0.327,0.538-0.661,0.809-0.99l3.278,2.817  c-0.355,0.538-0.71,1.062-1.068,1.581C53.174,60.155,52.167,59.02,51.179,57.867z M70.766,63.725l-0.97-0.741l1.445-1.586  c3.656-4.075,7.11-8.407,10.265-12.875c0.319-0.451,0.212-1.075-0.239-1.394c-0.454-0.319-1.075-0.211-1.394,0.239  c-3.11,4.405-6.516,8.676-10.115,12.688l-1.557,1.709l-5.944-4.541c-0.933-0.708-2.131-0.999-3.286-0.794  c-1.036,0.184-1.949,0.767-2.576,1.596l-3.139-2.698c8.187-10.223,14.929-21.455,20.052-33.474l19.227,10.884  C86.703,43.984,79.387,54.4,70.766,63.725z M82.668,46.064c0.322,0,0.64-0.156,0.832-0.444c1.078-1.611,2.135-3.264,3.141-4.91  c0.288-0.472,0.139-1.087-0.332-1.375c-0.472-0.289-1.087-0.14-1.375,0.332c-0.991,1.624-2.033,3.253-3.096,4.842  c-0.308,0.459-0.184,1.08,0.275,1.387C82.284,46.01,82.477,46.064,82.668,46.064z M30.146,32.652  c-0.256-0.488-0.859-0.68-1.35-0.422c-0.489,0.256-0.678,0.86-0.422,1.35c3.165,6.048,6.795,11.939,10.789,17.513  c0.195,0.273,0.502,0.418,0.813,0.418c0.201,0,0.405-0.061,0.581-0.188c0.449-0.321,0.553-0.946,0.23-1.395  C36.849,44.431,33.268,38.618,30.146,32.652z M22.966,26.716c0.107-0.997,1.002-1.718,1.999-1.611  c0.997,0.107,1.718,1.002,1.611,1.999c-0.107,0.997-1.002,1.718-1.999,1.611C23.58,28.607,22.859,27.712,22.966,26.716z   M73.509,26.714c0.107-0.997,1.002-1.718,1.999-1.611c0.997,0.107,1.718,1.002,1.611,1.999c-0.107,0.997-1.002,1.718-1.999,1.611  C74.123,28.605,73.402,27.71,73.509,26.714z M58.77,74.874c0.097-0.898,0.902-1.547,1.799-1.451c0.897,0.096,1.546,0.903,1.45,1.8  c-0.097,0.898-0.902,1.547-1.799,1.451C59.322,76.578,58.673,75.772,58.77,74.874z M37.685,74.872  c0.097-0.898,0.903-1.547,1.8-1.451s1.547,0.903,1.451,1.8c-0.097,0.898-0.903,1.547-1.8,1.451  C38.238,76.576,37.589,75.77,37.685,74.872z M53.278,68.847c0.084-0.781,0.785-1.346,1.566-1.262  c0.781,0.084,1.346,0.785,1.262,1.566c-0.084,0.781-0.785,1.346-1.566,1.262C53.759,70.329,53.194,69.628,53.278,68.847z   M43.59,68.848c0.084-0.781,0.785-1.346,1.566-1.262c0.781,0.084,1.346,0.785,1.262,1.566c-0.084,0.781-0.785,1.346-1.566,1.262  C44.071,70.33,43.506,69.629,43.59,68.848z M48.277,62.82c0.069-0.642,0.645-1.106,1.287-1.037c0.642,0.069,1.106,0.645,1.037,1.287  c-0.069,0.642-0.645,1.106-1.287,1.037C48.673,64.038,48.208,63.462,48.277,62.82z" />
                      </svg>
                      {days.day} {days.Month}/{days.date}
                    </span>
                  );
                })
              }
            </div>
          }
          {/* selected week based recipe view area */}

          {this.state.groceryClass !== 'active' ?
            <RecipeForWeekCalander
              mealData={this.state.mealData}
              dayDates={dayDates}
              color={color}
              cookingDates={cookingDates}
              dinnerDates={dinnerDates}
              lunchDates={lunchDates}
              length={length}
              buildPlan={() => this.clickeditSchedule('')}
              buildNextWeekPlan={() => this.buildNextWeekPlan()}
              buildPlanButton={this.state.error}
              previous={this.state.alertMessage}
              week={this.state.week}
              swapRecipe={() => this.swapRecipe()}
              batchDates={this.state.batchDates}
              servingArray={this.state.servingArray}
              sideDishArray={this.state.sideDishArray}
              pathname={this.state.pathname}
              loadData={loadData}
              error={error}
              alertMessage={this.state.alertMessage}
            />
            :
            <div>
              <GroceryList
                buildNextWeekPlan={() => this.buildNextWeekPlan()}
                dayDates={dayDates}
                previous={this.state.alertMessage}
                planExist={planType}
                loadData
                {...this.state}
              />
            </div>
          }
        </div>
      </div>
    );
  }
}
export default WeekCalanderFullArea;
