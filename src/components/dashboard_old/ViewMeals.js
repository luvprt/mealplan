import React, { Component } from 'react';
import RoutingConstants from 'lib/RoutingConstants';
import { getTimeStamp, compareRecipeTime, differnceTimeStamp, getFormatTime } from 'lib/services';
import Loader from 'lib/loader';
import { post } from 'lib/api';
import moment from 'moment';
import history from '../../history';
import ViewMainDish from './ViewMainDish'
import ViewsSideDish from './ViewsSideDish'

const BackBtn = props => (
  <div className="back-to-recipe" style={{float:'right'}}>
    <button className="btn back-to-plan" onClick={() => props.pressBackBtn()}>
      <svg
        height="512px"
        id="Layer_1"
        enableBackground="new 0 0 512 512"
        version="1.1"
        viewBox="0 0 512 512"
        width="512px"
        xmlSpace="preserve"
        xmlns="http://www.w3.org/2000/svg"
        xmlnsXlink="http://www.w3.org/1999/xlink"
      >
        <polygon points="352,115.4 331.3,96 160,256 331.3,416 352,396.7 201.5,256 " />
      </svg>
      Back to Weekly Plan
    </button>
  </div>
)
class ViewMeal extends Component {
  constructor(props){
    super(props)
    this.state = {
      sid: 0,
      recipe: {},
      recipeHandsTime: this.props.viewRecipeData.rHandsTime,
      inputs: {
        [`${this.props.viewRecipeData.type}`]: this.props.viewRecipeData.mealDate,
        recipe_id: this.props.viewRecipeData.id,
        meal_plan_schedule_id: this.props.viewRecipeData.weekId,
      },
      loadData: true,
      params:{},
      nutritionName: [],
      recipeNutritionName: [],
      sideNutritionName: [],
      recipeNutritionValue: [],
      message: false,
      loading: false,
      ...this.props,
    }
  }
  componentDidMount() {
    this.getMealData();
  }
  
  getMealData() {
    const url = RoutingConstants.fetchMealData;
    const recipe = [];
    const { inputs, sid, pathname, recipeNutritionName, viewRecipeData, sideNutritionName, recipeNutritionValue } = this.state;
    let { totalServing } = this.state;
    const nutritionName = [];
    if (pathname === '/dashboard') {
      totalServing = this.props.viewRecipeData.totalServing;
    } else {
      // totalServing = this.props.size;
      totalServing = this.props.viewRecipeData.totalServing;
    }
    post(url, inputs).then((response) => {
      if (response.result) {
        this.setState({
          mainDishValue: response.result,
          totalServing,
          // recipeNutrition: response.result.nutrition_link,
          loadData: false,
        });
        if(response.result.recipe_nutrition_data && response.result.recipe_nutrition_data.length > 0) {
          const nutritionData = response.result.recipe_nutrition_data && response.result.recipe_nutrition_data
          nutritionData.map((item) => {
           
            const attribute = item.nutrition_info_attribute.attribute
            let unit = '';
            if(item.nutrition_info_attribute.measurement_unit != null){
              unit = item.nutrition_info_attribute.measurement_unit
            }
            recipeNutritionName[`${attribute}`] = parseInt(item.value,0)
            nutritionName[`${attribute}`] = parseInt(item.value,0)
            nutritionName[`unit${attribute}`] =  unit
           
            return recipeNutritionName
          })
        }
        if (response.result.recipe_sides.length > 0) {
          this.setState({
            sideDishValue: response.result.recipe_sides,

          });
          
          this.state.sideDishValue.map((value) => {
            if (value.is_default === 1) {
              recipe.push(value.recipe);
              if(value.recipe_nutrition_data && value.recipe_nutrition_data.length > 0) {
                recipeNutritionValue.push(value.recipe_nutrition_data)
                const nutritionData = value.recipe_nutrition_data
                nutritionData.map((item) => {
                
                  const attribute = item.nutrition_info_attribute.attribute
                  let unit = '';
                  if(item.nutrition_info_attribute.measurement_unit != null){
                    unit = item.nutrition_info_attribute.measurement_unit
                  }
                  nutritionName[`${attribute}`] = nutritionName[`${attribute}`]+parseInt(item.value,0)
                  sideNutritionName[`${attribute}`] = parseInt(item.value,0)
                  nutritionName[`unit${attribute}`] =  unit
                
                  return nutritionData;
                })
              }
            }
            return value;
          });
          if (recipe) {
            let sideVertImg;
            let sideImg;
            if (recipe[sid]) {
              sideVertImg = recipe[sid].vert_image;
              sideImg = recipe[sid].image;
            }
            if (sideVertImg === null) {
              sideVertImg = '/images/small.png';
            }
            if (sideImg === null) {
              sideImg = '/images/small.png';
            }
            viewRecipeData.sideHandsOnTime = recipe[sid].hands_on_time
            viewRecipeData.sidePassiveTime = recipe[sid].passive_time
            viewRecipeData.sname = recipe[sid].name
            this.setState({
              sideId: recipe[sid].id,
              recipe,
              sideHandsOnTime: recipe[sid].hands_on_time,
              sidePassiveTime: recipe[sid].passive_time,
              sideVertImg,
              sideImg,
              viewRecipeData,
              sideNutrition: recipe[sid].nutrition_link,
              sideNutritionName, 
            });
          }
        }
        this.setState({
          nutritionName,
          recipeNutritionName,
          recipeNutritionValue
        })
      }
    }).catch((err) => {
      if (err.error === 'token_expired') {
        localStorage.removeItem('token');
        history.push('/login');
      }
    });
  }
  
  changeSide() {
    const {
      sideDishValue, recipe, mealDate, recipeNutritionValue,
      nutritionName, sideNutritionName, recipeNutritionName, viewRecipeData
    } = this.state;
    this.setState({ sideVertImg: '/images/small.png' });
    let { sid, saveBtn } = this.state;
    let { type } = this.state.viewRecipeData;
    type = type.split('_')[0];
    sideDishValue.map((value) => {
      const d = recipe.findIndex(x => x.id === value.side_id);
      if (d === -1) {
        recipe.push(value.recipe);
      }
      
      return recipe;
    });
    if ((sideDishValue.length - 1) === sid) {
      sid = 0;
      saveBtn = false
    } else {
      sid += 1;
      saveBtn = true
    }
    if(this.state.viewRecipeData.pathFrom === 'recipeList') {
      saveBtn = false
    }
    
    if (recipe) {
      recipeNutritionValue.push(sideDishValue[sid].recipe_nutrition_data)
      const nutritionData = sideDishValue[sid].recipe_nutrition_data
      nutritionData.map((item) => {
              
        const attribute = item.nutrition_info_attribute.attribute
        let unit = '';
        if(item.nutrition_info_attribute.measurement_unit != null){
          unit = item.nutrition_info_attribute.measurement_unit
        }
        nutritionName[`${attribute}`] = recipeNutritionName[`${attribute}`]+parseInt(item.value,0)
        sideNutritionName[`${attribute}`] = parseInt(item.value,0)
        nutritionName[`unit${attribute}`] =  unit
              
        return nutritionData;
      })
      viewRecipeData.sideHandsOnTime = recipe[sid].hands_on_time
      viewRecipeData.sidePassiveTime = recipe[sid].passive_time
      viewRecipeData.sname = recipe[sid].name
      this.setState({
        sname: recipe[sid].name,
        recipe,
        sid,
        sideId: recipe[sid].id,
        sideImg: recipe[sid].image || '/images/small.png',
        sideVertImg: recipe[sid].vert_image || '/images/small.png',
        sideNutrition: recipe[sid].nutrition_link,
        length: recipe.length,
        recipeNutritionValue,
        sideNutritionName,
        nutritionName,
        sideHandsOnTime: recipe[sid].hands_on_time,
        sidePassiveTime: recipe[sid].passive_time,
        viewRecipeData,
        saveBtn,
      });
      // alert(recipe[sid].hands_on_time)
      // track change side dish
      window.analytics.track('Change Side Dish', {
        recipeName: recipe[sid].name,
        date: moment(mealDate).format('dddd DD-MM-YYYY'),
        slot: type,
      });
    }
    // console.log(recipe);
  }

  clickServing() {
    const { totalServing, mealDate, rname } = this.state.viewRecipeData;
    let { type } = this.state.viewRecipeData;
    type = type.split('_')[0];
    // serving size event track
    window.analytics.track('Clicked Serving Size', {
      servingSize: totalServing,
      date: moment(mealDate).format('dddd DD-MM-YYYY'),
      recipeName: rname,
      slot: type,
    });
  }

  swapMeal(totalHandsTime, totalTime) {
    const {
      rname, size, fId, recipeImg, equipmentIcon, totalServing, mealDate,
      servingArray, id, dayDates
    } = this.state.viewRecipeData;
    let { type } = this.state.viewRecipeData;
    const { inputs } = this.state
    type = type.split('_')[0];
    const startDate = `${dayDates[0].fullMonth} ${dayDates[0].date}`;
    const endDate = `${dayDates[6].fullMonth} ${dayDates[6].date}`;
    let recipeSize;
    const index = servingArray.findIndex( x => x.id === id)
    if( index > -1) {
      recipeSize = (servingArray[index].servingSize / servingArray[index].basicSize)
    }
    // track swap meal option
    window.analytics.track('Swap Meal', {
      swapRecipe: rname,
      slot: type,
      date: moment(mealDate).format('dddd DD-MM-YYYY'),
    });

    history.push('/recipeList', {
      rname,
      size,
      totalHandsTime,
      totalTime,
      startDate,
      endDate,
      dayDates,
      weekId: inputs.meal_plan_schedule_id,
      type,
      fDate: this.props.mealDate,
      fId,
      recipeImg,
      equipmentIcon,
      totalServing,
      mealDate,
      recipeSize,
      previousRId: id
    });
  }

  save() {
    const {
      inputs, recipe, sid 
    } = this.state;
    const { mealDate, rname, sname } = this.state.viewRecipeData;
    let { type } = this.state.viewRecipeData;
    type = type.split('_')[0];
    inputs.side_dish_id = recipe[sid].id;
    const url = RoutingConstants.updateSideDish;
    this.setState({ message: true, loading: true})
    post(url, inputs).then(() => {
      setTimeout(() =>  {
        this.setState({saveBtn: false, message: false, loading: false});
      }, 1000)
      window.analytics.track('Save View Details', {
        sideDishName: sname,
        recipeName: rname,
        date: moment(mealDate).format('dddd DD-MM-YYYY'),
        slot: type,
      });
    }).catch(() => {
      // console.log(err);
    });
  }

  swapSave() {
    const url = RoutingConstants.dragAndDrop;
    const {
      type, fId, mealDate, rname, recipeName, weekId, dayDates, recipeSize
    } = this.state.viewRecipeData;
    const { inputs, params, sideId } = this.state;
    params.recipe_id = inputs.recipe_id;
    params.weakly_schedule_id = weekId;
    params.side_dish_id = sideId;
    params.moved_from_id = fId;
    params.moved_from_type = type;
    params.moved_from_date = mealDate;
    params.moved_to_id = '';
    params.moved_to_type = type;
    params.moved_to_date = mealDate;
    
    post(url, params).then(() => {
      params.from_id = this.props.viewRecipeData.previousRId;
      localStorage.setItem('method','back')
      // track swap meal option
      window.analytics.track('Swap Recipe', {
        swapFromRecipe: recipeName,
        swapToRecipe: rname,
        slot: type,
        date: moment(mealDate).format('dddd DD-MM-YYYY'),
      });
      history.push('/dashboard', { guestEmail: 'response.FirstName', type: '/login', dayDates, recipeSize, data: params});
    }).catch(() => {
      // console.log(err);
    });
  }
  // eslint-disable-next-line
  pressBackRecipe() {
    window.history.back();
  }

  pressBackBtn() {
    const { dayDates, recipeName, recipeSize } = this.state.viewRecipeData;
    // Canceled swap recipe
   
    window.analytics.track('Cancelled Swap Recipe', {
      recipeName,
      startDate: `${dayDates[0].fullDay} ${dayDates[0].date}-${dayDates[0].Month}-${dayDates[0].Year}`,
      endDate: `${dayDates[6].fullDay} ${dayDates[6].date}-${dayDates[6].Month}-${dayDates[6].Year}`,
    });

    localStorage.setItem('method', 'back');
    history.push('/dashboard', { guestEmail: 'response.FirstName', type: '/login', dayDates, recipeSize });
  }

  render() {
    const {
      sid, sideDishValue, recipeHandsTime, loadData, nutritionName, mainDishValue,
      saveBtn, message, loading,
    } = this.state;
    const { rname, sname, recipeImg, sidePassiveTime, totalServing, sideImg, pathFrom, mealDate } = this.state.viewRecipeData;
    const viewMealDate = moment(mealDate).format('dddd, MMMM DD')
    let { totalRecipeTime, sideHandsOnTime, type } = this.state.viewRecipeData;
    const enableSwap = pathFrom === 'recipeList'
    type = type.split('_')[0];
    // total hands on time
    const totalHandsOnTime = getTimeStamp(recipeHandsTime + sideHandsOnTime);
    const t = totalHandsOnTime.split('m');
    // side total time
    let sidetotaltime = '0';
    if (sideHandsOnTime) {
      sidetotaltime = sideHandsOnTime + sidePassiveTime;
      sideHandsOnTime = getTimeStamp(sideHandsOnTime);
      sideHandsOnTime = getFormatTime(sideHandsOnTime);
    }

    // compare total time
    let ht = compareRecipeTime(totalRecipeTime, sidetotaltime);
    ht = compareRecipeTime(t[0], ht);
    if (ht !== '0h') {
      ht = getTimeStamp(ht);
    }
    let passiveTime = ht;
    if (ht !== '0h') {
      passiveTime = differnceTimeStamp(ht, t[0]);
      passiveTime = getTimeStamp(passiveTime);
    }

    totalRecipeTime = getTimeStamp(totalRecipeTime);
    totalRecipeTime = getFormatTime(totalRecipeTime);

    sidetotaltime = getTimeStamp(sidetotaltime);
    sidetotaltime = getFormatTime(sidetotaltime);
    return(
      <div className="col-lg-9 main-cntnt-all" draggable={false}>
        <style type="text/css">
          {"@media print{@page {size: portrait; margin:0;  } }"}
        </style>
        {
          loadData ?
            <div className="loading" style={{marginTop:'20%'}}>
              <Loader />
              <div className="modal-backdrop fade in" />
            </div>
            :
            <div className="dash-cntnt-dtail swap-mode-dtail">
              <div>
                {loading && <div className="loading" style={{marginTop:'20%'}}>
                  <Loader />
                  <div className="modal-backdrop fade in" />
                </div>}
                <div className="tab-content">
                  {enableSwap ? 
                    <div className="back-to-recipe">
                      <button onClick={() => this.pressBackRecipe()} className="btn back-to-plan recipe-view">  
                        <svg
                          height="20px"
                          id="Layer_1"
                          enableBackground="new 0 0 512 512"
                          version="1.1"
                          viewBox="0 0 512 512"
                          width="20px"
                          xmlSpace="preserve"
                          xmlns="http://www.w3.org/2000/svg"
                          xmlnsXlink="http://www.w3.org/1999/xlink"
                        >
                          <polygon points="352,115.4 331.3,96 160,256 331.3,416 352,396.7 201.5,256 " />
                        </svg>
                      Recipe List
                      </button>
                    </div> :
                    <div className="recipe-top-btn">
                      <BackBtn 
                        pressBackBtn={()=> this.pressBackBtn()}
                      />
                      <div className="back-to-recipe">
                        <p>{type}: {viewMealDate}</p>
                      </div>
                    </div>
                  }
                  <div id="meal-dtail" className="tab-pane fade in active">
                    <div className="recipe-top-cntnt">
                      <h1>{rname}</h1>
                      {sname != null && 
                    <div className="side-tag-lft">
                      <div className="meal-breakdown-row-side-tag">Side</div>
                      <div className="side-hdng-cntnt">{sname}</div>
                    </div>
                      }
                  
                      <div className="meal-sumry-icn-box">
                        <div className="popup-main-icn-box small-box">
                          <div className="row">
                            <div className="meal-summary mb-hide">  Meal Summary </div>
                            <div className="meal-summary mb-border mb-only"> Meal Summary (Main + Side)</div>
                            <div className="meal-sumry-icn-inner chef-cap-small">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                xmlnsXlink="http://www.w3.org/1999/xlink"
                                version="1.1"
                                x="0px"
                                y="0px"
                                viewBox="0 0 100 100"
                                style={{ enableBackground: 'new 0 0 100 100' }}
                                xmlSpace="preserve"
                              >
                                <path d="M79,40.9c0-7.2-5.9-13.1-13.1-13.1c-1.8,0-3.5,0.4-5.2,1.1c-2.4-3.5-6.4-5.6-10.7-5.6s-8.3,2.1-10.7,5.6  c-1.6-0.7-3.4-1.1-5.2-1.1c-7.2,0-13.1,5.9-13.1,13.1c0,6.6,5,12.1,11.4,12.9v18.9c0,2.2,1.8,4,4,4h27.2c2.2,0,4-1.8,4-4V53.9  C74,53,79,47.5,79,40.9z M63.6,73.3H36.4c-0.3,0-0.6-0.3-0.6-0.6v-3.4h28.4v3.4C64.2,73,63.9,73.3,63.6,73.3z M65.9,50.6  c-2.1,0-4-0.6-5.7-1.9c-0.8-0.6-1.8-0.4-2.4,0.4c-0.6,0.8-0.4,1.8,0.4,2.4c1.8,1.3,3.9,2.1,6,2.4v12.1h-8v-6.8  c0-0.9-0.8-1.7-1.7-1.7s-1.7,0.8-1.7,1.7v6.8h-5.7v-6.8c0-0.9-0.8-1.7-1.7-1.7s-1.7,0.8-1.7,1.7v6.8h-8V53.8  c2.2-0.3,4.3-1.1,6.2-2.5c0.8-0.6,0.9-1.6,0.3-2.4c-0.6-0.8-1.6-0.9-2.4-0.3c-1.7,1.3-3.7,1.9-5.8,1.9c-5.3,0-9.7-4.3-9.7-9.7  s4.3-9.7,9.7-9.7c1.7,0,3.5,0.5,4.9,1.4c0.4,0.2,0.9,0.3,1.3,0.2c0.5-0.1,0.8-0.4,1-0.9c1.7-3.2,5-5.2,8.6-5.2s6.9,2,8.6,5.2  c0.2,0.4,0.6,0.7,1,0.9c0.5,0.1,0.9,0.1,1.3-0.2c1.5-0.9,3.2-1.4,4.9-1.4c5.3,0,9.7,4.3,9.7,9.7S71.2,50.6,65.9,50.6z" />
                              </svg>
                              <b>{totalHandsOnTime}</b>
                              <span>Hands On</span>
                              <span className="mb-only">:</span>
                            </div>
                            <div className="meal-sumry-icn-inner passive-only">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                xmlnsXlink="http://www.w3.org/1999/xlink"
                                version="1.1"
                                x="0px"
                                y="0px"
                                viewBox="0 0 44 44"
                                style={{ enableBackground: 'new 0 0 44 44' }}
                                xmlSpace="preserve"
                              >
                                <g>
                                  <path d="M35.879,27c-0.834,0-1.618,0.324-2.207,0.914l-5.793,5.793C27.69,33.896,27.438,34,27.171,34H23V18h15c0.552,0,1-0.448,1-1   c0-6.91-7.092-12.562-16-12.961V1c0-0.552-0.448-1-1-1s-1,0.448-1,1v3.039C12.092,4.438,5,10.09,5,17c0,0.552,0.448,1,1,1h15v16H8   c-1.654,0-3,1.346-3,3c0,1.453,1.038,2.666,2.412,2.941L6.382,42H6c-0.552,0-1,0.447-1,1s0.448,1,1,1h1   c0.379,0,0.725-0.214,0.895-0.553L9.618,40H21v1c0,0.552-0.449,1-1,1h-1c-0.552,0-1,0.447-1,1s0.448,1,1,1h6c0.552,0,1-0.447,1-1   s-0.448-1-1-1h-1c-0.551,0-1-0.448-1-1v-1h5.343c0.336,0,0.667-0.034,0.99-0.098l1.773,3.545C31.275,43.786,31.621,44,32,44h1   c0.552,0,1-0.447,1-1s-0.448-1-1-1h-0.382l-1.443-2.886c0.248-0.171,0.486-0.361,0.703-0.578l6.208-6.207   C38.667,31.747,39,30.943,39,30.121C39,28.4,37.6,27,35.879,27z M36.938,16h-7.965c-0.157-3.989-1.24-7.417-2.866-9.577   C31.966,7.648,36.374,11.422,36.938,16z M7.062,16c0.565-4.578,4.973-8.352,10.831-9.577c-1.626,2.16-2.709,5.588-2.866,9.577   H7.062z M17.021,16C17.274,10.058,19.759,6,22,6s4.726,4.058,4.979,10H17.021z M36.672,30.914l-6.208,6.207   C29.906,37.68,29.133,38,28.343,38H8c-0.551,0-1-0.448-1-1s0.449-1,1-1h19.171c0.801,0,1.555-0.312,2.122-0.879l5.793-5.793   C35.298,29.116,35.579,29,35.879,29C36.497,29,37,29.503,37,30.121C37,30.416,36.88,30.705,36.672,30.914z" />
                                </g>
                              </svg>
                              <b className="mb-hide">{passiveTime}</b>
                              <span className="mb-hide">Passive Time</span>
                              <span className="mb-only">Total Servings:</span>
                              <span className="mb-only">{totalServing}</span>
                            </div>
                            <div className="meal-sumry-icn-inner popup-time-icn">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                xmlnsXlink="http://www.w3.org/1999/xlink"
                                version="1.1"
                                x="0px"
                                y="0px"
                                viewBox="0 0 100 100"
                                style={{ enableBackground: 'new 0 0 100 100' }}
                                xmlSpace="preserve"
                              >
                                <g>
                                  <path d="M50,5C25.2,5,5,25.2,5,50s20.1,45,45,45s45-20.1,45-45S74.8,5,50,5z M50,89.7c-21.9,0-39.7-17.8-39.7-39.7   c0-21.9,17.8-39.7,39.7-39.7S89.7,28.1,89.7,50C89.7,71.9,71.9,89.7,50,89.7z" /><path d="M52.4,51.7V25.2c0-1.3-1.1-2.4-2.4-2.4s-2.4,1.1-2.4,2.4v27.3c0,0.3,0.1,0.6,0.1,0.8c0.1,0.4,0.3,0.8,0.6,1.1l19.3,19.3   c0.9,0.9,2.5,0.9,3.4,0c0.9-0.9,0.9-2.5,0-3.4L52.4,51.7z" />
                                </g>
                              </svg>
                              <b>{ht}</b>
                              <span>Total Time</span>
                              <span className="mb-only">:</span>
                            </div>
                          </div>
                        </div>

                        <div className="popup-main-icn-box popup-main-icn-box-rght small-box">
                          {mainDishValue.recipe_nutrition_data 
                            && mainDishValue.recipe_nutrition_data.length > 0 && 
                            <div className="row">
                              <div className="meal-summary mb-hide mb-border"> Nutritional summary (Main + Side)</div>
                              <div className="popup-main-icn-box-rght-inner-box">
                                <div className="meal-sumry-icn-inner">
                                  <b>{nutritionName.Calories}{ nutritionName.unitCalories}</b>
                                  <span>Calories</span>
                                </div>
                                <div className="meal-sumry-icn-inner">
                                  <b>{nutritionName.Protein}{nutritionName.unitProtein}</b>
                                  <span>Protein</span>
                                </div>
                                <div className="meal-sumry-icn-inner">
                                  <b>{nutritionName['Net Carbohydrates']}{nutritionName['unitNet Carbohydrates']}</b>
                                  <span>Net Carbs</span>
                                </div>
                                <div className="meal-sumry-icn-inner">
                                  <b>{nutritionName['Total Fat']}{nutritionName['unitTotal Fat']}</b>
                                  <span>Total Fat</span>
                                </div>
                              </div>
                            </div>}
                        </div>

                      </div>
                      <div className="row swap-meal">
                        {!enableSwap? 
                          !saveBtn ?
                            <button onClick={() => this.swapMeal(totalHandsOnTime, ht)} className="swap-meal-btn">
                              <svg
                                xmlns="http://www.w3.org/2000/svg"
                                xmlnsXlink="http://www.w3.org/1999/xlink"
                                version="1.1"
                                x="0px"
                                y="0px"
                                viewBox="17 -18 100 100"
                                enableBackground="new 17 -18 100 100"
                                xmlSpace="preserve"
                              >
                                <path d="M48.7,10.7C55,10.7,60.3,5.5,60.3-1s-5.2-11.7-11.7-11.7S37-7.5,37-1S42.2,10.7,48.7,10.7z M85.4,76.7  c6.5,0,11.7-5.2,11.7-11.7s-5.2-11.7-11.7-11.7c-6.3,0-11.7,5.2-11.7,11.7S78.9,76.7,85.4,76.7z M100.3,52L117,35.3H83.7L100.3,52z   M17,28.7h33.3L33.7,12L17,28.7z M67,72V58.7c-14.5,0-26.7-12.2-26.7-26.7v-7.3H27V32C27,53.3,45.7,72,67,72z M93.7,39.3H107V32  c0-21.3-18.7-40-40-40V5.3c14.5,0,26.7,12.2,26.7,26.7V39.3z" />
                              </svg>
                              <span>Swap Entire Meal</span>
                          
                            </button>
                            :
                            <div>
                              <button onClick={() => this.swapMeal(totalHandsOnTime, ht)} className="swap-meal-btn mb-hide">
                                <svg
                                  xmlns="http://www.w3.org/2000/svg"
                                  xmlnsXlink="http://www.w3.org/1999/xlink"
                                  version="1.1"
                                  x="0px"
                                  y="0px"
                                  viewBox="17 -18 100 100"
                                  enableBackground="new 17 -18 100 100"
                                  xmlSpace="preserve"
                                >
                                  <path d="M48.7,10.7C55,10.7,60.3,5.5,60.3-1s-5.2-11.7-11.7-11.7S37-7.5,37-1S42.2,10.7,48.7,10.7z M85.4,76.7  c6.5,0,11.7-5.2,11.7-11.7s-5.2-11.7-11.7-11.7c-6.3,0-11.7,5.2-11.7,11.7S78.9,76.7,85.4,76.7z M100.3,52L117,35.3H83.7L100.3,52z   M17,28.7h33.3L33.7,12L17,28.7z M67,72V58.7c-14.5,0-26.7-12.2-26.7-26.7v-7.3H27V32C27,53.3,45.7,72,67,72z M93.7,39.3H107V32  c0-21.3-18.7-40-40-40V5.3c14.5,0,26.7,12.2,26.7,26.7V39.3z" />
                                </svg>
                                <span>Swap Entire Meal</span>
                          
                              </button>
                              <div className="mb-only confirm-btn">
                                <button onClick={() => this.save()} className="recipe-btn"> 
                                Save Changes
                                </button>
                                {
                                  message &&
                               <p> Changes saved succesfully!</p>
                                }
                              </div>
                            </div>
                          :
                          <button onClick={() => this.swapSave()} className="recipe-btn confirm-swap">
                            <span>Confirm Swap</span>
                          </button>
                        }
                        {!enableSwap &&
                          <div className='col-md-4 popup-ftr-servings top-txt-only mb-hide'>
                            Total Servings: 
                            <strong onClick={() => this.clickServing()} className="serving">{totalServing}</strong>
                          </div>
                        }
                        {
                          saveBtn &&
                          <div className="mb-hide confirm-btn">
                            <button onClick={() => this.save()} className="recipe-btn"> 
                                Save Changes
                            </button>
                            {
                              message &&
                              <p> Changes saves succesfully!</p>
                              
                            }
                          </div>
                        }
                      </div>
                    </div>
                
                  </div>
              
               
                </div>
              </div>
              <ViewMainDish
                type={this.state.viewRecipeData.type}
                response={this.state.mainDishValue}
                recipeImg={recipeImg}
                { ...this.state}
              />
              {sname !=null &&
                <ViewsSideDish 
                  response={this.state.recipe}
                  sid={sid}
                  sideImg={sideImg}
                  changeSide={()=>this.changeSide()}
                  save={()=>this.save()}
                  length={this.state.length} 
                  sideDishValue={sideDishValue}
                  saveBtn={saveBtn}
                  {... this.state}
                />
              }
              {pathFrom === 'recipeList' && 
                <div className="mb-only confirm-btn">
                  <button onClick={() => this.swapSave()} className="recipe-btn"> 
                    Confirm Swap
                  </button>
                </div>
              }
            </div>

        }

      </div>
     
    
       
    )
  }
}
export default ViewMeal;
