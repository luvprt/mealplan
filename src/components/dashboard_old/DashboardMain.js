/*
Component Name: DashboardMain component
Contains : WeekCalanderFullArea
Default screen: Dashboard
*/

import React, { Component } from 'react';
import moment from 'moment';
import { Steps } from 'intro.js-react';
import ReactResizeDetector from 'react-resize-detector';
import { getTimeStamp, totalValue, compareRecipeTime } from 'lib/services';
import WeekCalanderFullArea from './WeekCalanderFullArea';
import ViewMeal from './ViewMeals';

const tourSteps = [
  {
    element: '.dash-cntnt-dtail',
    intro: 'Weekly Plan: This is your weekly meal plan that shows when you’ll eat all these delicious meals! Customize your plan by dragging & dropping, adding and deleting meals.',
    position: 'left',
  },
  {
    element: '.sidebar-cntnt',
    intro: 'Cooking Schedule: This schedule is created when you select your cook days each week and shows when you’ll cook each recipe. It automatically updates based on how you drag meals around your calendar.',
    position: 'right',
  },
  {
    element: '.days-cal',
    intro: 'Cook Days: Your cook days are also underlined at the top of your weekly plan.',
    position: 'right',
  },
  {
    element: '.cal-days-cntnt .meal-post',
    intro: 'View Details: Click a meal card to see the main and side dish ingredients and directions. You can click the swap button to choose a different recipe.',
    position: 'right',
  },
  {
    element: '.navbar-main',
    intro: 'After you’ve finalized your plan for the week, click here to view your grocery list.',
    position: 'right',
  },
  {
    element: '.purechat-widget',
    intro: 'Just click here if you need any help or want to share a cool feature idea.',
    position: 'top',
  },
];
const color = [
  'yellow-icn-box', 'blue-icn-box', 'hazel-icn-box', 'orange-icn-box', 'light-icn-box', 'green-icn-box', 'red-icn-box',
];
const block = ['yellow-block', 'blue-block', 'hazel-block', 'orange-block', 'light-block', 'green-block', 'red-block'];
let testDate = [];
let nonBatchDate = [];
let size = 0;
let sideDishId = [];

class DashboardMain extends Component {
  constructor(props) {
    super(props);
    this.state = {
      batchColor: [],
      ...this.props,
    };
  }

  componentDidMount() {
    // console.log(introJs().start())
    // introJs().start();
  }

  // unsafe
  // eslint-disable-next-line
  UNSAFE_componentWillReceiveProps(props) {
    this.setState({
      ...props,
    });
    this.isTourShowable();
  }

  onResize() {
    const { groceryClass } = this.state;
    let { grocery } = this.state;
    if (window.innerWidth < 1024 && (groceryClass === 'active' || this.props.pathname === '/viewRecipe')) {
      grocery = 'hide-cook-schedule';
    } else {
      grocery = '';
    }
    this.setState({ grocery });
  }

  // eslint-disable-next-line
  onTourExit() {
    localStorage.setItem('isTourCompleted', true);
  }

  // eslint-disable-next-line
  isTourShowable() {
    return !localStorage.getItem('isTourCompleted');
  }

  getWeekData(dayDates, props, cookingDates, dinnerDates, lunchDates, selectedWeek, previous) {
    this.setState({
      dayDates,
      cookingDates: [],
      dinnerDates: [],
      lunchDates: [],
      selectedWeek,
      previous,
    });
    this.props.getWeekMealData(
      dayDates, this.state.mealData, this.state.cookingDates,
      this.state.dinnerDates, this.state.lunchDates, '', selectedWeek, previous);
  }

  lunchMeal() {
    const lunchDates = [];
    const { mealData } = this.state;

    const cookDate = []; testDate = []; nonBatchDate = []; sideDishId = [];
    this.userNonBatchData(lunchDates);
    this.userWeeklyScheduleData(mealData, lunchDates);
    for (const x in lunchDates) {
      if (lunchDates) {
        cookDate.push(lunchDates[x]);
      }
    }
    return (
      <div>
        {
          cookDate.map((cookIndex) => {
            const cookLength = cookIndex.length - 1;
            let noCook = false;
            let badgeData;
            let cookDates;
            let cookDate1;
            let cookDate2;
            let lengthCheck = false;
            let totalHandsOnTime;
            let batchSvg;
            let	totalCookTime;
            if (cookIndex[cookLength]) {
              lengthCheck = true;
              if (cookIndex[cookLength].lunch_date === '00-00-00') {
                noCook = true;
                cookDates = cookIndex[cookLength].lunch_date;
              } else {
                noCook = false;
                cookDates = moment(cookIndex[cookLength].lunch_date).format('dddd');
                cookDate1 = moment(cookIndex[cookLength].lunch_date).format('MM');
                cookDate2 = moment(cookIndex[cookLength].lunch_date).format('DD');
              }

              badgeData = cookIndex.data;

              totalCookTime = cookIndex[cookLength].passive_time;
              totalHandsOnTime = getTimeStamp(cookIndex[cookLength].total_hands_on_time);
              batchSvg = true;
              totalCookTime = getTimeStamp(totalCookTime);

              if (badgeData === 'Single') {
                batchSvg = false;
              }
            }
            return (
              <div key={cookDates}>
                {lengthCheck &&
                <div className={`sidemenu-block ${block[cookIndex[cookLength].index]}`}>
                  {!noCook ?
                    <div className="sidemenu-row sidemenu-row-hdng">
                      <div className={`sidemenu-icn sidemenu-icn-small ${color[cookIndex[cookLength].index]}`}>
                        {!batchSvg ?
                          <svg viewBox="0 0 100 100" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                            <title>Icon / Single Clever</title>
                            <desc>Created with Sketch.</desc>
                            <defs>
                              <path
                                d="M70.4706293,57.8923313 C56.2289434,40.7664746 44.5386384,21.5393146 35.9792176,
                                0.965688495 C35.8069328,0.551391218 35.4654957,0.229683106 35.0410487,0.083737474
                                C34.6166016,-0.0606388497 34.1514328,-0.0166982294 33.7630089,0.203004872 L0.795534336,18.8950309 C0.0641071604,19.3093281 -0.209982252,20.2258039 0.1721767,20.9743638 C9.77313727,39.820182 21.969333,57.2238062 36.4208931,72.7003204 C36.9737706,73.2888108 37.880615,73.3625683 38.5149362,72.8745135 L52.9774598,61.8093238 C53.743344,61.2208333 54.7269334,60.9791599 55.6823308,61.1486452 C56.6220659,61.3134225 57.4177083,61.8422792 57.9282978,62.636349 C59.0951355,64.4583154 60.2932978,66.2536036 61.5055561,68.0363374 C70.4706293,80.4629731 65.1574713,73.5080859 70.4831591,80.3350031 C75.7237487,87.0218239 81.4138449,93.4795258 87.3983914,99.5354849 C87.6928418,99.8320841 88.0937954,100 88.5119775,100 C88.9473882,99.9968614 89.349908,99.8226682 89.6427922,99.5166532 L99.5648289,89.1513747 C99.8655441,88.8359438 100.023733,88.4090921 99.9971071,87.9743938 C99.9720474,87.5396955 99.7684382,87.1348141 99.433266,86.8570466 C92.3821201,81.0035283 85.7100006,74.7090344 79.4435335,68.039476 C70.4706293,57.8923313 79.4435335,68.039476 70.4706293,57.8923313 Z M56.2226785,58.0571087 C55.8452182,57.9911977 55.4661917,57.9582423 55.085599,57.9582423 C53.6478042,57.9582423 52.2303704,58.4306039 51.0744962,59.3141243 L37.741221,69.5146254 C24.2513232,54.8839682 12.789687,38.5349188 3.64606423,20.8817746 L33.7692738,3.80142781 C41.7852145,22.642538 52.4026552,40.3364842 65.1689569,56.33087 L60.2572747,60.561724 C59.2768177,59.2607678 57.8452878,58.344292 56.2226785,58.0571087 Z M96.1379281,88.1909583 L88.4931828,96.177166 C82.9957323,90.5323656 77.7598414,84.5485948 72.9170731,78.3655218 C69.1362054,73.5493159 65.4900331,68.4427881 62.0302416,63.1761909 L67.1439669,58.772713 C75.8866361,69.4220362 85.5847026,79.294552 96.1379281,88.1909583 Z M39.5956316,20.7468141 C39.194678,19.9809919 38.2502441,19.6796848 37.4812276,20.0845662 C36.7153434,20.486309 36.4193269,21.434171 36.8202805,22.2031318 C41.7773833,31.6943058 47.4627809,40.9390985 53.7182844,49.6864205 C54.0236983,50.1148416 54.5045294,50.3423912 54.9916255,50.3423912 C55.3064367,50.3423912 55.6259467,50.2466634 55.9016023,50.0473613 C56.6048374,49.5436135 56.7677249,48.5627961 56.2618341,47.8581768 C50.0940392,39.2316915 44.4853867,30.1093049 39.5956316,20.7468141 Z M28.3501346,11.4314027 C28.5177207,9.86680271 29.9194922,8.73533174 31.4810188,8.90324768 C33.0425453,9.07116362 34.1717937,10.4756942 34.0042076,12.0402941 C33.8366215,13.604894 32.4348499,14.736365 30.8733234,14.5684491 C29.3117968,14.3989638 28.1825485,12.9944333 28.3501346,11.4314027 Z M84.4272622,87.0061309 C84.579186,85.5968924 85.8399973,84.5784116 87.2449013,84.7290652 C88.6498054,84.8797187 89.6662855,86.1461502 89.5159279,87.5538193 C89.3640041,88.9630578 88.1031928,89.9815386 86.6982887,89.830885 C85.2918185,89.6802315 84.2753383,88.4153693 84.4272622,87.0061309 Z M75.8255533,77.5479124 C75.9571162,76.3222829 77.0550401,75.435624 78.278262,75.5674458 C79.5014839,75.6992677 80.3864011,76.7993525 80.2548382,78.024982 C80.1232753,79.2506114 79.0253514,80.1372703 77.8021295,80.0054485 C76.5789076,79.8736266 75.6939904,78.7735418 75.8255533,77.5479124 Z M67.992861,68.0896939 C68.1009305,67.0821982 69.0030763,66.3540394 70.0085928,66.4623216 C71.0141094,66.5706038 71.7408379,67.4745252 71.6327684,68.4820208 C71.5246989,69.4895165 70.6225531,70.2176753 69.6170365,70.1093931 C68.6130862,70.0011108 67.8847915,69.0971895 67.992861,68.0896939 Z"
                                id="path-1"
                              />
                            </defs>
                            <g id="Icon-/-Single-Clever" stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                              <mask id="mask-2" fill="white">
                                <use xlinkHref="#path-1" />
                              </mask>
                              <use
                                id="Mask"
                                fill="#4A4A4A"
                                fillRule="nonzero"
                                xlinkHref="#path-1"
                              />
                              <g id="Swatch-/-Dark-Gray" mask="url(#mask-2)" fill="#4A4A4A">
                                <rect id="Rectangle-5" x={0} y={0} width={100} height={100} />
                              </g>
                            </g>
                          </svg> :
                          <div className="knife-svg-lft-spl">
                            <svg
                              xmlns="http://www.w3.org/2000/svg"
                              xmlnsXlink="http://www.w3.org/1999/xlink"
                              version="1.1"
                              x="0px"
                              y="0px"
                              viewBox="0 0 100 100"
                              enableBackground="new 0 0 100 100"
                              xmlSpace="preserve"
                            >
                              <path
                                d="M94.353,31.47l-21.04-11.91c-0.248-0.141-0.546-0.169-0.816-0.076s-0.488,0.298-0.599,0.562  c-5.473,13.128-12.894,25.32-22.039,36.276c-9.093-10.913-16.557-23.165-22.022-36.275c-0.11-0.264-0.328-0.469-0.599-0.562  c-0.271-0.092-0.568-0.064-0.816,0.076L5.373,31.472c-0.467,0.264-0.642,0.848-0.398,1.325c6.13,12.009,13.917,23.099,23.144,32.961  c0.353,0.375,0.932,0.422,1.337,0.111l9.234-7.051c0.489-0.375,1.117-0.529,1.727-0.421c0.6,0.105,1.108,0.442,1.434,0.948  c0.745,1.161,1.51,2.305,2.284,3.441c-4.004,4.262-8.285,8.298-12.751,11.993c-0.214,0.178-0.345,0.436-0.36,0.714  c-0.017,0.277,0.084,0.549,0.277,0.749l6.341,6.601c0.187,0.195,0.443,0.306,0.713,0.308c0.003,0,0.006,0,0.008,0  c0.267,0,0.523-0.106,0.711-0.297c3.808-3.849,7.439-7.963,10.793-12.231c3.346,4.261,6.979,8.376,10.8,12.235  c0.188,0.189,0.444,0.296,0.711,0.296c0.003,0,0.005,0,0.008,0c0.27-0.002,0.527-0.113,0.714-0.308l6.335-6.605  c0.192-0.201,0.293-0.473,0.276-0.75c-0.016-0.277-0.146-0.535-0.36-0.712c-4.502-3.73-8.762-7.741-12.763-11.991  c0.771-1.108,1.543-2.254,2.297-3.438c0.325-0.507,0.835-0.844,1.437-0.95c0.606-0.109,1.237,0.044,1.725,0.415l9.229,7.051  c0.181,0.138,0.395,0.205,0.607,0.205c0.268,0,0.534-0.107,0.729-0.316c9.235-9.862,17.021-20.952,23.142-32.96  C94.994,32.317,94.818,31.733,94.353,31.47z M40.762,56.427c-0.241-0.042-0.483-0.063-0.726-0.063c-0.918,0-1.823,0.301-2.561,0.864  l-8.513,6.5c-8.613-9.323-15.931-19.741-21.769-30.99l19.233-10.884c5.118,12.006,11.897,23.281,20.048,33.473l-3.136,2.696  C42.712,57.194,41.798,56.61,40.762,56.427z M38.368,80.715l-4.884-5.084c4.124-3.467,8.083-7.222,11.812-11.167  c1.081,1.539,2.186,3.044,3.309,4.517c-0.101,0.13-0.201,0.263-0.302,0.391C45.204,73.316,41.866,77.128,38.368,80.715z   M66.247,75.629l-4.881,5.089c-3.51-3.597-6.853-7.41-9.945-11.35c-2.414-3.069-4.742-6.323-6.951-9.679l3.265-2.806  C53.317,63.669,59.509,69.96,66.247,75.629z M51.179,57.867c0.274-0.327,0.538-0.661,0.809-0.99l3.278,2.817  c-0.355,0.538-0.71,1.062-1.068,1.581C53.174,60.155,52.167,59.02,51.179,57.867z M70.766,63.725l-0.97-0.741l1.445-1.586  c3.656-4.075,7.11-8.407,10.265-12.875c0.319-0.451,0.212-1.075-0.239-1.394c-0.454-0.319-1.075-0.211-1.394,0.239  c-3.11,4.405-6.516,8.676-10.115,12.688l-1.557,1.709l-5.944-4.541c-0.933-0.708-2.131-0.999-3.286-0.794  c-1.036,0.184-1.949,0.767-2.576,1.596l-3.139-2.698c8.187-10.223,14.929-21.455,20.052-33.474l19.227,10.884  C86.703,43.984,79.387,54.4,70.766,63.725z M82.668,46.064c0.322,0,0.64-0.156,0.832-0.444c1.078-1.611,2.135-3.264,3.141-4.91  c0.288-0.472,0.139-1.087-0.332-1.375c-0.472-0.289-1.087-0.14-1.375,0.332c-0.991,1.624-2.033,3.253-3.096,4.842  c-0.308,0.459-0.184,1.08,0.275,1.387C82.284,46.01,82.477,46.064,82.668,46.064z M30.146,32.652  c-0.256-0.488-0.859-0.68-1.35-0.422c-0.489,0.256-0.678,0.86-0.422,1.35c3.165,6.048,6.795,11.939,10.789,17.513  c0.195,0.273,0.502,0.418,0.813,0.418c0.201,0,0.405-0.061,0.581-0.188c0.449-0.321,0.553-0.946,0.23-1.395  C36.849,44.431,33.268,38.618,30.146,32.652z M22.966,26.716c0.107-0.997,1.002-1.718,1.999-1.611  c0.997,0.107,1.718,1.002,1.611,1.999c-0.107,0.997-1.002,1.718-1.999,1.611C23.58,28.607,22.859,27.712,22.966,26.716z   M73.509,26.714c0.107-0.997,1.002-1.718,1.999-1.611c0.997,0.107,1.718,1.002,1.611,1.999c-0.107,0.997-1.002,1.718-1.999,1.611  C74.123,28.605,73.402,27.71,73.509,26.714z M58.77,74.874c0.097-0.898,0.902-1.547,1.799-1.451c0.897,0.096,1.546,0.903,1.45,1.8  c-0.097,0.898-0.902,1.547-1.799,1.451C59.322,76.578,58.673,75.772,58.77,74.874z M37.685,74.872  c0.097-0.898,0.903-1.547,1.8-1.451s1.547,0.903,1.451,1.8c-0.097,0.898-0.903,1.547-1.8,1.451  C38.238,76.576,37.589,75.77,37.685,74.872z M53.278,68.847c0.084-0.781,0.785-1.346,1.566-1.262  c0.781,0.084,1.346,0.785,1.262,1.566c-0.084,0.781-0.785,1.346-1.566,1.262C53.759,70.329,53.194,69.628,53.278,68.847z   M43.59,68.848c0.084-0.781,0.785-1.346,1.566-1.262c0.781,0.084,1.346,0.785,1.262,1.566c-0.084,0.781-0.785,1.346-1.566,1.262  C44.071,70.33,43.506,69.629,43.59,68.848z M48.277,62.82c0.069-0.642,0.645-1.106,1.287-1.037c0.642,0.069,1.106,0.645,1.037,1.287  c-0.069,0.642-0.645,1.106-1.287,1.037C48.673,64.038,48.208,63.462,48.277,62.82z"
                              />
                            </svg>
                          </div>
                        }
                        <span className="sidemenu-icn-tag-inner">{cookIndex.axe}</span>
                        <span>{badgeData}</span>
                      </div>
                      <div className="sidemenu-cntnt-row">
                        <h5>{cookDates} {cookDate1}/{cookDate2}</h5>
                        <p className="row-undrline">
                          <small>
                            <b>{totalHandsOnTime} </b>
                            Hands On
                            <span>
                              <b>{totalCookTime}</b> Total
                            </span>
                          </small>
                        </p>
                      </div>
                    </div> :
                    <div className="divider-small divider-top-big">
                      <span>No Cook Day Assigned</span>
                      <span className="divider-line" />

                    </div>
                  }

                  {cookIndex.map((index) => {

                    let noEmptyData = false;
                    if (index.name) {
                      noEmptyData = true;
                    }
                    if (index.lunch_date === '00-00-00') {
                      nonBatchDate.push({
                        id: index.recipe_id,
                        servingSize: index.servingSize,
                        basicSize: index.basic_size,
                      });
                    }
                    if (index.basic_size != null) {
                      size = index.basic_size;
                    }
                    if (index.lunch_date !== '00-00-00') {
                      testDate.push({
                        data: cookIndex.data,
                        date: index.lunch_date,
                        id: index.recipe_id,
                        color: color[cookIndex[cookLength].index],
                        type: index.type,
                      });

                      const nonBatchIndex = nonBatchDate.findIndex(x => x.id === index.recipe_id)
                      if (nonBatchIndex > -1) {
                        nonBatchDate[nonBatchIndex].servingSize += index.servingSize;
                        nonBatchDate[nonBatchIndex].basicSize = index.basic_size;
                      } else {
                        nonBatchDate.push({
                          id: index.recipe_id,
                          servingSize: index.servingSize,
                          basicSize: index.basic_size,
                        });
                      }
                    }
                    const handsOn = getTimeStamp(index.hands_on_time);
                    return (
                      <div key={index.name}>
                        <div className="sidemenu-row">
                          {noEmptyData ?
                            <div>
                              <div className="sidemenu-icn">
                                <img src={index.equipment_icn} alt="" className="equipment-icn" />
                                <span className="equipment-icon">{index.equipment_name}</span>
                              </div>
                              <div className="sidemenu-cntnt-row">
                                <p>{index.name}</p>
                                <p>
                                  <small>
                                    <b>{index.servingSize} </b>
                                    Total Servings
                                    <span><b>{handsOn}</b> Hands On</span>
                                  </small>
                                </p>
                              </div>
                            </div> :
                            <div className="divider-small">
                              <span>Empty Cook Day</span>
                              <span className="divider-line" />
                            </div>
                          }
                        </div>
                      </div>
                    );
                  })}
                </div>
                }
              </div>
            );
          })
        }
      </div>
    );
  }

  // Non Batch Data
  userNonBatchData(lunchDate) {
    const { mealData } = this.state;
    const nonBatchRecipeIds = [];
    let total = 0;
    let passive = 0;
    let equipmentName;
    let equipmentIcon;
    if (mealData.nonBatchData) {
      // dinner Data
      if (mealData.nonBatchData.dinner.length > 0) {
        const nonBatchDinnerArr = mealData.nonBatchData.dinner;
        nonBatchDinnerArr.map((dinner) => {
          if (nonBatchRecipeIds.includes(dinner.recipe_id) && dinner.deleted_at === null) {
            const count = totalValue(dinner.recipe_id, nonBatchRecipeIds);
            // eslint-disable-next-line
            for (let x in lunchDate) {
              // eslint-disable-next-line
              lunchDate[x].map((BatchMealData, key) => {
                if (BatchMealData.recipe_id === dinner.recipe_id) {
                  if (dinner.deleted_at === null) {
                    nonBatchRecipeIds.push(dinner.recipe_id);
                    sideDishId.push(dinner.side_dish_id);
                  }
                  total += (dinner.recipe.hands_on_time);
                  passive += (dinner.recipe.passive_time);
                  lunchDate[x][key].servingSize = dinner.recipe_serving_size * count;
                }
                return BatchMealData;
              });
            }
          } else {
            if (dinner.deleted_at === null) {
              sideDishId.push(dinner.side_dish_id);
              nonBatchRecipeIds.push(dinner.recipe_id);
              if (lunchDate.nonBatch);
              else {
                lunchDate.nonBatch = [];
              }
            }

            if (dinner.recipe && dinner.deleted_at === null) {
              if (dinner.recipe.recipe_equipment_type.length > 0) {
                equipmentIcon = `/images/${dinner.recipe.recipe_equipment_type[0].equipment_type.icon}`;
                equipmentName = dinner.recipe.recipe_equipment_type[0].equipment_type.title;
                if (equipmentIcon === '/images/') {
                  equipmentIcon = '/images/grill.svg';
                }
              } else {
                equipmentIcon = '/images/none.svg';
                equipmentName = 'None';
              }
              total += dinner.recipe.hands_on_time;
              passive += dinner.recipe.passive_time;
              lunchDate.nonBatch.push({
                lunch_date: '00-00-00',
                recipe_id: dinner.recipe_id,
                data: 'Single',
                axe: '1x',
                index: '',
                name: dinner.recipe.name,
                servingSize: dinner.recipe_serving_size,
                basic_size: dinner.recipe_serving_size,
                hands_on_time: dinner.recipe.hands_on_time + dinner.hands_on_time,
                total_hands_on_time: total,
                passive_time: passive,
                equipment_name: equipmentName,
                equipment_icn: equipmentIcon,
              });
            }
          }
          return lunchDate;
        });

        if (mealData.nonBatchData.lunch.length > 0) {
          const lunchArr = mealData.nonBatchData.lunch;
          lunchArr.map((lunch) => {
            if (nonBatchRecipeIds.includes(lunch.recipe_id) && lunch.deleted_at === null) {
              const count = totalValue(lunch.recipe_id, nonBatchRecipeIds);
              // eslint-disable-next-line
              for (let x in lunchDate) {
                // eslint-disable-next-line
                lunchDate[x].map((BatchMealData, key) => {
                  if (BatchMealData.recipe_id === lunch.recipe_id) {
                    if (lunch.deleted_at === null) {
                      nonBatchRecipeIds.push(lunch.recipe_id);
                      sideDishId.push(lunch.side_dish_id);
                    }
                    lunchDate[x][key].servingSize = lunch.recipe_serving_size * count;
                  }
                  return BatchMealData;
                });
              }
            } else {
              if (lunch.deleted_at === null) {
                nonBatchRecipeIds.push(lunch.recipe_id);
                sideDishId.push(lunch.side_dish_id);
                if (lunchDate.nonBatch);
                else {
                  lunchDate.nonBatch = [];
                }
              }

              if (lunch.recipe && lunch.deleted_at == null) {
                if (lunch.recipe.recipe_equipment_type.length > 0) {
                  equipmentIcon = `/images/${lunch.recipe.recipe_equipment_type[0].equipment_type.icon}`;
                  equipmentName = lunch.recipe.recipe_equipment_type[0].equipment_type.title;
                  if (equipmentIcon === '/images/') {
                    equipmentIcon = '/images/grill.svg';
                  }
                } else {
                  equipmentIcon = '/images/none.svg';
                  equipmentName = 'None';
                }
                total += lunch.recipe.hands_on_time;
                passive += (lunch.recipe.passive_time);
                lunchDate.nonBatch.push({
                  lunch_date: '00-00-00',
                  recipe_id: lunch.recipe_id,
                  data: 'Single',
                  axe: '1x',
                  index: '',
                  name: lunch.recipe.name,
                  servingSize: lunch.recipe_serving_size,
                  basic_size: lunch.recipe_serving_size,
                  hands_on_time: lunch.recipe.hands_on_time + lunch.hands_on_time,
                  total_hands_on_time: total,
                  passive_time: passive,
                  equipment_name: equipmentName,
                  equipment_icn: equipmentIcon,
                });
              }
            }
            return lunchDate;
          });
        }
      }
    }
    if (mealData.userWeeklyScheduleData) {
      const lunchWeeklyArr = mealData.userWeeklyScheduleData[0].user_lunch_date;
      const weekCookingValue = mealData.userWeeklyScheduleData[0].cooking_date;

      const weeklyIndex = lunchWeeklyArr.findIndex(x => x.lunch_date === weekCookingValue);
      if (weeklyIndex > -1) {
        if (nonBatchRecipeIds.includes(lunchWeeklyArr[weeklyIndex].recipe_id) &&
        lunchWeeklyArr[weeklyIndex].deleted_at == null) {
          const count = totalValue(lunchWeeklyArr[weeklyIndex].recipe_id, nonBatchRecipeIds);
          // eslint-disable-next-line
          for (let x in lunchDate) {
            // eslint-disable-next-line
            lunchDate[x].map((BatchMealData, key) => {
              if (BatchMealData.recipe_id === lunchWeeklyArr[weeklyIndex].recipe_id) {
                if (lunchWeeklyArr[weeklyIndex].deleted_at == null) {
                  sideDishId.push(lunchWeeklyArr[weeklyIndex].side_dish_id);
                  nonBatchRecipeIds.push(lunchWeeklyArr[weeklyIndex].recipe_id);
                }
                total += (lunchWeeklyArr[weeklyIndex].recipe.hands_on_time);
                passive += (lunchWeeklyArr[weeklyIndex].recipe.passive_time);
                lunchDate[x][key].servingSize = lunchWeeklyArr[weeklyIndex].recipe_serving_size * count;
              }
              return BatchMealData;
            });
          }
        } else {
          if (lunchWeeklyArr[weeklyIndex].deleted_at == null) {
            if (lunchDate.nonBatch);
            else {
              lunchDate.nonBatch = [];
            }
            sideDishId.push(lunchWeeklyArr[weeklyIndex].side_dish_id);
            nonBatchRecipeIds.push(lunchWeeklyArr[weeklyIndex].recipe_id);
          }
          if (lunchWeeklyArr[weeklyIndex].recipe && lunchWeeklyArr[weeklyIndex].deleted_at == null) {
            if (lunchWeeklyArr[weeklyIndex].recipe.recipe_equipment_type.length > 0) {
              equipmentIcon = `/images/${lunchWeeklyArr[weeklyIndex].recipe.recipe_equipment_type[0].equipment_type.icon}`;
              equipmentName = lunchWeeklyArr[weeklyIndex].recipe.recipe_equipment_type[0].equipment_type.title;
              if (equipmentIcon === '/images/') {
                equipmentIcon = '/images/grill.svg';
              }
            } else {
              equipmentIcon = '/images/none.svg';
              equipmentName = 'None';
            }
            total += lunchWeeklyArr[weeklyIndex].recipe.hands_on_time;
            passive += (lunchWeeklyArr[weeklyIndex].recipe.passive_time);
            lunchDate.nonBatch.push({
              lunch_date: '00-00-00',
              recipe_id: lunchWeeklyArr[weeklyIndex].recipe_id,
              data: 'Single',
              axe: '1x',
              index: '',
              name: lunchWeeklyArr[weeklyIndex].recipe.name,
              servingSize: lunchWeeklyArr[weeklyIndex].recipe_serving_size,
              basic_size: lunchWeeklyArr[weeklyIndex].recipe_serving_size,
              hands_on_time: lunchWeeklyArr[weeklyIndex].recipe.hands_on_time + lunchWeeklyArr[weeklyIndex].hands_on_time,
              total_hands_on_time: total,
              passive_time: passive,
              equipment_name: equipmentName,
              equipment_icn: equipmentIcon,
            });
          }
        }
      }
    }
    return lunchDate;
  }

  // Batch Schedule Data
  userWeeklyScheduleData(mealData, lunchDate) {
    const recipeIds = [];
    let totalSideTime = 0;
    let totalRecipeTime = 0;
    let handOn = 0;
    let totalCookingTime = 0;
    let equipmentIcon;
    let equipmentName;
    const { lunchDates } = this.state;
    if (mealData.userWeeklyScheduleData) {
      const length = mealData.userWeeklyScheduleData.length;
      mealData.userWeeklyScheduleData.map((BatchData, index) => {
        let total = 0;
        let passive = 0;

        const lunchArr = BatchData.user_lunch_date;
        const dinnerArr = BatchData.user_dinner_date;
        const data = 'Batch';

        dinnerArr.map((dinner) => {
          if (recipeIds.includes(dinner.recipe_id) && dinner.deleted_at === null) {
            const count = totalValue(dinner.recipe_id, recipeIds);
            if (dinner.deleted_at === null) {
              sideDishId.push(dinner.side_dish_id);
              recipeIds.push(dinner.recipe_id);
            }
            // eslint-disable-next-line
            for (let x in lunchDate) {
              lunchDate[x].map((BatchMealData, key) => {
                if (BatchMealData.recipe_id === dinner.recipe_id && BatchMealData.lunch_date !== '00-00-00' && dinner.deleted_at === null) {
                  lunchDate[x][key].servingSize = dinner.recipe_serving_size * count;
                }
                if (lunchDate[x].length > 1) {
                  lunchDate[x][key].data = 'Batch';
                } else {
                  lunchDate[x][key].data = 'Single';
                }
                return BatchMealData;
              });
            }
          } else if (dinner.deleted_at === null) {
            recipeIds.push(dinner.recipe_id);
            sideDishId.push(dinner.side_dish_id);
            if (lunchDate[`${BatchData.cooking_date}`]);
            else {
              lunchDate[`${BatchData.cooking_date}`] = [];
            }

            if (dinner.recipe) {
              if (dinner.recipe.recipe_equipment_type.length > 0) {
                equipmentIcon = `/images/${dinner.recipe.recipe_equipment_type[0].equipment_type.icon}`;
                equipmentName = dinner.recipe.recipe_equipment_type[0].equipment_type.title;
                if (equipmentIcon === '/images/') {
                  equipmentIcon = '/images/grill.svg';
                }
              } else {
                equipmentIcon = '/images/none.svg';
                equipmentName = 'None';
              }
              total = total + dinner.recipe.hands_on_time + dinner.hands_on_time;

              totalRecipeTime = dinner.recipe.hands_on_time + dinner.recipe.passive_time;
              totalSideTime = dinner.hands_on_time + dinner.passive_time;
              handOn = dinner.recipe.hands_on_time + dinner.hands_on_time;
              totalCookingTime = compareRecipeTime(totalRecipeTime, totalSideTime);
              totalCookingTime = compareRecipeTime(totalCookingTime, handOn);

              passive += totalCookingTime;

              lunchDate[`${BatchData.cooking_date}`].push({
                lunch_date: BatchData.cooking_date,
                recipe_id: dinner.recipe_id,
                data,
                axe: '',
                type: 'dinner',
                index,
                name: dinner.recipe.name,
                servingSize: dinner.recipe_serving_size,
                basic_size: dinner.recipe_serving_size,
                hands_on_time: handOn,
                total_hands_on_time: total,
                passive_time: passive,
                equipment_icn: equipmentIcon,
                equipment_name: equipmentName,

              });
            }
          }
          return lunchDates;
        });

        lunchArr.map((lunch) => {
          if (BatchData.cooking_date !== lunch.lunch_date) {
            if (recipeIds.includes(lunch.recipe_id) && lunch.deleted_at === null) {
              const count = totalValue(lunch.recipe_id, recipeIds);
              if (lunch.deleted_at === null) {
                sideDishId.push(lunch.side_dish_id);
                recipeIds.push(lunch.recipe_id);
              }
              // eslint-disable-next-line
              for (let x in lunchDate) {
                // eslint-disable-next-line
                lunchDate[x].map((BatchMealData, key) => {
                  if (BatchMealData.recipe_id === lunch.recipe_id && BatchMealData.lunch_date !== '00-00-00') {
                    lunchDate[x][key].servingSize = lunch.recipe_serving_size * count;
                  }
                  if (lunchDate[x].length > 1) {
                    lunchDate[x][key].data = 'Batch';
                  } else {
                    lunchDate[x][key].data = 'Single';
                  }
                  return BatchMealData;
                });
              }
            } else if (lunch.deleted_at === null) {
              sideDishId.push(lunch.side_dish_id);
              recipeIds.push(lunch.recipe_id);
              if (lunchDate[`${BatchData.cooking_date}`]);
              else {
                lunchDate[`${BatchData.cooking_date}`] = [];
              }

              if (lunch.recipe) {
                if (lunch.recipe.recipe_equipment_type.length > 0) {
                  equipmentIcon = `/images/${lunch.recipe.recipe_equipment_type[0].equipment_type.icon}`;
                  equipmentName = lunch.recipe.recipe_equipment_type[0].equipment_type.title;
                  if (equipmentIcon==='/images/'){
                    equipmentIcon = '/images/grill.svg';
                  }
                } else {
                  equipmentIcon = '/images/none.svg';
                  equipmentName = 'None';
                }
                total = total + lunch.recipe.hands_on_time + lunch.hands_on_time;
                totalRecipeTime = lunch.recipe.hands_on_time + lunch.recipe.passive_time;
                totalSideTime = lunch.hands_on_time + lunch.passive_time;
                handOn = lunch.recipe.hands_on_time + lunch.hands_on_time;
                totalCookingTime = compareRecipeTime(totalRecipeTime, totalSideTime);
                totalCookingTime = compareRecipeTime(totalCookingTime, handOn);
                passive += totalCookingTime;

                lunchDate[`${BatchData.cooking_date}`].push({
                  lunch_date: BatchData.cooking_date,
                  recipe_id: lunch.recipe_id,
                  data,
                  axe: '',
                  index,
                  type: 'lunch',
                  name: lunch.recipe.name,
                  servingSize: lunch.recipe_serving_size,
                  basic_size: lunch.recipe_serving_size,
                  hands_on_time: handOn,
                  total_hands_on_time: total,
                  passive_time: passive,
                  equipment_icn: equipmentIcon,
                  equipment_name: equipmentName,

                });
              }

            }
          }
          return lunchDate;
        });

        if (index < length - 1) {
          const nextLunchArr = mealData.userWeeklyScheduleData[index + 1].user_lunch_date;
          const nextCooking = mealData.userWeeklyScheduleData[index + 1].cooking_date;

          if (nextLunchArr) {
            const indexs = nextLunchArr.findIndex(x => x.lunch_date === nextCooking);
            if (indexs > -1) {
              if (recipeIds.includes(nextLunchArr[indexs].recipe_id) && nextLunchArr[indexs].deleted_at === null) {
                const count = totalValue(nextLunchArr[indexs].recipe_id, recipeIds);
                // eslint-disable-next-line
                for (let x in lunchDate) {
                  // eslint-disable-next-line
                  lunchDate[x].map((BatchMealData, key) => {
                    if (BatchMealData.recipe_id === nextLunchArr[indexs].recipe_id &&
                      BatchMealData.lunch_date !== '00-00-00') {
                      if (nextLunchArr[indexs].deleted_at === null) {
                        sideDishId.push(nextLunchArr[indexs].side_dish_id);
                        recipeIds.push(nextLunchArr[indexs].recipe_id);
                      }
                      lunchDate[x][key].servingSize = nextLunchArr[indexs].recipe_serving_size * count;
                    }
                    if (lunchDate[x].length > 1) {
                      lunchDate[x][key].data = 'Batch';
                    } else {
                      lunchDate[x][key].data = 'Single';
                    }
                    return BatchMealData;
                  });
                }
              } else {
                if (nextLunchArr[indexs].deleted_at === null) {
                  sideDishId.push(nextLunchArr[indexs].side_dish_id);
                  recipeIds.push(nextLunchArr[indexs].recipe_id);
                  if (lunchDate[`${BatchData.cooking_date}`]);
                  else {
                    lunchDate[`${BatchData.cooking_date}`] = [];
                  }
                }
                if (nextLunchArr[indexs].recipe && nextLunchArr[indexs].deleted_at === null) {
                  if (nextLunchArr[indexs].recipe.recipe_equipment_type.length > 0) {
                    equipmentIcon = `/images/${nextLunchArr[indexs].recipe.recipe_equipment_type[0].equipment_type.icon}`;
                    equipmentName = nextLunchArr[indexs].recipe.recipe_equipment_type[0].equipment_type.title;
                    if (equipmentIcon === '/images/') {
                      equipmentIcon = '/images/grill.svg';
                    }
                  } else {
                    equipmentIcon = '/images/none.svg';
                    equipmentName = 'None';
                  }
                  total = total + nextLunchArr[indexs].hands_on_time + nextLunchArr[indexs].recipe.hands_on_time;

                  totalRecipeTime = nextLunchArr[indexs].recipe.hands_on_time + nextLunchArr[indexs].recipe.passive_time;
                  totalSideTime = nextLunchArr[indexs].hands_on_time + nextLunchArr[indexs].passive_time;
                  handOn = nextLunchArr[indexs].recipe.hands_on_time + nextLunchArr[indexs].hands_on_time;
                  totalCookingTime = compareRecipeTime(totalRecipeTime, totalSideTime);
                  totalCookingTime = compareRecipeTime(totalCookingTime, handOn);
                  passive = totalCookingTime + passive;

                  lunchDate[`${BatchData.cooking_date}`].push({
                    lunch_date: BatchData.cooking_date,
                    recipe_id: nextLunchArr[indexs].recipe_id,
                    data,
                    axe: '',
                    index,
                    type: 'lunch',
                    name: nextLunchArr[indexs].recipe.name,
                    servingSize: nextLunchArr[indexs].recipe_serving_size,
                    basic_size: nextLunchArr[indexs].recipe_serving_size,
                    hands_on_time: nextLunchArr[indexs].recipe.hands_on_time + nextLunchArr[indexs].hands_on_time,
                    total_hands_on_time: total,
                    passive_time: passive,
                    equipment_icn: equipmentIcon,
                    equipment_name: equipmentName,
                  });
                }
              }
            }
          }
        }

        if (lunchDate[`${BatchData.cooking_date}`]);
        else {
          lunchDate[`${BatchData.cooking_date}`] = [];
          lunchDate[`${BatchData.cooking_date}`].push({
            lunch_date: BatchData.cooking_date,
            recipe_id: '',
            data: 'Single',
            axe: '1x',
            index,
            name: null,
            servingSize: null,
            basic_size: null,
            hands_on_time: null,
            total_hands_on_time: null,
            passive_time: null,
            equipment_icn: null,
            equipment_name: null,

          });
        }
        if (lunchDate[`${BatchData.cooking_date}`].length > 1) {
          lunchDate[`${BatchData.cooking_date}`].data = 'Batch';
          lunchDate[`${BatchData.cooking_date}`].axe = '';
        } else {
          lunchDate[`${BatchData.cooking_date}`].data = 'Single';
          lunchDate[`${BatchData.cooking_date}`].axe = '1x';
        }
        return BatchData;
      });
    }
    return lunchDates;
  }

  buildNextWeekPlan() {
    this.props.buildNextWeekPlan();
  }



  render() {
    const { groceryClass, grocery, error, path, viewRecipeData } = this.state;
    const topText = groceryClass !== 'active' ? 'Cooking Schedule' :
      'Grocery List Auto-created From This Cook Schedule';
    const mainClass = groceryClass !== 'active' && (path === null || path === undefined) ? '' : 'swap-page grocery-list-page';
    const viewMeal = path === null || path === undefined ;
    return (
      <section className={`dashboard-main ${mainClass}`}>
        <Steps
          enabled={this.isTourShowable()}
          steps={tourSteps}
          initialStep={0}
          onExit={this.onTourExit}
        />
        <ReactResizeDetector handleWidth handleHeight onResize={() => this.onResize()} />
        <div className="container">
          {/* left side bar */}
          <div className="col-lg-3 sidebar">
            <div className={`sidebar-cntnt ${grocery}`}>
              <div className="sidebar-cntnt-bg" />
              <h4 data-step="1" data-intro="This is your cooking schedule">{topText}</h4>
              {
                !error ?
                  this.lunchMeal()
                  :
                  <h4 className="empty-cook">
                    There’s nothing here yet. Start by building your plan first.
                  </h4>
              }

            </div>
          </div>
          {/* main content area with custom calander */}
          { viewMeal ?
            <WeekCalanderFullArea
              mealData={this.state.mealData}
              getWeekData={(dayDates, data, cookingDates, dinnerDates, lunchDates, selectedWeek, previous) =>
                this.getWeekData(dayDates, data, cookingDates, dinnerDates, lunchDates, selectedWeek, previous)}

              cookingDates={this.state.cookingDates}
              color={color}
              lunchDates={this.state.lunchDates}
              dinnerDates={this.state.dinnerDates}
              length={this.state.length}
              recipeLength={this.state.recipeLength}
              cookDate={testDate}
              servingSize={size}
              error={this.state.error}
              dayDates={this.state.dayDates}
              {...this.state}
              servingArray={nonBatchDate}
              sideDishArray={sideDishId}
            />
            :
            <ViewMeal
              viewRecipeData={viewRecipeData}
              loader={this.state.loadData}
              dayDates={this.state.dayDates}
              servingArray={nonBatchDate}
            />
          }

        </div>
      </section>
    );
  }
}

export default DashboardMain;
