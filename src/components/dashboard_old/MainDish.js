import React, { Component } from 'react';
import ReactHtmlParser from 'react-html-parser';

import { fraction } from 'mathjs';
import { getTimeStamp, getFormatTime } from 'lib/services';
import moment from "moment";

export default class MainDish extends Component {
  constructor(props) {
    super(props);
    this.state = {
      // response: this.props.response || null,
      servingSize: this.props.size,
      type: this.props.type.split('_')[0],
    };
  }
  // change UNSAFE
  // eslint-disable-next-line
  UNSAFE_componentWillReceiveProps(props) {
    if (props.response) {
      const { name } = props.response;
      this.setState({
        name,
        handsOnTime: props.response.hands_on_time,
        passiveTime: props.response.passive_time,
        recipeIngredients: props.response.recipe_ingredients,
        enableSwapButton: props.enableSwapButton,
        servingSize: props.size,
        recipeNutrition: props.response.nutrition_link,
        mealDate: props.fDate,
      });
      if (props.response.recipe_instructions) {
        this.setState({
          recipeInstruction: props.response.recipe_instructions,
        });
      }
    }
  }

  onSave() {
    const { enableSwapButton } = this.state;
    if (enableSwapButton) {
      this.props.onSave();
    } else {
      this.props.swapSave();
    }
  }

  saveAnalytics() {
    const { recipeNutrition, name, mealDate, type } = this.state
    window.analytics.track('Click Recipe Nutrition in Main Dish Tab', {
      recipeName: name,
      link: recipeNutrition,
      slot: type,
      date: moment(mealDate).format('dddd DD-MM-YYYY'),
    });
  }

  render() {
    const {
      name, passiveTime, recipeIngredients, recipeInstruction, enableSwapButton,
      servingSize, recipeNutrition
    } = this.state;
    let { handsOnTime } = this.state;
    let totaltime = getTimeStamp(handsOnTime + passiveTime);
    totaltime = getFormatTime(totaltime);
    handsOnTime = getTimeStamp(handsOnTime);
    handsOnTime = getFormatTime(handsOnTime);
    const recipeLink = recipeNutrition !== undefined;
    const buttonTxt = enableSwapButton ? 'Save' : 'Swap & Save';
    let check = false;
    let instructions = false;
    if (recipeIngredients) {
      check = true;
    }
    if (recipeInstruction) {
      instructions = true;
    }
    return (
      <div id="main-dish" className="tab-pane fade">
        <div className="tab-top-cntnt">
          <h3>{name}</h3>
          <div className="after-hdng-cntnt">
            <span>
              Hands On:
              <b> {handsOnTime}</b>
            </span>
            <span>
              Total Cook Time:
              <b> {totaltime}</b>
            </span>
          </div>
          { recipeLink &&
            <div className="recipe-link" onClick={() => this.saveAnalytics()}>
              <a href={recipeNutrition} target="_blank"> Recipe Nutrition Data </a>
            </div>
          }
          <div className="popup-ingredients">
            <h4>Ingredients</h4>

            { check &&
              recipeIngredients.map((value) => {
                let quantity = value.quantity;
                quantity *= (servingSize / 4);
                let a = 0;
                quantity = quantity.toString().split('.');
                quantity[0] = parseInt(quantity[0], 0);
                if (quantity[0] === 0) {
                  quantity[0] = '';
                }
                if (quantity.length > 1 && quantity[1] !== '00') {
                  quantity[1] = `${0}.${quantity[1]}`;
                  a = fraction(parseFloat(quantity[1]));
                  if (a.d > 0) {
                    a.d = Math.round(a.d / a.n);
                    a.n = 1;
                  }
                }
                if (a !== 0 && a.d > 1) {
                  a = ReactHtmlParser(`${quantity[0]}<sup>${a.n}</sup>&frasl;<sub>${a.d}</sub>`);
                } else if (a !== 0) {
                  a = a.n;
                } else {
                  a = quantity[0];
                }
                return (
                  <ul key={value.ingredient.name}>
                    <li key={value.id}>
                      {a} {value.unit} {value.ingredient.name }
                    </li>
                  </ul>
                );
              })
            }
          </div>
          {instructions &&
          <div className="popup-ingredients popup-instructions">
            <h4>Instructions</h4>
            <ol className="instructions">
              {
                recipeInstruction.map(value => (
                  <li key={value.id}>
                    {ReactHtmlParser(value.instruction) }
                  </li>
                ))
              }
            </ol>
          </div>
          }
        </div>
        {/* {<!--tab top content end-->} */}
        <div className="popup-ftr-nw">
          <div className="row">
            <div className="col-md-4 text-right pull-right">
              <button className="btn green-btn" onClick={() => this.onSave()}>
                {buttonTxt}
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
