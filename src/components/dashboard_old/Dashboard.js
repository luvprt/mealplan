/*
Component Name: Dashboard component
Contains : DashHeader, DashboardMain, BuildPlanScreen
Imcludes : Services for custom calander
Default screen: Dashboard
*/
import React, { Component } from 'react';
import RoutingConstants from 'lib/RoutingConstants';
import { post, get } from 'lib/api';
import { getSelectedWeekDates, getWeekDays } from 'lib/services';
import DashHeader from './DashHeader';
import BuildPlanScreen from '../plans/BuildPlanScreen';
import DashboardMain from './DashboardMain';
import ReplaceMultipleRecipe from './ReplaceMultipleRecipe';
import history from '../../history';
import Login from '../login/Login';
import UpgradePlan from '../billingDetails/UpgradePlan';
import ConfirmSubscription from '../billingDetails/ConfirmSubscription';

class Dashboard extends Component {
  constructor(props) {
    super(props);
    const weekStartEnd = getSelectedWeekDates('currentWeek');
    const dayDates = getWeekDays(weekStartEnd);
    const planType = this.props.location.state ? this.props.location.state.exist : 'yes';
    const path = this.props.location.state ? this.props.location.state.path : null;
    this.state = {
      isAuthenticated: false, // change to true
      guestEmail: null,
      mealData: {},
      dayDates,
      inputs: {},
      cookingDates: [],
      dinnerDates: [],
      lunchDates: [],
      length: '',
      slide: '',
      dashClass: 'active',
      slideRepeat: false,
      reminder: false,
      subscriptionPlan: [],
      confirmPop: false,
      loadData: true,
      showMultipleRecipe: false,
      planType,
      path,
      viewRecipeData: this.props.location.state || null,
      location:{
        pathname:'/dashboard'
      }
    };

  }

  componentDidMount() {
    this.getDateData();
  }

  onClose() {
    this.setState({ reminder: true });
  }

  onPopupClose(previous) {
    this.setState({ confirmPop: false, showMultipleRecipe: false, previous });
    localStorage.setItem('test', 'test');
  }

  getDateData() {

    const {
      cookingDate, dinnerDate, lunchDate, length, mealData
    } = this.state;
    let { dayDate, reminder,showMultipleRecipe } = this.state;
    let previous = false;
    if (this.props.location.state) {
      if (this.props.location.state.reminder) {
        reminder = this.props.location.state.reminder;
        if ((reminder === true && !localStorage.getItem('test') )|| localStorage.reminder) {
          this.setValues();
          this.getPlans(reminder);
        } else {
          reminder = false;
        }
      }
      if (this.props.location.state.type === '/login' ||
        localStorage.getItem('type') === 'login') {
        this.setValues();
      }
      if (this.props.location.state.previous === true) {
        previous = true;
      }

      if (localStorage.getItem('method') === 'back') {
        dayDate = this.props.location.state.dayDates;
        if(this.props.location.state.recipeSize > 1 && this.props.location.state.data) {
          showMultipleRecipe = true
        };
        this.setState({ dashClass: 'active', rdata: this.props.location.state.data });
        localStorage.removeItem('method');
      } else {
        const weekStartEnd = getSelectedWeekDates('currentWeek');
        dayDate = getWeekDays(weekStartEnd);
      }
    } else if (localStorage.type === 'login' || !localStorage.test ) {
      this.setValues();
      if(localStorage.reminder) {
        reminder = true;
        this.getPlans(true);
      }
    }
    if (this.props.location.pathname === '/groceryList') {
      this.setState({ groceryClass: 'active', dashClass: '' });
      if (this.props.location.state) {
        dayDate = this.props.location.state.dayDates;
      }
    }
    if (this.props.location.pathname === '/viewRecipe') {
      if (this.props.location.state) {
        dayDate = this.props.location.state.dayDates;
      }
    }

    this.getData(dayDate, mealData, cookingDate, dinnerDate, lunchDate, length, reminder, previous, showMultipleRecipe);
  }

  setValues() {
    let { loadData } = this.state;
    if (this.props.location.state) {
      if (this.props.location.state.reminder && localStorage.getItem('test') !== 'test') {
        loadData = true;
      }
    }
    this.setState({ isAuthenticated: true, loadData });
  }

  getData(dayDate, mealData, cookingDate, dinnerDate, lunchDate, len, reminder, previous, showMultipleRecipe) {
    const cookingDates = []
    const dinnerDates = [];
    const lunchDates = [];
    let recipeLength = 0;

    let { dayDates, loadData } = this.state;
    // return false;

    const url = RoutingConstants.getUserWeeklySchedule;
    const weakEndPoints = getSelectedWeekDates('currentWeek');
    /* get the days and the dates for current week */

    dayDates = dayDate || getWeekDays(weakEndPoints);
    const startDate = `${dayDates[0].Year}-${dayDates[0].Month}-${dayDates[0].date}`;
    const endDate = `${dayDates[6].Year}-${dayDates[6].Month}-${dayDates[6].date}`;

    const inputs = this.state.inputs;
    inputs.week_start_date = startDate;
    inputs.week_end_date = endDate;

    post(url, inputs).then((response) => {
      // console.log('tes', response.result.nonBatchData);
      if (response.result) {
        // get weekly schedule functionality

        if (response.result.userWeeklyScheduleData) {
          response.result.userWeeklyScheduleData.map((itemDate) => {
            const cookingIndex = cookingDates.findIndex(x => x.cooking_date === itemDate.cooking_date);
            let data = 'Batch';
            if (itemDate.user_lunch_date.length <= 1 &&
              itemDate.user_dinner_date.length <= 1
            ) {
              data = 'Single';
            } else {
              data = 'Batch';
            }

            if (cookingIndex === -1) {
              cookingDates.push({ cooking_date: itemDate.cooking_date, data });
            } else {
              cookingDates.splice(cookingIndex, 1, { cooking_date: itemDate.cooking_date, data });
            }
            itemDate.user_dinner_date.map((dinner) => {
              const dinnerIndex = dinnerDates.findIndex(x => x.dinner_date === dinner.dinner_date);
              if (dinnerIndex === -1 && dinner.deleted_at === null) {
                dinnerDates.push({ dinner_date: dinner.dinner_date });
              }
              if (dinner.recipe) {
                recipeLength += 1;
              }
              return dinnerDates;
            });
            itemDate.user_lunch_date.map((lunch) => {
              const lunchIndex = lunchDates.findIndex(x => x.lunch_date === lunch.lunch_date);
              if (lunchIndex === -1 && lunch.deleted_at === null) {
                lunchDates.push({ lunch_date: lunch.lunch_date });
              }
              if (lunch.recipe) {
                recipeLength += 1;
              }
              return lunchDates;
            });
            return itemDate;
          });
        }

        if (response.result.nonBatchData.dinner) {
          response.result.nonBatchData.dinner.map((dinner) => {
            const dinnerIndex = dinnerDates.findIndex(x => x.dinner_date === dinner.dinner_date);
            if (dinnerIndex === -1 && dinner.deleted_at === null) {
              dinnerDates.push({ dinner_date: dinner.dinner_date });
            }
            if (dinner.recipe) {
              recipeLength += 1;
            }
            return dinnerDates;
          });
        }
        if (response.result.nonBatchData.lunch) {
          response.result.nonBatchData.lunch.map((lunch) => {
            const lunchIndex = lunchDates.findIndex(x => x.lunch_date === lunch.dinner_date);
            if (lunchIndex === -1 && lunch.deleted_at === null) {
              lunchDates.push({ lunch_date: lunch.lunch_date });
            }
            if (lunch.recipe) {
              recipeLength += 1;
            }
            return lunchDates;
          });
        }
        if (reminder) {
          loadData = '';
        } else {
          loadData = reminder;
        }
        if(this.state.groceryClass === 'active') {
          this.groceryList(false);
        }
        this.setState({
          dayDates,
          dinnerDates,
          lunchDates,
          cookingDates,
          mealData: response.result,
          recipeLength,
          error: false,
          reminder,
          previous,
          loadData,
          planType: 'yes',
          showMultipleRecipe
        });
      }
      sessionStorage.setItem('planExist', 'no');
    }).catch((err) => {
      if(localStorage.getItem('type') !== 'login' || this.state.groceryClass === 'active') {
        this.setState({ loadData: false })
      }
      else if ((reminder || localStorage.reminder)) {
        reminder = true;
      }
      else {
        this.setState({ loadData: false })
      }
      this.setState({
        dayDates, mealData: {}, error: true, reminder, previous,
      });
      if (err.error === 'token_expired') {
        localStorage.removeItem('token');
        history.push('/login', {location: this.state.location});
      }
    });
  }

  getWeekData(dayDates, mealdata, cookingDates, dinnerDates, lunchDates, length, selectedWeek, previous) {
    const { slideRepeat } = this.state;
    let slide;
    if (selectedWeek === 'nextWeek') {
      slide= `slideright${slideRepeat}`
    } else if (selectedWeek === 'previousWeek') {
      slide= `slideleft${slideRepeat}`
    }
    this.setState({
      slide,
      cookingDates: [],
      dinnerDates: [],
      lunchDates: [],
      slideRepeat: !slideRepeat,
      recipeLength: 0,
      groceryList: [],
      loadData: true,
      customItems:[]
    });

    this.getData(dayDates, mealdata, cookingDates, dinnerDates, lunchDates, length, false, previous);
  }
  /* temp authentication  behaviour guest user to check and move to dashboard page */

  getWeeklySchedule(value, mealData) {
    if (value) {
      this.setState({
        isAuthenticated: true,
        mealData,
      });
    }
  }

  async getPlans(reminder) {
    const url = RoutingConstants.getSubscriptionPlan;
    await get(url).then((response) => {
      let result = response.result;
      if(response.result && response.result.data) {
        result = response.result.data
      }
      this.setState({ subscriptionPlan: result, reminder, loadData: false });
    }).catch(() => {
      // console.log('error', err);
    });
  }

  moveToDash(guestEmail, dayDates, mealData, dinnerDates, cookingDates, lunchDates) {
    const { length } = this.state;
    this.setState({
      isAuthenticated: true,
      guestEmail,
      dayDates,
      mealData,
      error: false,
    });

    localStorage.setItem('type', 'login');
    this.getData(dayDates, cookingDates, dinnerDates, lunchDates, length, false, false);
  }

  confirmScreen(planDetail) {
    this.setState({ reminder: false, planDetail, confirmPop: true });
  }

  saveMultiple(dayDates, previous) {
    const { mealdata, cookingDates, dinnerDates, lunchDates, length } = this.state;
    this.getData(dayDates, mealdata, cookingDates, dinnerDates, lunchDates, length, false, previous);
  }

  groceryList(error) {
    const { inputs } = this.state;
    const url = RoutingConstants.groceryList;
    if (error === true) {
      return false;
    }
    post(url, inputs).then((response) => {
      this.setState({
        groceryList: response.result.groceries,
        customItems: response.result.custom,
        loadData: true
      });
      if (this.state.error === true) {
        this.setState({ groceryList: [], recipeList: [] });
      }
    }).catch((err) => {
      if (err.error === 'token_expired') {
        localStorage.removeItem('token');
        history.push('/login');
      }
    });
    return inputs;
  }

  /* authentication behaviour end */
  render() {
    const {
      mealData, dayDates, cookingDates, length, dinnerDates, lunchDates, recipeLength,
      reminder, subscriptionPlan, confirmPop, planDetail, loadData, planType, location,
      showMultipleRecipe, rdata
    } = this.state;
    /* Check the history props for unauthorised url access */
    if (!this.props.location.state && localStorage.type ==='checkUser') {
      localStorage.removeItem('token');
      return <Login location={location}/>;
    }
    return (
      <div className="wrapper dash-cntnt">
        <DashHeader
          dashClass={this.state.dashClass}
          groceryClass={this.state.groceryClass}
          dayDates={dayDates}
          subscriptionPlan={subscriptionPlan}
          previous={this.state.previous}
          planExist={planType}
          reminder={reminder}
        />
        {!this.state.isAuthenticated ?
          <BuildPlanScreen
            userType="newUser"
            guestEmail="email"
            onClickBuild={(data, dayDat) => { this.moveToDash(data, dayDat, mealData, dinnerDates, cookingDates, lunchDates); }}
            getSchedule={(value, data) => this.getWeeklySchedule(value, data)}
            dayDates={dayDates}
            planExist={planType}
            reminder={reminder}
          /> :
          <div className={this.state.slide}>
            <DashboardMain
              mealData={mealData}
              dayDates={dayDates}
              cookingDates={cookingDates}
              dinnerDates={dinnerDates}
              length={length}
              lunchDates={lunchDates}
              getWeekMealData={(dayDate, meal, cookingDate, dinnerDate, lunchDate, len, selectedWeek, previous) => this.getWeekData(dayDate, meal, cookingDate, dinnerDate, lunchDate, len, selectedWeek, previous)}
              recipeLength={recipeLength}
              error={this.state.error}
              {... this.state}
              {...this.props.location}
            />
          </div>
        }
        {reminder &&
          !loadData &&
          <div>
            <UpgradePlan
              reminder={reminder}
              onClosedForm={data => this.confirmScreen(data)}
              onClose={() => this.onClose()}
              subscriptionPlan={subscriptionPlan}
              display="block"
              zIndex={1}
              page="User Dashboard"
            />
          </div>
        }
        { confirmPop &&
          <div>
            <ConfirmSubscription
              value={planDetail}
              onClose={() => this.onPopupClose()}
            />
            <div className="modal-backdrop fade in" />
          </div>
        }
        { showMultipleRecipe &&
          <div>
            <ReplaceMultipleRecipe
              value={planDetail}
              onClose={(previousWeek) => this.onPopupClose(previousWeek)}
              data={rdata}
              dayDates={dayDates}
              onSave={(previousWeek)=>this.saveMultiple(dayDates, previousWeek)}
            />
            <div className="modal-backdrop fade in" />
          </div>
        }
      </div>
    );
  }
}

export default Dashboard;
