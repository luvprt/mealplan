/*
Component Name: DashHeader component
Default screen: Dashboard
*/

import React, { Component } from 'react';
// import { PropTypes } from 'prop-types';
import history from '../../history';


class DashHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      /* for switch cases for grocery and cook mode as well */
      name: '',
      ...this.props,
      check: true,
    };
  }

  componentDidMount() {
    this.userData();
  }

  // unsafe
  // eslint-disable-next-line
  UNSAFE_componentWillReceiveProps(props) {
    this.setState({
      dashClass: props.dashClass || null,
      groceryClass: props.groceryClass || null,
      supportClass: props.supportClass || null,
      dayDates: props.dayDates,
      previous: props.previous,
      ...props,
    });
  }

  setIcon() {
    const { check } = this.state;
    this.setState({ check: !check });
  }

  userData() {
    const firstName = localStorage.getItem('firstName');
    const lastName = localStorage.getItem('lastName');
    const name = `${firstName} ${lastName}`;
    this.setState({ name });
  }

  dashboard() {
    const { dayDates, planExist, reminder, previous } = this.state;
    localStorage.setItem('method', 'back');
    history.push('/dashboard', {
      guestEmail: 'response.FirstName', type: '/login', dayDates, exist: planExist, reminder, previous
    });
  }

  grocery() {
    const {
      dayDates, previous, planExist, reminder,
    } = this.state;
    history.push('/groceryList', {
      guestEmail: 'response.FirstName', type: '/login', dayDates, previous, exist: planExist, reminder,
    });
  }

  // eslint-disable-next-line
  logOut() {
    localStorage.removeItem('token');
    localStorage.removeItem('reminder');
    history.push('/login');
  }

  // eslint-disable-next-line
  support() {
    const { planExist, reminder } = this.state;
    history.push('/support', { exist: planExist, reminder });
  }

  // eslint-disable-next-line
  billingDetail() {
    const { planExist, reminder } = this.state;
    history.push('/billingDetails', { exist: planExist, reminder });
  }

  // eslint-disable-next-line
  userProfile() {
    const { planExist, reminder } = this.state;
    history.push('/profileSettings', { exist: planExist, reminder });
  }

  render() {
    const {
      dashClass, groceryClass, check, supportClass,
    } = this.state;
    return (
      <header>
        <div className="container">
          <nav className="navbar navbar-default navbar-tall navbar-full">
            <div className="row">
              <div className="navbar-header col-md-3">
                <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#global-nav" aria-expanded="false">
                  <span className="sr-only">Toggle navigation</span>
                  <span className="icon-bar" />
                  <span className="icon-bar" />
                  <span className="icon-bar" />
                </button>
                <a className="navbar-brand " onClick={() => this.dashboard()}>
                  <img src="/images/logo.png" alt="" />
                </a>
              </div>
              <div className="navbar-collapse collapse" id="global-nav" aria-expanded="false">
                <div className="navbar-main col-md-9 nav-dash">
                  <ul className="nav navbar-nav nav-centr">
                    <li className={dashClass}>
                      <button className="headerLink" onClick={() => this.dashboard()}>
                        Weekly Plan
                      </button>
                    </li>
                    <li className={groceryClass}>
                      <button className="headerLink" onClick={() => this.grocery()}>
                        Grocery List
                      </button>
                    </li>
                    <li className={supportClass}>
                      <button className="headerLink" onClick={() => this.support()}>
                        Request a Feature
                      </button>
                    </li>
                    <li className="dropdown " onClick={() => this.setIcon()}>
                      <a className="dropdown-toggle" data-toggle="dropdown">
                        <span className="top-usr-drpdwn"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 0c88.366 0 160 71.634 160 160s-71.634 160-160 160S96 248.366 96 160 167.634 0 256 0zm183.283 333.821l-71.313-17.828c-74.923 53.89-165.738 41.864-223.94 0l-71.313 17.828C29.981 344.505 0 382.903 0 426.955V464c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48v-37.045c0-44.052-29.981-82.45-72.717-93.134z" /></svg></span>&nbsp;
                        <strong>{this.state.name}</strong>
                        {check ?
                          <svg
                            className="svIcon"
                            enableBackground="new 0 0 48 48"
                            height="10px"
                            id="Layer_3"
                            version="1.1"
                            viewBox="0 0 48 48"
                            width="10px"
                            xmlSpace="preserve"
                            xmlns="http://www.w3.org/2000/svg"
                            xmlnsXlink="http://www.w3.org/1999/xlink"
                          >
                            <polygon fill="#241F20" points="0,12.438 48,12.438 24,35.562 " />
                          </svg>
                          :
                          <svg
                            className="svIcon"
                            enableBackground="new 0 0 32 32"
                            height="13px"
                            id="svg2"
                            version="1.1"
                            viewBox="0 0 32 32"
                            width="13px"
                            xmlSpace="preserve"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <g id="backbord"><rect fill="none" height="32" width="32" /></g>
                            <g id="arrow_x5F_up">
                              <polygon points="30,22 16.001,8 2.001,22  " />
                            </g>
                          </svg>

                        }

                      </a>
                      <div className="dropdown-menu">
                        <a className="dropdown-item gray" onClick={() => this.userProfile()}>
                          My Ninja Profile
                        </a>
                        <a className="dropdown-item gray" onClick={() => this.billingDetail()}>
                          Billing Detail
                        </a>
                        <a className="dropdown-item gray" onClick={() => this.support()}>
                          Support / Report Bug
                        </a>
                        <a className="dropdown-item gray1" onClick={() => this.logOut()}>
                          Logout
                        </a>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </nav>
        </div>
      </header>
    );
  }
}

export default DashHeader;


// DashHeader.propTypes = {
//   selectedPage: PropTypes.object,
// };
