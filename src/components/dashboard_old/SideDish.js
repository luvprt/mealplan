import React, { Component } from 'react';
import ReactHtmlParser from 'react-html-parser';
import moment from 'moment';
import { getTimeStamp, totalValue } from 'lib/services';

import { fraction } from 'mathjs';

export default class SideDish extends Component {
  constructor(props) {
    super(props);
    this.state = {
      type: this.props.type.split('_')[0],
      ...this.props,
    };
  }

  // unsafe
  // eslint-disable-next-line
  UNSAFE_componentWillReceiveProps(props) {
    const id = props.sid;
    if (props.response[id]) {
      this.setState({
        name: props.response[id].name,
        handsOnTime: props.response[id].hands_on_time,
        passiveTime: props.response[id].passive_time,
        sideNutrition: props.response[id].nutrition_link,
        recipeIngredients: props.response[id].recipe_ingredients,
        enableSwapButton: props.enableSwapButton,
        servingSize: props.servingSize,
        mealDate: props.fDate,
      });
      if (props.response[id].recipe_instructions) {
        this.setState({
          recipeInstruction: props.response[id].recipe_instructions,
          sid: props.response[id].id,
          sideDishArray: props.sideDishArray,
        });
      }
    }
  }

  onSave() {
    const { enableSwapButton } = this.state;
    if (enableSwapButton) {
      this.props.onSave();
    } else {
      this.props.swapSave();
    }
  }

  saveAnalytics() {
    const { recipeNutrition, name, mealDate, type } = this.state
    window.analytics.track('Click Recipe Nutrition in Side Dish Tab', {
      sideRecipeName: name,
      link: recipeNutrition,
      slot: type,
      date: moment(mealDate).format('dddd DD-MM-YYYY'),
    });
  }

  render() {
    const {
      name, handsOnTime, passiveTime, recipeIngredients, recipeInstruction, enableSwapButton,
      sid, sideDishArray, servingSize, sideNutrition
    } = this.state;
    let totaltime = getTimeStamp(handsOnTime + passiveTime);
    totaltime = totaltime.split('m');
    let check = false;
    const sideLink = sideNutrition !== undefined;
    let instructions = false;
    let size = totalValue(sid, sideDishArray, 0);
    size *= servingSize;
    if (recipeIngredients) {
      check = true;
    }
    if (recipeInstruction) {
      instructions = true;
    }
    if(size === 0) {
      size = servingSize
    }
    return (
      <div id="side-dish" className="tab-pane fade">
        <div className="tab-top-cntnt">
          <h3>{name}</h3>
          <div className="after-hdng-cntnt">
            <span>
              Hands On Time:
              <b> {handsOnTime} mins</b>
            </span>
            <span>
              Total Cook Time:
              <b> {totaltime[0]} mins</b>
            </span>
          </div>
          { sideLink &&
            <div className="recipe-link" onClick={()=> this.saveAnalytics()}>
              <a href={sideNutrition} target="_blank"> Recipe Nutrition Data </a>
            </div>
          } 
          <div className="popup-ingredients">
            <h4>Ingredients</h4>
            { check &&
              recipeIngredients.map((value) => {
                let ingredientName = '';
                let quantity = value.quantity;
                quantity *= (size / 4);
                let a = 0;

                quantity = quantity.toString().split('.');
                quantity[0] = parseInt(quantity[0], 0);

                if (quantity[0] === 0) {
                  quantity[0] = '';
                }
                if (quantity.length > 1 && quantity[1] !== '00') {
                  quantity[1] = `${0}.${quantity[1]}`;
                  a = fraction(parseFloat(quantity[1]));
                  if (a.d > 0) {
                    a.d = Math.round(a.d / a.n);
                    a.n = 1;
                  }
                }
                if (a !== 0 && a.d > 1) {
                  a = ReactHtmlParser(`${quantity[0]}<sup>${a.n}</sup>&frasl;<sub>${a.d}</sub>`);
                } else if (a !== 0) {
                  a = a.n;
                } else {
                  a = quantity[0];
                }
                if (value.ingredient) {
                  ingredientName = value.ingredient.name;
                }
                return (
                  <ul key={ingredientName}>
                    <li key={value.id}>
                      {a} {value.unit} {ingredientName}
                    </li>
                  </ul>
                );
              })
            }
          </div>
          {instructions &&
          <div className="popup-ingredients popup-instructions">
            <h4>Instructions</h4>
            <ol className="instructions">
              {
                recipeInstruction.map(value => (
                  <li key={value.id}>
                    {ReactHtmlParser(value.instruction) }
                  </li>
                ))
              }
            </ol>
          </div>
          }
        </div>
        {/* <!--tab top content end--> */}
        <div className="popup-ftr-nw">
          <div className="row">
            <div className="col-md-4 text-right pull-right">
              <button
                className="btn green-btn"
                onClick={() => this.onSave()}
              >
                {enableSwapButton ? 'Save' : 'Swap & Save'}
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
