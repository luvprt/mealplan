/*
Component Name: RecipeForWeekCalander component
Default screen: Dashboard
*/

import React, { Component } from 'react';
import RoutingConstants from 'lib/RoutingConstants';
import { post, delet } from 'lib/api';
import Loader from 'lib/loader';
import { getTimeStamp, compareRecipeTime } from 'lib/services';
import LazyLoad from 'react-image-lazy-load';
import moment from 'moment';
import MealDetail from './MealDetail';
import history from '../../history';

class RecipeForWeekCalander extends Component {
  constructor(props) {
    super(props);
    this.state = {
      mealData: this.props.mealData,
      dayDates: this.props.dayDates,
      color: this.props.color || null,
      cookingDates: this.props.cookingDates || null,
      lunchDates: this.props.lunchDates || null,
      mealDetail: false,
      display: 'none',
      length: this.props.length,
      id: '',
      weekId: '',
      mealDate: '',
      type: null,
      dragEffect: '',
      showDeleteButton: 'none',
      buildPlanButton: this.props.buildPlanButton,
      ...this.props,
    };
  }

  // unsafe
  // eslint-disable-next-line
  UNSAFE_componentWillReceiveProps(props) {
    const { dayDates } = props;
    this.setState({
      mealData: props.mealData || {},
      dayDates,
      length: props.length,
      cookingDates: props.cookingDates || [],
      dinnerDates: props.dinnerDates || [],
      lunchDates: props.lunchDates || [],
      test: true,
      buildPlanButton: props.buildPlanButton,
      week: props.week,
      batchDates: props.batchDates,
      
      ...props,
    });
    
  }

  onClose() {
    this.setState({
      mealDetail: false,
    });
  }

  onSave() {
    this.setState({
      mealDetail: false,
    });
    this.props.swapRecipe(this.state.mealData);
  }

  // Drag Start
  onDragEvent(event, id, fdate, ftype, fid, weekId, name) {
    const { reminder } = this.state;

    if (!reminder) {
      const crt = event.target.cloneNode(true);
      this.setState({
        object1: event.target,
        dragEffect: id,
        clone: crt,
        fdate,
        ftype,
        fid,
        weekId,
        dragRecipe: name,
      });
      event.target.style.opacity = 0.3;
      event.target.style.border = '';
      event.dataTransfer.setData('html', event.target);
      crt.classList.add('hidden-drag-ghost');
      event.dataTransfer.effectAllowed = 'move';
      event.dataTransfer.dropEffect = 'move';
      event.target.appendChild(crt);
      event.dataTransfer.setDragImage(crt, 0, 122);
    }
  }

  // Drag Enter event
  onDragCaptureEvent(event) {
    const { reminder } = this.state;
    if (!reminder) {
      let parentId;
      let type;
      if (event.target.id && event.target.attributes.getNamedItem('name')) {
        type = event.target.attributes.getNamedItem('name').value;
        parentId = event.target.id.split('.');
        parentId[1] = type + parentId[1];
        // console.log(parentId[1], event.target);
        const g = document.getElementById(parentId[1]);
        g.style.border = '2px dashed black';
      }
    }
  }

  // Drag Leave
  onDragLeaveEvent(event) {
    const { reminder } = this.state;
    if (!reminder) {
      let parentId;
      let type;
      if (event.target.id && event.target.attributes.getNamedItem('name')) {
        parentId = event.target.id.split('.');
        type = event.target.attributes.getNamedItem('name').value;
        parentId[1] = type + parentId[1];
        const g = document.getElementById(parentId[1]);
        g.style.border = '';
      }
    }
  }

  onDragEndEvent() {
    const { reminder } = this.state;
    if (!reminder) {
      const { object1 } = this.state;
      object1.style.opacity = 1;
      if (this.state.clone.parentNode) {
        this.state.clone.parentNode.removeChild(this.state.clone);
      }
    }
  }

  // Drop event
  onDropEvent(event, tDate, ttype, tId, name) {
    const { reminder } = this.state;
    if (!reminder) {
      let parentId;
      let type;
      if (event.target.id && event.target.attributes.getNamedItem('name')) {
        parentId = event.target.id.split('.');
        type = event.target.attributes.getNamedItem('name').value;
        parentId[1] = type + parentId[1];
        const g = document.getElementById(parentId[1]);
        g.style.border = '';
        this.setState({
          dragEffect: '',
          tDate,
          ttype,
          tId,
          dropR: name,
        });

        const obj1 = this.state.object1;
        const obj2 = g;
        obj1.style.opacity = 1;
        this.state.clone.parentNode.removeChild(this.state.clone);
        obj2.style.border = 'none';
        this.swapRecipe(tDate, ttype, tId, name);
      }
    }
  }

  buildNextWeekPlan(daydates) {
    const { dayDates } = this.state;

    // track swap meal option
    window.analytics.track('Clicks Build New Plan', {
      startDate: `${dayDates[0].fullDay} ${dayDates[0].date}-${dayDates[0].Month}-${dayDates[0].Year}`,
      endDate: `${dayDates[6].fullDay} ${dayDates[6].date}-${dayDates[6].Month}-${dayDates[6].Year}`,
    });
    this.props.buildNextWeekPlan(daydates);
  }

  // swap api call
  async swapRecipe(tDate, ttype, tId, dropR) {
    const url = RoutingConstants.dragAndDrop;
    const inputs = {};
    const {
      fid, fdate, ftype, weekId, dragRecipe,
    } = this.state;
    this.setState({ loadData: true })
    // track drag-n-drop recipe
    window.analytics.track('Drag And Drop Recipe', {
      dragRecipe,
      dragDate: moment(fdate).format('dddd DD-MM-YYYY'),
      dragFrom: ftype,
      dropOnRecipe: dropR,
      dropDate: moment(tDate).format('dddd DD-MM-YYYY'),
      dragTo: ttype,
    });

    inputs.weakly_schedule_id = weekId;
    inputs.moved_to_id = tId;
    inputs.moved_to_date = tDate;
    inputs.moved_to_type = ttype;
    inputs.moved_from_id = fid;
    inputs.moved_from_date = fdate;
    inputs.moved_from_type = ftype;

    await post(url, inputs).then((response) => {
      this.setState({ mealData: this.props.mealData });
      this.props.swapRecipe(response);
    }).catch(() => {
      // console.log(error);
    });
  }

  allowDrop(event) {
    const { reminder } = this.state;
    if (!reminder) {
      event.dataTransfer.effectAllowed = 'copyMove';
      event.dataTransfer.dropEffect = 'move';
      event.preventDefault();
    }
  }

  // mouse over event
  mouseOverEvent(event, id) {
    const { reminder } = this.state;
    if (!reminder) {
      const g = document.getElementById(event);
      g.className = 'delete close';
      const h = document.getElementsByClassName(id)[0];
      h.classList.add('overlay-black');
    }
  }

  // mouse out event
  mouseOutEvent(event, id) {
    const { reminder } = this.state;
    if (!reminder) {
      const g = document.getElementById(event);
      g.className = 'test';
      const h = document.getElementsByClassName(id)[0];
      h.classList.remove('overlay-black');
    }
  }

  deleteMeal(id, type, name, mDate) {
    const { reminder } = this.state;
    this.setState({ loadData: true })
    if (!reminder) {
      const url = `${RoutingConstants.deleteMeal + id}?type=${type}`;
      delet(url).then((response) => {
        this.props.swapRecipe(response);
        // track delete recipe
        window.analytics.track('Delete Recipe', {
          recipeName: name,
          slot: type,
          date: moment(mDate).format('dddd DD-MM-YYYY'),
        });
      }).catch(() => {
        // console.log(err);
      });
    }
  }

  // dinner Data
  dinnerData(mealData) {
    const {
      dayDates, dragEffect, batchDates,
    } = this.state;
    let color;
    const recipeIds = [];
    let type;
    const dinnerDates = [];
    if (mealData && mealData.nonBatchData) {
      mealData.nonBatchData.dinner.map((dinnerDate) => {
        let name = null;
        if (dinnerDate.name !== null) {
          name = ` with ${dinnerDate.name}`;
        }
        if (dinnerDate.recipe) {
          this.dinnerDataAdd(dinnerDates, dinnerDate, name, '', '', '');
        }
        return dinnerDates;
      });
    }
    if (mealData && mealData.userWeeklyScheduleData) {
      let data = '';
      mealData.userWeeklyScheduleData.map((item) => {
        item.user_dinner_date.map((dinnerDate) => {
          let name = null;
          if (dinnerDate.name !== null) {
            name = dinnerDate.name;
          }
          if (dinnerDate.recipe) {
            if (this.state.batchDates) {
              const badgeIndex = batchDates.findIndex(x => x.id === dinnerDate.recipe.id);
              if (badgeIndex > -1) {
                data = batchDates[badgeIndex].data;
                color = batchDates[badgeIndex].color;
                type = batchDates[badgeIndex].type;
              }
            }
            this.dinnerDataAdd(dinnerDates, dinnerDate, name, data, color, type);
          }
          return dinnerDates;
        });
        return item;
      });
    }
    if (dinnerDates.length === 0 && this.state.buildPlanButton === true && !this.state.previous) {
      return (
        <div className="build-plan" />
      );
    }
    return (

      <div className="cal-days-cntnt">
        {
          dayDates.map((dayDate) => {
            let check = false;
            let leftOver = false;
            const mealDate = `${dayDate.Year}-${dayDate.Month}-${dayDate.date}`;
            let dinnerIndex;
            if (dinnerDates) {
              dinnerIndex = dinnerDates.findIndex(x => x.dinner_date === mealDate);
              // cookingIndex = cookingDates.findIndex(x => x.cooking_date === mealDate);
            }
            if (dinnerIndex > -1 && dinnerDates[dinnerIndex].visible == null) {
              const handsOnTime = getTimeStamp(dinnerDates[dinnerIndex].hands_on_time);
              const totalTime = getTimeStamp(dinnerDates[dinnerIndex].total_time);
              if (dinnerDates[dinnerIndex].data === 'Batch') {
                check = true;
              }
              if (dinnerDates[dinnerIndex].data === 'Single') {
                check = false;
                if (dinnerDates[dinnerIndex].type === 'lunch') {
                  leftOver = true;
                } else if (recipeIds.includes(dinnerDates[dinnerIndex].recipe_id)) {
                  leftOver = true;
                } else {
                  recipeIds.push(dinnerDates[dinnerIndex].recipe_id);
                }
              }
              const dDate = dinnerDates[dinnerIndex].dinner_date;
              let img = dinnerDates[dinnerIndex].img;
              let vertImg = dinnerDates[dinnerIndex].vertImg;
              let sideImg = dinnerDates[dinnerIndex].sideImg;

              // alert(img)
              if (dinnerDates[dinnerIndex].img === null) {
                img = '/images/small.png';
              }
              if (vertImg === null) {
                vertImg = '/images/small.png';
              }
              if (sideImg === null) {
                sideImg = '/images/small.png';
              }
              return (
                <div
                  className={dragEffect === `d${dDate}` ? 'meal-post drag-effect' : 'meal-post'}
                  key={dDate}
                  name="d."
                  onDragStart={event => this.onDragEvent(event, `d${dinnerDates[dinnerIndex].dinner_date}`, dinnerDates[dinnerIndex].dinner_date, 'dinner', dinnerDates[dinnerIndex].id, dinnerDates[dinnerIndex].weekly_id, dinnerDates[dinnerIndex].recipe_name)}
                  onDragOver={event => this.allowDrop(event)}
                  onDragOverCapture={event => this.onDragCaptureEvent(event)}
                  onDragLeave={event => this.onDragLeaveEvent(event)}
                  onDragEnd={() => this.onDragEndEvent()}
                  draggable
                  id={`d.${dDate}`}
                  onDrop={event => this.onDropEvent(event, dinnerDates[dinnerIndex].dinner_date, 'dinner', dinnerDates[dinnerIndex].id, dinnerDates[dinnerIndex].recipe_name)}
                  onMouseOver={() => this.mouseOverEvent(`dbutton.${dDate}`, `recipe_img ${dDate}`)}
                  onMouseOut={() => this.mouseOutEvent(`dbutton.${dDate}`, `recipe_img ${dDate}`)}
                >
                  <div
                    name="d."
                    id={`di.${dDate}`}
                    className="sidemenu-close"
                  >
                    <button
                      className="test"
                      name="d.button"
                      id={`dbutton.${dDate}`}
                      onClick={() => this.deleteMeal(dinnerDates[dinnerIndex].id, 'dinner', dinnerDates[dinnerIndex].recipe_name, dDate)}

                    >
                      <svg
                        viewBox="0 0 20 20"
                        version="1.1"
                        xmlns="http://www.w3.org/2000/svg"
                        xmlnsXlink="http://www.w3.org/1999/xlink"
                      >
                        <desc>Created with Sketch.</desc>
                        <defs />
                        <g id="Symbols" stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                          <g id="Icon-/-Delete-(White)" fill="#FFFFFF" fillRule="nonzero">
                            <g id="noun_1054717">
                              <path d="M10,0.104166667 C4.54166667,0.104166667 0.104166667,4.54166667 0.104166667,10 C0.104166667,15.4583333 4.54166667,19.8958333 10,19.8958333 C15.4583333,19.8958333 19.8958333,15.4583333 19.8958333,10 C19.8958333,4.54166667 15.4583333,0.104166667 10,0.104166667 Z M10,18.1458333 C5.5,18.1458333 1.85416667,14.5 1.85416667,10 C1.85416667,5.5 5.5,1.85416667 10,1.85416667 C14.5,1.85416667 18.1458333,5.5 18.1458333,10 C18.1458333,14.5 14.5,18.1458333 10,18.1458333 Z" id="Shape" />
                              <path d="M14.7708333,6.70833333 L13.2916667,5.22916667 C13.125,5.0625 12.875,5.0625 12.7083333,5.22916667 L10,7.9375 L7.29166667,5.22916667 C7.125,5.0625 6.875,5.0625 6.70833333,5.22916667 L5.22916667,6.70833333 C5.0625,6.875 5.0625,7.125 5.22916667,7.29166667 L7.9375,10 L5.22916667,12.7083333 C5.0625,12.875 5.0625,13.125 5.22916667,13.2916667 L6.70833333,14.7708333 C6.875,14.9375 7.125,14.9375 7.29166667,14.7708333 L10,12.0625 L12.7083333,14.7708333 C12.875,14.9375 13.125,14.9375 13.2916667,14.7708333 L14.7708333,13.2916667 C14.9375,13.125 14.9375,12.875 14.7708333,12.7083333 L12.0625,10 L14.7708333,7.29166667 C14.9375,7.125 14.9375,6.875 14.7708333,6.70833333 Z" id="Shape" />
                            </g>
                          </g>
                        </g>
                      </svg>
                    </button>
                  </div>
                  <a
                    onClick={() => this.mealDetail(
                      dinnerDates[dinnerIndex].dinner_date,
                      dinnerDates[dinnerIndex].recipe_id,
                      dinnerDates[dinnerIndex].weekly_id,
                      'dinner_date',
                      dinnerDates[dinnerIndex].serving_size,
                      dinnerDates[dinnerIndex].recipe_name,
                      dinnerDates[dinnerIndex].name,
                      handsOnTime,
                      totalTime,
                      dinnerDates[dinnerIndex].recipe_hands_on_time,
                      dinnerDates[dinnerIndex].recipe_time,
                      dinnerDates[dinnerIndex].side_hands_on,
                      dinnerDates[dinnerIndex].side_passive,
                      dinnerDates[dinnerIndex].id,
                      img, vertImg, sideImg,
                      dinnerDates[dinnerIndex].equipmentIcn,
                      dinnerDates[dinnerIndex].equipmentName,
                      dinnerDates[dinnerIndex].totalServing,
                    )}
                    draggable={false}
                    id={`a.${dDate}`}
                    name="d."
                  >

                    { check ?
                      <div className={`sidemenu-icn ${dinnerDates[dinnerIndex].color}`}>
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          xmlnsXlink="http://www.w3.org/1999/xlink"
                          version="1.1"
                          x="0px"
                          y="0px"
                          name="d."
                          id={`sideme.${dDate}`}
                          viewBox="0 0 100 100"
                          enableBackground="new 0 0 100 100"
                          xmlSpace="preserve"
                        >
                          <path
                            d="M94.353,31.47l-21.04-11.91c-0.248-0.141-0.546-0.169-0.816-0.076s-0.488,0.298-0.599,0.562  c-5.473,13.128-12.894,25.32-22.039,36.276c-9.093-10.913-16.557-23.165-22.022-36.275c-0.11-0.264-0.328-0.469-0.599-0.562  c-0.271-0.092-0.568-0.064-0.816,0.076L5.373,31.472c-0.467,0.264-0.642,0.848-0.398,1.325c6.13,12.009,13.917,23.099,23.144,32.961  c0.353,0.375,0.932,0.422,1.337,0.111l9.234-7.051c0.489-0.375,1.117-0.529,1.727-0.421c0.6,0.105,1.108,0.442,1.434,0.948  c0.745,1.161,1.51,2.305,2.284,3.441c-4.004,4.262-8.285,8.298-12.751,11.993c-0.214,0.178-0.345,0.436-0.36,0.714  c-0.017,0.277,0.084,0.549,0.277,0.749l6.341,6.601c0.187,0.195,0.443,0.306,0.713,0.308c0.003,0,0.006,0,0.008,0  c0.267,0,0.523-0.106,0.711-0.297c3.808-3.849,7.439-7.963,10.793-12.231c3.346,4.261,6.979,8.376,10.8,12.235  c0.188,0.189,0.444,0.296,0.711,0.296c0.003,0,0.005,0,0.008,0c0.27-0.002,0.527-0.113,0.714-0.308l6.335-6.605  c0.192-0.201,0.293-0.473,0.276-0.75c-0.016-0.277-0.146-0.535-0.36-0.712c-4.502-3.73-8.762-7.741-12.763-11.991  c0.771-1.108,1.543-2.254,2.297-3.438c0.325-0.507,0.835-0.844,1.437-0.95c0.606-0.109,1.237,0.044,1.725,0.415l9.229,7.051  c0.181,0.138,0.395,0.205,0.607,0.205c0.268,0,0.534-0.107,0.729-0.316c9.235-9.862,17.021-20.952,23.142-32.96  C94.994,32.317,94.818,31.733,94.353,31.47z M40.762,56.427c-0.241-0.042-0.483-0.063-0.726-0.063c-0.918,0-1.823,0.301-2.561,0.864  l-8.513,6.5c-8.613-9.323-15.931-19.741-21.769-30.99l19.233-10.884c5.118,12.006,11.897,23.281,20.048,33.473l-3.136,2.696  C42.712,57.194,41.798,56.61,40.762,56.427z M38.368,80.715l-4.884-5.084c4.124-3.467,8.083-7.222,11.812-11.167  c1.081,1.539,2.186,3.044,3.309,4.517c-0.101,0.13-0.201,0.263-0.302,0.391C45.204,73.316,41.866,77.128,38.368,80.715z   M66.247,75.629l-4.881,5.089c-3.51-3.597-6.853-7.41-9.945-11.35c-2.414-3.069-4.742-6.323-6.951-9.679l3.265-2.806  C53.317,63.669,59.509,69.96,66.247,75.629z M51.179,57.867c0.274-0.327,0.538-0.661,0.809-0.99l3.278,2.817  c-0.355,0.538-0.71,1.062-1.068,1.581C53.174,60.155,52.167,59.02,51.179,57.867z M70.766,63.725l-0.97-0.741l1.445-1.586  c3.656-4.075,7.11-8.407,10.265-12.875c0.319-0.451,0.212-1.075-0.239-1.394c-0.454-0.319-1.075-0.211-1.394,0.239  c-3.11,4.405-6.516,8.676-10.115,12.688l-1.557,1.709l-5.944-4.541c-0.933-0.708-2.131-0.999-3.286-0.794  c-1.036,0.184-1.949,0.767-2.576,1.596l-3.139-2.698c8.187-10.223,14.929-21.455,20.052-33.474l19.227,10.884  C86.703,43.984,79.387,54.4,70.766,63.725z M82.668,46.064c0.322,0,0.64-0.156,0.832-0.444c1.078-1.611,2.135-3.264,3.141-4.91  c0.288-0.472,0.139-1.087-0.332-1.375c-0.472-0.289-1.087-0.14-1.375,0.332c-0.991,1.624-2.033,3.253-3.096,4.842  c-0.308,0.459-0.184,1.08,0.275,1.387C82.284,46.01,82.477,46.064,82.668,46.064z M30.146,32.652  c-0.256-0.488-0.859-0.68-1.35-0.422c-0.489,0.256-0.678,0.86-0.422,1.35c3.165,6.048,6.795,11.939,10.789,17.513  c0.195,0.273,0.502,0.418,0.813,0.418c0.201,0,0.405-0.061,0.581-0.188c0.449-0.321,0.553-0.946,0.23-1.395  C36.849,44.431,33.268,38.618,30.146,32.652z M22.966,26.716c0.107-0.997,1.002-1.718,1.999-1.611  c0.997,0.107,1.718,1.002,1.611,1.999c-0.107,0.997-1.002,1.718-1.999,1.611C23.58,28.607,22.859,27.712,22.966,26.716z   M73.509,26.714c0.107-0.997,1.002-1.718,1.999-1.611c0.997,0.107,1.718,1.002,1.611,1.999c-0.107,0.997-1.002,1.718-1.999,1.611  C74.123,28.605,73.402,27.71,73.509,26.714z M58.77,74.874c0.097-0.898,0.902-1.547,1.799-1.451c0.897,0.096,1.546,0.903,1.45,1.8  c-0.097,0.898-0.902,1.547-1.799,1.451C59.322,76.578,58.673,75.772,58.77,74.874z M37.685,74.872  c0.097-0.898,0.903-1.547,1.8-1.451s1.547,0.903,1.451,1.8c-0.097,0.898-0.903,1.547-1.8,1.451  C38.238,76.576,37.589,75.77,37.685,74.872z M53.278,68.847c0.084-0.781,0.785-1.346,1.566-1.262  c0.781,0.084,1.346,0.785,1.262,1.566c-0.084,0.781-0.785,1.346-1.566,1.262C53.759,70.329,53.194,69.628,53.278,68.847z   M43.59,68.848c0.084-0.781,0.785-1.346,1.566-1.262c0.781,0.084,1.346,0.785,1.262,1.566c-0.084,0.781-0.785,1.346-1.566,1.262  C44.071,70.33,43.506,69.629,43.59,68.848z M48.277,62.82c0.069-0.642,0.645-1.106,1.287-1.037c0.642,0.069,1.106,0.645,1.037,1.287  c-0.069,0.642-0.645,1.106-1.287,1.037C48.673,64.038,48.208,63.462,48.277,62.82z"
                          />
                        </svg>
                        <span name="d." id={`sp.${dDate}`} >{dinnerDates[dinnerIndex].data}</span>
                      </div> :
                      leftOver ?
                        <div name="d." id={`div.${dDate}`} className={`sidemenu-icn ${dinnerDates[dinnerIndex].color}`}>
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            xmlnsXlink="http://www.w3.org/1999/xlink"
                            viewBox="0 0 24 24"
                            version="1.1"
                            x="0px"
                            y="0px"
                            name="d."
                            className="leftOver"
                          >
                            <desc>Created with Sketch.</desc><g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd"><g transform="translate(-93.000000, -45.000000)" fill="#000000"><g transform="translate(95.000000, 45.000000)"><path d="M2.00276013,0 L16.9972399,0 C18.1033337,0 19,0.901627257 19,2.00086422 L19,20.9991358 C19,22.1041826 18.1064574,23 16.9972399,23 L2.00276013,23 C0.896666251,23 0,22.0983727 0,20.9991358 L0,2.00086422 C0,0.895817426 0.893542647,0 2.00276013,0 Z M1,20.9991358 C1,21.5483309 1.4511921,22 2.00276013,22 L16.9972399,22 C17.5530645,22 18,21.553006 18,20.9991358 L18,2.00086422 C18,1.45166907 17.5488079,1 16.9972399,1 L2.00276013,1 C1.44693549,1 1,1.44699395 1,2.00086422 L1,20.9991358 Z" /><rect x="3" y="22" width="1" height="2" /><rect x="15" y="22" width="1" height="2" /><rect x="11" y="0" width="1" height="23" /><path d="M3,10 L7,10 L7,14 L3,14 L3,10 Z M4,11 L6,11 L6,13 L4,13 L4,11 Z" /><rect x="9" y="8" width="1" height="8" /><rect x="13" y="8" width="1" height="8" /></g></g></g>
                          </svg>

                          <span id={`sp.${dDate}`} name="d." >leftover</span>
                        </div> :
                        <div />
                    }
                    <div>
                      <LazyLoad
                        loaderImage
                        originalSrc={img}
                        imageProps={{
                          src: '/images/small.png',
                          draggable: false,
                          name: 'd.',
                          id: `j.${dDate}`,
                        }}
                        id={`i.${dDate}`}
                        className={`recipe_img ${dDate}`}
                      />
                    </div>
                    <h5 id={`h.${dDate}`} name="d.">{dinnerDates[dinnerIndex].recipe_name}</h5>
                    <div id={`mp.${dDate}`} name="d." className="meal-post-data">
                      <div id={`mp1.${dDate}`} name="d." className="meal-post-data-box meal-post-data-img">
                        <img
                          src={dinnerDates[dinnerIndex].equipmentIcn}
                          className="equipment-icn"
                          alt=""
                        />
                      </div>
                      <div id={`mp2.${dDate}`} name="d." className="meal-post-data-box">
                        <b id={`b1.${dDate}`} name="d." > {dinnerDates[dinnerIndex].serving_size} </b> Serv.
                      </div>
                      <div className="meal-post-data-box" id={`mp.${dDate}`} name="d.">
                        <b id={`b2.${dDate}`} name="d." > {handsOnTime} </b>
                        Hands On
                      </div>
                      <div className="meal-post-data-box" id={`d3.${dDate}`} name="d.">
                        <b id={`b3.${dDate}`} name="d.">{totalTime}</b>
                        Total
                      </div>
                    </div>
                  </a>
                </div>
              );
            }
            // eslint-disable-next-line
              let dId = '';
            if (dinnerIndex > -1 && dinnerDates[dinnerIndex].visible != null) {
              dId = dinnerDates[dinnerIndex].id;
            }
            return (
              <div
                key={mealDate}
                className="meal-post no-shadow-post"
                onDragStart={(event, id) => this.onDragEvent(event, `d${id}`)}
                onDragOver={event => this.allowDrop(event)}
                onDragOverCapture={event => this.onDragCaptureEvent(event)}
                onDragLeave={event => this.onDragLeaveEvent(event)}
                onDragEnd={event => this.onDragEndEvent(event)}
                draggable
                id={`d.${mealDate}`}
                name="d."
                onDrop={event => this.onDropEvent(event, mealDate, 'dinner', dId, 'null')}
              >
                <a
                  id={`a.${mealDate}`}
                  name="d."
                  className="add-meal-post"
                  onClick={event => this.addRecipe(event, `${dayDate.Year}-${dayDate.Month}-${dayDate.date}`, 'dinner_date', 'dinnerDates')}
                >
                  <svg id={`s.${mealDate}`} name="d." width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                    {/* Generator: Sketch 49.2 (51160) - http://www.bohemiancoding.com/sketch */}
                    <desc>Created with Sketch.</desc>
                    <defs />
                    <g id="Symbols" stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                      <g id="Icon-/-Add-Item-(Green)" fill="#13B75E" fillRule="nonzero">
                        <g id="noun_1015634">
                          <path d="M10,0 C4.4771525,3.70074342e-16 -6.41153797e-15,4.4771525 -7.10542736e-15,10 C-7.79931675e-15,15.5228475 4.4771525,20 10,20 C15.5228475,20 20,15.5228475 20,10 C20,7.3478351 18.9464316,4.80429597 17.0710678,2.92893219 C15.195704,1.0535684 12.6521649,9.25185854e-16 10,0 Z M10,17.5 C5.85786438,17.5 2.5,14.1421356 2.5,10 C2.5,5.85786438 5.85786438,2.5 10,2.5 C14.1421356,2.5 17.5,5.85786438 17.5,10 C17.5,11.9891237 16.7098237,13.896778 15.3033009,15.3033009 C13.896778,16.7098237 11.9891237,17.5 10,17.5 Z M11.25,8.75 L14.375,8.75 L14.375,11.25 L11.25,11.25 L11.25,14.375 L8.75,14.375 L8.75,11.25 L5.625,11.25 L5.625,8.75 L8.75,8.75 L8.75,5.625 L11.25,5.625 L11.25,8.75 Z" id="Shape" />
                        </g>
                      </g>
                    </g>
                  </svg>
                  <span id={`sp.${mealDate}`} name="d.">Add a Meal</span>
                </a>
              </div>
            );
          })
        }
      </div>
    );
  }

  // eslint-disable-next-line
  dinnerDataAdd(dinnerDates, dinnerDate, name, data, color, type) {
    const { servingArray } = this.state;
    // eslint-disable-next-line
    let handsTime = 0, totalRecipeTime = 0, totalSideTime = 0;
    // eslint-disable-next-line
    let totalDinnerTime = 0, equipmentIcon, equipmentName;
    let totalServing = 1;
    if (dinnerDate.recipe.recipe_equipment_type.length > 0) {
      equipmentIcon = `/images/${dinnerDate.recipe.recipe_equipment_type[0].equipment_type.icon}`;
      equipmentName = dinnerDate.recipe.recipe_equipment_type[0].equipment_type.title;
      if (equipmentIcon === '/images/') {
        equipmentIcon = '/images/none.svg';
      }
    } else {
      equipmentIcon = '/images/none.svg';
      equipmentName = 'None';
    }
    let servingIndex;
    if (servingArray && servingArray.length > 0) {
      servingIndex = servingArray.findIndex(x => x.id === dinnerDate.recipe.id);
    }
    if (servingIndex > -1) {
      totalServing = servingArray[servingIndex].servingSize;
    }
    // compare recipe and side time
    handsTime = dinnerDate.recipe.hands_on_time + dinnerDate.hands_on_time;
    totalRecipeTime = (dinnerDate.recipe.hands_on_time + dinnerDate.recipe.passive_time);
    totalSideTime = (dinnerDate.hands_on_time + dinnerDate.passive_time);
    totalDinnerTime = compareRecipeTime(totalRecipeTime, totalSideTime);
    totalDinnerTime = compareRecipeTime(totalDinnerTime, handsTime);
    dinnerDates.push({
      dinner_date: dinnerDate.dinner_date,
      data,
      recipe_name: dinnerDate.recipe.name,
      name,
      img: dinnerDate.recipe.image,
      vertImg: dinnerDate.recipe.vert_image,
      sideImg: dinnerDate.image,
      visible: dinnerDate.deleted_at,
      equipmentIcn: equipmentIcon,
      equipmentName,
      color,
      type,
      recipe_id: dinnerDate.recipe.id,
      weekly_id: dinnerDate.user_weekly_schedule_id,
      id: dinnerDate.id,
      serving_size: dinnerDate.recipe_serving_size,
      hands_on_time: handsTime,
      recipe_hands_on_time: dinnerDate.recipe.hands_on_time,
      total_time: totalDinnerTime,
      recipe_time: totalRecipeTime,
      side_hands_on: dinnerDate.hands_on_time,
      side_passive: dinnerDate.passive_time,
      totalServing,
    });
  }

  lunchData(mealData) {
    const {
      dayDates, cookingDates, dragEffect, batchDates,
    } = this.state;
    const recipeIds = [];
    let color;
    const lunchDates = [];
    if (mealData && mealData.nonBatchData) {
    //  bLength = mealData.nonBatchData.lunch.length;
      mealData.nonBatchData.lunch.map((lunchDate) => {
        if (lunchDate.recipe) {
          let name = null;
          if (lunchDate.name !== null) {
            name = lunchDate.name;
          }
          this.lunchDataAdd(lunchDates, lunchDate, name, '', '', '');
        }
        return lunchDates;
      });
    }
    let data = 'Single';
    if (mealData && mealData.userWeeklyScheduleData) {
      mealData.userWeeklyScheduleData.map((item, key) => {
        // map lunch function
        item.user_lunch_date.map((lunchDate) => {
          let type;
          // batch and single condition
          if (lunchDate.recipe) {
            if (this.state.batchDates) {
              const badgeIndex = batchDates.findIndex(x => x.id === lunchDate.recipe.id);
              if (badgeIndex > -1) {
                data = batchDates[badgeIndex].data;
                color = batchDates[badgeIndex].color;
                type = batchDates[badgeIndex].type;
              }
            }
            if (item.cooking_date === lunchDate.lunch_date && key === 0) {
              data = '';
            }
            let name = null;
            if (lunchDate.name !== null) {
              name = lunchDate.name;
            }
            this.lunchDataAdd(lunchDates, lunchDate, name, data, color, type);
          }
          return lunchDates;
        });
        return false;
      });
    }
    if (lunchDates.length === 0 &&
      this.state.buildPlanButton === true &&
      !this.state.previous) {
      return (
        <div className="build-plan">
          <p><span>It{'\'s'}  the start of new week Ninja!</span></p>
          <p><span>Just a couple of inputs from you and this plan will build itself</span></p>
          <button className="build-plan-btn" onClick={daydates => this.buildNextWeekPlan(daydates)}>Build This Week&apos;s Plan</button>
        </div>
      );
    }
    return (
      <div className="cal-days-cntnt">
        {
          dayDates.map((dayDate, key) => {
            let check = false;
            let leftOver = false;
            const mealDate = `${dayDate.Year}-${dayDate.Month}-${dayDate.date}`;
            let lunchIndex;
            if (cookingDates) {
              lunchIndex = lunchDates.findIndex(x => x.lunch_date === mealDate);
            }
            if (lunchIndex > -1 && lunchDates[lunchIndex].visible == null) {
              const handsOnTime = getTimeStamp(lunchDates[lunchIndex].hands_on_time);
              const totalTime = getTimeStamp(lunchDates[lunchIndex].total_time);

              if (lunchDates[lunchIndex].data === 'Batch') {
                check = true;
              }
              if (lunchDates[lunchIndex].data === 'Single') {
                check = false;
                if (lunchDates[lunchIndex].type === 'dinner') {
                  leftOver = true;
                } else if (recipeIds.includes(lunchDates[lunchIndex].recipe_id)) {
                  leftOver = true;
                } else {
                  recipeIds.push(lunchDates[lunchIndex].recipe_id);
                }
              }

              const lDate = lunchDates[lunchIndex].lunch_date;
              let img = lunchDates[lunchIndex].img;
              let vertImg = lunchDates[lunchIndex].vertImg;
              let sideImg = lunchDates[lunchIndex].sideImg;
              if (lunchDates[lunchIndex].img === null) {
                img = '/images/small.png';
              }
              if (vertImg === null) {
                vertImg = '/images/small.png';
              }
              if (sideImg === null) {
                sideImg = '/images/small.png';
              }

              return (
                <div
                  className={dragEffect === `l${key}` ? 'meal-post drag-effect' : 'meal-post'}
                  key={lDate}
                  onDragStart={event => this.onDragEvent(event, `l${lDate}`, lunchDates[lunchIndex].lunch_date, 'lunch', lunchDates[lunchIndex].id, lunchDates[lunchIndex].weekly_id, lunchDates[lunchIndex].recipe_name)}
                  onDragOver={event => this.allowDrop(event)}
                  onDragOverCapture={event => this.onDragCaptureEvent(event)}
                  onDragLeave={event => this.onDragLeaveEvent(event)}
                  onDragEnd={event => this.onDragEndEvent(event)}
                  name="l."
                  id={`l.${lDate}`}
                  draggable
                  onDrop={event => this.onDropEvent(event, lunchDates[lunchIndex].lunch_date, 'lunch', lunchDates[lunchIndex].id, lunchDates[lunchIndex].recipe_name)}
                  onMouseOver={() => this.mouseOverEvent(`button.${lDate}`, `recipe_img d.${lDate}`)}
                  onMouseOut={() => this.mouseOutEvent(`button.${lDate}`, `recipe_img d.${lDate}`)}
                >
                  <div
                    name="l."
                    id={`di.${lDate}`}
                    className="sidemenu-close"
                  >
                    <button
                      className="test"
                      name="l.button"
                      id={`button.${lDate}`}
                      onClick={() => this.deleteMeal(lunchDates[lunchIndex].id, 'lunch', lunchDates[lunchIndex].recipe_name, lDate)}
                    >
                      <svg

                        viewBox="0 0 20 20"
                        version="1.1"
                        xmlns="http://www.w3.org/2000/svg"
                        xmlnsXlink="http://www.w3.org/1999/xlink"
                      >
                        <desc>Created with Sketch.</desc>
                        <defs />
                        <g id="Symbols" stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                          <g id="Icon-/-Delete-(White)" fill="#FFFFFF" fillRule="nonzero">
                            <g id="noun_1054717">
                              <path d="M10,0.104166667 C4.54166667,0.104166667 0.104166667,4.54166667 0.104166667,10 C0.104166667,15.4583333 4.54166667,19.8958333 10,19.8958333 C15.4583333,19.8958333 19.8958333,15.4583333 19.8958333,10 C19.8958333,4.54166667 15.4583333,0.104166667 10,0.104166667 Z M10,18.1458333 C5.5,18.1458333 1.85416667,14.5 1.85416667,10 C1.85416667,5.5 5.5,1.85416667 10,1.85416667 C14.5,1.85416667 18.1458333,5.5 18.1458333,10 C18.1458333,14.5 14.5,18.1458333 10,18.1458333 Z" id="Shape" />
                              <path d="M14.7708333,6.70833333 L13.2916667,5.22916667 C13.125,5.0625 12.875,5.0625 12.7083333,5.22916667 L10,7.9375 L7.29166667,5.22916667 C7.125,5.0625 6.875,5.0625 6.70833333,5.22916667 L5.22916667,6.70833333 C5.0625,6.875 5.0625,7.125 5.22916667,7.29166667 L7.9375,10 L5.22916667,12.7083333 C5.0625,12.875 5.0625,13.125 5.22916667,13.2916667 L6.70833333,14.7708333 C6.875,14.9375 7.125,14.9375 7.29166667,14.7708333 L10,12.0625 L12.7083333,14.7708333 C12.875,14.9375 13.125,14.9375 13.2916667,14.7708333 L14.7708333,13.2916667 C14.9375,13.125 14.9375,12.875 14.7708333,12.7083333 L12.0625,10 L14.7708333,7.29166667 C14.9375,7.125 14.9375,6.875 14.7708333,6.70833333 Z" id="Shape" />
                            </g>
                          </g>
                        </g>
                      </svg>
                    </button>
                  </div>
                  <a
                    onClick={
                      () => this.mealDetail(
                        lunchDates[lunchIndex].lunch_date,
                        lunchDates[lunchIndex].recipe_id,
                        lunchDates[lunchIndex].weekly_id,
                        'lunch_date',
                        lunchDates[lunchIndex].serving_size,
                        lunchDates[lunchIndex].recipe_name,
                        lunchDates[lunchIndex].name,
                        handsOnTime,
                        totalTime,
                        lunchDates[lunchIndex].recipe_hands_on_time,
                        lunchDates[lunchIndex].recipe_time,
                        lunchDates[lunchIndex].side_hands_on,
                        lunchDates[lunchIndex].side_passive,
                        lunchDates[lunchIndex].id,
                        img, vertImg, sideImg,
                        lunchDates[lunchIndex].equipmentIcn,
                        lunchDates[lunchIndex].equipmentName,
                        lunchDates[lunchIndex].totalServing,
                      )}
                    name="l."
                    id={`a.${lDate}`}
                  >
                    { check ?
                      <div className={`sidemenu-icn ${lunchDates[lunchIndex].color}`}>
                        <svg
                          xmlns="http://www.w3.org/2000/svg"
                          xmlnsXlink="http://www.w3.org/1999/xlink"
                          version="1.1"
                          x="0px"
                          y="0px"
                          viewBox="0 0 100 100"
                          enableBackground="new 0 0 100 100"
                          xmlSpace="preserve"
                        >
                          <path
                            d="M94.353,31.47l-21.04-11.91c-0.248-0.141-0.546-0.169-0.816-0.076s-0.488,0.298-0.599,0.562  c-5.473,13.128-12.894,25.32-22.039,36.276c-9.093-10.913-16.557-23.165-22.022-36.275c-0.11-0.264-0.328-0.469-0.599-0.562  c-0.271-0.092-0.568-0.064-0.816,0.076L5.373,31.472c-0.467,0.264-0.642,0.848-0.398,1.325c6.13,12.009,13.917,23.099,23.144,32.961  c0.353,0.375,0.932,0.422,1.337,0.111l9.234-7.051c0.489-0.375,1.117-0.529,1.727-0.421c0.6,0.105,1.108,0.442,1.434,0.948  c0.745,1.161,1.51,2.305,2.284,3.441c-4.004,4.262-8.285,8.298-12.751,11.993c-0.214,0.178-0.345,0.436-0.36,0.714  c-0.017,0.277,0.084,0.549,0.277,0.749l6.341,6.601c0.187,0.195,0.443,0.306,0.713,0.308c0.003,0,0.006,0,0.008,0  c0.267,0,0.523-0.106,0.711-0.297c3.808-3.849,7.439-7.963,10.793-12.231c3.346,4.261,6.979,8.376,10.8,12.235  c0.188,0.189,0.444,0.296,0.711,0.296c0.003,0,0.005,0,0.008,0c0.27-0.002,0.527-0.113,0.714-0.308l6.335-6.605  c0.192-0.201,0.293-0.473,0.276-0.75c-0.016-0.277-0.146-0.535-0.36-0.712c-4.502-3.73-8.762-7.741-12.763-11.991  c0.771-1.108,1.543-2.254,2.297-3.438c0.325-0.507,0.835-0.844,1.437-0.95c0.606-0.109,1.237,0.044,1.725,0.415l9.229,7.051  c0.181,0.138,0.395,0.205,0.607,0.205c0.268,0,0.534-0.107,0.729-0.316c9.235-9.862,17.021-20.952,23.142-32.96  C94.994,32.317,94.818,31.733,94.353,31.47z M40.762,56.427c-0.241-0.042-0.483-0.063-0.726-0.063c-0.918,0-1.823,0.301-2.561,0.864  l-8.513,6.5c-8.613-9.323-15.931-19.741-21.769-30.99l19.233-10.884c5.118,12.006,11.897,23.281,20.048,33.473l-3.136,2.696  C42.712,57.194,41.798,56.61,40.762,56.427z M38.368,80.715l-4.884-5.084c4.124-3.467,8.083-7.222,11.812-11.167  c1.081,1.539,2.186,3.044,3.309,4.517c-0.101,0.13-0.201,0.263-0.302,0.391C45.204,73.316,41.866,77.128,38.368,80.715z   M66.247,75.629l-4.881,5.089c-3.51-3.597-6.853-7.41-9.945-11.35c-2.414-3.069-4.742-6.323-6.951-9.679l3.265-2.806  C53.317,63.669,59.509,69.96,66.247,75.629z M51.179,57.867c0.274-0.327,0.538-0.661,0.809-0.99l3.278,2.817  c-0.355,0.538-0.71,1.062-1.068,1.581C53.174,60.155,52.167,59.02,51.179,57.867z M70.766,63.725l-0.97-0.741l1.445-1.586  c3.656-4.075,7.11-8.407,10.265-12.875c0.319-0.451,0.212-1.075-0.239-1.394c-0.454-0.319-1.075-0.211-1.394,0.239  c-3.11,4.405-6.516,8.676-10.115,12.688l-1.557,1.709l-5.944-4.541c-0.933-0.708-2.131-0.999-3.286-0.794  c-1.036,0.184-1.949,0.767-2.576,1.596l-3.139-2.698c8.187-10.223,14.929-21.455,20.052-33.474l19.227,10.884  C86.703,43.984,79.387,54.4,70.766,63.725z M82.668,46.064c0.322,0,0.64-0.156,0.832-0.444c1.078-1.611,2.135-3.264,3.141-4.91  c0.288-0.472,0.139-1.087-0.332-1.375c-0.472-0.289-1.087-0.14-1.375,0.332c-0.991,1.624-2.033,3.253-3.096,4.842  c-0.308,0.459-0.184,1.08,0.275,1.387C82.284,46.01,82.477,46.064,82.668,46.064z M30.146,32.652  c-0.256-0.488-0.859-0.68-1.35-0.422c-0.489,0.256-0.678,0.86-0.422,1.35c3.165,6.048,6.795,11.939,10.789,17.513  c0.195,0.273,0.502,0.418,0.813,0.418c0.201,0,0.405-0.061,0.581-0.188c0.449-0.321,0.553-0.946,0.23-1.395  C36.849,44.431,33.268,38.618,30.146,32.652z M22.966,26.716c0.107-0.997,1.002-1.718,1.999-1.611  c0.997,0.107,1.718,1.002,1.611,1.999c-0.107,0.997-1.002,1.718-1.999,1.611C23.58,28.607,22.859,27.712,22.966,26.716z   M73.509,26.714c0.107-0.997,1.002-1.718,1.999-1.611c0.997,0.107,1.718,1.002,1.611,1.999c-0.107,0.997-1.002,1.718-1.999,1.611  C74.123,28.605,73.402,27.71,73.509,26.714z M58.77,74.874c0.097-0.898,0.902-1.547,1.799-1.451c0.897,0.096,1.546,0.903,1.45,1.8  c-0.097,0.898-0.902,1.547-1.799,1.451C59.322,76.578,58.673,75.772,58.77,74.874z M37.685,74.872  c0.097-0.898,0.903-1.547,1.8-1.451s1.547,0.903,1.451,1.8c-0.097,0.898-0.903,1.547-1.8,1.451  C38.238,76.576,37.589,75.77,37.685,74.872z M53.278,68.847c0.084-0.781,0.785-1.346,1.566-1.262  c0.781,0.084,1.346,0.785,1.262,1.566c-0.084,0.781-0.785,1.346-1.566,1.262C53.759,70.329,53.194,69.628,53.278,68.847z   M43.59,68.848c0.084-0.781,0.785-1.346,1.566-1.262c0.781,0.084,1.346,0.785,1.262,1.566c-0.084,0.781-0.785,1.346-1.566,1.262  C44.071,70.33,43.506,69.629,43.59,68.848z M48.277,62.82c0.069-0.642,0.645-1.106,1.287-1.037c0.642,0.069,1.106,0.645,1.037,1.287  c-0.069,0.642-0.645,1.106-1.287,1.037C48.673,64.038,48.208,63.462,48.277,62.82z"
                          />
                        </svg>

                        <span>{lunchDates[lunchIndex].data}</span>
                      </div> :
                      leftOver ?
                        <div className={`sidemenu-icn ${lunchDates[lunchIndex].color}`}>
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            xmlnsXlink="http://www.w3.org/1999/xlink"
                            viewBox="0 0 24 24"
                            version="1.1"
                            x="0px"
                            y="0px"
                            className="leftOver"
                          >
                            <desc>Created with Sketch.</desc><g stroke="none" strokeWidth="1" fill="none" fillRule="evenodd"><g transform="translate(-93.000000, -45.000000)" fill="#000000"><g transform="translate(95.000000, 45.000000)"><path d="M2.00276013,0 L16.9972399,0 C18.1033337,0 19,0.901627257 19,2.00086422 L19,20.9991358 C19,22.1041826 18.1064574,23 16.9972399,23 L2.00276013,23 C0.896666251,23 0,22.0983727 0,20.9991358 L0,2.00086422 C0,0.895817426 0.893542647,0 2.00276013,0 Z M1,20.9991358 C1,21.5483309 1.4511921,22 2.00276013,22 L16.9972399,22 C17.5530645,22 18,21.553006 18,20.9991358 L18,2.00086422 C18,1.45166907 17.5488079,1 16.9972399,1 L2.00276013,1 C1.44693549,1 1,1.44699395 1,2.00086422 L1,20.9991358 Z" /><rect x="3" y="22" width="1" height="2" /><rect x="15" y="22" width="1" height="2" /><rect x="11" y="0" width="1" height="23" /><path d="M3,10 L7,10 L7,14 L3,14 L3,10 Z M4,11 L6,11 L6,13 L4,13 L4,11 Z" /><rect x="9" y="8" width="1" height="8" /><rect x="13" y="8" width="1" height="8" /></g></g></g>
                          </svg>

                          <span>leftover</span>
                        </div> :
                        <div />
                    }
                    <div>
                      <LazyLoad
                        loaderImage
                        originalSrc={img}
                        imageProps={{
                          src: '/images/small.png',
                          draggable: false,
                          name: 'l.',
                          id: `i.${lDate}`,
                        }}
                        id={`i.${lDate}`}
                        className={`recipe_img d.${lDate}`}
                      />
                    </div>
                    <h5 name="l." id={`h.${lDate}`}>
                      {lunchDates[lunchIndex].recipe_name}
                    </h5>
                    <div name="l." id={`d1.${lDate}`}className="meal-post-data">
                      <div name="l." id={`d2.${lDate}`} className="meal-post-data-box meal-post-data-img">
                        <img
                          src={lunchDates[lunchIndex].equipmentIcn}
                          className="equipment-icn"
                          alt="icon"
                        />
                      </div>
                      <div name="l." id={`d3.${lDate}`} className="meal-post-data-box">
                        <b name="l." id={`b.${lDate}`}>
                          {lunchDates[lunchIndex].serving_size}
                        </b> Serv.
                      </div>
                      <div name="l." id={`d4.${lDate}`} className="meal-post-data-box">
                        <b name="l." id={`b2.${lDate}`}> {handsOnTime} </b>
                        Hands On
                      </div>
                      <div name="l." id={`d5.${lDate}`} className="meal-post-data-box">
                        <b name="l." id={`b3.${lDate}`}>{totalTime}</b>
                        Total
                      </div>
                    </div>
                  </a>
                </div>
              );
            }
            check = true;
            let lId = '';
            if (lunchIndex > -1 && lunchDates[lunchIndex].visible != null) {
              lId = lunchDates[lunchIndex].id;
            }
            return (
              <div
                className="meal-post no-shadow-post"
                key={mealDate}
                onDragStart={(event, id) => this.onDragEvent(event, `l${id}`)}
                onDragOver={event => this.allowDrop(event)}
                onDragOverCapture={event => this.onDragCaptureEvent(event)}
                onDragLeave={event => this.onDragLeaveEvent(event)}
                onDragEnd={event => this.onDragEndEvent(event)}
                draggable
                id={`l.${mealDate}`}
                name="l."
                onDrop={event => this.onDropEvent(event, mealDate, 'lunch', lId, 'null')}
              >
                <a
                  id={`a.${mealDate}`}
                  name="l."
                  className="add-meal-post"
                  onClick={event => this.addRecipe(event, `${dayDate.Year}-${dayDate.Month}-${dayDate.date}`, 'lunch_date', 'lunchDates')}
                >
                  <svg id={`s.${mealDate}`} name="l." width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                    {/* Generator: Sketch 49.2 (51160) - http://www.bohemiancoding.com/sketch */}
                    <desc>Created with Sketch.</desc>
                    <defs />
                    <g id="Symbols" stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                      <g id="Icon-/-Add-Item-(Green)" fill="#13B75E" fillRule="nonzero">
                        <g id="noun_1015634">
                          <path d="M10,0 C4.4771525,3.70074342e-16 -6.41153797e-15,4.4771525 -7.10542736e-15,10 C-7.79931675e-15,15.5228475 4.4771525,20 10,20 C15.5228475,20 20,15.5228475 20,10 C20,7.3478351 18.9464316,4.80429597 17.0710678,2.92893219 C15.195704,1.0535684 12.6521649,9.25185854e-16 10,0 Z M10,17.5 C5.85786438,17.5 2.5,14.1421356 2.5,10 C2.5,5.85786438 5.85786438,2.5 10,2.5 C14.1421356,2.5 17.5,5.85786438 17.5,10 C17.5,11.9891237 16.7098237,13.896778 15.3033009,15.3033009 C13.896778,16.7098237 11.9891237,17.5 10,17.5 Z M11.25,8.75 L14.375,8.75 L14.375,11.25 L11.25,11.25 L11.25,14.375 L8.75,14.375 L8.75,11.25 L5.625,11.25 L5.625,8.75 L8.75,8.75 L8.75,5.625 L11.25,5.625 L11.25,8.75 Z" id="Shape" />
                        </g>
                      </g>
                    </g>
                  </svg>
                  <span id={`sp.${mealDate}`} name="l." >Add a Meal</span>
                </a>
              </div>
            );
          })
        }
      </div>
    );

    // this.setState({addPlan: false})
  }

  lunchDataAdd(lunchDates, lunchDate, name, data, color, type) {
    // eslint-disable-next-line
    let handsTime = 0, totalRecipeTime = 0, totalSideTime = 0, totalLunchTime = 0, equipmentIcon, equipmentName;
    const { servingArray } = this.state;
    let totalServing = 1;
    if (lunchDate.recipe.recipe_equipment_type.length > 0) {
      equipmentIcon = `/images/${lunchDate.recipe.recipe_equipment_type[0].equipment_type.icon}`;
      equipmentName = lunchDate.recipe.recipe_equipment_type[0].equipment_type.title;
      if (equipmentIcon === '/images/') {
        equipmentIcon = '/images/grill.svg';
      }
    } else {
      equipmentIcon = '/images/none.svg';
      equipmentName = 'None';
    }
    let servingIndex;
    if (servingArray && servingArray.length > 0) {
      servingIndex = servingArray.findIndex(x => x.id === lunchDate.recipe.id);
    }
    if (servingIndex > -1) {
      totalServing = servingArray[servingIndex].servingSize;
    }
    handsTime = lunchDate.recipe.hands_on_time + lunchDate.hands_on_time;
    totalRecipeTime = (lunchDate.recipe.hands_on_time + lunchDate.recipe.passive_time);
    totalSideTime = (lunchDate.hands_on_time + lunchDate.passive_time);
    totalLunchTime = compareRecipeTime(totalRecipeTime, totalSideTime);
    totalLunchTime = compareRecipeTime(totalLunchTime, handsTime);
    // compare recipe and side time
    lunchDates.push({
      lunch_date: lunchDate.lunch_date,
      data,
      id: lunchDate.id,
      weekly_id: lunchDate.user_weekly_schedule_id,
      visible: lunchDate.deleted_at,
      recipe_name: lunchDate.recipe.name,
      img: lunchDate.recipe.image,
      vertImg: lunchDate.recipe.vert_image,
      sideImg: lunchDate.image,
      recipe_id: lunchDate.recipe.id,
      name,
      equipmentIcn: equipmentIcon,
      equipmentName,
      color,
      type,
      serving_size: lunchDate.recipe_serving_size,
      hands_on_time: handsTime,
      recipe_hands_on_time: lunchDate.recipe.hands_on_time,
      total_time: totalLunchTime,
      recipe_time: totalRecipeTime,
      side_hands_on: lunchDate.hands_on_time,
      side_passive: lunchDate.passive_time,
      totalServing,
    });
  }

  mealDetail(
    mealDate, id, weekId, type, size, rname, sname,
    totalHandsOn, totalTime, rHandsTime, totalRecipeTime,
    sideHandsOn, sidePassive, fId, recipeImg, recipeVertImg, sideImg,
    equipmentIcon, equipmentName, totalServing,
  ) {
    const { reminder, servingArray, dayDates } = this.state;
    // track recipe view
    window.analytics.track('Dashboard View detail', {
      recipeName: rname,
      slot: type.split('_')[0],
      date: moment(mealDate).format('dddd DD-MM-YYYY'),
    });
    if (!reminder) {
      history.push('/viewRecipe',
        { path:'viewRecipe', mealDate, id, rname, weekId, type, size, sname,
          totalHandsOn, totalTime, rHandsTime, totalRecipeTime, sideHandsOnTime: sideHandsOn,
          sidePassiveTime: sidePassive, fId, recipeImg, recipeVertImg, sideImg, equipmentIcon,
          equipmentName, totalServing, servingArray, dayDates
        })
      this.setState({
        // mealDetail: true,
        display: 'block',
        mealDate,
        id,
        rname,
        weekId,
        type,
        size,
        sname,
        totalHandsOn,
        totalTime,
        rHandsTime,
        totalRecipeTime,
        sideHandsOn,
        sidePassive,
        fId,
        recipeImg,
        recipeVertImg,
        sideImg,
        equipmentIcon,
        equipmentName,
        totalServing,
      });
    }
  }

  // add Recipe
  async addRecipe(event, mealDate, dateType, type) {
    const { reminder, error } = this.state;
    // track drag recipe
    window.analytics.track('Add Recipe', {
      slot: type.split('D')[0],
      date: moment(mealDate).format('dddd DD-MM-YYYY'),
    });
    if (!reminder && !error) {
      this.setState({ loadData: true })
      const newMealDate = [];
      const inputs = {};
      const {
        dayDates, lunchDates, dinnerDates, cookingDates, previous,
      } = this.state;
      if (dateType === 'dinner_date') {
        dinnerDates.push({ [`${dateType}`]: mealDate });
      } else {
        lunchDates.push({ [`${dateType}`]: mealDate });
      }
      cookingDates.map((value) => {
        newMealDate.push({ cooking_date: value.cooking_date });
        return newMealDate;
      });
      inputs.lunchDates = lunchDates;
      inputs.dinnerDates = dinnerDates;
      inputs.cookingDates = newMealDate;

      inputs.week_start_date = `${dayDates[0].Year}-${dayDates[0].Month}-${dayDates[0].date}`;
      inputs.week_end_date = `${dayDates[6].Year}-${dayDates[6].Month}-${dayDates[6].date}`;
      // console.log('inputs',inputs);
      const url = RoutingConstants.editUserWeeklySchedule;
      if (!previous && ((dinnerDates.length > 0 && lunchDates.length > 0) || newMealDate.length > 0)) {
        await post(url, inputs).then((response) => {
          this.props.swapRecipe(response);
        }).catch(() => {
          // console.log(err);
        });
      }
    }
  }

  render() {
    const { mealData, mealDetail, display, loadData } = this.state;
    return (

      <div>
        {
          loadData &&
          <div className="loading">
            <Loader />
            <div className="modal-backdrop fade in" />
          </div>
        }
        <div className="demo">
          <div className="cal-table-cntnt">
            <div className="cal-table-cntnt-row">
              <div className="side-hdng">Lunch</div>
              <div className="cal-days-cntnt">
                {
                  this.lunchData(mealData)
                }
              </div>
            </div>
          </div>
          <div className="lunch-recipes-box">
            <div className="cal-table-cntnt">
              <div className="cal-table-cntnt-row">
                <div className="side-hdng">Dinner</div>
                <div className="cal-days-cntnt">
                  {
                    this.dinnerData(mealData)
                  }
                </div>
              </div>
            </div>
          </div>
        </div>
        <div>
          {
            mealDetail &&
            <div>
              <MealDetail
                display={display}
                onClose={() => this.onClose()}
                onSave={() => this.onSave()}
                recipeHandsTime={this.state.rHandsTime}
                enableSwapButton
                {...this.state}
              />
              <div className="modal-backdrop fade in" />
            </div>
          }
        </div>
      </div>

    );
  }
}

export default RecipeForWeekCalander;
