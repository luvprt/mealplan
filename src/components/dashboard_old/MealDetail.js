import React, { Component } from 'react';
import LazyLoad from 'react-image-lazy-load';
import RoutingConstants from 'lib/RoutingConstants';
import { getTimeStamp, compareRecipeTime, differnceTimeStamp, getFormatTime } from 'lib/services';
import { post } from 'lib/api';
import moment from 'moment';
import MainDish from './MainDish';
import SideDish from './SideDish';
import history from '../../history';


class MealDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      sid: 0,
      display: this.props.display || 'none',
      imgDisplay: this.props.sname != null,
      swapDish: false,
      inputs: {
        [`${this.props.type}`]: this.props.mealDate,
        recipe_id: this.props.id,
        meal_plan_schedule_id: this.props.weekId,
      },
      params: {},
      mainDishValue: {},
      sideDishValue: {},
      size: this.props.size || null,
      recipe: {},
      sideHandsOnTime: this.props.sideHandsOn,
      sidePassiveTime: this.props.sidePassive,
      sideId: '',
      recipeNutrition: '',
      sideNutrition: '',
      ...this.props,
    };
  }

  componentDidMount() {
    this.getMealData();
  }
  

  getMealData() {
    const url = RoutingConstants.fetchMealData;
    const recipe = [];
    const { inputs, sid, pathname } = this.state;
    let { totalServing } = this.state;
    if (pathname === '/dashboard') {
      totalServing = this.props.totalServing;
    } else {
      totalServing = this.props.size;
    }
    post(url, inputs).then((response) => {
      if (response.result) {
        this.setState({
          mainDishValue: response.result,
          totalServing,
          recipeNutrition: response.result.nutrition_link
        });

        if (response.result.recipe_sides.length > 0) {
          this.setState({
            sideDishValue: response.result.recipe_sides,

          });
          this.state.sideDishValue.map((value) => {
            if (value.is_default === 1) {
              recipe.push(value.recipe);
            }
            return value;
          });
          if (recipe) {
            let sideVertImg;
            let sideImg;
            if (recipe[sid]) {
              sideVertImg = recipe[sid].vert_image;
              sideImg = recipe[sid].image;
            }
            if (sideVertImg === null) {
              sideVertImg = '/images/small.png';
            }
            if (sideImg === null) {
              sideImg = '/images/small.png';
            }
            this.setState({
              sideId: recipe[sid].id,
              recipe,
              sideHandsOnTime: recipe[sid].hands_on_time,
              sidePassiveTime: recipe[sid].passive_time,
              sideVertImg,
              sideImg,
              sideNutrition: recipe[sid].nutrition_link
            });
          }
        }
      }
    }).catch((err) => {
      if (err.error === 'token_expired') {
        localStorage.removeItem('token');
        history.push('/login');
      }
    });
  }

  close() {
    this.props.onClose();
  }
  
  saveAnalytics(linkName, name, recipeType) {
    const { mealDate, type } = this.state
    window.analytics.track('Click Recipe Nutrition in Meal Tab', {
      [recipeType]: name,
      link: linkName,
      slot: type, 
      date: moment(mealDate).format('dddd DD-MM-YYYY'),
    });
  }

  save() {
    const {
      inputs, recipe, sid, mealDate, rname, sname,
    } = this.state;
    let { type } = this.state;
    type = type.split('_')[0];

    // inputs.recipe_id = inputs.recipe_id;
    // inputs.meal_plan_schedule_id = inputs.meal_plan_schedule_id;
    inputs.side_dish_id = recipe[sid].id;
    const url = RoutingConstants.updateSideDish;
    post(url, inputs).then(() => {
      this.props.onSave();
      // track save side dish option
      window.analytics.track('Save View Details', {
        sideDishName: sname,
        recipeName: rname,
        date: moment(mealDate).format('dddd DD-MM-YYYY'),
        slot: type,
      });
    }).catch(() => {
      // console.log(err);
    });
  }

  swapSave() {
    const url = RoutingConstants.dragAndDrop;
    const {
      inputs, type, fId, fDate, params, sideId, rname, recipeName,
    } = this.state;

    params.recipe_id = inputs.recipe_id;
    params.weakly_schedule_id = this.props.weekId;
    params.side_dish_id = sideId;
    params.moved_from_id = fId;
    params.moved_from_type = type;
    params.moved_from_date = fDate;
    params.moved_to_id = '';
    params.moved_to_type = type;
    params.moved_to_date = fDate;
    
    post(url, params).then(() => {
      params.from_id = this.props.previousRId;
      this.props.onSave(params);

      // track swap meal option
      window.analytics.track('Swap Recipe', {
        swapFromRecipe: recipeName,
        swapToRecipe: rname,
        slot: type,
        date: moment(fDate).format('dddd DD-MM-YYYY'),
      });
    }).catch(() => {
      // console.log(err);
    });
  }

  mainDishTab(value) {
    this.setState({ imgDisplay: false });
    const { rname, sname, mealDate } = this.state;
    let { type } = this.state;
    type = type.split('_')[0];

    if (value === 'sideDish') {
      this.setState({ swapDish: true });

      window.analytics.track('View Side Dish', {
        sideRecipeName: sname,
        date: moment(mealDate).format('dddd DD-MM-YYYY'),
        slot: type,
      });
    } else {
      this.setState({ swapDish: false });

      window.analytics.track('View Main Dish', {
        recipeName: rname,
        date: moment(mealDate).format('dddd DD-MM-YYYY'),
        slot: type,
      });
    }
  }

  // change side dish logic

  changeSide() {
    const { sideDishValue, recipe, mealDate } = this.state;
    this.setState({ sideVertImg: '/images/small.png' });
    let { sid } = this.state;
    let { type } = this.state;
    type = type.split('_')[0];

    sideDishValue.map((value) => {
      const d = recipe.findIndex(x => x.id === value.side_id);
      if (d === -1) {
        recipe.push(value.recipe);
      }
      return recipe;
    });
    if ((sideDishValue.length - 1) === sid) {
      sid = 0;
    } else {
      sid += 1;
    }
    if (recipe) {
      this.setState({
        sname: recipe[sid].name,
        recipe,
        sid,
        sideId: recipe[sid].id,
        sideImg: recipe[sid].image || '/images/small.png',
        sideVertImg: recipe[sid].vert_image || '/images/small.png',
        sideHandsOnTime: recipe[sid].hands_on_time,
        sidePassiveTime: recipe[sid].passive_time,
        sideNutrition: recipe[sid].nutrition_link
      });

      // track change side dish
      window.analytics.track('Change Side Dish', {
        recipeName: recipe[sid].name,
        date: moment(mealDate).format('dddd DD-MM-YYYY'),
        slot: type,
      });
    }
    // console.log(recipe);
  }

  swapMeal(totalHandsTime, totalTime) {
    const {
      rname, size, dayDates, inputs, fId, recipeImg, equipmentIcon, totalServing, mealDate, servingArray
    } = this.state;
    let { type } = this.state;
    type = type.split('_')[0];
    const startDate = `${dayDates[0].fullMonth} ${dayDates[0].date}`;
    const endDate = `${dayDates[6].fullMonth} ${dayDates[6].date}`;
    let recipeSize;
    const index = servingArray.findIndex( x => x.id === this.props.id)
    if( index > -1) {
      recipeSize = (servingArray[index].servingSize / servingArray[index].basicSize)
    }
    // track swap meal option
    window.analytics.track('Swap Meal', {
      swapRecipe: rname,
      slot: type,
      date: moment(mealDate).format('dddd DD-MM-YYYY'),
    });

    history.push('/recipeList', {
      rname,
      size,
      totalHandsTime,
      totalTime,
      startDate,
      endDate,
      dayDates,
      weekId: inputs.meal_plan_schedule_id,
      type,
      fDate: this.props.mealDate,
      fId,
      recipeImg,
      equipmentIcon,
      totalServing,
      mealDate,
      recipeSize,
      previousRId: this.props.id
    });
  }

  mealDtail() {
    if (this.state.sname != null) {
      this.setState({ imgDisplay: true, swapDish: false });
    } else {
      this.setState({ imgDisplay: false, swapDish: false });
    }
  }

  clickServing() {
    const { totalServing, mealDate, rname } = this.state;
    let { type } = this.state;
    type = type.split('_')[0];
    // serving size event track
    window.analytics.track('Clicked Serving Size', {
      servingSize: totalServing,
      date: moment(mealDate).format('dddd DD-MM-YYYY'),
      recipeName: rname,
      slot: type,
    });
  }

  render() {
    const {
      display, equipmentName, sid, recipeImg, inputs, recipeVertImg, swapDish,
      sideImg, sidePassiveTime, sideDishValue, sideVertImg, recipeHandsTime, totalServing,
      sideDishArray, recipeNutrition, sideNutrition, mealDate
    } = this.state;
    
    let recipeLink = false;
    let sideLink = false
    
    const { rname, sname, enableSwapButton } = this.state;
    let { totalRecipeTime, sideHandsOnTime } = this.state;
    const { imgDisplay } = this.state;

    let recipetime = getTimeStamp(recipeHandsTime);
    recipetime = getFormatTime(recipetime);
    if(recipeNutrition) {
      recipeLink = true
    }
    if(sideNutrition) {
      sideLink = true
    }

    // total hands on time
    const totalHandsOnTime = getTimeStamp(recipeHandsTime + sideHandsOnTime);
    const t = totalHandsOnTime.split('m');

    // side total time
    let sidetotaltime = '0';
    if (sideHandsOnTime) {
      sidetotaltime = sideHandsOnTime + sidePassiveTime;
      sideHandsOnTime = getTimeStamp(sideHandsOnTime);
      sideHandsOnTime = getFormatTime(sideHandsOnTime);
    }
    // compare total time
    let ht = compareRecipeTime(totalRecipeTime, sidetotaltime);
    ht = compareRecipeTime(t[0], ht);
    if (ht !== '0h') {
      ht = getTimeStamp(ht);
    }
    let passiveTime = ht;
    if (ht !== '0h') {
      passiveTime = differnceTimeStamp(ht, t[0]);
      passiveTime = getTimeStamp(passiveTime);
    }

    totalRecipeTime = getTimeStamp(totalRecipeTime);
    totalRecipeTime = getFormatTime(totalRecipeTime);

    sidetotaltime = getTimeStamp(sidetotaltime);
    sidetotaltime = getFormatTime(sidetotaltime);
    return (
      <div
        id="meal-detail-popup"
        className="modal fade meal-detail-popup in"
        style={{ display, paddingRight: '15px' }}
        role="dialog"
      >
        <div className="modal-dialog">
          {/*  Modal content */}
          <div className="modal-content">
            {imgDisplay ?
              <div className="meal-img-dtail">
                <div>
                  <LazyLoad
                    loaderImage
                    originalSrc={recipeImg}
                    imageProps={{
                      src: '/images/small.png',
                    }}
                    className="img-side1"
                  />
                </div>
                <div >
                  <LazyLoad
                    loaderImage
                    originalSrc={sideImg}
                    imageProps={{
                      src: '/images/small.png',

                    }}
                    className="img-side2"
                  />
                </div>
              </div>
              :
              <div className="meal-img-dtail">
                { swapDish && sideDishValue.length > 1 &&
                  <div className="change-dish-box">
                    <button className="swap-meal-btn" onClick={() => this.changeSide()}>
                      <svg
                        xmlns="http://www.w3.org/2000/svg"
                        xmlnsXlink="http://www.w3.org/1999/xlink"
                        version="1.1"
                        x="0px"
                        y="0px"
                        viewBox="17 -18 100 100"
                        enableBackground="new 17 -18 100 100"
                        xmlSpace="preserve"
                      >
                        <path d="M48.7,10.7C55,10.7,60.3,5.5,60.3-1s-5.2-11.7-11.7-11.7S37-7.5,37-1S42.2,10.7,48.7,10.7z M85.4,76.7  c6.5,0,11.7-5.2,11.7-11.7s-5.2-11.7-11.7-11.7c-6.3,0-11.7,5.2-11.7,11.7S78.9,76.7,85.4,76.7z M100.3,52L117,35.3H83.7L100.3,52z   M17,28.7h33.3L33.7,12L17,28.7z M67,72V58.7c-14.5,0-26.7-12.2-26.7-26.7v-7.3H27V32C27,53.3,45.7,72,67,72z M93.7,39.3H107V32  c0-21.3-18.7-40-40-40V5.3c14.5,0,26.7,12.2,26.7,26.7V39.3z" />
                      </svg>
                      Change Side
                    </button>
                  </div>
                }
                <LazyLoad
                  originalSrc={!swapDish ? recipeVertImg : sideVertImg}
                  imageProps={{
                    src: '/images/small.png',
                    className: 'img-side-full',
                    ref: 'image',
                  }}
                  loaderImage
                />

              </div>
            }

            <div className="meal-cntnt-dtail">
              <div className="meal-dtail-hdng">
                <ul className="nav nav-tabs">
                  <li className="active">
                    <a data-toggle="tab" href="#meal-dtail" onClick={() => this.mealDtail()}>
                      Meal
                    </a>
                  </li>
                  <li>
                    <a data-toggle="tab" href="#main-dish" onClick={() => this.mainDishTab()}>
                      Main Dish
                    </a>
                  </li>
                  <li>
                    {sname != null &&
                      <a data-toggle="tab" href="#side-dish" onClick={() => this.mainDishTab('sideDish')}>
                        Side Dish
                      </a>
                    }
                  </li>
                </ul>
                <button type="button" className="close dtail-popup-close" onClick={() => this.close()}>
                  <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">

                    <desc>Created with Sketch.</desc>
                    <defs />
                    <g id="Symbols" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
                      <g id="Icon-/-Delete-(White)" fill="#FFFFFF" fillRule="nonzero">
                        <g id="noun_1054717">
                          <path d="M10,0.104166667 C4.54166667,0.104166667 0.104166667,4.54166667 0.104166667,10 C0.104166667,15.4583333 4.54166667,19.8958333 10,19.8958333 C15.4583333,19.8958333 19.8958333,15.4583333 19.8958333,10 C19.8958333,4.54166667 15.4583333,0.104166667 10,0.104166667 Z M10,18.1458333 C5.5,18.1458333 1.85416667,14.5 1.85416667,10 C1.85416667,5.5 5.5,1.85416667 10,1.85416667 C14.5,1.85416667 18.1458333,5.5 18.1458333,10 C18.1458333,14.5 14.5,18.1458333 10,18.1458333 Z" id="Shape" />
                          <path d="M14.7708333,6.70833333 L13.2916667,5.22916667 C13.125,5.0625 12.875,5.0625 12.7083333,5.22916667 L10,7.9375 L7.29166667,5.22916667 C7.125,5.0625 6.875,5.0625 6.70833333,5.22916667 L5.22916667,6.70833333 C5.0625,6.875 5.0625,7.125 5.22916667,7.29166667 L7.9375,10 L5.22916667,12.7083333 C5.0625,12.875 5.0625,13.125 5.22916667,13.2916667 L6.70833333,14.7708333 C6.875,14.9375 7.125,14.9375 7.29166667,14.7708333 L10,12.0625 L12.7083333,14.7708333 C12.875,14.9375 13.125,14.9375 13.2916667,14.7708333 L14.7708333,13.2916667 C14.9375,13.125 14.9375,12.875 14.7708333,12.7083333 L12.0625,10 L14.7708333,7.29166667 C14.9375,7.125 14.9375,6.875 14.7708333,6.70833333 Z" id="Shape" />
                        </g>
                      </g>
                    </g>
                  </svg>
                </button>
              </div>

              <div className="tab-content">
                <div id="meal-dtail" className="tab-pane fade in active">
                  <div className="tab-top-cntnt">
                    <h3>{rname}</h3>
                    {sname != null && <div className="after-hdng-cntnt">with {sname}</div>}

                    <div className="popup-main-icn-box">
                      <div className="row">
                        <div className="col-md-3 dtail-popup-gril-icn">
                          <img
                            src={this.state.equipmentIcon}
                            className="equipment-icn1"
                            alt="equipment"
                          />
                          <b className="equipment-icon equipment-icon--cntnt-size">{equipmentName}</b>
                        </div>
                        <div className="col-md-3 chef-cap-small">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            xmlnsXlink="http://www.w3.org/1999/xlink"
                            version="1.1"
                            x="0px"
                            y="0px"
                            viewBox="0 0 100 100"
                            style={{ enableBackground: 'new 0 0 100 100' }}
                            xmlSpace="preserve"
                          >
                            <path d="M79,40.9c0-7.2-5.9-13.1-13.1-13.1c-1.8,0-3.5,0.4-5.2,1.1c-2.4-3.5-6.4-5.6-10.7-5.6s-8.3,2.1-10.7,5.6  c-1.6-0.7-3.4-1.1-5.2-1.1c-7.2,0-13.1,5.9-13.1,13.1c0,6.6,5,12.1,11.4,12.9v18.9c0,2.2,1.8,4,4,4h27.2c2.2,0,4-1.8,4-4V53.9  C74,53,79,47.5,79,40.9z M63.6,73.3H36.4c-0.3,0-0.6-0.3-0.6-0.6v-3.4h28.4v3.4C64.2,73,63.9,73.3,63.6,73.3z M65.9,50.6  c-2.1,0-4-0.6-5.7-1.9c-0.8-0.6-1.8-0.4-2.4,0.4c-0.6,0.8-0.4,1.8,0.4,2.4c1.8,1.3,3.9,2.1,6,2.4v12.1h-8v-6.8  c0-0.9-0.8-1.7-1.7-1.7s-1.7,0.8-1.7,1.7v6.8h-5.7v-6.8c0-0.9-0.8-1.7-1.7-1.7s-1.7,0.8-1.7,1.7v6.8h-8V53.8  c2.2-0.3,4.3-1.1,6.2-2.5c0.8-0.6,0.9-1.6,0.3-2.4c-0.6-0.8-1.6-0.9-2.4-0.3c-1.7,1.3-3.7,1.9-5.8,1.9c-5.3,0-9.7-4.3-9.7-9.7  s4.3-9.7,9.7-9.7c1.7,0,3.5,0.5,4.9,1.4c0.4,0.2,0.9,0.3,1.3,0.2c0.5-0.1,0.8-0.4,1-0.9c1.7-3.2,5-5.2,8.6-5.2s6.9,2,8.6,5.2  c0.2,0.4,0.6,0.7,1,0.9c0.5,0.1,0.9,0.1,1.3-0.2c1.5-0.9,3.2-1.4,4.9-1.4c5.3,0,9.7,4.3,9.7,9.7S71.2,50.6,65.9,50.6z" />
                          </svg>
                          <b>{totalHandsOnTime}</b>
                          <span>Hands On</span>
                        </div>
                        <div className="col-md-3">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            xmlnsXlink="http://www.w3.org/1999/xlink"
                            version="1.1"
                            x="0px"
                            y="0px"
                            viewBox="0 0 44 44"
                            style={{ enableBackground: 'new 0 0 44 44' }}
                            xmlSpace="preserve"
                          >
                            <g>
                              <path d="M35.879,27c-0.834,0-1.618,0.324-2.207,0.914l-5.793,5.793C27.69,33.896,27.438,34,27.171,34H23V18h15c0.552,0,1-0.448,1-1   c0-6.91-7.092-12.562-16-12.961V1c0-0.552-0.448-1-1-1s-1,0.448-1,1v3.039C12.092,4.438,5,10.09,5,17c0,0.552,0.448,1,1,1h15v16H8   c-1.654,0-3,1.346-3,3c0,1.453,1.038,2.666,2.412,2.941L6.382,42H6c-0.552,0-1,0.447-1,1s0.448,1,1,1h1   c0.379,0,0.725-0.214,0.895-0.553L9.618,40H21v1c0,0.552-0.449,1-1,1h-1c-0.552,0-1,0.447-1,1s0.448,1,1,1h6c0.552,0,1-0.447,1-1   s-0.448-1-1-1h-1c-0.551,0-1-0.448-1-1v-1h5.343c0.336,0,0.667-0.034,0.99-0.098l1.773,3.545C31.275,43.786,31.621,44,32,44h1   c0.552,0,1-0.447,1-1s-0.448-1-1-1h-0.382l-1.443-2.886c0.248-0.171,0.486-0.361,0.703-0.578l6.208-6.207   C38.667,31.747,39,30.943,39,30.121C39,28.4,37.6,27,35.879,27z M36.938,16h-7.965c-0.157-3.989-1.24-7.417-2.866-9.577   C31.966,7.648,36.374,11.422,36.938,16z M7.062,16c0.565-4.578,4.973-8.352,10.831-9.577c-1.626,2.16-2.709,5.588-2.866,9.577   H7.062z M17.021,16C17.274,10.058,19.759,6,22,6s4.726,4.058,4.979,10H17.021z M36.672,30.914l-6.208,6.207   C29.906,37.68,29.133,38,28.343,38H8c-0.551,0-1-0.448-1-1s0.449-1,1-1h19.171c0.801,0,1.555-0.312,2.122-0.879l5.793-5.793   C35.298,29.116,35.579,29,35.879,29C36.497,29,37,29.503,37,30.121C37,30.416,36.88,30.705,36.672,30.914z" />
                            </g>
                          </svg>
                          <b>{passiveTime}</b>
                          <span>Passive Time</span>
                        </div>
                        <div className="col-md-3 popup-time-icn">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            xmlnsXlink="http://www.w3.org/1999/xlink"
                            version="1.1"
                            x="0px"
                            y="0px"
                            viewBox="0 0 100 100"
                            style={{ enableBackground: 'new 0 0 100 100' }}
                            xmlSpace="preserve"
                          >
                            <g>
                              <path d="M50,5C25.2,5,5,25.2,5,50s20.1,45,45,45s45-20.1,45-45S74.8,5,50,5z M50,89.7c-21.9,0-39.7-17.8-39.7-39.7   c0-21.9,17.8-39.7,39.7-39.7S89.7,28.1,89.7,50C89.7,71.9,71.9,89.7,50,89.7z" /><path d="M52.4,51.7V25.2c0-1.3-1.1-2.4-2.4-2.4s-2.4,1.1-2.4,2.4v27.3c0,0.3,0.1,0.6,0.1,0.8c0.1,0.4,0.3,0.8,0.6,1.1l19.3,19.3   c0.9,0.9,2.5,0.9,3.4,0c0.9-0.9,0.9-2.5,0-3.4L52.4,51.7z" />
                            </g>
                          </svg>
                          <b>{ht}</b>
                          <span>Total Time</span>
                        </div>
                      </div>
                    </div>
                    
                    {/* {  <!--popup main icon box end-->} */}

                    <div className="meal-breakdown">
                      <h4>Meal Breakdown:</h4>
                      <div className="meal-breakdown-row">
                        <div className="meal-breakdown-row-tag">Main</div>
                        <h5>{rname}</h5>
                        <span>Hands On: <b>{recipetime} </b></span>
                        <span>Total Time: <b>{totalRecipeTime}</b></span>
                        { recipeLink &&
                          <div className="recipe-link" onClick={() => this.saveAnalytics(recipeNutrition, rname, 'recipeName')}>
                            <a href={recipeNutrition} target="_blank"> Recipe Nutrition Data </a>
                          </div>
                        }
                      </div>
                      
                      {sname != null &&
                        <div className="meal-breakdown-row">
                          <div className="meal-breakdown-row-tag">Side</div>
                          <h5>{sname}</h5>
                          <span>Hands On: <b>{sideHandsOnTime}</b></span>
                          <span>Total Time: <b>{sidetotaltime} </b></span>
                          { sideLink &&
                            <div className="recipe-link" onClick={() => this.saveAnalytics(sideNutrition, sname, 'sideRecipeName')}>
                              <a href={sideNutrition} target="_blank"> Recipe Nutrition Data </a>
                            </div>
                          } 
                        </div>
                      }
                    </div>
                  </div>
                  {/* {<!--tab top content end-->} */}
                  <div className="popup-ftr-nw">
                    <div className="row">
                      <div className="col-md-4 text-left">
                        { enableSwapButton &&
                        <button onClick={() => this.swapMeal(totalHandsOnTime, ht)} className="swap-meal-btn">
                          <svg
                            xmlns="http://www.w3.org/2000/svg"
                            xmlnsXlink="http://www.w3.org/1999/xlink"
                            version="1.1"
                            x="0px"
                            y="0px"
                            viewBox="17 -18 100 100"
                            enableBackground="new 17 -18 100 100"
                            xmlSpace="preserve"
                          >
                            <path d="M48.7,10.7C55,10.7,60.3,5.5,60.3-1s-5.2-11.7-11.7-11.7S37-7.5,37-1S42.2,10.7,48.7,10.7z M85.4,76.7  c6.5,0,11.7-5.2,11.7-11.7s-5.2-11.7-11.7-11.7c-6.3,0-11.7,5.2-11.7,11.7S78.9,76.7,85.4,76.7z M100.3,52L117,35.3H83.7L100.3,52z   M17,28.7h33.3L33.7,12L17,28.7z M67,72V58.7c-14.5,0-26.7-12.2-26.7-26.7v-7.3H27V32C27,53.3,45.7,72,67,72z M93.7,39.3H107V32  c0-21.3-18.7-40-40-40V5.3c14.5,0,26.7,12.2,26.7,26.7V39.3z" />
                          </svg>
                            Swap Meal
                        </button>
                        }
                      </div>
                      <div className='col-md-4 popup-ftr-servings top-txt-only'>
                        Total Servings: 
                        <strong onClick={() => this.clickServing()}>{totalServing}</strong>
                      </div>
                      <div className="col-md-4 text-right">
                        {enableSwapButton ?
                          <button className="btn green-btn" onClick={() => this.save()}>Save</button> :
                          <button className="btn green-btn" onClick={() => this.swapSave()}>Swap & Save</button>
                        }
                      </div>
                    </div>
                  </div>
                </div>
                {/* {<!--meal detail tab content end-->} */}

                <MainDish
                  onClose={() => this.close()}
                  response={this.state.mainDishValue}
                  onSave={() => this.save()}
                  swapSave={() => this.swapSave()}
                  enableSwapButton={enableSwapButton}
                  size={totalServing}
                  fDate={mealDate}    
                  type = {this.props.type}  
                />
                {/* <!--meal dish tab content end--> */}
                <SideDish
                  onClose={() => this.close()}
                  onSave={() => this.save()}
                  swapSave={() => this.swapSave()}
                  response={this.state.recipe}
                  sid={sid}
                  recipe_id={inputs.recipe_id}
                  meal_plan_schedule_id={inputs.meal_plan_schedule_id}
                  enableSwapButton={enableSwapButton}
                  size={totalServing}
                  servingSize={this.props.size}
                  sideDishArray={sideDishArray}
                  fDate={mealDate}    
                  type = {this.props.type}  
                />
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default MealDetail;
