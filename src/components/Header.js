import React from 'react';
import { Link } from 'react-router-dom';

const Header = () => (
  <header>
    <div className="container">
      <nav className="navbar navbar-default navbar-tall navbar-full">
        <div className="navbar-header col-md-4">
          <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#global-nav" aria-expanded="false">
            <span className="sr-only">Toggle navigation</span>
            <span className="icon-bar" />
            <span className="icon-bar" />
            <span className="icon-bar" />
          </button>
          <a className="navbar-brand"><img src="/images/logo.png" alt="" /></a>
        </div>
        <div className="navbar-collapse collapse" id="global-nav" aria-expanded="false">
          <div className="navbar-main">
            <ul className="nav navbar-nav nav-centr">
              <li className=""><a>Features</a></li>
              <li><a>How it Works</a></li>
              <li><a>Pricing</a></li>
              <li><a>Blog</a></li>
              <li><a>Contact Us</a></li>
            </ul>
            <ul className="nav navbar-nav navbar-right">
              <li>
                <Link to="/login" href>
                  Login
                </Link>
              </li>
              <li><Link className="signup-btn-nav" to='/register'>SignUp</Link></li>
            </ul>
          </div>
        </div>
      </nav>
    </div>
  </header>
);
export default Header;
