/* eslint-disable no-underscore-dangle */
import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { post,registerGuestUser } from 'lib/api';
import RoutingConstants from 'lib/RoutingConstants';
import { validateInputs } from 'lib/services';
import history from '../../history';
import SocialLoginButton from './SocialLoginButton'
import FacebookLogo from '../../assets/images/facebook.png';
import GoogleLogo from '../../assets/images/google.png';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputs: {},
      errorMessage: null,
    };
    window.scroll(0,0)
  }

  componentDidMount() {
    if (localStorage.getItem('token') && this.props.location &&
      this.props.location.pathname !== '/checkUser' && localStorage.type !=='checkUser') {
      history.push('/dashboard', { guestEmail: '', type: '/login', reminder: localStorage.reminder });
    } else if(localStorage.getItem('token') && this.props.location){
      history.push('/adminDashboard', {
        guestEmail: '', type: '/checkUser'
      });
    }
  }

  onClickLogin() {
    const {
      email,
      password,
    } = this.state.inputs;
    let { errorMessage } = this.state;
    const { inputs } = this.state;
    const url = RoutingConstants.login;
    if (email && password) {
      if (!validateInputs('email', email)) {
        errorMessage = 'Please enter a valid email address';
      } else {
        errorMessage = null;
      }
    } else {
      errorMessage = 'Sorry! Incorrect email or password, try again';
    }
    if (this.props.location.pathname !== '/checkUser') {
      inputs.is_admin = false;
    } else{
      inputs.is_admin = true;
    }
    this.setState({ errorMessage });
    post(url, this.state.inputs).then((response) => {
      if (response.result) {
        localStorage.setItem('token', response.result.token);
        localStorage.setItem('firstName', response.result.first_name);
        if(response.result.last_name) {
          localStorage.setItem('lastName', response.result.last_name);
        }
        localStorage.setItem('email', email);
        if (!response.result.remainder) {
          localStorage.setItem('test', '');
        }
        localStorage.setItem('type', 'login');
        localStorage.removeItem('role');
        window.analytics.identify(email, {
          first_name: response.first_name,
          last_name: response.last_name,
          email,
        },{
          integrations: {
            'All': true,
            'Facebook Pixel': false,
          }
        });
        const exist = response.result.is_week_exist;
        if (this.props.location.pathname !== '/checkUser' && response.result.is_week_exist) {
          history.push('/dashboard', {
            guestEmail: response.result.email, type: '/login', reminder: response.result.remainder, exist,
          });
        } else if (!response.result.is_week_exist) {
          localStorage.setItem('type', 'checkUser');
          history.push('/adminDashboard', {
            guestEmail: response.result.email, type: '/checkUser', reminder: response.result.remainder, exist,
          });
        }
      }else {
        this.setState({ errorMessage: 'Sorry! incorrect email or password, Try again.' });
      }
    }).catch(() => {
      // console.log('login error', err);
      errorMessage = 'Sorry! incorrect email or password, Try again.';
      this.setState({ errorMessage });
    });
  }

  keyEvent(event) {
    if (event.key === 'Enter') {
      this.onClickLogin();
    }
  }

  updateInputs(ref, text) {
    const { inputs } = this.state;
    inputs[`${ref}`] = text.trim();
    this.setState({ inputs });
  }

  handleSocialLogin(user){
    const userProfile = user._profile;
    userProfile.loginType = user._provider;    
    // email, firstName ,id, lastName , name , profilePicURL
    if (userProfile.name && userProfile.email) {
      const url = RoutingConstants.socialLogin;

      registerGuestUser(url, userProfile).then(async (response) => {
        if (response.result) {
          sessionStorage.removeItem('guestAccount');
          localStorage.removeItem('lastName');         
          localStorage.setItem('analytics', 'User New SignUp did');
          localStorage.removeItem('isTourCompleted');          
          localStorage.setItem('firstName', response.result.first_name);
          localStorage.setItem('token', response.result.token);
          if(response.result.last_name) {
            localStorage.setItem('lastName', response.result.last_name);
          }
          localStorage.setItem('email', response.result.email);
          if (!response.result.remainder) {
            localStorage.setItem('test', '');
          }
          localStorage.setItem('type', 'login');
          localStorage.removeItem('role');
          window.analytics.identify(response.result.email, {
            first_name: response.first_name,
            last_name: response.last_name,
            email:response.result.email,
          },{
            integrations: {
              'All': true,
              'Facebook Pixel': false,
            }
          });

             
          setTimeout(()=>{
            if(response.result.alreadyUser === true){           
              history.push('/dashboard', {
                guestEmail: response.result.email, type: '/login', reminder: response.result.remainder
              });
            }else{
              // stored the guest account data in seesion strorage similar to the Direct signup          
              sessionStorage.setItem('guestAccount', JSON.stringify({basic:{email:response.result.email}}));          
              history.push('/register',{currentStep:'mealPlan'});
            }
          },500);                  
          
        }
      }).catch(() => {
        this.setState({errorMessage: 'Something went wrong on server.'})
      });     
    } else {
      this.setState({errorMessage:'Something went wrong in Create Account!!'})

      // track error
      window.analytics.track('Error Create Account', {
        error: 'Something went wrong',
      });
    }
  }
   
  handleSocialLoginFailure(err){
    if(err){
      this.setState({errorMessage:'Something went wrong!!'})
    }
  }


  render() {
    const { errorMessage } = this.state;
    let check = true;
    if (this.props.location && this.props.location.pathname !== '/checkUser') {
      check = false;
    }
    if (window.innerWidth < 1024) {
      return (

        <div className="wrapper" style={{ transition: 'ease-in 0.5s'}}>
          <header>
            <div className="container">
              <img src="/images/logo.png" alt="" onClick={() => history.push('/')} />
            </div>
          </header>
          <section className="main-cntnt slidetest" style={{right: '-300px'}}>
            <div className="container">
              <div className="main-cntnt-box">
                <h2>Welcome Back Ninja!</h2>
                <p className="hdng-para">Ready for another week of meal planning greatness</p>
                <div className="main-cntnt-inner">
                  <div className="col-xs-12 col-md-6 login-sectn">
                    {errorMessage ? <span className="login-error-msg">{errorMessage}</span> : null}
                    <div className="row">
                      <div className="col-md-12 form-row">
                        <label htmlFor="email">Email:</label>
                        <input
                          type="text"
                          id="email"
                          onChange={e => this.updateInputs('email', e.target.value)}
                        />
                      </div>
                    </div>
                    <div className="row">
                      <div className="col-md-12 form-row">
                        <label htmlFor="password">Password:</label>
                        <input
                          id="password"
                          type="password"
                          onKeyDown={event => this.keyEvent(event)}
                          onChange={e => this.updateInputs('password', e.target.value)}
                        />
                      </div>
                    </div>
                    <div className="row">
                      <div className="form-row text-center col-md-12">
                        <button type="button" className="login-btn" onClick={() => this.onClickLogin()}>Login</button>
                        {!check &&
                          <p className="forgot-pass-txt">
                            <Link to="/forgetPassword" href>
                              Forgot your password?
                            </Link>
                          </p>
                        }
                      </div>
                    </div>
                    { !check &&
                    <p className="dont-have-ac-p"> Don{'\''}t have an account yet?&nbsp;
                      <Link href className="signup-link" to="/register">
                        Sign Up
                      </Link>
                    </p>
                    }
                  </div>

                </div>
              </div>
            </div>
          </section>
        </div>
      );
    }

    

    return (

      <div className="wrapper" style={{ transition: 'ease-in 0.5s'}}>
        <header>
          <div className="container">
            <img src="/images/logo.png" alt="" onClick={() => history.push('/')} />
          </div>
        </header>
        <section className="main-cntnt slidetest" style={{right: '-300px'}}>
          <div className="container">
            <div className="main-cntnt-box">
              <h2>Welcome Back Ninja!</h2>
              <p className="hdng-para">Ready for another week of meal planning greatness? We certainly are... Sign in to your account and let{'\''}s get this thing going. Wax on, wax off right?</p>
              <div className="main-cntnt-inner">

                <div className="col-xs-12 col-md-6 intro-sectn">
                  <p className="section1-hdng">A simple rule to healthy eating:</p>
                  <p>&#34;If it came from a plant - <b>eat it.</b>&#34;</p>
                  <p>&#34;If it was made in a plant - <b>don&rsquo;t!</b>&#34;</p>
                  <span>- Wise Guru</span>
                </div>

                <div className="col-xs-12 col-md-6 login-sectn">
                  <h4>Login to Your Ninja Account</h4>
                  {errorMessage ? <span className="login-error-msg">{errorMessage}</span> : null}
                  <div className="row">
                    <div className="col-md-12 form-row">
                      <label htmlFor="email">Email:</label>
                      <input
                        type="text"
                        id="email"
                        onChange={e => this.updateInputs('email', e.target.value)}
                      />
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-12 form-row">
                      <label htmlFor="password">Password:</label>
                      <input
                        id="password"
                        type="password"
                        onKeyDown={event => this.keyEvent(event)}
                        onChange={e => this.updateInputs('password', e.target.value)}
                      />
                    </div>
                  </div>
                  <div className="row">
                    <div className="form-row text-center col-md-12">
                      <button type="button" className="login-btn" onClick={() => this.onClickLogin()}>Login</button>
                      {!check &&
                        <p className="forgot-pass-txt">
                          <Link to="/forgetPassword" href>
                            Forgot your password?
                          </Link>
                        </p>
                      }
                    </div>
                  </div>
                  <div className="row">
                    <SocialLoginButton
                      provider='facebook'
                      appId='1306170349549515'
                      onLoginSuccess={(e) => this.handleSocialLogin(e)}
                      onLoginFailure={(e) =>this.handleSocialLoginFailure(e)}
                    >
                      <img src={FacebookLogo} alt="Login with Faceebook"/>
                    </SocialLoginButton>
                    <SocialLoginButton
                      provider='google'
                      appId='726295967537-7q1n60a4cr9q22gcs5ttnmu1cp3epdbp.apps.googleusercontent.com'
                      onLoginSuccess={(e) => this.handleSocialLogin(e)}
                      onLoginFailure={(e) =>this.handleSocialLoginFailure(e)}
                    >
                      <img src={GoogleLogo} alt="Login with Google"/> 
                    </SocialLoginButton>
                  </div>
                  { !check &&
                  <p> Don{'\''}t have an account yet?&nbsp;
                    <Link href className="signup-link" to="/register">
                      Sign Up!
                    </Link>
                  </p>
                  }
                </div>

              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}

export default Login;
