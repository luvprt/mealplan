import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import RoutingConstants from 'lib/RoutingConstants';
import { validateInputs } from 'lib/services';
import { post } from 'lib/api';

class VerifyEmail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputs: {},
      errorMessage: null,
      confirmMessage: false,
    };
  }

  onClickReset() {
    const { email } = this.state.inputs;
    // inputs['email'] = 'mealplanninja@yopmail.com'
    let { disable, errorMessage } = this.state;
    const url = RoutingConstants.forgotPassword;
    this.setState({ txt: '...', disable: true });
    if (email) {
      if (!validateInputs('email', email)) {
        errorMessage = 'Please enter a valid email address';
        disable = false;
      } else {
        errorMessage = null;
        post(url, this.state.inputs).then((response) => {
          if (response.result.token) {
            this.setState({ confirmMessage: true, disable: false, txt: '' });
            localStorage.setItem('resetToken', response.token);
          }
        }).catch(() => {
          errorMessage = 'Sorry! email address not exist, Try another ';
          this.setState({ errorMessage, disable: false, txt: '' });
          // console.log(err);
        });
      }
    } else {
      errorMessage = 'Sorry! incorrect email, Try again.';
    }
    this.setState({ errorMessage, disable });
  }

  updateInputs(text) {
    const { inputs, confirmMessage } = this.state;
    inputs.email = text.trim();
    this.setState({ inputs });
    if (confirmMessage === true) {
      this.setState({ confirmMessage: false });
    }
  }

  render() {
    const {
      errorMessage, confirmMessage, txt, disable,
    } = this.state;
    return (
      <section className="main-cntnt">
        <div className="container">
          <div className="main-cntnt-box pass-reset-main">
            <h2>Welcome Back Ninja!</h2>
            <p className="hdng-para">Let&apos;s get that password recovered.</p>
            <div className="main-cntnt-inner">
              <div className="col-xs-12 col-md-6 intro-sectn">
                <p className="section1-hdng">A simple rule to healthy eating:</p>
                <p>&quot;If it came from a plant - <b>eat it.</b>&quot;</p>
                <p>&quot;If it was made in a plant - <b>don&apos;t!</b>&quot;</p>
                <span>- Wise Guru</span>
              </div>
              <div className="col-xs-12 col-md-6 login-sectn pass-reset-sectn">
                <h4>Password Reset</h4>
                <p className="reset-pass-txt">Enter your email below.</p>
                {errorMessage ? <span className="login-error-msg" style={{ position: 'static' }}>{errorMessage}</span> : null}
                {!confirmMessage ?
                  <div>
                    <div className="row">
                      <div className="col-md-12 form-row">
                        <label htmlFor="email">Email:</label>
                        <input type="text" onChange={e => this.updateInputs(e.target.value)} />
                      </div>
                    </div>
                    <div className="row">
                      <div className="form-row text-center col-md-12 form-btn-box">
                        <button type="button" className="login-btn" disabled={disable} onClick={() => this.onClickReset()}>Reset{txt}</button>
                        <p className="forgot-pass-txt">
                          Just remembered your password?&nbsp;
                          <Link className="signup-link" to="/login">Try Logging in.</Link>
                        </p>
                      </div>
                    </div>
                    <p>
                      Don{'\''}t have any account yet?&nbsp;
                      <Link className="signup-link" to="/register">Sign Up!</Link>
                    </p>
                  </div>
                  :
                  <div>
                    <div className="row">
                      <div className="form-row text-center col-md-12 form-btn-box">
                        <div className="home-reset-pass-msg">
                          <div className="row">
                            <span>We&apos;ve sent an email to </span>
                            <span className="pwd-forgot-msg">{this.state.inputs.email} </span>
                          </div>
                          <div className="row ">
                          when you receive it, click the link create a new password
                          </div>
                        </div>
                        <button type="button" className="login-btn email-send">Sent!</button>
                      </div>
                    </div>
                    <div className="row support-email-cntn">
                      <span>Didn’t receive your email? Check your spam folder or email us at </span>
                      <span className="pwd-forgot-msg">support@mealplanninja.com </span>
                      <span>for help!</span>
                    </div>
                  </div>
                }
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}

export default VerifyEmail;
