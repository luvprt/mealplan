/*
Component Name: ForgetPassword component
Contains : VerifyEmail, ResetPassword
Imcludes : Services PropTypes
Default screen: Dashboard
*/

import React, { Component } from 'react';
import VerifyEmail from './VerifyEmail';
import ResetPassword from './ResetPassword';

class ForgetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentStep: 'veryfiyEmail',
    };
  }

  onClickNext(currentState, nextState, data) {
    let guestAccount = {};
    const guestAccountData = sessionStorage.getItem('guestAccount');
    if (guestAccountData && guestAccountData != null) {
      guestAccount = JSON.parse(guestAccountData);
    }
    guestAccount[`${currentState}`] = data;
    sessionStorage.setItem('guestAccount', JSON.stringify(guestAccount));
    this.setState({ currentStep: nextState });
  }

  /* diffrent Login and Reset screens conditional rendering */
  renderContent() {
    switch (this.state.currentStep) {
    case 'veryfiyEmail':
      return (
        <VerifyEmail onClickNext={data => this.onClickNext('veryfiyEmail', 'resetPassword', data)} {...this.props} />
      );
    case 'resetPassword':
      return (
        <ResetPassword onClickNext={data => this.onClickNext('resetPassword', 'resetPassword1', data)} {...this.props} />
      );
    default:
    }
    return this.state.currentStep;
  }

  render() {
    const { history } = this.props;
    return (
      <div className="wrapper transition-item detail-page">
        <header>
          <div className="container">
            <img src="/images/logo.png" alt="" onClick={() => { history.push('/'); }} />
          </div>
        </header>
        {this.renderContent()}
      </div>
    );
  }
}

export default ForgetPassword;

