import React, { Component } from 'react';
import RoutingConstants from 'lib/RoutingConstants';
import { Link } from 'react-router-dom';
import { post } from 'lib/api';
import { validateInputs } from 'lib/services';
import history from '../../history';


class ResetPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputs: {
        token: props.match.params.id,
        email: props.location.search.split('=')[1],
      },
      errorText: false,
      errorMessage: {},
    };
  }

  componentDidMount() {
    const url = RoutingConstants.verifyToken + this.props.match.params.id;
    let { errorText } = this.state;
    post(url).then(() => {
      errorText = true;
      this.setState({ errorText });
    }).catch(() => {
      errorText = false;
      this.setState({ errorText });
    });
  }

  onClickReset() {
    const {
      password,
      confirmPassword,
    } = this.state.inputs;
    const errorMessage = this.state.errorMessage;
    const inputs = this.state.inputs;
    inputs.password_confirmation = confirmPassword;
    // inputs.token = localStorage.getItem('resetToken');
    // inputs.email = 'mealplanninja@yopmail.com';

    if (password && confirmPassword) {
      if (!validateInputs('password', password)) {
        errorMessage.password = 'Create a secure password. It must contain 8+ characters with 1 number or special character.';
      } else {
        errorMessage.password = false;
      }

      if (password !== confirmPassword) {
        errorMessage.confirmPassword = 'Password must match Please give it another try.';
      } else {
        errorMessage.confirmPassword = false;
        if (!errorMessage.password && !errorMessage.confirmPassword) {
          this.setState({ inputs });
          const url = RoutingConstants.resetPassword;
          post(url, inputs).then(() => {
            history.push('/login');
          }).catch(() => {
            // console.log(err);
          });
        }
      }
    } else if (!password) {
      errorMessage.password = 'Create a secure password. It must contain 8+ characters with 1 number or special character.';
    } else {
      errorMessage.confirmPassword = 'Password must match Please give it another try.';
    }
    this.setState({ errorMessage });
  }

  verifyPage(errorText) {
    this.setState({ errorText });
  }

  updateInputs(ref, text) {
    const inputs = this.state.inputs;
    inputs[`${ref}`] = text.trim();
    this.setState({ inputs });
  }

  render() {
    const { errorMessage, errorText } = this.state;
    // const {history} = this.props;
    return (

      <section className="main-cntnt">
        <div className="container">
          {errorText ?
            <div className="main-cntnt-box pass-recover-box">
              <h2>Reset Your Password:</h2>
              <p className="hdng-para">Let&apos;s get that password recovered.</p>
              <div className="main-cntnt-inner">
                <div className="col-xs-12 col-md-12 login-sectn pass-reset-sectn">
                  <h4>Enter a New Secure Password</h4>
                  <p className="reset-pass-txt">Password must contain 8+ characters with atleast 1 number of special character.</p>
                  <div className="row">
                    <div className="pass-form-row">
                      <div className="col-md-12 form-row">
                        <label htmlFor="password" className={errorMessage.password ? 'error-label' : null}>New Password:</label>
                        <input type="password" onChange={e => this.updateInputs('password', e.target.value)} />
                      </div>
                      {errorMessage.password ? <span className="error-msg">{errorMessage.password}</span> : null}
                    </div>
                    <div className="pass-form-row">
                      <div className="col-md-12 form-row">
                        <label htmlFor="confirmPassword" className={errorMessage.confirmPassword ? 'error-label' : null}>Confirm Password:</label>
                        <input type="password" onChange={e => this.updateInputs('confirmPassword', e.target.value)} />
                      </div>
                      {errorMessage.confirmPassword ? <span className="error-msg">{errorMessage.confirmPassword}</span> : null}
                    </div>

                  </div>
                  <div className="row">
                    <div className="form-row text-center col-md-12">
                      <button className="login-btn save-signin-btn" onClick={() => this.onClickReset()}>Save & Sign In</button>
                    </div>
                  </div>
                </div>
              </div>
            </div> :
            <div className="main-cntnt-box pass-recover-box">
              <p>Invalid Link! Please
                <strong>
                  <Link to="/login" className="colr"> Click Here </Link>
                </strong>
                for Login
              </p>
            </div>
          }
        </div>
      </section>
    );
  }
}

export default ResetPassword;
