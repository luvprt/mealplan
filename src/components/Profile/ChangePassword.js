import React, { Component } from 'react';
import { post } from 'lib/api';
import { validateInputs } from 'lib/services';
import RoutingConstant from 'lib/RoutingConstants';
import history from '../../history';

class ChangePassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputs: {},
      errorMessage: {
        oldPassword: true,
        confirmPassword: true,
        password: true,
      },
      updateText: 'Update password',
    };
  }

  updatePassword() {
    const url = RoutingConstant.changePassword;
    const { inputs, errorMessage } = this.state;
    let { updateText } = this.state;

    this.setState({ updateText: 'Updating password....' });

    if (!errorMessage.oldPassword &&
        !errorMessage.password &&
        !errorMessage.confirmPassword
    ) {
      post(url, inputs).then(() => {
        this.setState({ updateText: 'Update password' });
        localStorage.removeItem('token');

        // track password success
        window.analytics.track('Changed Password', {
          status: 'success',
        });
        history.push('/login');
      }).catch((err) => {
        // track password failure
        window.analytics.track('Changed Password', {
          status: 'failure',
        });

        if (err.message === 'Password Not matched') {
          errorMessage.oldPassword = 'You have entered wrong password. Please enter correct password.';
          this.setState({ errorMessage });
        }
        if (err.error === 'token_expired' || err.error === 'token_not_provided') {
          localStorage.removeItem('token');
          history.push('/login');
        }
      });
    } else if (errorMessage.oldPassword === true) {
      errorMessage.oldPassword = 'Please enter your old password ';
      updateText = 'Update password';
    } else if (errorMessage.password) {
      errorMessage.password = 'Please enter your new password ';
      updateText = 'Update password';
    } else if (errorMessage.confirmPassword) {
      errorMessage.confirmPassword = 'Please enter new password for confirmation';
      updateText = 'Update password';
    } else {
      errorMessage.oldPassword = 'Please enter your old password ';
      errorMessage.password = 'Please enter your new password ';
      errorMessage.confirmPassword = 'Please enter new password for confirmation';
      updateText = 'Update password';
    }
    this.setState({ errorMessage, updateText });

    // track changed password
  }

  changeValue(event) {
    const { errorMessage, inputs } = this.state;
    if ((event.target.name === 'old_password')) {
      if (!validateInputs('password', event.target.value)) {
        errorMessage.oldPassword = 'Please enter your old password ';
      } else {
        errorMessage.oldPassword = false;
      }
    }
    if (event.target.name === 'password') {
      if (!validateInputs('password', event.target.value)) {
        errorMessage.password = 'Create a secure password. It must contain 8+ characters with 1 number or special character.';
      } else {
        errorMessage.password = false;
      }
    }
    if (event.target.name === 'confirm_password') {
      if (event.target.value !== inputs.password) {
        errorMessage.confirmPassword = 'Your password is not same';
      } else {
        errorMessage.confirmPassword = false;
      }
    }
    this.setState({ errorMessage });
    // console.log('err', errorMessage);
    if ((!errorMessage.password || !errorMessage.oldPassword)
        && event.target.name !== 'confirm_password') {
      inputs[event.target.name] = event.target.value;
      this.setState({ inputs });
    }
  }

  render() {
    const { password } = this.state.inputs;
    const {
      errorMessage, updateText, inputs,
    } = this.state;
    // const  updateText = !content ? 'Update password' : 'Updating password ....';
    return (
      <div className="grocery-list-cntnt box-margin">
        <div className="row ">
          <div className="col-md-12 spc">
            <div className="top-section">
              <span> Change Your Password</span>
              <p>Enter a new, secure password.
                Passwords must contain 8+ characters with at least 1 number of special character.
              </p>
              <div className="col-md-12 login-sectn pass-reset-sectn password_section left_border ">
                <div className="row ">
                  <div className="col-md-8 form-row spc_fild ">
                    <label
                      className={
                        errorMessage.oldPassword !== false
                        && errorMessage.oldPassword !== true ? 'error-label' : null
                      }
                      htmlFor="label"
                    >
                      Old Password:
                    </label>
                    <input
                      type="password"
                      value={inputs.old_password}
                      name="old_password"
                      id="label"
                      onChange={event => this.changeValue(event)}
                      className={errorMessage.oldPassword !== false &&
                        errorMessage.oldPassword !== true ? 'error-border' : null}
                    />
                    {errorMessage.oldPassword !== false ?
                      <span className="error-msg">
                        {errorMessage.oldPassword}
                      </span>
                      :
                      null
                    }
                  </div>

                  <div className="col-md-8 form-row spc_fild top-section ">
                    <p className="text">Enter the current password that you signed in with.</p>
                  </div>
                </div>

                <div className="row">
                  <div className="col-md-8 form-row spc_fild">
                    <label
                      className={
                        errorMessage.password !== false &&
                        errorMessage.password !== true ? 'error-label' : null
                      }
                      htmlFor="label2"
                    >
                      New Password:
                    </label>
                    <input
                      type="password"
                      value={password}
                      name="password"
                      id="label2"
                      onChange={event => this.changeValue(event)}
                      className={errorMessage.password !== false &&
                        errorMessage.password !== true ? 'error-border' : null}
                    />
                    {errorMessage.password !== false ?
                      <span className="error-msg">
                        {errorMessage.password}
                      </span>
                      :
                      null
                    }
                  </div>

                  <div className="col-md-9 form-row spc_fild top-section ">
                    <p className="text">
                      Create a secure password.
                      It must contain 8+ characters with 1 number or special character.
                    </p>
                  </div>
                </div>

                <div className="row">
                  <div className="col-md-9 form-row spc_fild">
                    <label
                      className={
                        errorMessage.confirmPassword !== false &&
                        errorMessage.confirmPassword !== true ? 'error-label' : null
                      }
                      htmlFor="label3"
                    >
                      Confirm Password:
                    </label>
                    <input
                      type="password"
                      value={inputs.confirm_password}
                      name="confirm_password"
                      id="label3"
                      onChange={event => this.changeValue(event)}
                      className={
                        errorMessage.confirmPassword !== false
                        && errorMessage.confirmPassword !== true ? 'error-border' : null
                      }
                    />
                    {errorMessage.confirmPassword !== false ?
                      <span className="error-msg">
                        {errorMessage.confirmPassword}
                      </span>
                      :
                      null
                    }
                  </div>

                  <div className="col-md-9 form-row spc_fild top-section ">
                    <p className="text">
                      In order to confirm your password you must enter it again.
                      Passwords must match!
                    </p>
                  </div>
                </div>

                <div className="row">
                  <div className="form-row text-right col-md-8 margn">
                    <button
                      className="next-btn color "
                      onClick={() => this.updatePassword()}
                    >
                      {updateText}
                    </button>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </div>
      </div>

    );
  }
}
export default ChangePassword;
