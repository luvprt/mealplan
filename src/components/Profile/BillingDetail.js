import React from 'react';
import SubscriptionDetail from '../billingDetails/SubscriptionDetail';
// eslint-disable-next-line
import CardDetail from '../billingDetails/CardDetail';

const BillingDetail = props => (
  <div className="col-lg-9 dash-main-cntnt edit-profile-cntnt">
    <div className="dash-cntnt-dtail">
      <div className="swap-mode-dtail-cntnt">
        <h4>Your Meal Plan Ninja Profile</h4>
        <SubscriptionDetail {... props} />
        <CardDetail {... props} />
      </div>
    </div>
  </div>
);

export default BillingDetail;
