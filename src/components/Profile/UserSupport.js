import React, { Component } from 'react';
import { post } from 'lib/api';
import { validateInputs } from 'lib/services';
import RoutingConstant from 'lib/RoutingConstants';
import history from '../../history';


class UserSupport extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputs: {},
      errorMessage: {
        name: true,
        email: true,
        message: true,
      },
      content: 'Send',
    };
  }

  sendEmail() {
    const url = RoutingConstant.contactSupport;
    const { inputs, errorMessage } = this.state;
    this.setState({ content: 'Sending...' });
    if (!errorMessage.name && !errorMessage.email && !errorMessage.message) {
      post(url, inputs).then(() => {
        this.setState({ content: 'Message Sent' });
        // track support ticket
        window.analytics.track('Support Ticket Submitted', {
          status: 'success',
        });
      }).catch((err) => {
        this.setState({ content: 'Send' });
        // track support ticket
        window.analytics.track('Support Ticket Submitted', {
          status: 'failure',
        });
        if (err.error === 'token_expired' || err.error === 'token_not_provided') {
          localStorage.removeItem('token');
          history.push('/login');
        }
      });
    }else {
      if (errorMessage.name === true) {
        errorMessage.name = 'Please fill your name';
      } else if (errorMessage.email) {
        errorMessage.email = 'Please enter your email';
      } else if (errorMessage.message) {
        errorMessage.message = 'Please enter your message';
      } else {
        errorMessage.name = 'Please fill your name';
        errorMessage.message = 'Please enter your message';
        errorMessage.email = 'Please enter your email';
      }
      this.setState({ errorMessage, content: 'Send' });
    }
  }

  changeValue(event) {
    const { errorMessage, inputs } = this.state;
    if ((event.target.name === 'name')) {
      if (!validateInputs('alphabetics', event.target.value)) {
        errorMessage.name = 'Please enter your name ';
      } else {
        errorMessage.name = false;
      }
    }
    if (event.target.name === 'email') {
      if (!validateInputs('email', event.target.value)) {
        errorMessage.email = 'Please enter valid email ';
      } else {
        errorMessage.email = false;
      }
    }
    if (event.target.name === 'message') {
      if (event.target.value === '') {
        errorMessage.message = 'Please enter your message ';
      } else {
        errorMessage.message = false;
      }
    }
    this.setState({ errorMessage});
    if (!errorMessage.name || !errorMessage.email || !errorMessage.message) {
      inputs[event.target.name] = event.target.value;
      this.setState({ inputs });
    }
  }

  render() {
    const {
      name, email, message, errorMessage, content,
    } = this.state;
    const sendText = content;
    return (
      <div className="col-lg-9 dash-main-cntnt outer_background">
        <div className="dash-cntnt-dtail swap-mode-dtail">
          <div className="dash-cntnt-dtail">
            <div className="swap-mode-dtail-cntnt">
              <div className="dash-cntnt-dtail-hdr box-margin">

                <div className="swap-mode-dtail-cntnt Report_bug centre_part">
                  <h4>Meal Plan Ninja - Comments & Support</h4>


                  <div className="row contact_form">
                    <div className="col-md-12 support">
                      <div className="top-section">
                        <span>Leave Us a Comment or Contact Our Support Team:</span>
                        <p className="report_bug"> Have a question? Have a concern? Found something that doesn’t work (especially this kind of stuff - we want to fix that!)? Let us know! We seriously care about making Meal Plan Ninja work for you. Drop us a line and we’ll get back to you!</p>

                        <div className="col-md-12 login-sectn pass-reset-sectn password_section left_border">

                          <div className="row ">
                            <div className="col-md-8 form-row lbe ">
                              <label
                                className={
                                  errorMessage.name !== false &&
                                  errorMessage.name !== true ?
                                    'error-label' : null
                                }
                                htmlFor="supportPage"
                              >
                                Your Name:
                              </label>
                              <input
                                type="text"
                                value={name}
                                name="name"
                                onChange={event => this.changeValue(event)}
                                className={
                                  errorMessage.name !== false &&
                                  errorMessage.name !== true ?
                                    'error-border' : null
                                }
                              />
                              {errorMessage.name !== false ?
                                <span className="error-msg">
                                  {errorMessage.name}
                                </span>
                                :
                                null
                              }
                            </div>
                          </div>

                          <div className="row ">
                            <div className="col-md-8 form-row lbe">
                              <label
                                className={
                                  errorMessage.email !== false &&
                                  errorMessage.email !== true ?
                                    'error-label' : null
                                }
                                htmlFor="supportPage"
                              >
                                Email:
                              </label>
                              <input
                                type="email"
                                id="supportPage"
                                value={email}
                                name="email"
                                className={
                                  errorMessage.email !== false &&
                                  errorMessage.email !== true ?
                                    'error-border' : null
                                }
                                onChange={event => this.changeValue(event)}
                              />
                              {errorMessage.email !== false ?
                                <span className="error-msg">
                                  {errorMessage.email}
                                </span>
                                :
                                null
                              }
                            </div>
                          </div>

                          <div className="row ">
                            <div className="col-md-8 form-row lbe">
                              <label className="msg_box" htmlFor="supportPage">
                                What can we help with? Or describe the issue:
                              </label>
                            </div>
                          </div>

                          <div className="row">
                            <div className="form-group message_box ">
                              <textarea
                                className={
                                  errorMessage.message !== false
                                  && errorMessage.message !== true ?
                                    'form-control area error-text-area' : 'form-control area'
                                }
                                id="exampleFormControlTextarea1"
                                rows="3"
                                name="message"
                                value={message}
                                onChange={event => this.changeValue(event)}
                              />
                            </div>
                          </div>
                          {errorMessage.message !== false ?
                            <div className="row">
                              <div className="col-md-6 form-row mesg_text ">
                                <span className="error-msg">
                                  {errorMessage.message}
                                </span>
                              </div>
                            </div>
                            :
                            <div className="row">
                              <div className="col-md-6 form-row mesg_text" />
                            </div>
                          }

                          <div className="row">
                            <div className="col-md-7 form-row mesg_text ">
                              <p className="">Have a photo or screenshot of the issue that you can send us?
                                You’re awesome! Send it to:<strong className="colr"> support@mealplanninja.com</strong>
                              </p>
                            </div>
                          </div>


                          <div className="form-row text-right ">
                            <button className="next-btn color" onClick={() => this.sendEmail()} disabled={!content}>{sendText}</button>
                          </div>
                          <p style={{color: '#12B75E'}}>Thank you for submitting a comment / contacting our support team. We{`'`}ll be in touch ASAP!</p>

                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

    );
  }
}
export default UserSupport;
