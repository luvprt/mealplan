import React, { Component } from 'react';
import RoutingConstants from 'lib/RoutingConstants';
import { get } from 'lib/api';
import Loader from 'lib/loader';
import ProfileBar from './ProfileBar';
import UserProfile from './UserProfile';
import ChangePassword from './ChangePassword';
import EditMeal from './EditMeal';
import DashHeader from '../dashboard/DashHeader';
import UserSupport from './UserSupport';
import BillingDetail from './BillingDetail';
import DashboardBottom from '../dashboard/DashboardBottom'

export const ProfileSetting = props => (
  <div className="col-lg-9 dash-main-cntnt">
    <div className="dash-cntnt-dtail swap-mode-dtail">
      <div className="dash-cntnt-dtail">
        <div className="swap-mode-dtail-cntnt">
          <UserProfile {...props} />
          <EditMeal {...props} />
          <ChangePassword />
        </div>
      </div>
    </div>
  </div>
);

class Settings extends Component {
  constructor(props) {
    super(props);
    this.state = {
      value: '',
      subscriptionPlan: [],
      loadData: false,
      dashClass: 'active',
      exist: props.location.state.exist,
      dayDates: props.location.state.dayDates,
      subscriptionReminder: props.location.state.reminder,
    };
  }
  componentDidMount() {
    let { value } = this.state;
    if (this.props.location.pathname === '/support') {
      value = 3;
      this.setValue(value, '', 'active');
    } else {
      if (this.props.location.pathname === '/billingDetails') {
        value = 2;
        this.getBillingDetail(value);
      } else {
        value = 1;
        this.setValue(value, 'active', '');
      }
      this.getProfileData(value);
    }
  }

  // profile detail
  getProfileData(value) {
    const url = RoutingConstants.fetchProfileDetail;
    get(url).then((response) => {
      if (response.result && response.result.length > 0) {
        const data = response.result[0];
        const mealPlan = data.user_settings;
        localStorage.setItem('default_serving_size',mealPlan.default_serving_size);
        // eslint-disable-next-line
        let reminder, renewDate, planId, plan;
        const name = `${data.first_name} ${data.last_name?data.last_name:''}`;
        if (response.result[0].stripe_id !== '' && response.result[0].stripe_id !== null) {
          reminder = false;
          if (data.subscription) {
            renewDate = data.subscription.updated_at;
            planId = data.subscription.stripe_plan;
          }
        } else {
          reminder = true;
        }
        this.setState({
          name,
          email: data.email,
          DOB: data.date_of_birth,
          servingSize: mealPlan ? mealPlan.default_serving_size : '' || 2,
          id: data.id || 1,
          cookingStyle: mealPlan ? mealPlan.preferred_cooking_style  : '' || 'Batch Cook',
          dietType: mealPlan ? mealPlan.preferred_diet_type  : '' || 'Balanced',
          exclusion: data.user_exclusion,
          reminder,
          lastDigits: data.card_last_four,
          activeMember: data.created_at,
          renewDate,
          planId,
          value,
          dashClass: 'active',
          supportClass: '',
          trialEndDate: data.trial_ends_at,
        });
      }
    });
  }

  // billing detail
  getBillingDetail(value) {
    const url = RoutingConstants.getSubscriptionPlan;
    get(url).then((response) => {
      let result = response.result;
      if(response.result && response.result.data) {
        result = response.result.data
      }
      this.setState({ subscriptionPlan: result, value, loadData: false });
      this.getProfileData(value);
      return response.result.data;
    }).catch(() => {
      // console.log('error', err);
    });
  }

  // set value
  setValue(value, dashClass, supportClass) {
    this.setState({ value, dashClass, supportClass });
  }

  render() {
    const {
      value, loadData, dashClass, supportClass, exist, subscriptionReminder, dayDates
    } = this.state;
    return (
      <div className="wrapper dash-cntnt">
        <DashHeader
          dashClass={dashClass}
          supportClass={supportClass}
          planExist={exist}
          reminder={subscriptionReminder}
          dayDates={dayDates}
        />
        {loadData &&
          <Loader />
        }
        <section className="dashboard-main profile-dash">
          <div className="container">
            <div className="usr-profile">
              <ProfileBar {... this.state} />
              { value === 3 ?
                <UserSupport />
                :
                value === 2 ?
                  <BillingDetail {... this.state} />
                  :
                  value === 1 ?
                    <ProfileSetting {...this.state} />
                    :
                    <div className="loading">
                      <Loader />
                    </div>
              }
            </div>
            <DashboardBottom 
              groceryClass=""
              dashClass=""
              accountClass="active"
              mbClass="mb-only"
              editRecipe={false}
            />
          </div>
        </section>
      </div>
    );
  }
}
export default Settings;
