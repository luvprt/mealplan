import React, { Component } from 'react';
import RoutingConstant from 'lib/RoutingConstants';

import { get, post } from 'lib/api';

class EditMeal extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...this.props,
      batch: false,
      weekNight: false,
      freshCook: false,
      PROTEIN_LIST: [],
      exclusionId: [],
      PROTEIN_EXCLUSION_LIST: [],
      inputs: {},
      content: false,
      confirmMessage: '',
      previousSetting: [],
    };
  }

  // unsafe
  // eslint-disable-next-line
  componentDidMount() {
    this.getProteinList();
  }

  // unsafe
  // eslint-disable-next-line
  UNSAFE_componentWillReceiveProps(props) {
    const { PROTEIN_EXCLUSION_LIST, previousSetting } = this.state;
    const { servingSize, cookingStyle, dietType } = props;
    this.setState({
      ...props,
    });
    if (props.exclusion && props.exclusion.length > 0) {
      props.exclusion.map((item) => {
        PROTEIN_EXCLUSION_LIST.push({ id: item.exclusion_types_data_id });
        return item;
      });
      const length = PROTEIN_EXCLUSION_LIST.length;
      previousSetting.push({
        servingSize,
        cookingStyle,
        dietType,
        noOfExcludeProtein: length,
      });
    }
  }

  getProteinList() {
    const url = RoutingConstant.fetchProteinExclusion;
    get(url).then((response) => {
      this.setState({ PROTEIN_LIST: response });
    }).catch(() => {
    });
  }

  updateList(value) {
    value = parseInt(value, 0);
    const { PROTEIN_EXCLUSION_LIST, exclusionId } = this.state;
    if (PROTEIN_EXCLUSION_LIST) {
      const index = PROTEIN_EXCLUSION_LIST.findIndex(x => x.id === value);
      if (index > -1) {
        PROTEIN_EXCLUSION_LIST.splice(index, 1);
        this.setState({ PROTEIN_EXCLUSION_LIST, exclusionId });
      } else {
        PROTEIN_EXCLUSION_LIST.push({ id: value });
        this.setState({ PROTEIN_EXCLUSION_LIST, exclusionId });
      }
    }
  }

  updateMealPlan() {
    const url = RoutingConstant.updateMealData;
    this.setState({ content: true });
    const { inputs } = this.state;
    const {
      servingSize, cookingStyle, dietType, PROTEIN_EXCLUSION_LIST, previousSetting,
    } = this.state;
    inputs.default_serving_size = servingSize;
    inputs.preferred_diet_type = dietType;
    inputs.preferred_cooking_style = cookingStyle;
    inputs.exclusion = PROTEIN_EXCLUSION_LIST;
    post(url, inputs).then((response) => {
      const confirmMessage = 'You have successfully update your meal plan settings';
      localStorage.setItem('default_serving_size',inputs.default_serving_size);

      // eslint-disable-next-line
      this.setState({ content: false, response, confirmMessage });

      const length = PROTEIN_EXCLUSION_LIST.length;
      // track changed meal preferences
      window.analytics.track('Changed Meal Preferences', {
        previousMealSetting: previousSetting,
        changeMeal: {
          servingSize,
          dietType,
          cookingStyle,
          noOfProteinExclude: length,
        },
      });
    }).catch((err) => {
      // eslint-disable-next-line
      this.setState({ content: false, err });
    });
  }

  updateData(event) {
    let {
      batch, freshCook, weekNight, cookingStyle,
    } = this.state;
    this.setState({
      [event.target.name]: event.target.value,
    });

    if (event.target.name === 'cookingStyle') {
      if (event.target.value === 'Batch Cook') {
        batch = true;
        freshCook = false;
        weekNight = false;
        cookingStyle = 'Batch Cook';
      } else if (event.target.value === 'Batch & weeknight') {
        batch = false;
        freshCook = false;
        weekNight = true;
        cookingStyle = 'Batch & weeknight';
      } else {
        batch = false;
        freshCook = true;
        weekNight = false;
        cookingStyle = 'Cook fresh';
      }
      this.setState({
        batch, freshCook, weekNight, cookingStyle,
      });
    }
  }

  message() {
    this.setState({ confirmMessage: '' });
  }

  render() {
    const {
      servingSize, cookingStyle, dietType, PROTEIN_LIST, PROTEIN_EXCLUSION_LIST, content, confirmMessage,
    } = this.state;
    let { batch, weekNight, freshCook } = this.state;
    let data = false;
    const saveText = content === false ? 'Save Changes' : 'Saving...';
    if (PROTEIN_LIST.result) {
      data = true;
    }
    if (cookingStyle === 'Batch Cook') {
      batch = true;
    } else if (cookingStyle === 'Batch & weeknight') {
      weekNight = true;
    } else {
      freshCook = true;
    }

    return (
      <div>
        <div className="row">
          <div className="col-md-12">
            <div className="top-section box-margin ">
              <span> Edit Your Meal Preferences: </span>
              <p>
                  What is your meal planning style?
                  We use this information to help us guide our product development efforts
                  and to help craft intelligent meal plans each week.
              </p>
            </div>
          </div>
        </div>
        <div className="col-xs-12 col-md-6 login-sectn box-margin">

          <div className="row">
            <div className="col-md-12 form-row select-row-small">
              <label htmlFor="text">
                How many people will you be meal planning for on a weekly basis?
              </label>
              <div className="btn-group bootstrap-select custm-selct custm-selct-numbr">
                <select
                  className="selectpicker show-tick form-control"
                  name="servingSize"
                  value={servingSize}
                  onChange={event => this.updateData(event)}
                >
                  <option value="1">1</option>
                  <option value="2">2</option>
                  <option value="3">3</option>
                  <option value="4">4</option>
                  <option value="5">5</option>
                </select>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12 form-row">
              <p className="inner-check-row-hdng">What is your preffered cooking stye (pick 1):</p>
              <div className="check-row">
                <label className="rbutton check_font" htmlFor="option-a">
                  <input
                    id="option-a"
                    type="radio"
                    name="cookingStyle"
                    value="Batch Cook"
                    checked={batch}
                    onChange={event => this.updateData(event)}
                  />
                  <span className="checkmark" />
                  <b>Batch Cook:</b> Make multiple meals in a larger cook session(60-90 mins), two+ times per week. Cook twice, eat all week.
                </label>
              </div>
              <div className="check-row">
                <label className="rbutton check_font" htmlFor="option-b">
                  <input
                    id="option-b"
                    type="radio"
                    name="cookingStyle"
                    value="Batch & weeknight"
                    checked={weekNight}
                    onChange={event => this.updateData(event)}
                  />
                  <span className="checkmark" />
                  <b>Batch & Weeknight:</b>Batch cook on the weekend for the first half of the week then cook night meals for the last half.
                </label>
              </div>
              <div className="check-row ">
                <label className="rbutton check_font" htmlFor="option-c">
                  <input
                    id="option-c"
                    type="radio"
                    name="cookingStyle"
                    value="Cook fresh"
                    checked={freshCook}
                    onChange={event => this.updateData(event)}
                  />
                  <span className="checkmark" />
                  <b>Cook fresh:</b> Cook quick, fresh dinners each night of the week.
                </label>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="form-row text-right col-md-12">

              <a
                className="next-btn color"
                onClick={() => this.updateMealPlan()}
              >
                {saveText}
              </a>
            </div>
            {confirmMessage !== '' &&
              <div className="colr">
                {confirmMessage}
                <div style={{ display: 'none' }}> {setTimeout(() => this.message(), 3000)} </div>
              </div>
            }
          </div>
        </div>

        <div className="col-xs-12 col-md-6 login-sectn meal_plan">
          <div className="row">
            <div className="col-md-12 form-row select-row-small">
              <label htmlFor="profile-span">Select your preffered diet type:
                <span className="profile-span">
                  We auto-recommend dishes within your preferred diet preferrences.
                  You can swap and select from our full recipe library at any time.
                </span>
              </label>
              <select
                className="selectpicker show-tick form-control dietType select"
                name="dietType"
                onChange={event => this.updateData(event)}
                value={dietType}
              >
                <option value="">Select</option>
                <option value="low_carbs">Low carb</option>
                <option value="balanced">Balanced</option>
                <option value="keto">Keto</option>
                <option value="Paleo">Paleo</option>
                <option value="Vegetarian">Vegetarian</option>
                <option value="Whole30">Whole30</option>
              </select>
            </div>
          </div>
          <div className="row">
            <div className="col-md-12 form-row select-row-small label-row-full meal-view">
              <label htmlFor="protein">Your Protein <u>Exclusion</u> List
                <span className="profile-span">Select any proteins you want <strong><i>excluded</i></strong> from your weekly plan recommendations. We <strong><u>actively</u></strong> utilize this information to help craft your meal plan each week.</span>

              </label>
              <div className="row select-checks-box">
                { data && PROTEIN_LIST.result.map((item) => {
                  let check = false;
                  let value = 4;
                  const text = item.exclusion_types_data;
                  if (text.match(/Mixed.*/)) {
                    value = 8;
                  }
                  if (PROTEIN_EXCLUSION_LIST) {
                    const index = PROTEIN_EXCLUSION_LIST.findIndex(x => x.id === item.id);
                    if (index > -1) {
                      check = true;
                    } else {
                      check = false;
                    }
                  }
                  return (
                    <div className={`col-md-${value}`} key={item.id}>
                      <label className="container1" htmlFor={item.name}>
                        <input
                          type="checkbox"
                          onChange={
                            e => this.updateList(e.target.value)
                          }
                          value={item.id}
                          name="protein"
                          checked={check}
                        />
                        {item.exclusion_types_data} <span className="checkmark1" />
                      </label>
                    </div>
                  );
                })}
              </div>
            </div>
          </div>

        </div>
      </div>

    );
  }
}
export default EditMeal;
