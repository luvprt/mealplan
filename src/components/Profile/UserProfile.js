import React, { Component } from 'react';
import moment from 'moment';

class UserProfile extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: this.props.name,
    };
  }

  // unsafe
  // eslint-disable-next-line
  UNSAFE_componentWillReceiveProps(props) {
    this.setState({
      ...props,
    });
  }
  render() {
    const { name, email } = this.state;
    let { DOB } = this.state;
    if (DOB) {
      DOB = moment(DOB).format('DD / MM / YYYY');
    }
    return (
      <div className="dash-cntnt-dtail-hdr box-margin">
        <h4>Your Meal Plan Ninja Profile</h4>
        <div className="grocery-list-cntnt userProfile">
          <div className="row">
            <div className="col-md-12">
              <div className="top-section">
                <span> Your Information </span>
                <p>
                  This is for informational and billing purposes only.
                  Read our privacy policy if you have any questions!
                  Contact us if you need to change any of this personal info
                </p>
                <div className="form-section fild_space">
                  <label htmlFor="name"> Name: {name} </label>
                  <label htmlFor="email"> Email: {email} </label>
                  <label htmlFor="DOB"> Birthday: {DOB} </label>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default UserProfile;
