import React, { Component } from 'react';
import history from '../../history';

class ProfileBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...this.props,
    };
  }

  // unsafe
  // eslint-disable-next-line
  UNSAFE_componentWillReceiveProps(props) {
    let { profileClass, billingClass, supportClass } = this.state;
    if (props.value === 1) {
      profileClass = 'active';
      billingClass = '';
      supportClass = '';
    } else if (props.value === 2) {
      billingClass = 'active';
      profileClass = '';
      supportClass = '';
    } else if (props.value === 3) {
      supportClass = 'active';
      profileClass = '';
      billingClass = '';
    }
    this.setState({
      supportClass,
      billingClass,
      profileClass,
      ...props,
    });
  }

  // eslint-disable-next-line
  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('reminder');
    history.push('/login');
  }

  // eslint-disable-next-line
  support() {
    const { subscriptionReminder, exist, dayDates } = this.state;
    sessionStorage.setItem('method', 'back');
    history.push('/support', { reminder: subscriptionReminder, exist, dayDates });
  }

  // eslint-disable-next-line
  billingDetail() {
    const { subscriptionReminder, exist, dayDates } = this.state;
    sessionStorage.setItem('method', 'back');
    history.push('/billingDetails', { reminder: subscriptionReminder, exist, dayDates });
  }

  // eslint-disable-next-line
  userProfile() {
    const { subscriptionReminder, exist, dayDates } = this.state;
    sessionStorage.setItem('method', 'back');
    history.push('/profileSettings', { reminder: subscriptionReminder, exist, dayDates });
  }

  render() {
    const { profileClass, supportClass, billingClass } = this.state;
    return (
      <div className="col-lg-3 sidebar">
        <div className="sidebar-cntnt">
          <div className="sidebar-cntnt-bg" />
          <h4>Your Ninja Profile</h4>
          <ul className="profile-lft-menu">
            <li className={`${profileClass}`}>
              <a onClick={() => this.userProfile()}>My Ninja Profile</a>
            </li>
            <li className={`${billingClass}`}>
              <a onClick={() => this.billingDetail()}>Billing Details</a>
            </li>
            <li className={`${supportClass}`}>
              <a onClick={() => this.support()}>Support / Report Bug</a>
            </li>
            <li className="logout-link">
              <a onClick={() => this.logout()}>
                Logout
                <svg
                  xmlns="http://www.w3.org/2000/svg"
                  xmlnsXlink="http://www.w3.org/1999/xlink"
                  width="20"
                  height="20"
                  viewBox="0 0 20 20"
                >
                  <defs>
                    <path id="a" d="M12.716 12.101a.658.658 0 0 0-.658.658v4.07a.658.658 0 0 1-.657.658H2.193a.658.658 0 0 1-.658-.658V3.061c0-.363.295-.658.658-.658H11.4c.362 0 .657.295.657.658v4.07a.658.658 0 1 0 1.316 0v-4.07A1.975 1.975 0 0 0 11.4 1.088H2.193A1.975 1.975 0 0 0 .219 3.06v13.768c0 1.088.885 1.973 1.974 1.973H11.4a1.975 1.975 0 0 0 1.973-1.973v-4.07a.658.658 0 0 0-.658-.658zm7.059-2.161a.677.677 0 0 0-.013-.124c-.004-.022-.012-.042-.019-.064-.006-.019-.01-.039-.018-.058-.01-.027-.025-.05-.04-.075-.006-.013-.012-.026-.02-.038a.663.663 0 0 0-.084-.103l-3.78-3.78a.657.657 0 1 0-.93.93l2.659 2.66H5.7a.658.658 0 1 0 0 1.315h11.83l-2.66 2.66a.657.657 0 1 0 .93.93l3.783-3.783.01-.012a.64.64 0 0 0 .072-.088c.011-.018.02-.037.03-.056.01-.019.022-.037.03-.057.01-.023.015-.047.022-.071.005-.017.012-.034.015-.052a.661.661 0 0 0 .014-.13l-.001-.004z" />
                  </defs>
                  <g fill="none" fillRule="evenodd">
                    <mask id="b" fill="#fff">
                      <use xlinkHref="#a" />
                    </mask>
                    <use fill="#000" fillRule="nonzero" xlinkHref="#a" />
                    <g fill="#4A4A4A" mask="url(#b)">
                      <path d="M0 0h20v20H0z" />
                    </g>
                  </g>
                </svg>

              </a>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}
export default ProfileBar;
