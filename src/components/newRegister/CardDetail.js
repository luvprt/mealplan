import React, { Component } from 'react';
import RoutingConstants from 'lib/RoutingConstants';
import { get } from 'lib/api';
import { Elements, StripeProvider } from 'react-stripe-elements';
import CardForm from './CardForm';


export default class CardDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      input: this.props.input || {},
    };
  }

  componentDidMount() {
    this.loadData();
  }
  
  onSubmit(data) {
    const { input, planId, trialDay } = this.state;
    input.stripe_token = data
    input.plan_id = planId
    input.trial_days = trialDay
    this.setState({
      input
    })
    this.props.onClickNext(input);
  }

  async loadData() {
    const url = RoutingConstants.getSubscriptionPlan;
    let price;
    get(url).then(res => {
      let result = res.result;
      if(res.result && res.result.data) {
        result = res.result.data
      }
      result.map((plan) => {
        if(plan.interval ==="month" && plan.interval_count === 1 && plan.active && plan.trial_period_days !== null) {
          price = (plan.amount) / 100;
          this.setState ({ planId: plan.id, trialDay: plan.trial_period_days, price  });
        }
        return plan;
      });
      
    })
  }
  

  render() {
    const { trialDay, price } = this.state;
    return (
      <div className="main-cntnt-box">
        <h2>Healthy Eating Takes Commitment!</h2>
        <p className="hdng-para more-top-margn">
           In order to start your free trial, enter your credit card details below.
        </p>
        <p className="hdng-para plan-detail">
           Don&apos;t worry you can cancel at any time. 
           We won&apos;t charge you until your {trialDay} day
           trial. After {trialDay} days your plan will renew 
           at the monthly price of ${price} per month 
        </p>
        <h4 className='hdng-billing'> 
          Enter Your Billing Details To Start Your Free Trial 
          <span> (you won&apos;t be charged for {trialDay} days) </span>
        </h4>
        <div className="main-cntnt-inner">
          <div className="col-xs-12 col-md-8 login-sectn new-register"> 
            <div>
              <div className="row">
                <div className="col-md-12 form-row select-row-small mealplan-top-row">
                  <div className="row">
                    <div className="col-md-12 form-row">
                      <StripeProvider apiKey={RoutingConstants.key}>
                        <Elements>
                          <CardForm onSubmit={(data) => this.onSubmit(data)}/>
                        </Elements>
                      </StripeProvider>
                    </div>
                  </div>
                 
                 
                </div>
              </div>
              
            </div>
          </div>
        </div>
      </div>
    );
  }
}
