import React, { Component } from 'react';
import { getGuestData, updateGuestData, removeTag } from 'lib/api';
import moment from 'moment';
import Basic from './Basic';
import CardDetail from './CardDetail';
// eslint-disable-next-line
import FoodPrefrences from './FoodPrefrences';

class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentStep: 'basic',
    };
  }

  // dummy purpose
  componentDidMount() {
    localStorage.setItem('token', '');
  }

  onClickFinish(data) {
    const guestEmail = data;
    const guestData = sessionStorage.getItem('guestAccount') ?
      JSON.parse(sessionStorage.getItem('guestAccount')) : null;
    if (guestData) {
      const inputData = {};
      inputData.email = guestEmail;
      getGuestData(inputData).then((responseJson) => {
        if (responseJson && responseJson.result_code === 1) {
          inputData.id = responseJson.id;
          inputData.tags = '';
          inputData['p[6]'] = 6;
          inputData['field[%PEOPLE_COOKING_FOR%, 0]'] = guestData.mealPlan.PEOPLE_COOKING_FOR;
          inputData['field[%COOKING_STYLE%, 0]'] = guestData.mealPlan.COOKING_STYLE;
          inputData['field[%DIET_TYPE%, 0]'] = guestData.foodPrefrences.DIET_TYPE;
          inputData['field[%FREE_TRIAL_SIGN_UP_DATE%, 0]'] = moment();
          updateGuestData(inputData).then(() => {
            const inputs = {};
            inputs.email = guestEmail;
            inputs.tags = 'Incomplete Trial Sign Up';
            removeTag(inputs).then((responseJson2) => {
              if (responseJson2 && responseJson2.result_code === 1);
            });
          });
          if (guestEmail && guestEmail != null) {
            this.props.history.push('/dashboard', { guestEmail, exist: 'no' });
          } else {
            this.props.history.push('/login');
          }
        }
      }).catch(() => {
      });
    }
  }

  onClickNext(currentState, nextState, data) {
    
    this.setState({ currentStep: nextState, input: data });
  }

  renderContent() {
    switch (this.state.currentStep) {
    case 'basic':
      return (
        <Basic onClickNext={data => this.onClickNext('basic', 'cardForm', data)} {...this.props} />
      );

    case 'cardForm':
      return (
        <CardDetail
          onClickNext={data => this.onClickNext('cardForm', 'foodPrefrences', data)}
          {...this.state}
        />
      );

    case 'foodPrefrences':
      return (
        <FoodPrefrences
          onClickNext={data => this.onClickNext('foodPrefrences', 'cardForm', data)}
          {...this.state}
        />
      );
    default:
    }
    return this.state.currentStep;
  }
  render() {
    const { history } = this.props;

    return (
      <div className="wrapper transition-item detail-page">
        <header>
          <div className="container">
            <img src="/images/logo.png" alt="" onClick={() => { history.push('/'); }} />
          </div>
        </header>
        <section className="main-cntnt">
          <div className="container">
            {this.renderContent()}
          </div>
        </section>
        <section className="copyright">
          <div className="container">
            <p>© Copyright - Meal Plan Ninja, Inc. 2018</p>
          </div>
        </section>
      </div>
    );
  }
}

export default Register;
