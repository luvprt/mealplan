import React, { Component } from 'react';
import {  CardNumberElement,  CardExpiryElement,
  CardCVCElement, PostalCodeElement,  injectStripe } from 'react-stripe-elements';


class CardForm extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cnumber:'',
      cvc: '',
      expiry: '',
      zip: '',
      stateClass: '',
      errMessage: '',
    };
  }

  

  onPressNext() {
    let name;
    let { errMessage } = this.state;
    this.setState({isLoading : true})
    this.props.stripe.createToken({
      number: document.getElementById('card-number').value,
      cvc: document.getElementById('cvc').value,
      expiry: document.getElementById('expiration').value,
    }).then((response) => {
      if(response.token){
        this.props.onSubmit(response.token.id);
      }
      if(response.error) {
        if(response.error.code === "card_declined") {
          errMessage = "Please Enter Valid Card Number";
        }
        this.setState({isLoading : false, errMessage})
        if(this.state.cardNumber !== 'done') {
          name = 'cnumber'
        } 
        if (this.state.cvcCode !== 'done') { 
          name = 'cvc'
        }
        if (this.state.cExpire !== 'done') { 
          name = 'expiry'
        }
        if (this.state.zipCode !== 'done') {
          name = 'zip'
        }
        if (this.state.state !== 'done') {
          name = 'stateClass'
        } 
        this.setState({ [name] : 'error'});
      }
    })
  }

  focusClass(event, name) {
    this.setState({
      [name]:'cnumber'
    }) 
  }

  blurClass(event, name ) {
    if((name === 'cnumber' && this.state.cardNumber !== 'done') ||
    (name === 'cvc' && this.state.cvcCode !== 'done') ||
    (name === 'expiry' && this.state.cExpire !== 'done') ||
    (name === 'zip' && this.state.zipCode !== 'done') ||
    (name === 'stateClass' && this.state.state !== 'done') 
    ) {
      this.setState( {
        [name]: ''
      })
    } else {
      this.setState( {
        [name]: 'cnumber'
      })
    }
  }
  
  afterComplete(event, name) {
    let value;
    let { errMessage } = this.state;
    if ((name === 'cardNumber'  || name === 'cExpire'  || name === 'cvcCode' || name === 'zipCode') && !event.empty) {
      value = 'done'
      errMessage = false
    } else if(name !=='state') {
      value = ''
    } else {
      value = 'done'
    }
    this.setState({ [name]: value, errMessage})
  }

  render() {
    const { cnumber, expiry, cvc, stateClass, zip, errMessage } = this.state;
    return (
      <div>
        <div className="row">
          <div className="col-md-12">
            {errMessage ? <span className="error-msg new-form">{errMessage}</span> : null}
            <div className="card-numbr-box">
              <label htmlFor="card-number">
                <span className={`static-label ${cnumber}`}>
                  <span> Card Number</span>
                </span>
                <CardNumberElement id='card-number'
                  placeholder=""
                  onFocus={(event) => this.focusClass(event, 'cnumber')}
                  onBlur={(event) => this.blurClass(event, 'cnumber')}
                  onChange={(event) => this.afterComplete(event, 'cardNumber')}
                />  
              
              </label>
            </div>   
          </div>      
        </div>  
       
        <div className="row">
          <div className="col-md-6">
            <div className="card-numbr-box">
              <label>
                <span className={`static-label ${expiry}`}>
                  <span> Expiration</span>
                </span>
                <CardExpiryElement
                  id="expiration"
                  placeholder=""
                  onFocus={(event) => this.focusClass(event, 'expiry')}
                  onBlur={(event) => this.blurClass(event, 'expiry')}
                  onChange={(event) => this.afterComplete(event, 'cExpire')}
                />  
              </label>
            </div>
          </div>
          <div className="col-md-6">
            <div className="card-numbr-box">
              <label>
                <span className={`static-label ${cvc}`}>
                  <span> CVC</span>
                </span>
                <CardCVCElement
                  id='cvc'
                  placeholder=""
                  onFocus={(event) => this.focusClass(event, 'cvc')}
                  onBlur={(event) => this.blurClass(event, 'cvc')}
                  onChange={(event) => this.afterComplete(event, 'cvcCode')}
                />                 
              </label>
            </div> 
          </div>
        </div>

      



        <div className="row">
                    
        </div>
        <div className="row">
          <div className="col-md-6">
            <div className="card-numbr-box">
              <label>
                <span className={`static-label ${stateClass}`}>
                  <span>State</span>
                </span>
                <input
                  type="text" 
                  onFocus={(event) => this.focusClass(event, 'stateClass')}
                  onBlur={(event) => this.blurClass(event, 'stateClass')}
                  onChange={(event) => this.afterComplete(event, 'state')}
                />
              </label>
            </div>         
          </div>         
          <div className="col-md-6">
            <div className="card-numbr-box">
              <label>
                <span className={`static-label ${zip}`}>
                  <span> Zip</span>
                </span>
                <PostalCodeElement
                  label="Zip"
                  placeholder=""
                  onFocus={(event) => this.focusClass(event, 'zip')}
                  onBlur={(event) => this.blurClass(event, 'zip')}
                  onChange={(event) => this.afterComplete(event, 'zipCode')}
                />
              </label>
            </div>  
          </div>  
              
        </div>
        <div className="row">
          <div className="form-row text-center col-md-12">
            {
              this.state.isLoading ?
                <button className="next-btn new-register-btn">Loading...</button>
                :
                <button className="next-btn new-register-btn" onClick={() => this.onPressNext()}>Next</button>
            }
          </div>
        </div>
                  
      </div>
    );
  }
}

export default injectStripe(CardForm)