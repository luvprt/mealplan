import React, { Component } from 'react';
import RoutingConstants from 'lib/RoutingConstants';
import moment from 'moment';
import { post, get, getGuestData, updateGuestData, removeTag } from 'lib/api';
import history from '../../history';

export default class FoodPrefrences extends Component {
  constructor(props) {
    super(props);
    this.state = {
      PROTEIN_EXCLUSION_LIST: [],
      PROTEIN_LIST: [],
      errorMessage: {},
      isLoading: false,
      input: this.props.input || {},
    };
  }

  componentDidMount() {
    this.loadData();
  }


  onClickNext() {
    const { PROTEIN_EXCLUSION_LIST, input, errorMessage } = this.state;
    input.userExclusionsIds = PROTEIN_EXCLUSION_LIST;
    const url = RoutingConstants.newRegister;
    this.setState({ isLoading: true});
    if (input.best_describe_you === '') {
      errorMessage.best_describe_you = "Please select your choice"
    } else {
      errorMessage.best_describe_you = false
    }
    if (input.feel_about_meal_plan === '') {
      errorMessage.feel_about_meal_plan = "Please select your choice"
    } else {
      errorMessage.feel_about_meal_plan = false
    }
    if (input.why_we_use === '') {
      errorMessage.why_we_use = "Please select your choice"
    } else {
      errorMessage.why_we_use = false;
    }
    if (input.COOKING_STYLE === '') {
      errorMessage.COOKING_STYLE = "Please select your choice"
    } else {
      errorMessage.COOKING_STYLE = false;
    }
    if(!errorMessage.best_describe_you &&
       !errorMessage.feel_about_meal_plan &&
       !errorMessage.why_we_use &&
       !errorMessage.COOKING_STYLE
    ) {
      post(url, input).then( responseJson => {
        if (responseJson.result) {
          localStorage.removeItem('token');
          localStorage.setItem('token', responseJson.result.token);
          localStorage.setItem('firstName', responseJson.result.first_name);
          localStorage.setItem('lastName', responseJson.result.last_name);
          localStorage.setItem('email', responseJson.result.email);
          localStorage.removeItem('type');
          localStorage.removeItem('isTourCompleted');
          window.analytics.identify(input.email, {
            first_name: input.first_name,
            last_name: input.last_name,
            email: input.email,
          });

          getGuestData(input).then((response) => {
            if (response && response.result_code === 1) {
              input.id = response.id;
              input.tags = '';
              input['p[6]'] = 6;
              input['field[%PEOPLE_COOKING_FOR%, 0]'] = input.PEOPLE_COOKING_FOR;
              input['field[%COOKING_STYLE%, 0]'] = input.COOKING_STYLE;
              input['field[%FREE_TRIAL_SIGN_UP_DATE%, 0]'] = moment();
              updateGuestData(input).then(() => {
                const inputs = {};
                inputs.email = input.email;
                inputs.tags = 'Incomplete Trial Sign Up';
                removeTag(inputs).then((responseJson2) => {
                  if (responseJson2 && responseJson2.result_code === 1);
                });
              });
            }
          });
          history.push('/dashboard', { guestEmail: responseJson.result.email, exist: 'no' });
        }
      }).catch(() => {
        this.setState({ isLoading: false})
      })
      // track food preferences
      window.analytics.track('Completes Step 3: Food Preferences', {
        date: moment().format('dddd DD-MM-YYYY'),
        no_of_proteins_excluded: PROTEIN_EXCLUSION_LIST.length,
      });
    } else {
      this.setState({ isLoading: false })
    }

  }

  updateValue(event) {
    const { input, errorMessage } = this.state;
    input[event.target.name] = event.target.value;
    errorMessage[event.target.name] = false;
    this.setState ({ input, errorMessage })
  }

  loadData() {
    const url = RoutingConstants.fetchProteinExclusion;
    const { input } = this.state;
    input.best_describe_you =  ""
    input.feel_about_meal_plan = ""
    input.why_we_use = ""
    input.COOKING_STYLE = ""
    get(url).then((response) => {
      this.setState({ PROTEIN_LIST: response });
    }).catch(() => {
      // console.log('protein exclusion error',err)
    });
  }

  updateList(ref, value) {
    let isValueExists = false;
    const { PROTEIN_EXCLUSION_LIST } = this.state;
    if (PROTEIN_EXCLUSION_LIST) {
      isValueExists = PROTEIN_EXCLUSION_LIST.includes(value);
      if (isValueExists) {
        const index = PROTEIN_EXCLUSION_LIST.indexOf(value);
        if (index > -1) {
          PROTEIN_EXCLUSION_LIST.splice(index, 1);
          this.setState({ PROTEIN_EXCLUSION_LIST });
        }
      } else {
        PROTEIN_EXCLUSION_LIST.push(value);
        this.setState({ PROTEIN_EXCLUSION_LIST });
      }
    }
  }

  render() {
    const { PROTEIN_LIST, errorMessage } = this.state;

    let data = false;
    if (PROTEIN_LIST.result) {
      data = true;
    }
    return (
      <div className="main-cntnt-box">
        <h2>One Last Step!</h2>
        <p className="hdng-para more-top-margn">
           Let{`'`}s get a head start on your meal planning preferences.
        </p>
        <div className="main-cntnt-inner">
          <div className="col-md-12 text-center">
            <div className="login-sectn new-register text-left">
              <div>
                <h4>Your Meal Plan Preferences</h4>
                <div className="row">
                  <div className="col-md-12 form-row select-row-small label-row-full">
                    <label htmlFor="taste">Protein <u>Exclusion</u> List - (things you DO NOT eat):
                      <span>
                      Select the proteins
                      you want <strong> excluded </strong> from your weekly plan recommendations.You can always change these preferences later;
                      </span>
                    </label>
                    <div className="row select-checks-box">
                      { data && PROTEIN_LIST.result.map((item) => {
                        const text = item.exclusion_types_data;
                        let value = 4;
                        if (text.match(/Mixed.*/) || text.match(/mixed.*/)) {
                          value = 8;
                        }
                        return (
                          <div className={`col-md-${value}`} key={item.id}>
                            <label className="container1" htmlFor={item.id}>
                              <input
                                type="checkbox"
                                id={item.id}
                                onChange={e => this.updateList('PROTEIN_EXCLUSION_LIST', e.target.value)}
                                value={item.id}
                              />
                              { item.exclusion_types_data !== '' &&
                                 item.exclusion_types_data
                              }
                              <span className="checkmark1" />
                            </label>
                          </div>
                        );
                      })
                      }
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-12 form-row">
                    <p
                      className={errorMessage.best_describe_you ?
                        'inner-check-row-hdng error-label' : 'nner-check-row-hdng'}
                    >
                  Which of the following best describes you:
                    </p>
                    {errorMessage.best_describe_you ? <span className="error-msg new-form">{errorMessage.best_describe_you}</span> : null}
                    <div className="check-row">
                      <label className="rbutton" htmlFor="try">
                        <input
                          type="radio"
                          name="best_describe_you"
                          id="try"

                          value="I'm trying meal planning for the first time"
                          onClick={(event) => this.updateValue(event)}
                        />
                        <span className="checkmark" />
                      I&apos;m trying meal planning for the first time
                      </label>
                    </div>

                    <div className="check-row">
                      <label className="rbutton" htmlFor="weeks">
                        <input
                          type="radio"
                          name="best_describe_you"
                          id="weeks"
                          value="I actively meal plan at least 2 weeks per month"
                          onClick={(event) => this.updateValue(event)}
                        />
                        <span className="checkmark" />
                      I actively meal plan at least 2 weeks per month
                      </label>
                    </div>
                    <div className="check-row">
                      <label className="rbutton" htmlFor="once">
                        <input
                          type="radio"
                          name="best_describe_you"
                          id="once"
                          value=" I meal plan every once in a while"
                          onClick={(event) => this.updateValue(event)}
                        />
                        <span className="checkmark" />
                      I meal plan every once in a while
                      </label>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-12 form-row">
                    <p
                      className={errorMessage.COOKING_STYLE ?
                        'inner-check-row-hdng error-label' : 'nner-check-row-hdng'}
                    >
                    What is your preferred cooking style?
                    </p>
                    {errorMessage.COOKING_STYLE ? <span className="error-msg new-form">{errorMessage.COOKING_STYLE}</span> : null}
                    <div className="check-row">
                      <label className="rbutton" htmlFor="Batch">
                        <input type="radio" name="COOKING_STYLE" id="Batch" onChange={(event) => this.updateValue(event)} value="Batch Cook" />
                        <span className="checkmark" />
                        <b>Batch Cook:</b> Make multiple meals in a larger cook session (60-90 mins), two+ times per week. Cook twice, eat all week.
                      </label>
                    </div>

                    <div className="check-row">
                      <label className="rbutton" htmlFor="Weeknight">
                        <input type="radio" name="COOKING_STYLE" id="Weeknight" onChange={(event) => this.updateValue(event)} value="Batch & weeknight" />
                        <span className="checkmark" />
                        <b>Batch & Weeknight:</b> Batch cook on the weekend for the first half of the week then cook nightly meals for the last half.
                      </label>
                    </div>
                    <div className="check-row">
                      <label className="rbutton" htmlFor="fresh">
                        <input type="radio" name="COOKING_STYLE" id="fresh" onChange={(event) => this.updateValue(event)} value="Cook fresh" />
                        <span className="checkmark" />
                        <b>Cook fresh:</b> Cook quick, fresh dinners each night of the week.
                      </label>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-12 form-row">
                    <p
                      className={errorMessage.feel_about_meal_plan ?
                        'inner-check-row-hdng error-label' : 'nner-check-row-hdng'}
                    >
                    How do you feel about meal planning each week?
                    </p>
                    {errorMessage.feel_about_meal_plan ? <span className="error-msg new-form">{errorMessage.feel_about_meal_plan}</span> : null}
                    <div className="check-row">
                      <label className="rbutton" htmlFor="difficult">
                        <input
                          type="radio"
                          name="feel_about_meal_plan"
                          id="difficult"

                          onChange={(event) => this.updateValue(event)}
                          value="It's difficult (I really don't look forward to it)"
                        />
                        <span className="checkmark" />
                      1- It&apos;s difficult (I really don&apos;t look forward to it)
                      </label>
                    </div>

                    <div className="check-row">
                      <label className="rbutton" htmlFor="inconvenient">
                        <input
                          type="radio"
                          name="feel_about_meal_plan"
                          id="inconvenient"
                          onChange={(event) => this.updateValue(event)}
                          value="It's inconvenient (I don't always have time or want to)"
                        />
                        <span className="checkmark" />
                      2- It&apos;s inconvenient (I don&apos;t always have time or want to)
                      </label>
                    </div>
                    <div className="check-row">
                      <label className="rbutton" htmlFor="Neutral">
                        <input
                          type="radio"
                          name="feel_about_meal_plan"
                          id="Neutral"
                          onChange={(event) => this.updateValue(event)}
                          value="Neutral (It's ok...)"
                        />
                        <span className="checkmark" />
                      3- Neutral (It&apos;s ok...)
                      </label>
                    </div>
                    <div className="check-row">
                      <label className="rbutton" htmlFor="good">
                        <input
                          type="radio"
                          name="feel_about_meal_plan"
                          id="good"
                          onChange={(event) => this.updateValue(event)}
                          value="Good (It's not too much of a burden / It's easy!)"
                        />
                        <span className="checkmark" />
                      4- Good (It&apos;s not too much of a burden / It&apos;s easy!)
                      </label>
                    </div>

                    <div className="check-row">
                      <label className="rbutton" htmlFor="easy">
                        <input
                          type="radio"
                          name="feel_about_meal_plan"
                          id="easy"
                          onChange={(event) => this.updateValue(event)}
                          value="It's easy! (I kind of look forward to it!)"
                        />
                        <span className="checkmark" />
                      5- It&apos;s easy! (I kind of look forward to it!)
                      </label>
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="col-md-12 form-row">
                    <p
                      className={errorMessage.why_we_use ?
                        'inner-check-row-hdng error-label' : 'nner-check-row-hdng'}
                    >
                    What do you think is the most difficult part about your meal planning ?
                    Basically, why use Meal Plan Ninja ?
                    </p>
                    {errorMessage.why_we_use ? <span className="error-msg new-form">{errorMessage.why_we_use}</span> : null}
                    <span className="inner-hint"> Pick one: Select the item that is most painful for you</span>
                    <div className="check-row">
                      <label className="rbutton" htmlFor="Reseaching">
                        <input
                          type="radio"
                          name="why_we_use"
                          id="Reseaching"

                          onChange={(event) => this.updateValue(event)}
                          value="Reseaching new recipes each week"
                        />
                        <span className="checkmark" />
                      Reseaching new recipes each week
                      </label>
                    </div>

                    <div className="check-row">
                      <label className="rbutton" htmlFor="planning">
                        <input
                          type="radio"
                          name="why_we_use"
                          id="planning"
                          onChange={(event) => this.updateValue(event)}
                          value="Actually selecting / planning out the recipes that i want for the week"
                        />
                        <span className="checkmark" />
                      Actually selecting / planning out the recipes that i want for the week
                      </label>
                    </div>
                    <div className="check-row">
                      <label className="rbutton" htmlFor="Creating">
                        <input
                          type="radio"
                          name="why_we_use"
                          id="Creating"
                          onChange={(event) => this.updateValue(event)}
                          value="Creating a grocery list that calculates the ingredient totals
                        for all my meals for the week."
                        />
                        <span className="checkmark" />
                      Creating a grocery list that calculates the ingredient totals
                      for all my meals for the week.
                      </label>
                    </div>
                    <div className="check-row">
                      <label className="rbutton" htmlFor="selecting">
                        <input
                          type="radio"
                          name="why_we_use"
                          id="selecting"
                          onChange={(event) => this.updateValue(event)}
                          value="Selecting the meals that go well together for meal prep / batch cooking
                        so I don't have to spend hours cooking in the kitchen"
                        />
                        <span className="checkmark" />
                      Selecting the meals that go well together for meal prep / batch cooking
                      so I don&apos;t have to spend hours cooking in the kitchen
                      </label>
                    </div>

                    <div className="check-row">
                      <label className="rbutton" htmlFor="poke">
                        <input
                          type="radio"
                          name="why_we_use"
                          id="poke"
                          onChange={(event) => this.updateValue(event)}
                          value="Not Applicable (I'm just poking around)"
                        />
                        <span className="checkmark" />
                      Not Applicable (I&apos;m just poking around)
                      </label>
                    </div>
                    <div className="check-row">
                      <label className="rbutton" htmlFor="other">
                        <input
                          type="radio"
                          name="why_we_use"
                          id="other"
                          onChange={(event) => this.updateValue(event)}
                          value="other"
                        />
                        <span className="checkmark" />
                      Other (please specify below)
                      </label>
                    </div>
                    <div className="check-row">

                      <textarea
                        className='form-control area new-comment'
                        id="exampleFormControlTextarea1"
                        rows="3"
                        name="why_we_use"
                        onChange={event => this.updateValue(event)}
                      />
                    </div>
                  </div>
                </div>
                <div className="row">
                  <div className="form-row text-center col-md-12">
                    {
                      this.state.isLoading ?
                        <button className="next-btn new-register-btn">Loading...</button>
                        :
                        <button className="next-btn new-register-btn" onClick={() => this.onClickNext()}>Done! Build Your First Plan!</button>
                    }
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
