/*
Component Name: DashHeader component
Default screen: Dashboard
*/

import React, { Component } from 'react';
// import { PropTypes } from 'prop-types';
import history from '../../history';


class AdminDashHeader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      /* for switch cases for grocery and cook mode as well */
      name: '',
      ...this.props,
      check: true,
    };
  }
  componentDidMount() {
    this.adminData();
  }

  // unsafe
  // eslint-disable-next-line
  UNSAFE_componentWillReceiveProps(props) {
    this.setState({
      supportClass: props.supportClass || null,
      ...props,
    });
  }

  setIcon() {
    const { check } = this.state;
    this.setState({ check: !check });
  }

  adminData() {
    const firstName = localStorage.getItem('firstName');
    const lastName = localStorage.getItem('lastName');
    const name = `${firstName} ${lastName}`;
    this.setState({ name });
  }

  // eslint-disable-next-line
  logOut() {
    localStorage.removeItem('token');
    localStorage.removeItem('role');
    localStorage.removeItem('lastName')
    localStorage.removeItem('firstName')
    history.push('/checkUser');
  }

  updateSearch(event) {
    if (event.target.value.length > 2) {
      this.props.searchValue(event.target.value);
    }
  }

  render() {
    const { check, supportClass } = this.state;
    return (
      <header>
        <div className="container">
          <nav className="navbar navbar-default navbar-tall navbar-full">
            <div className="row">
              <div className="navbar-header col-md-3">
                <button type="button" className="navbar-toggle collapsed" data-toggle="collapse" data-target="#global-nav" aria-expanded="false">
                  <span className="sr-only">Toggle navigation</span>
                  <span className="icon-bar" />
                  <span className="icon-bar" />
                  <span className="icon-bar" />
                </button>
                <a className="navbar-brand ">
                  <img src="/images/logo.png" alt="" />
                </a>
              </div>
              <div className="navbar-collapse collapse" id="global-nav" aria-expanded="false">
                <div className="navbar-main col-md-9 nav-dash">
                  <ul className="nav navbar-nav nav-centr">
                    <li />
                    <li />
                    <li className={`${supportClass} hdr-search`}>
                      <input
                        className="form-control"
                        type="search"
                        placeholder="Search the site..."
                        aria-label="Search through site content"
                        onChange={event => this.updateSearch(event)}
                      />
                      <svg aria-hidden="true" data-prefix="fas" data-icon="search" className="svg-inline--fa fa-search fa-w-16" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path fill="currentColor" d="M505 442.7L405.3 343c-4.5-4.5-10.6-7-17-7H372c27.6-35.3 44-79.7 44-128C416 93.1 322.9 0 208 0S0 93.1 0 208s93.1 208 208 208c48.3 0 92.7-16.4 128-44v16.3c0 6.4 2.5 12.5 7 17l99.7 99.7c9.4 9.4 24.6 9.4 33.9 0l28.3-28.3c9.4-9.4 9.4-24.6.1-34zM208 336c-70.7 0-128-57.2-128-128 0-70.7 57.2-128 128-128 70.7 0 128 57.2 128 128 0 70.7-57.2 128-128 128z" /></svg>
                    </li>
                    <li className="dropdown " onClick={() => this.setIcon()}>
                      <a className="dropdown-toggle" data-toggle="dropdown">
                        <span className="top-usr-drpdwn"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512"><path d="M256 0c88.366 0 160 71.634 160 160s-71.634 160-160 160S96 248.366 96 160 167.634 0 256 0zm183.283 333.821l-71.313-17.828c-74.923 53.89-165.738 41.864-223.94 0l-71.313 17.828C29.981 344.505 0 382.903 0 426.955V464c0 26.51 21.49 48 48 48h416c26.51 0 48-21.49 48-48v-37.045c0-44.052-29.981-82.45-72.717-93.134z" /></svg></span>&nbsp;
                        <strong>{this.state.name}</strong>
                        {check ?
                          <svg
                            className="svIcon"
                            enableBackground="new 0 0 48 48"
                            height="10px"
                            id="Layer_3"
                            version="1.1"
                            viewBox="0 0 48 48"
                            width="10px"
                            xmlSpace="preserve"
                            xmlns="http://www.w3.org/2000/svg"
                            xmlnsXlink="http://www.w3.org/1999/xlink"
                          >
                            <polygon fill="#241F20" points="0,12.438 48,12.438 24,35.562 " />
                          </svg>
                          :
                          <svg
                            className="svIcon"
                            enableBackground="new 0 0 32 32"
                            height="13px"
                            id="svg2"
                            version="1.1"
                            viewBox="0 0 32 32"
                            width="13px"
                            xmlSpace="preserve"
                            xmlns="http://www.w3.org/2000/svg"
                          >
                            <g id="backbord"><rect fill="none" height="32" width="32" /></g>
                            <g id="arrow_x5F_up">
                              <polygon points="30,22 16.001,8 2.001,22  " />
                            </g>
                          </svg>
                        }

                      </a>
                      <div className="dropdown-menu">
                        <a className="dropdown-item gray1" onClick={() => this.logOut()}>
                          Logout
                        </a>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </nav>
        </div>
      </header>
    );
  }
}

export default AdminDashHeader;


// DashHeader.propTypes = {
//   selectedPage: PropTypes.object,
// };
