import React, { Component } from 'react';
import history from '../../history';

class AdminBar extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...this.props,
    };
  }

  // unsafe
  // eslint-disable-next-line
  UNSAFE_componentWillReceiveProps(props) {
    let { profileClass, billingClass, supportClass } = this.state;
    if (props.value === 1) {
      profileClass = 'active';
      billingClass = '';
      supportClass = '';
    } else if (props.value === 2) {
      billingClass = 'active';
      profileClass = '';
      supportClass = '';
    } else if (props.value === 3) {
      supportClass = 'active';
      profileClass = '';
      billingClass = '';
    }
    this.setState({
      supportClass,
      billingClass,
      profileClass,
      ...props,
    });
  }

  // eslint-disable-next-line
  logout() {
    localStorage.removeItem('token');
    localStorage.removeItem('role');
    history.push('/checkUser');
  }

  render() {
    return (
      <div className="col-lg-3 sidebar">
        <div className="sidebar-cntnt">
          <div className="sidebar-cntnt-bg" />
          <h4>Dashboard</h4>
          <ul className="profile-lft-menu">
            <li>
              no data here
            </li>
          </ul>
        </div>
      </div>
    );
  }
}
export default AdminBar;
