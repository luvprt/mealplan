import React, { Component } from 'react';
import Loader from 'lib/loader';
import AdminDashHeader from './AdminDashHeader';
import AdminBar from './AdminBar';
import UserList from './UserList';
import history from '../../history';

class AdminPage extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loadData: false,
      email: localStorage.email,
    };
  }
  componentDidMount() {
    if(localStorage.type !== 'checkUser') {
      localStorage.removeItem('token');
      history.push('/login');
    }
    else if (localStorage.getItem('role') === 'MPN' || !localStorage.getItem('token')) {
      localStorage.removeItem('token');
      localStorage.removeItem('role');
      history.push('/checkUser');
    }
  }

  searchUser(searchEmail) {
    this.setState({ searchEmail });
  }

  render() {
    const { loadData, dashClass, supportClass } = this.state;
    return (
      <div className="wrapper dash-cntnt">
        <AdminDashHeader
          dashClass={dashClass}
          supportClass={supportClass}
          searchValue={value => this.searchUser(value)}
        />
        {loadData &&
          <Loader />
        }
        <section className="dashboard-main swap-page" style={{ padding: '60px 0 0 0'}}>
          <div className="container">
            <AdminBar />
            <div className="col-lg-9 dash-main-cntnt">
              <div className="dash-cntnt-dtail swap-mode-dtail">
                <div className="dash-cntnt-dtail-hdr" />
                <UserList {... this.state} />
              </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
export default AdminPage;
