/*
Component Name: userList component
Default screen: Dashboard
*/


import React, { Component } from 'react';
import RoutingConstants from 'lib/RoutingConstants';
import { get, post } from 'lib/api';
import InfiniteScroll from 'react-infinite-scroller';
import history from '../../history';

class UserList extends Component {
  constructor(props) {
    super(props);
    this.state = {
      ...this.props,
      userList: [],
      offset: 0,
      counter: 0,
      hasMore: true,
      isRecord: true,
      inputs: {},
      searchEmail: ''
    };
  }

  componentDidMount() {
    this.userList(0, '');
  }

  // eslint-disable-next-line
  UNSAFE_componentWillReceiveProps(props) {
    
    if(props.searchEmail !== undefined ) {
      this.setState({ searchEmail: props.searchEmail})
      this.userList(0, props.searchEmail);
    }
  }
  userList(offsets, email) {
    let { offset, userList } = this.state;
    offset = offsets;
    const url = `${RoutingConstants.userList}offset=${offset}&limit=20&email=${email}`;
    let hasMore = true;
    let { counter, isRecord } = this.state;
    if (offset === 0) {
      isRecord = true;
      counter = 0;
      userList = [];
      hasMore = true;
    } else {
      isRecord = true;
      counter += 20;
    }

    if (isRecord === true && offset <= counter) {
      get(url).then((response) => {
        if (response.is_records_exist === 1) {
          hasMore = true;
          isRecord = true;
        } else if ((response.is_records_exist === 0)) {
          isRecord = false;
          hasMore = false;
        }
        if (response.result) {
          response.result.map((item) => {
            userList.push(item);
            return item;
          });
          this.setState({
            userList,
            offset: response.next_offset,
            hasMore,
            isRecord,
            counter,
          });
        }
      }).catch((err) => {
        if (err.error === 'token_expired') {
          localStorage.removeItem('token');
          history.push('/checkUser');
        }
      });
    }
    return this.state.userList;
  }

  userData() {
    const { userList, hasMore, offset, searchEmail } = this.state;
    let listCheck = false;
    if (userList && userList.length > 0) {
      listCheck = true;
    }

    return (
      <div className="admin-user-list">
        <div className="row">
          <div className="col-md-6 admin-user-list-hdng text-left">
            <h4>Email</h4>
          </div>
          <div className="col-md-6 admin-user-list-hdng text-right">
            <h4>Name</h4>
          </div>
        </div>
        <div className="admin-user-list-cntnt">
          <ul>
            { listCheck &&
            <InfiniteScroll
              loadMore={() => this.userList(offset, searchEmail)}
              hasMore={hasMore || false}
              threshold={500}
              useWindow
              useCapture={false}
            >
              {
                userList.map((items) => (
                  <li
                    htmlFor={items.email}
                    className="bordr-top-hdng"
                    onClick={() => this.userLogin(items.email)}
                    key={items.email}
                  >
                    <button style={{ width: '100%' }}>
                      <div className="col-md-6 text-left">
                        {items.email}
                      </div>
                      <div className="col-md-6 text-right">
                        {`${items.first_name} ${items.last_name!== null ? items.last_name : ''}`}
                      </div>
                    </button>
                  </li>
                ))
              }
            </InfiniteScroll>
            }
          </ul>
        </div>
      </div>
    );
  }

  userLogin(email) {
    const { inputs } = this.state;
    inputs.email = email;
    inputs.admin_token = localStorage.getItem('token');
    const url = RoutingConstants.login;
    post(url, inputs).then((response) => {
      localStorage.removeItem('lastName')
      if (response.result) {
        localStorage.setItem('token', response.result.token);
        localStorage.setItem('firstName', response.result.first_name);
        if(response.result.last_name) {
          localStorage.setItem('lastName', response.result.last_name);
        }
        localStorage.setItem('email', email);
        localStorage.setItem('test', '');
        localStorage.setItem('role', 'MPN');
        localStorage.setItem('type','logins');
        window.analytics.identify(email, {
          first_name: response.first_name,
          last_name: response.last_name,
          email,
        });
        const exist = response.result.is_week_exist;
        history.push('/dashboard', {
          guestEmail: response.result.email, type: '/login', reminder: response.result.remainder, exist,
        });
      }
    });
  }


  render() {
    // eslint-disable-next-line
    const groceryText = 'Here\'s Your User List' 
    return (
      <div className="swap-mode-dtail-cntnt">
        <div>
          <h4>{ groceryText }</h4>
        </div>
        <div className="grocery-list-cntnt" id="grocery-inner-list">
          {this.userData()}
        </div>
      </div>
    );
  }
}
export default UserList;
