import React from 'react'
import moment from 'moment'
import { sortByKey } from 'lib/services'

export const SelectRecipeVariety = props => {
  const { inputs } = props
  return (
    <div className="variety-options">
      <div className="variety-indicatn">
        <span className="left">Max Variety</span>
        <span className="right">Less Variety</span>
      </div>
      <div className="input-dash-bordr" />
      <div className="row">
        <div className="variety-inner-optn col-md-3 col-sm-12">
          <div className="variety-custm-check">
            <input 
              type="radio" 
              name="variety-radio" 
              onChange={()=>props.updateValue('No Leftovers', 'Only fresh-cooked meals',1)}
              checked={inputs && inputs.recipeVariety === 'No Leftovers'}
            />
            <span />
          </div>
          <p>No Leftovers</p>
          <span className="light">Only fresh-cooked meals</span>
        </div>
        <div className="variety-inner-optn col-md-3 col-sm-12">
          <div className="variety-custm-check">
            <input 
              type="radio" 
              name="variety-radio" 
              onChange={()=>props.updateValue('Standard', '1 leftover per recipe',2)} 
              checked={inputs && inputs.recipeVariety === 'Standard'}
            />
            <span />
          </div>
          <p>Standard</p>
          <span className="light">About 1 leftover per recipe.</span>
        </div>
        <div className="variety-inner-optn col-md-3 col-sm-12">
          <div className="variety-custm-check">
            <input 
              type="radio" 
              name="variety-radio" 
              onChange={()=>props.updateValue('Extra Leftovers', 'Eat each recipe a few times a week',3)} 
              checked={inputs && inputs.recipeVariety === 'Extra Leftovers'}
            />
            <span />
          </div>
          <p>Extra Leftovers</p>
          <span className="light">Eat each recipe a few times a week.</span>
        </div>
        <div className="variety-inner-optn col-md-3 col-sm-12">
          <div className="variety-custm-check">
            <input 
              type="radio" 
              name="variety-radio" 
              onChange={()=>props.updateValue('Big Batch', 'Few recipes, more repeats',4)} 
              checked={inputs && inputs.recipeVariety === 'Big Batch'}
            />
            <span />
          </div>
          <p>Big Batch</p>
          <span className="light">Few recipes, more repeats.</span>
        </div>
      </div>
    </div>
  )
}

export const RecipeDetail = props => {
  const { totalLength, servingSize, inputs, variety, varietyIndex } = props
  const result = totalLength / varietyIndex
  let { cookingDates } = props
  cookingDates = cookingDates.sort(sortByKey('cooking_date','desc'))
  // count recipe range according to variety
  let lb = Math.floor(result);
  let ub = Math.ceil(result);
  if(lb === 0) {
    lb += 1
  }
  if(varietyIndex === 1) {
    lb = Math.round(result/2)
  }
  if(lb === ub) {
    ub += 1
  }
  return(
    <div className="recommendatn-cntnt">
      <p className="mb-only">We’ve crunched the numbers - and based on your settings we think you should select:</p>
      <h3>{lb}-{ub} Recipes</h3>
      <div className="recommendatn-cntnt-inner">
        <span className="light">Recommendation based on:</span>
        <p>Total Meals: <span>{totalLength} meals for {servingSize} people</span></p>
        <p>Approx. Servings: <span>{totalLength * servingSize} servings</span></p>
        <p>Variety: <span className="recommendation-variety">{inputs && inputs.recipeVariety} ({variety})</span></p>
        <p>Cook Days: 
          {cookingDates.map(value => {
            const data = moment(value.cooking_date).format('ddd')
            return (
              <span key={data}>{data}<span>,</span></span>
            )
          })} 
        </p>
      </div>
    </div>
  )
}
