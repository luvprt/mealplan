import React, { Component } from 'react';
import RoutingConstants from 'lib/RoutingConstants';
import { post } from 'lib/api';
import { getSelectedWeekDates, getWeekDays } from 'lib/services';
import moment from 'moment';
import history from '../../history';
import SelectWeekPlan from './SelectWeekPlan';

class BuildPlanScreen extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dayDates: [],
      inputs: {},
      dinnerDates: props.dinnerDates || [],
      lunchDates: [],
      cookingDates: props.cookingDates || [],
      mealData: {},
      content: 'Save',
      planExist: props.planExist,
      newWeek: props.newWeek,
      selectWeek: 'this',
      showDates: false,
      type: props.newUser,
    };
  }

  componentDidMount() {
    this.setWeekData();
  }

  onPressBuildPlan() {
    const dataEmail = this.props.guestEmail;
    const {
      inputs, dinnerDates, lunchDates, cookingDates, dayDates, mealData, planExist,
    } = this.state;
    let text = 'Create New Plan';
    inputs.dinnerDates = dinnerDates;
    inputs.lunchDates = lunchDates;
    inputs.cookingDates = cookingDates;
    const url = RoutingConstants.addUserWeeklySchedule;
    if (planExist === 'no') {
      text = 'Builds First Plan';
    }
    post(url, inputs).then(() => {
      // track build new plan
      window.analytics.track(text, {
        noOfLunches: lunchDates.length,
        noOfDinners: dinnerDates.length,
        noOfCookDays: cookingDates.length,
      });
      this.props.onClickBuild(dataEmail, dayDates, mealData, dinnerDates, cookingDates, lunchDates);
    }).catch(() => {
      this.props.onClickBuild(dataEmail, dayDates, mealData);
    });
  }

  setWeekData() {
    const weekEndPoints = getSelectedWeekDates('currentWeek');
    const dayDates = this.props.dayDates || getWeekDays(weekEndPoints);
    const startDate = this.props.startDate || `${dayDates[0].Year}-${dayDates[0].Month}-${dayDates[0].date}`;
    const endDate = this.props.endDate || `${dayDates[6].Year}-${dayDates[6].Month}-${dayDates[6].date}`;
    const inputs = this.state.inputs;
    inputs.week_start_date = startDate;
    inputs.week_end_date = endDate;
    let showDates = true;
    const startWeekDate = `${dayDates[0].halfMonth} ${dayDates[0].date}`;
    const endWeekDate = `${dayDates[6].halfMonth} ${dayDates[6].date}`;
    if(this.props.planExist === 'no') {
      showDates = false
    }
    this.setState({
      dinnerDates: this.props.dinnerDate || [],
      lunchDates: this.props.lunchDates || [],
      cookingDates: this.props.cookingDates || [],
      dayDates,
      inputs,
      disable: false,
      showDates,
      startWeekDate,
      endWeekDate
    });
  }

  updateDinnerDates(value) {
    let isValueExists = false;
    const { dinnerDates, dayDates } = this.state;
    let day;
    if (dinnerDates) {
      isValueExists = dinnerDates.findIndex(x => x.dinner_date === value);

      if (isValueExists > -1) {
        dinnerDates.splice(isValueExists, 1);
        day = 'Dinner Deleted';
      } else {
        dinnerDates.push({ dinner_date: value });
        day = 'Dinner Add';
      }
      this.setState({ dinnerDates });
      // track dinners
      window.analytics.track(day, {
        dinnerDate: moment(value).format('dddd DD-MM-YYYY'),
        startDate: `${dayDates[0].fullDay} ${dayDates[0].date}-${dayDates[0].Month}-${dayDates[0].Year}`,
        endDate: `${dayDates[6].fullDay} ${dayDates[6].date}-${dayDates[6].Month}-${dayDates[6].Year}`,
      });
    }
  }

  updateLunchDates(value) {
    let isValueExists = false;
    const { lunchDates, dayDates } = this.state;
    let day;
    if (lunchDates) {
      isValueExists = lunchDates.findIndex(x => x.lunch_date === value);

      if (isValueExists > -1) {
        lunchDates.splice(isValueExists, 1);
        day = 'Lunch Deleted';
      } else {
        lunchDates.push({ lunch_date: value });
        day = 'Lunch Add';
      }
      // track cook date
      window.analytics.track(day, {
        lunchDate: moment(value).format('dddd DD-MM-YYYY'),
        startDate: `${dayDates[0].fullDay} ${dayDates[0].date}-${dayDates[0].Month}-${dayDates[0].Year}`,
        endDate: `${dayDates[6].fullDay} ${dayDates[6].date}-${dayDates[6].Month}-${dayDates[6].Year}`,
      });
      this.setState({ lunchDates });
    }
    // console.log(lunchDates);
  }

  updateCookDates(value) {
    let isValueExists = false;
    const { cookingDates, dayDates } = this.state;
    let day;
    if (cookingDates) {
      isValueExists = cookingDates.findIndex(x => x.cooking_date === value);
      if (isValueExists > -1) {
        cookingDates.splice(isValueExists, 1);
        day = 'Cook Day Deleted';
      } else {
        cookingDates.push({ cooking_date: value });
        day = 'New cook Day Add';
      }
      // track cook date
      window.analytics.track(day, {
        cookingDate: moment(value).format('dddd DD-MM-YYYY'),
        startDate: `${dayDates[0].fullDay} ${dayDates[0].date}-${dayDates[0].Month}-${dayDates[0].Year}`,
        endDate: `${dayDates[6].fullDay} ${dayDates[6].date}-${dayDates[6].Month}-${dayDates[6].Year}`,
      });
      this.setState({ cookingDates });
    }
  }

  async saveBuilPlanComponent() {
    const {
      inputs, dinnerDates, lunchDates, cookingDates, mealData,
    } = this.state;
    const cDates = [];
    inputs.dinnerDates = dinnerDates;
    inputs.lunchDates = lunchDates;
    cookingDates.map((value) => {
      cDates.push({ cooking_date: value.cooking_date });
      return cookingDates;
    });
    inputs.cookingDates = cDates;
    this.setState({ disable: true, content: 'Saving...' });

    const url = RoutingConstants.editUserWeeklySchedule;
    await post(url, inputs).then(() => {
      this.props.onClickSaveBtn(mealData);
    }).catch((err) => {
      if (err.error === 'token_expired') {
        localStorage.removeItem('token');
        history.push('/login');
      }else {
        this.setState({ disable: false, content: 'Save' });
      }
    });
  }

  closeBuilPlanComponent() {
    const { dayDates, newWeek } = this.state;
    // track click print
    if (newWeek) {
      window.analytics.track('Cancelled Build New Plan', {
        startDate: `${dayDates[0].fullDay} ${dayDates[0].date}-${dayDates[0].Month}-${dayDates[0].Year}`,
        endDate: `${dayDates[6].fullDay} ${dayDates[6].date}-${dayDates[6].Month}-${dayDates[6].Year}`,
      });
    }
    return this.props.onClickCloseBtn();
  }

  nextWeek(nextWeekDates, nextWeekStart, nextWeekEnd) {
    const startDate = this.props.startDate || `${nextWeekDates[0].Year}-${nextWeekDates[0].Month}-${nextWeekDates[0].date}`;
    const endDate = this.props.endDate || `${nextWeekDates[6].Year}-${nextWeekDates[6].Month}-${nextWeekDates[6].date}`;
    const inputs = this.state.inputs;
    inputs.week_start_date = startDate;
    inputs.week_end_date = endDate;

    this.setState({
      dayDates: nextWeekDates,
      showDates: true,
      startWeekDate: nextWeekStart,
      endWeekDate: nextWeekEnd,
      inputs,
      selectWeek: 'next'
    })
  }

  currentWeek(dayDates) {
    const startWeekDate = `${dayDates[0].halfMonth} ${dayDates[0].date}`;
    const endWeekDate = `${dayDates[6].halfMonth} ${dayDates[6].date}`;
    this.setState({
      dayDates,
      showDates: true,
      startWeekDate,
      endWeekDate,
      selectWeek: 'this'
    });
  }

  render() {
    const {
      dayDates, lunchDates, cookingDates, planExist, startWeekDate, endWeekDate, selectWeek, type
    } = this.state;
    let { showDates } = this.state;
    let headingClass = '';
    if (planExist === 'yes') {
      headingClass = 'popup-heading';
    }
    if(type && type.type === '/login') {
      showDates = true
    }
    return (
      <section className="dashboard-main">
        <div className="container">
          <div id="schedule-popup" className="modal fade in" role="dialog" style={{ display: 'block' }}>
            <div className="modal-dialog">
              <div className="modal-content">
                <div className="popup-hdng">
                  {/* close button condition based view ( hidden for the userType newUSer ) */}
                  {(this.props.showBuilPlanComponent === 'show' || this.props.showBuilPlanComponent === 'display') ?
                    <button
                      onClick={data => this.closeBuilPlanComponent(data)}
                      type="button"
                      className="close"
                    >
                      <svg width="20px" height="20px" viewBox="0 0 20 20" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                        <desc>Created with Sketch.</desc>
                        <defs />
                        <g id="Symbols" stroke="none" strokeWidth={1} fill="none" fillRule="evenodd">
                          <g id="Icon-/-Delete-(White)" fill="#FFFFFF" fillRule="nonzero">
                            <g id="noun_1054717">
                              <path d="M10,0.104166667 C4.54166667,0.104166667 0.104166667,4.54166667 0.104166667,10 C0.104166667,15.4583333 4.54166667,19.8958333 10,19.8958333 C15.4583333,19.8958333 19.8958333,15.4583333 19.8958333,10 C19.8958333,4.54166667 15.4583333,0.104166667 10,0.104166667 Z M10,18.1458333 C5.5,18.1458333 1.85416667,14.5 1.85416667,10 C1.85416667,5.5 5.5,1.85416667 10,1.85416667 C14.5,1.85416667 18.1458333,5.5 18.1458333,10 C18.1458333,14.5 14.5,18.1458333 10,18.1458333 Z" id="Shape" />
                              <path d="M14.7708333,6.70833333 L13.2916667,5.22916667 C13.125,5.0625 12.875,5.0625 12.7083333,5.22916667 L10,7.9375 L7.29166667,5.22916667 C7.125,5.0625 6.875,5.0625 6.70833333,5.22916667 L5.22916667,6.70833333 C5.0625,6.875 5.0625,7.125 5.22916667,7.29166667 L7.9375,10 L5.22916667,12.7083333 C5.0625,12.875 5.0625,13.125 5.22916667,13.2916667 L6.70833333,14.7708333 C6.875,14.9375 7.125,14.9375 7.29166667,14.7708333 L10,12.0625 L12.7083333,14.7708333 C12.875,14.9375 13.125,14.9375 13.2916667,14.7708333 L14.7708333,13.2916667 C14.9375,13.125 14.9375,12.875 14.7708333,12.7083333 L12.0625,10 L14.7708333,7.29166667 C14.9375,7.125 14.9375,6.875 14.7708333,6.70833333 Z" id="Shape" />
                            </g>
                          </g>
                        </g>
                      </svg>
                    </button>
                    : null
                  }
                  <svg xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" version="1.1" x="0px" y="0px" viewBox="0 0 24 24" style={{ enableBackground: 'new 0 0 24 24' }} xmlSpace="preserve"><g><path d="M18.939,3.726h-1.601V2.125H16.27v1.601H7.73V2.125H6.662v1.601H5.061c-1.472,0-2.669,1.197-2.669,2.669v12.811   c0,1.472,1.197,2.669,2.669,2.669h13.878c1.472,0,2.669-1.197,2.669-2.669V9.064V7.997V6.395   C21.608,4.924,20.411,3.726,18.939,3.726z M20.541,19.206c0,0.883-0.718,1.601-1.601,1.601H5.061c-0.883,0-1.601-0.718-1.601-1.601   V9.064h17.081V19.206z M20.541,7.997H3.459V6.395c0-0.883,0.718-1.601,1.601-1.601h1.601v1.068H7.73V4.794h8.541v1.068h1.068V4.794   h1.601c0.883,0,1.601,0.718,1.601,1.601V7.997z" /><path d="M10.932,14.402h2.135c0.589,0,1.068-0.479,1.068-1.068v-2.135c0-0.589-0.479-1.068-1.068-1.068h-2.135   c-0.589,0-1.068,0.479-1.068,1.068v2.135C9.865,13.923,10.344,14.402,10.932,14.402z M10.932,11.199h2.135l0.001,2.135h-2.136   V11.199z" /><path d="M16.27,14.402h2.135c0.589,0,1.068-0.479,1.068-1.068v-2.135c0-0.589-0.479-1.068-1.068-1.068H16.27   c-0.589,0-1.068,0.479-1.068,1.068v2.135C15.203,13.923,15.682,14.402,16.27,14.402z M16.27,11.199h2.135l0.001,2.135H16.27V11.199   z" /><path d="M5.595,19.74H7.73c0.589,0,1.068-0.479,1.068-1.068v-2.135c0-0.589-0.479-1.068-1.068-1.068H5.595   c-0.589,0-1.068,0.479-1.068,1.068v2.135C4.527,19.261,5.006,19.74,5.595,19.74z M5.595,16.537H7.73l0.001,2.135H5.595V16.537z" /><path d="M10.932,19.74h2.135c0.589,0,1.068-0.479,1.068-1.068v-2.135c0-0.589-0.479-1.068-1.068-1.068h-2.135   c-0.589,0-1.068,0.479-1.068,1.068v2.135C9.865,19.261,10.344,19.74,10.932,19.74z M10.932,16.537h2.135l0.001,2.135h-2.136V16.537   z" /><path d="M16.27,19.74h2.135c0.589,0,1.068-0.479,1.068-1.068v-2.135c0-0.589-0.479-1.068-1.068-1.068H16.27   c-0.589,0-1.068,0.479-1.068,1.068v2.135C15.203,19.261,15.682,19.74,16.27,19.74z M16.27,16.537h2.135l0.001,2.135H16.27V16.537z" /></g></svg>
                  {planExist === 'no' ?
                    <span>Time to Build Your First Plan!</span>
                    :
                    (this.props.showBuilPlanComponent === 'show') ?
                      <span>Edit Your Weekly Schedule</span> :
                      <span>Schedule Your Meals For The Week</span>
                  }
                </div>
                {
                  planExist === 'no' && !showDates &&
                    <SelectWeekPlan
                      nextWeek={(nextWeekDates, weekStart, weekEnd) => this.nextWeek(nextWeekDates, weekStart, weekEnd)}
                      currentWeek={() => this.currentWeek(dayDates)}
                      dayDates={dayDates}
                    />
                }
                {showDates &&
                    <div className="schedule-popup-cntnt">
                      <h5 className={headingClass}> Which meals do you need {selectWeek} week? </h5>
                      { planExist === 'no' &&
                    <p className="planExist">
                      Based on your schedule, select the dinners and lunches
                      that you want to meal plan for the week.
                    </p>
                      }
                      <div className="popup-week">
                        { planExist === 'no' &&
                          showDates
                         &&
                          <div className="popup-side-hdng popup-side-hdng-date">
                            <b>Planning For:</b>
                            {startWeekDate} - {endWeekDate}
                          </div>
                        }
                        {
                          dayDates.map(days => (
                            <div className="popup-week-day" key={days.date}><b>{days.date}</b> {days.day}</div>
                          ))
                        }
                      </div>
                      <div className="popup-week-check">
                        <div className="popup-week">
                          <span className="popup-side-hdng">Lunch</span>
                          {
                            dayDates.map((days) => {
                              let check = false;
                              const dayDate = `${days.Year}-${days.Month}-${days.date}`;
                              if (this.state.lunchDates) {
                                this.state.lunchDates.map((i) => {
                                  if (i.lunch_date === dayDate) { check = true; }
                                  return lunchDates;
                                });
                              }
                              return (
                                <div className="popup-week-day" key={dayDate}>
                                  <label className="container1" htmlFor={days.date}>
                                    <input
                                      type="checkbox"
                                      id={days.date}
                                      onChange={e => this.updateLunchDates(e.target.value)}
                                      value={dayDate}
                                      checked={check}
                                    />
                                    <span className="checkmark1" />
                                  </label>
                                </div>
                              );
                            })
                          }
                        </div>
                        <div className="popup-week">
                          <span className="popup-side-hdng">Dinner</span>
                          {
                            dayDates.map((days) => {
                              const dayDate = `${days.Year}-${days.Month}-${days.date}`;
                              let check = false;
                              if (this.state.dinnerDates) {
                                this.state.dinnerDates.map((i) => {
                                  if (i.dinner_date === dayDate) {
                                    check = true;
                                  }
                                  return i;
                                });
                              }
                              return (
                                <div className="popup-week-day" key={dayDate}>
                                  <label className="container1" htmlFor={dayDate}>
                                    <input
                                      type="checkbox"
                                      onChange={e => this.updateDinnerDates(e.target.value)}
                                      value={dayDate}
                                      id={dayDate}
                                      checked={check}
                                    />
                                    <span className="checkmark1" />
                                  </label>
                                </div>);
                            })

                          }
                        </div>
                      </div>
                      <div className="popup-checkbox-copy">
                        <h5 className={headingClass}>When do you want to cook those meals?</h5>
                        {planExist === 'no' &&
                      <p className="planExist">
                        This helps us organize your cooking schedule.
                        Some weeks you might want to cook fresh every night,
                        while other weeks you might want to batch cook multiple
                        meals at and just cook a couple of days.
                      </p>
                        }
                        <div className="popup-week">
                          {
                            dayDates.map(days => (
                              <div className="popup-week-day" key={days.day}>{days.day}</div>
                            ))
                          }
                        </div>
                        <div className="popup-week-check">
                          <div className="popup-week">
                            <span className="popup-side-hdng">Cook Days</span>
                            {
                              dayDates.map((days) => {
                                let check = false;
                                const dayDate = `${days.Year}-${days.Month}-${days.date}`;

                                if (this.state.cookingDates) {
                                  this.state.cookingDates.map((i) => {
                                    if (i.cooking_date === dayDate) {
                                      check = true;
                                    }
                                    return cookingDates;
                                  });
                                }
                                return (
                                  <div className="popup-week-day" key={dayDate}>
                                    <label className="container1" htmlFor={days.fullDay}>
                                      <input
                                        type="checkbox"
                                        id={days.fullDay}
                                        onChange={e => this.updateCookDates(e.target.value)}
                                        value={dayDate}
                                        checked={check}
                                      />
                                      <span className="checkmark1" />
                                    </label>
                                  </div>
                                );
                              })

                            }
                          </div>
                        </div>
                      </div>
                      <div className="popup-ftr">
                        {(this.props.showBuilPlanComponent === 'show') ?
                          <div>
                            <div className="popup-alert-msg" />
                            <button
                              className="btn green-btn"
                              onClick={data => this.saveBuilPlanComponent(data)}
                              disabled={this.state.disable}
                            >
                              {this.state.content}
                            </button>
                          </div>
                          :
                          <button className="btn green-btn" onClick={() => this.onPressBuildPlan()}>Build Plan</button>
                        }
                      </div>
                    </div>
                }
              </div>
            </div>
          </div>
        </div>
      </section>
    );
  }
}
export default BuildPlanScreen;
