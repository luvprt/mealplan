import React, { Component } from 'react';
// import RoutingConstants from 'lib/RoutingConstants';
// import { post } from 'lib/api';
import { getSelectedWeekDates, getWeekDays, sortByKey } from 'lib/services';
import moment from 'moment';
import {isIOS} from 'react-device-detect';
import history from '../../history'
import  { RecipeDetail, SelectRecipeVariety } from './BuildPlanSteps'

const HeadingTop = props => {
  const { cookDays, RecipeType, showRecipeDetails } = props
  let setClass = ''
  if(showRecipeDetails) {
    setClass = 'hdng1'
  }
  return (
    <h2 className={`mb-hide ${setClass}`}>
      { !cookDays ? 
        'Which meals do you want to plan for?': 
        !RecipeType ?
          'Select Cook Days: When do you want to cook?'
          :
          !showRecipeDetails ?
            'How many times do you want to eat each recipe?'
            :
            'Time to Choose Your Meals!'
      }
    </h2>
  )
}

const SubHeadingTop = props => {
  const { cookDays, RecipeType, showRecipeDetails } = props
  let setClass = ''
  if(showRecipeDetails) {
    setClass = 'mb-hide text-center'
  } else if(RecipeType) {
    setClass = 'mb-hide'
  }
  return (
    <p className={`after-hdng ${setClass}`}>
      { !cookDays ? 
        'Take a look at your calendar for the week & we’ll help you create a simple, fast meal plan.'
        :
        !RecipeType ?
          'Most ninjas cook a couple times so their meals are fresh throughout the week. Up to you!'
          :
          !showRecipeDetails ?
            'Last Question: Let us know how much variety you want.'
            :
            'Based on your settings, we recommend that you select:'
      }
    </p>
  )
}

const MealTotalDisplay = props => {
  const { cookDays, RecipeType, showRecipeDetails, 
    lunchDates, dinnerDates, totalLength, recipeVariety } = props
  let { cookingDates } = props
  cookingDates = cookingDates.sort(sortByKey('cooking_date','asc'))
  return (
    <div>
      { !cookDays ? 
        <div>{totalLength > 0 && 
          <div className="selected-status">
            <p>{totalLength} Meals Selected</p>
            {lunchDates.length} Lunch <span>.</span> {dinnerDates.length} Dinner
          </div>
        }
        </div>
        :
        !RecipeType ?
          <div>{cookingDates.length > 0 && 
          <div className="selected-status">
            <p>{cookingDates.length} Cook Days</p>
            {cookingDates.map((value, index) => {
              const data = moment(value.cooking_date).format('ddd')
              if(index < 3) {
                return (
                  <div key={data+0}>{data}<span>.</span></div>
                )
              } 
              return null;
            })}
            {
              cookingDates.length > 3 &&
              <div key='extra'><span>.</span>ETC</div>
            }
          </div>
          }
          </div>
          :
          !showRecipeDetails ?
            <div className="selected-status">
              {recipeVariety &&   <p>Recipe Repitition</p>}
              {recipeVariety}
            </div>
            :
            null
      }
    </div>
  )

}



class SelectMealDays extends Component {
  constructor(props) {
    super(props);
    const servingSize = this.props.location.state && this.props.location.state.servingSize !== undefined ? this.props.location.state.servingSize : 2;
     
    this.state = {
      dinnerDates: [],
      lunchDates: [],
      cookingDates:[],
      trackCooking:[],
      inputs:{
        // recipeVariety:'No Leftovers'
      },
      dayDates: [],
      cookDays: false,
      RecipeType: false,
      showRecipeDetails: false,
      variety: '',
      servingSize,
      disable: true,
      slideClass: ''
    };
  }

  componentDidMount() {
    window.scrollTo(0,0);
    localStorage.removeItem('path')
    this.setWeekData();
  }

  onPressNext() {
    let {cookDays, RecipeType, showRecipeDetails, disable, slideClass } = this.state
    const { variety, cookingDates, lunchDates, dinnerDates, trackCooking, inputs } = this.state
    const email = localStorage.getItem('email')
    if(!cookDays) {
      cookDays = true
      slideClass = window.innerWidth < 768 ?'sliderightfalse' :''
      window.analytics.track(`Weekly Planner: Choose Meals`, {
        numberOfLunches: lunchDates.length,
        numberOfDinners: dinnerDates.length,
        email
      });
      if(cookingDates.length === 0)
        disable = true
    } else if(cookDays && !RecipeType) {
      RecipeType = true
      slideClass = window.innerWidth < 768 ? 'sliderighttrue': ''
      window.analytics.track('Weekly Planner: Choose Cook Days', {
        numberOfCookDays: cookingDates.length,
        daysSelected: trackCooking,
        email
        
      });
      if(variety === '')
        disable = true
    } else if (RecipeType) {
      showRecipeDetails = true
      slideClass = window.innerWidth < 768? 'sliderightfalse': ''
      window.analytics.track('Weekly Planner: Variety Setting', {
        varietySettings: inputs.recipeVariety,
        email
      });
    }
    this.setState({ cookDays, RecipeType, showRecipeDetails, disable, slideClass})
  }

  onPressBack() {
    let {cookDays, RecipeType, showRecipeDetails, disable, slideClass } = this.state
    const { dayDates, servingSize } = this.state
    if(RecipeType && showRecipeDetails) {
      showRecipeDetails = false
      slideClass = window.innerWidth < 768 ?'slideleftfalse': ''
    } else if (RecipeType && cookDays) {
      RecipeType = false
      disable = false
      slideClass = window.innerWidth < 768 ?'slidelefttrue': ''
    } else if(cookDays){
      cookDays = false
      disable = false
      slideClass = window.innerWidth < 768 ? 'slideleftfalse':''
    } else if(!cookDays && (sessionStorage.getItem('plan') === undefined || sessionStorage.getItem('plan') === null)) {
      this.props.history.push('/onboarding',{startPosition: 4, dayDates})
    } else if( sessionStorage.getItem('plan') && sessionStorage.getItem('plan') === 'create') {
      history.push('/dashboard',{dayDates, defaultServingSize: servingSize})
    }
    this.setState({ cookDays, RecipeType, showRecipeDetails, disable, slideClass})
  }

  setWeekData() {
    const weekEndPoints = getSelectedWeekDates('currentWeek');
    const weekDate = this.props.location.state && this.props.location.state.dayDates !== undefined && this.props.location.state.dayDates
    const dayDates = weekDate || getWeekDays(weekEndPoints);
    const startDate = this.props.startDate || `${dayDates[0].Year}-${dayDates[0].Month}-${dayDates[0].date}`;
    const endDate = this.props.endDate || `${dayDates[6].Year}-${dayDates[6].Month}-${dayDates[6].date}`;
    let { 
      cookingDates, servingSize, recipeRange, lunchDates, dinnerDates, 
      varietyIndex, variety, inputs, disable, cookDays, RecipeType, showRecipeDetails 
    } = this.state
    inputs.week_start_date = startDate;
    inputs.week_end_date = endDate;
    if(this.props.location.state && this.props.location.state.cookingDates !== undefined) {
      cookingDates = this.props.location.state.cookingDates || []
      dinnerDates = this.props.location.state.dinnerDates || []
      lunchDates = this.props.location.state.lunchDates || []
      servingSize = this.props.location.state.servingSize || 2
      recipeRange = this.props.location.state.recipeRange || '1-2'
      varietyIndex = this.props.location.state.varietyIndex || '1'
      variety = this.props.location.state.variety || ''
      inputs = this.props.location.state.inputs || {}
      disable = false
      cookDays = true
      RecipeType = true
      showRecipeDetails = true
    }
    this.setState({
      dinnerDates,
      lunchDates,
      cookingDates,
      dayDates,
      servingSize,
      recipeRange,
      inputs,
      varietyIndex,
      variety,
      disable,
      cookDays,
      RecipeType,
      showRecipeDetails
    });
  }

  updateDinnerDates(event, value, dinnerClass) {
    let isValueExists = false;
    const { dinnerDates, lunchDates } = this.state;
    let { disable } = this.state
    if (dinnerDates) {
      isValueExists = dinnerDates.findIndex(x => x.dinner_date === value);

      if (isValueExists > -1) {
        dinnerDates.splice(isValueExists, 1); 
        event.target.className = dinnerClass
        if(lunchDates.length === 0 && dinnerDates.length === 0) {
          disable = true
        }
      } else {
        dinnerDates.push({ dinner_date: value });
        event.target.className += ' active'
        disable = false
      }
      this.setState({ dinnerDates, disable });
     
    }
  }

  updateLunchDates(event, value, lunchClass) {
    let isValueExists = false;
    const { lunchDates, dinnerDates } = this.state;
    let  { disable } = this.state
    if (lunchDates) {
      isValueExists = lunchDates.findIndex(x => x.lunch_date === value);

      if (isValueExists > -1) {
        lunchDates.splice(isValueExists, 1); 
        event.target.className = lunchClass
        if(lunchDates.length === 0 && dinnerDates.length === 0) {
          disable = true
        }
      } else {
        lunchDates.push({ lunch_date: value });
        event.target.className += ' active'
        disable = false
      }
      this.setState({ lunchDates, disable });
     
    }
  }
  
  updateCookingDates(event, value, cookingClass) {
    let isValueExists = false;
    const { cookingDates, trackCooking } = this.state;
    let { disable } = this.state
    if (cookingDates) {
      isValueExists = cookingDates.findIndex(x => x.cooking_date === value);

      if (isValueExists > -1) {
        cookingDates.splice(isValueExists, 1); 
        trackCooking.splice(isValueExists, 1); 
        event.target.className = cookingClass
        if(cookingDates.length === 0 ) {
          disable = true
        }
      } else {
        cookingDates.push({ cooking_date: value });
        trackCooking.push(moment(value).format('ddd'));
        event.target.className += ' active'
        disable = false
      }
      this.setState({ cookingDates, disable, trackCooking });
     
    }
  }

  updateValue(value, variety, varietyIndex) {
    const { inputs } = this.state
    inputs.recipeVariety = value
    this.setState({ inputs, variety, disable: false, varietyIndex })
  }

  chooseRecipe() {
    const { cookingDates, servingSize, lunchDates, 
      dinnerDates, inputs, variety, varietyIndex, dayDates } = this.state
    const totalLength = lunchDates.length + dinnerDates.length
    const result = totalLength / varietyIndex
    const email = localStorage.getItem('email')
    let lb = Math.floor(result);
    let ub = Math.ceil(result);
    if(lb === 0) {
      lb += 1
    }
    if(varietyIndex === 1) {
      lb = Math.round(result/2)
    }
    if(lb === ub) {
      ub += 1
    }
    window.analytics.track('Weekly Planner: Recommendation',{email});
    history.push('/chooseRecipe',{
      recipeRange:`${lb}-${ub}`, cookingDates, servingSize, lunchDates, 
      dinnerDates, inputs, variety, varietyIndex, dayDates
    })
  }

  render() {
    const { 
      dayDates, lunchDates, dinnerDates, cookDays, RecipeType, varietyIndex,
      showRecipeDetails, cookingDates, variety, inputs, servingSize, disable, slideClass
    } = this.state
    const totalLength = lunchDates.length + dinnerDates.length
    const result = totalLength / varietyIndex
    let lb = Math.floor(result);
    let ub = Math.ceil(result);
    if(lb === 0) {
      lb += 1
    }
    if(varietyIndex === 1) {
      lb = Math.round(result/2)
    }
    if(lb === ub) {
      ub += 1
    }
    return (
      <div className='wrapper'>
        <style>
          {`
          @media(min-width:320px) and (max-width:767px){
            #PureChatWidget { display:none!important;}
          }
          `}
        </style>
        <header>
          <div className="container">
            <a href="/" className="mb-hide" ><img src="/images/logo.png" alt="" /></a>
            <div className="slect-meal-top">
              <div className="top-back-row-comon-hdr">
                <a onClick={()=>this.onPressBack()}>  
                  <svg enableBackground="new 0 0 256 256" height="256px" id="Layer_1" version="1.1" viewBox="0 0 256 256" width="256px" xmlSpace="preserve" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                    <path d="M91.474,33.861l-89.6,89.6c-2.5,2.5-2.5,6.551,0,9.051l89.6,89.6c2.5,2.5,6.551,2.5,9.051,0s2.5-6.742,0-9.242L21.849,134  H249.6c3.535,0,6.4-2.466,6.4-6s-2.865-6-6.4-6H21.849l78.676-78.881c1.25-1.25,1.875-2.993,1.875-4.629s-0.625-3.326-1.875-4.576  C98.024,31.413,93.974,31.361,91.474,33.861z" />
                  </svg>
                </a>
              </div>
              <h1 className="mb-only"> Weekly Planner </h1>
              <p className="after-hdng mb-only">
                Answer a couple questions to get started!
              </p>
            </div>
          </div>
        </header>
		 
        <section className="select-meal-cntnt">
          <div className="container">
            <div className="select-meal-cntnt-box sel-meal-plan-page">
              <div className="top-back-row mb-hide">
			          <a onClick={()=>this.onPressBack()}>
                  <svg 
                    enableBackground="new 0 0 256 256" 
                    height="256px" 
                    id="Layer_1" 
                    version="1.1" 
                    viewBox="0 0 256 256" 
                    width="256px" 
                    xmlSpace="preserve" 
                    xmlns="http://www.w3.org/2000/svg" 
                    xmlnsXlink="http://www.w3.org/1999/xlink"
                  >
                    <path d="M91.474,33.861l-89.6,89.6c-2.5,2.5-2.5,6.551,0,9.051l89.6,89.6c2.5,2.5,6.551,2.5,9.051,0s2.5-6.742,0-9.242L21.849,134  H249.6c3.535,0,6.4-2.466,6.4-6s-2.865-6-6.4-6H21.849l78.676-78.881c1.25-1.25,1.875-2.993,1.875-4.629s-0.625-3.326-1.875-4.576  C98.024,31.413,93.974,31.361,91.474,33.861z"/>
                  </svg> 
                  Back
                </a>
              </div>
              <div className={slideClass} style={{ float: 'left', padding: isIOS &&'0 20px'}}>
                <HeadingTop 
                  cookDays={cookDays}
                  RecipeType={RecipeType}
                  showRecipeDetails={showRecipeDetails}
                />

                <h2 className="mb-only">
                  {!cookDays ?
                    'Create First Plan'
                    :
                    !RecipeType ?
                      'Select Cook Days'
                      :
                      !showRecipeDetails ?
                        'Last Question'
                        :
                        `Time to choose your meals for the week!`
                  }</h2>
                <SubHeadingTop 
                  cookDays={cookDays}
                  RecipeType={RecipeType}
                  showRecipeDetails={showRecipeDetails}
                />
                {!cookDays ?
                  <p className="mb-only hdng-aftr2"> Which meals do you want to plan for?</p>
                  :
                  !RecipeType ?
                    <p className="mb-only hdng-aftr2"> When do you want to cook? </p>
                    :
                    !showRecipeDetails &&
                    <p className="mb-only hdng-aftr2"> How many times do you typically want to eat each recipe? </p>
                }
                {
                  !cookDays? 
                    <div>
                      <div className="week-food-days">
                        <label>Lunch:</label>
                        <ul>
                          {
                            dayDates.length > 0 &&
                    dayDates.map((days, index) => {
                      const dayDate = `${days.Year}-${days.Month}-${days.date}`;
                      const isValueExists = lunchDates.findIndex(x => x.lunch_date === dayDate);
                      let lunchClass = ''
                      if (isValueExists > -1) {
                        lunchClass = 'active'
                      } 
                      let mDays = days.day.substring(0,1)
                      if(index === 4 || index === 6) {
                        mDays = days.day.substring(0,2).toUpperCase()
                      }
                      return(
                        <li key={days.date}>
                          <a className={`mb-hide ${lunchClass}`} onClick={(event)=>this.updateLunchDates(event, dayDate,'mb-hide')}>{days.day}</a>
                          <a className={`mb-only ${lunchClass}`} onClick={(event)=>this.updateLunchDates(event, dayDate, 'mb-only')}>{mDays}</a></li>
                      )})
                          }
                        </ul>
                      </div>
			   
                      <div className="week-food-days">
                        <label>Dinner:</label>
                        <ul>
                          {
                            dayDates.length > 0 &&
                          dayDates.map((days, index) => {
                            const dayDate = `${days.Year}-${days.Month}-${days.date}`;
                            const isValueExists = dinnerDates.findIndex(x => x.dinner_date === dayDate);
                            let dinnerClass = ''
                            if (isValueExists > -1) {
                              dinnerClass = 'active'
                            } 
                            let mDays = days.day.substring(0,1)
                            if(index === 4 || index === 6) {
                              mDays = days.day.substring(0,2).toUpperCase()
                            }
                            return(
                              <li key={days.date+days.Month}>
                                <a className={`mb-hide ${dinnerClass}`} onClick={(event)=>this.updateDinnerDates(event, dayDate,'mb-hide')}>{days.day}</a>
                                <a className={`mb-only ${dinnerClass}`} onClick={(event)=>this.updateDinnerDates(event, dayDate, 'mb-only')}>{mDays}</a></li>
                            )}
                          )
                          }
                        </ul>
                      </div>
                    </div>
                    :
                    !RecipeType ?
                      <div className="week-food-days">
                        <label className="cook-label">Cook Days:</label>
                        <ul>
                          {
                            dayDates.length > 0 &&
                    dayDates.map((days, index) => {
                      const dayDate = `${days.Year}-${days.Month}-${days.date}`;
                      const isValueExists = cookingDates.findIndex(x => x.cooking_date === dayDate);
                      let cookClass = ''
                      if (isValueExists > -1) {
                        cookClass = 'active'
                      } 
                      let mDays = days.day.substring(0,1)
                      if(index === 4 || index === 6) {
                        mDays = days.day.substring(0,2).toUpperCase()
                      }
                      return(
                        <li key={days.date+days.Year}>
                          <a className={`mb-hide ${cookClass}`} onClick={(event)=>this.updateCookingDates(event, dayDate,'mb-hide')}>{days.day}</a>
                          <a className={`mb-only ${cookClass}`} onClick={(event)=>this.updateCookingDates(event, dayDate, 'mb-only')}>{mDays}</a></li>
                      )})
                          }
                        </ul>
                      </div>
                      :
                      !showRecipeDetails ?
                        <SelectRecipeVariety
                          updateValue={(value, data, index)=>this.updateValue(value,data, index)}
                          inputs={inputs}
                        />
                        :
                        <RecipeDetail
                          totalLength={totalLength}
                          inputs={inputs}
                          servingSize={servingSize}
                          cookingDates={cookingDates}
                          variety={variety}
                          varietyIndex={varietyIndex}
                        />

                }
              </div>
              <div className="select-meal-btns">
                <MealTotalDisplay 
                  cookDays={cookDays}
                  RecipeType={RecipeType}
                  showRecipeDetails={showRecipeDetails}
                  totalLength={totalLength}
                  dinnerDates={dinnerDates}
                  lunchDates={lunchDates}
                  cookingDates={cookingDates}
                  recipeVariety={inputs.recipeVariety}
                />
              
                {!showRecipeDetails ?
                  <button type="button" 
                    className="next-btn" 
                    onClick={()=>this.onPressNext()} 
                    disabled={disable}
                  >
                    <span aria-label="Next">Next</span>
                  </button>
                  :
                  <div className="select-meal-btns recomendation-btn-box">
                    <button 
                      type="button" 
                      className="next-btn" 
                      onClick={()=>this.chooseRecipe()}>
                      <span aria-label="Next">Choose My Recipes</span>
                    </button>
                  </div>
                }
              </div>
              
              
            </div>
          </div>
        </section>
      </div>
    );
  }
}
export default SelectMealDays;
