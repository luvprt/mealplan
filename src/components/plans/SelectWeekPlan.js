import React, { Component } from 'react';
import { getSelectedWeekDates, getWeekDays } from 'lib/services';
import moment from 'moment';

class SelectWeekPlan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      currentDay: moment().format('dddd'),
      currentDate: moment().format('dddd, MMMM DD '),
      currentWeekStart: props.dayDates[0],
      currentWeekEnd: props.dayDates[6],
    };
  }

  componentDidMount() {
    this.weekPlanDates();
  }
  // eslint-disable-next-line
  UNSAFE_componentWillReceiveProps(props) {
    this.setState({
      currentWeekStart: `${props.dayDates[0].halfMonth} ${props.dayDates[0].date}`,
      currentWeekEnd: `${props.dayDates[6].halfMonth} ${props.dayDates[6].date}`,
    })
  }

  weekPlanDates() {
    const weekStartPoints = getSelectedWeekDates('currentWeek');
    const weekEndPoints = getSelectedWeekDates('nextWeek', weekStartPoints);
    const dayDates = getWeekDays(weekEndPoints);
    this.setState({
      nextWeekStart: `${dayDates[0].halfMonth} ${dayDates[0].date}`,
      nextWeekEnd: `${dayDates[6].halfMonth} ${dayDates[6].date}`,
      nextDayDates: dayDates,
    })
  }

  currentWeek() {
    this.props.currentWeek();
  }

  render() {
    const {
      currentDate, currentDay, currentWeekStart, currentWeekEnd,
      nextWeekStart, nextWeekEnd, nextDayDates,
    } = this.state;
    const chooseWeek = moment().day();
    let selectWeek = 'This Week';
    if(chooseWeek > 3) {
      selectWeek = 'Next Week'      
    }
    return (
      <div className="schedule-popup-cntnt">
        <h5 className="popup-heading build-screen"> Which week do you want to plan right now? </h5>
        <h5 className="popup-heading build-screen"> Today is: 
          <span className="week-day build-screen"> {currentDate} </span>
        </h5>
        <h5 className="popup-heading">
          Most people that start on 
          <span className="week-day"> {currentDay} </span>
          choose to plan for 
          <span className="week-day"> {selectWeek} </span>
        </h5>
        <div className="popup-plans weekbtns-box">
          <div className="col-md-6">
            <div className="week-plan-green-box">
              <button onClick={() => this.currentWeek()}> 
                <p> This week </p>
                <span> {currentWeekStart} - {currentWeekEnd}</span>
              </button>
            </div>
          </div>
          <div className="col-md-6">
            <div className="week-plan-green-box">
              <button onClick={() => this.props.nextWeek(nextDayDates, nextWeekStart, nextWeekEnd )}>
                <p>Next week </p>
                <span> {nextWeekStart} - {nextWeekEnd}</span>
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}
export default SelectWeekPlan;
