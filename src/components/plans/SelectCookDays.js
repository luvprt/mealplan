import React, { Component } from 'react';
import {  sortByKey, totalCook, totalCookCount,  getSelectedWeekDates, getWeekDays
} from 'lib/services';
import moment from 'moment'
import _ from 'lodash'
import Loader from 'lib/loader';
import RoutingConstants from 'lib/RoutingConstants'
import { post } from 'lib/api'
import EditCookDay from './EditCookDay';
import history from '../../history';
import CookBottom from './CookBottom'
import {RecipeData, SubIcon, PlusIcon } from '../recipeList/RecipeData'

class SelectCookDays extends Component {
  constructor(props) {
    super(props);
    const cookingDate = this.props.location.state ? this.props.location.state.cookingDates : [];
    const dinnerDates = this.props.location.state ? this.props.location.state.dinnerDates : [];
    const lunchDates = this.props.location.state ? this.props.location.state.lunchDates : [];
    const servingSize = this.props.location.state && this.props.location.state.servingSize? parseInt(this.props.location.state.servingSize,0) : 2;
    const recipeArr = this.props.location.state ? this.props.location.state.recipeArr : [];
    const recipeCount = this.props.location.state ? this.props.location.state.recipeCount : 0;
    const recipeRange = this.props.location.state ? this.props.location.state.recipeRange : '1-2';
    const variety = this.props.location.state && this.props.location.state.variety ? this.props.location.state.variety : '1';
    const varietyIndex = this.props.location.state && this.props.location.state.varietyIndex ? this.props.location.state.varietyIndex : '1';
    const inputs = this.props.location.state ? this.props.location.state.inputs : {};
    const slideClass = window.innerWidth < 768 ? 'sliderighttrue' :''
    const weekEndPoints = getSelectedWeekDates('currentWeek');
    const dayDates = this.props.location.state && this.props.location.state.dayDates !== undefined ? this.props.location.state.dayDates : getWeekDays(weekEndPoints);
    this.state = {
      dayDates,
      recipeArr,
      cookingDate,
      lastStep: false,
      cookArr: [],
      editCookDay: false,
      count: 0,
      servingSize,
      recipeRange,
      cookDayArr: [],
      // dayDates:[],
      error: false,
      dinnerDates,
      lunchDates,
      variety,
      varietyIndex,
      inputs,
      slideClass,
      loader: false,
      recipeCount
    };
    
  }

  componentDidMount() {
    window.scrollTo(0,0)
    // not required as we can set the dayDates in constructor
    // this.setWeek()
  }

  setWeek() {
    const weekEndPoints = getSelectedWeekDates('currentWeek');
    const dayDates = this.props.location.state && this.props.location.state.dayDates !== undefined ? this.props.location.state.dayDates : getWeekDays(weekEndPoints);
    this.setState({ dayDates });
  }

  setServingSize(index, cookDate, getDate , getServingSize ,recipeId ) {
    // const { recipeArr } = this.state
    // const servingSize = parseInt(recipeArr[index].servingSize,0)
    // const servingText = servingSize > 1 ? 'Servings' : 'Serving'

    const servingText = getServingSize > 1 ? 'Servings' : 'Serving'
    return(
      <ul className="set-serving">

        <li>
          {/* <a className="mb-hide red-circle" onClick={()=>this.removeServing(index, servingSize, cookDate,getDate)}> */}
          <a className="mb-hide red-circle" onClick={()=>this.removeServing(index, getServingSize, cookDate,getDate,recipeId)}>

            <SubIcon />
          </a></li>
        {/* <li> <h6> {servingSize} {servingText} </h6></li> */}
        
        <li> <h6> {getServingSize} {servingText} </h6></li>
        <li>
          {/* <a className="mb-hide Green-circle" onClick={()=>this.addServing(index, servingSize, cookDate,getDate)}> */}
          <a className="mb-hide Green-circle" onClick={()=>this.addServing(index, getServingSize, cookDate,getDate,recipeId)}>
            <PlusIcon />
          </a>
        </li>
      </ul>

    )
  }

  addServing(index, servingSize, cookDate,getDate,recipeId) {
    const { recipeArr } = this.state
    let { cookingDate } = this.state
    if(servingSize < 100) {
      // recipeArr[index].servingSize =  servingSize +1
      const newServing = servingSize +1;
      cookingDate = this.updateData(index,cookDate,getDate,newServing,recipeId)
    }
    this.setState({ recipeArr, cookingDate })
  }

  removeServing(index, servingSize, cookDate,getDate,recipeId) {
    const { recipeArr } = this.state
    let { cookingDate } = this.state
    if(servingSize > 1) {
      // recipeArr[index].servingSize =  servingSize - 1
      const newServing = servingSize -1;
      cookingDate = this.updateData(index,cookDate,getDate,newServing,recipeId)
    }
    this.setState({ recipeArr, cookingDate })
  }

  cookingDays(index) {
    let { cookingDate } = this.state
    const { cookArr } = this.state
    cookingDate = cookingDate.sort(sortByKey('cooking_date','asc'))
    return cookingDate.map((days)=>{
      const data = moment(days.cooking_date).format('ddd')
      const cookIndex = cookArr.findIndex(x=>x.data === data && x.key === index)
      let check;
      if(cookIndex > -1){
        check = true
      }
      else {
        check = false
      }
      return(
        <li key={index+data}>
          <input type="radio" defaultChecked={check} onChange={()=>this.selectCookDay(index, data, days.cooking_date)} name={index} />
          <span className="checkmark1 mb-hide alignt" tabIndex="0" role="button">
            {data}
          </span>
        </li>
      )
    })
  }

  selectCookDay(event, data, cookDate) {
    let { cookArr, count, cookDayArr, cookingDate } = this.state
    const { recipeArr } = this.state
    // for radio button : don't remove
    // const index = cookArr.findIndex(x=>x.key === event)

    cookingDate = this.addData(event, data, cookDate);

    // for radio button: don't remove
    // if(index > -1){
    //   cookArr[index].data = data
    // }
    // else {    
    //   cookArr.push({key: event, data })
    //   if(count === 0)
    //     count = 1      
    // }

    // for new functionality checkboxes don't remove
    if(cookArr && cookArr[0]){
      const cookIndex = cookArr.findIndex((arr)=> arr.data === data && arr.key === event);
      if(cookIndex > -1){
        cookArr.splice(cookIndex,1);        
      }else{
        cookArr.push({key: event, data })             
      }            
    }else{
      cookArr.push({key: event, data })
      if(count === 0)
        count = 1  
    }

    // for new functionality checkboxes don't remove
    if(recipeArr[event] && recipeArr[event].cooking_date && recipeArr[event].cooking_date[0]){
      const ifIndex = recipeArr[event].cooking_date.findIndex((arr)=> arr === cookDate);
      if(ifIndex > -1){
        recipeArr[event].cooking_date.splice(ifIndex,1);
      }else{
        recipeArr[event].cooking_date.push(cookDate);
      }
      
    }else{
      recipeArr[event].cooking_date = [];
      recipeArr[event].cooking_date.push(cookDate);
    }

    // for radio button : don't remove
    // recipeArr[event].cooking_date = cookDate

    cookArr = cookArr.sort(sortByKey('data','asc'))
    count = totalCook(cookArr, count)
    cookDayArr = totalCookCount(cookArr)
    this.setState({ cookArr, count, cookDayArr, error: false, cookingDate })
  }

  addData(event, data, cookDate) {
    
    const { recipeArr, cookingDate } = this.state
    const cookIndex = cookingDate.findIndex(x=>x.cooking_date===cookDate)
    const recipeId = recipeArr[event].items.recipe_id;

    cookingDate.map(index=>{
      if(!index.recipe || index.recipe.length === 0){
        index.recipe = []
      }
      return null
    })

    if(cookingDate[cookIndex] && cookingDate[cookIndex].recipe && cookingDate[cookIndex].recipe.length){
      const recipeIndex = cookingDate[cookIndex].recipe.findIndex(recipe => recipe.recipe_id === recipeId);
      if(recipeIndex > -1){
        cookingDate[cookIndex].recipe.splice(recipeIndex,1);
      }else{
        const sideDish =recipeArr[event].items.recipe.recipe_sides.length > 0 ?
          recipeArr[event].items.recipe.recipe_sides[0].side_id : null
        cookingDate[cookIndex].recipe.push({
          serving_size: recipeArr[event].servingSize,
          recipe_id: recipeArr[event].items.recipe_id,
          side_dish_id:sideDish,
          index: event
        })
      }
    }else{
      cookingDate[cookIndex].recipe = [];
      const sideDish =recipeArr[event].items.recipe.recipe_sides.length > 0 ?
        recipeArr[event].items.recipe.recipe_sides[0].side_id : null
      cookingDate[cookIndex].recipe.push({
        serving_size: recipeArr[event].servingSize,
        recipe_id: recipeArr[event].items.recipe_id,
        side_dish_id:sideDish,
        index: event
      })
    }
     
    return cookingDate
  }

  updateData(event, cookDate, getDate,newServings,recipeId) {
    const { cookingDate } = this.state
    // const { recipeArr, cookingDate } = this.state

    // const cookIndex = cookingDate.findIndex(x=>x.cooking_date===cookDate)
    const cookIndex = cookingDate.findIndex(x=>x.cooking_date===getDate);    
    if(cookIndex > -1){
      const recipeIndex = cookingDate[cookIndex].recipe.findIndex(x=>x.recipe_id === recipeId)
      // console.log(cookingDate, cookIndex, getDate , recipeIndex , cookingDate[cookIndex].recipe[recipeIndex].serving_size, recipeArr[event], newServings )
      if(recipeIndex > -1) { 
       
        cookingDate[cookIndex].recipe[recipeIndex].serving_size = newServings    
        // console.log(cookingDate[cookIndex].recipe[recipeIndex].serving_size,'here')      
      }
    }
    return cookingDate
  }
  editCookDay(index) {
    const { editCookDay } = this.state
    this.setState({ editCookDay: !editCookDay, indexValue: index })
  }

  saveCookDay(index, cookDay, cookDate) {
    if(cookDay) {
      const { cookingDate } = this.state
      const indexValue = cookingDate.findIndex(x=>x.cooking_date === cookDate)
      if(indexValue === -1) {
        cookingDate.push({ cooking_date: cookDate})
      }
      this.setState({ editCookDay: false, cookingDate})
      this.selectCookDay(index, cookDay, cookDate)
    }
  }

  finalStep() {
    let { recipeArr, slideClass } = this.state
    const { cookArr, lastStep, count } = this.state
    const email = localStorage.getItem('email')
    // earlier code
    // if(recipeArr.length === cookArr.length) {
    // new code
    if(recipeArr.length === _.uniqBy(cookArr,'key').length) {
      // earlier code
      // recipeArr = recipeArr.sort(sortByKey('cooking_date','asc'))
      // new code
      recipeArr = _.sortBy(recipeArr, (o) => o.items.tag==='Breakfast');

      if(window.innerWidth < 768 && !lastStep) {
        slideClass = 'sliderightfalse'
      } else {
        this.setState({ loader: true})
      }
      if(lastStep) {
        sessionStorage.removeItem('lastStep')
        this.buildPlan()
      } else {
        window.scrollTo(0,0)
        window.analytics.track(`Set Cook Schedule Completed`, {
          cookDaysNumber: count,
          email
        });
        sessionStorage.setItem('lastStep', true)
      }
      this.setState({ lastStep: true, recipeArr, slideClass })
    } else {
      this.setState({ error: true})
    }
  }

  async buildPlan() {
    const url = RoutingConstants.buildPlan
    const { cookingDate, recipeArr, lastStep, cookArr, dayDates } = this.state
    const params = {}
    params.week_start_date = `${dayDates[0].Year}-${dayDates[0].Month}-${dayDates[0].date}`
    params.week_end_date = `${dayDates[6].Year}-${dayDates[6].Month}-${dayDates[6].date}`;
    params.cookingDates = []
    await cookingDate.map((item)=>{
      if(item.recipe.length > 0) {
        params.cookingDates.push(item)
      } 
      return 0
    })
    
    const email = localStorage.getItem('email')
    
    await post( url, params).then(()=>{
      sessionStorage.setItem('path','dashboard')
      let totalServing = 0;
      recipeArr.map((item) =>{
        totalServing += parseInt(item.servingSize,0)
        return null;
      })
      window.analytics.track(`Set Serving Sizes Completed`, {
        recipeServingsNumber: totalServing,
        email
      });

      history.push('/dashboard',{ recipeArr, lastStep, cookingDate, cookArr, dayDates, slideClass:''  })
    }).catch(err=>{
      if (err.error === 'token_expired') {
        localStorage.removeItem('token');
        history.push('/login');
      } else if (err.message === 'These dates already exist') {
        history.push('/dashboard',{slideClass:''})
      }
      else {this.setState({ loader: false })}
    })
  }

  sortRecipeData() {
    let { cookingDate } = this.state
    const { recipeArr, lastStep, cookArr, loader } = this.state
    cookingDate = cookingDate.sort(sortByKey('cooking_date','asc'))
    // let count = 0
    if(lastStep && loader && sessionStorage.getItem('lastStep')) {
      setTimeout(() => {
        this.setState({ loader: false})
      }, 1000);
    }
    return(
      <div>
        {
          cookingDate.map((days)=>{
            const cooking = moment(days.cooking_date).format('dddd M/D')
            const cookindex = recipeArr.findIndex((x)=> x.cooking_date.includes(days.cooking_date));            
            
            if(cookindex >= -1) {
              // count +=1
              return (
                <div key={cooking} className="top_rcpe_heading">
                  <h4> {cooking}</h4>
                  {days.recipe.length === 0 ?  <div className="noMeals"><span>No Meals Planned</span></div>  : 
                    <RecipeData
                      length={days.cooking_date}
                      recipeArr={recipeArr}
                      lastStep={lastStep}
                      cookingDate={cookingDate}
                      cookingDays={(index)=>this.cookingDays(index)}
                      cookArr={cookArr}
                      setServingSize={(index, cookDate, getDate, getServingSize, recipeId)=>this.setServingSize(index, cookDate, getDate, getServingSize,recipeId)}
                      location="cook"
                      viewRecipe={()=>this.viewRecipe()}
                    />
                  }
                </div>
              )
            }
            return null;
          })
        }
      </div>
    )
  }

  pressBack() {
    const {
      lastStep, servingSize, recipeRange, lunchDates, dinnerDates, inputs,
      variety, varietyIndex, dayDates, recipeCount
    } = this.state
    let { recipeArr } = this.state
    const slideClass = window.innerWidth < 768 ? 'slidelefttrue' : ''
    if(lastStep) {
      // earlier code
      // recipeArr = recipeArr.sort(sortByKey('cooking_date',''))
      // new code
      recipeArr = _.sortBy(recipeArr, (o) => o.items.tag==='Breakfast');
      this.setState({ lastStep: !lastStep, recipeArr, slideClass})
    } else {
      const cookingDates = this.props.location.state ? this.props.location.state.cookingDates : []
      history.push('/chooseRecipe', {
        cookingDates, servingSize, recipeArr, recipeRange, lunchDates,
        dinnerDates, variety, varietyIndex, inputs, dayDates, recipeCount,slideClass
      })
    }
  }

  // eslint-disable-next-line
  viewRecipe(){  }

  render() {
    const {
      recipeArr, cookArr, editCookDay, indexValue, count, lastStep, cookingDate, servingSize,
      totalServing, cookDayArr, dayDates, error, loader, lunchDates, dinnerDates
    } = this.state
    const totalLength = (lunchDates.length + dinnerDates.length) * servingSize
    const { slideClass } = this.state
    // if(slideClass !== '') {
    //   setTimeout(() => {
    //     this.setState({ slideClass: ''})
    //   }, 500);
    // }
    return (
      <div className={`wrapper ${slideClass}`}>
        <header>
          <div className="container">
            <a href="/" className="mb-hide"><img src="/images/logo.png" alt="" /></a>
            <div className="mb-only choose-recipe-cntnt-box mb-top">
              <div className="top-back-row-comon-hdr">
                <a onClick={()=>this.pressBack()}>
                  <svg enableBackground="new 0 0 256 256" height="256px" id="Layer_1" version="1.1" viewBox="0 0 256 256" width="256px" xmlSpace="preserve" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                    <path d="M91.474,33.861l-89.6,89.6c-2.5,2.5-2.5,6.551,0,9.051l89.6,89.6c2.5,2.5,6.551,2.5,9.051,0s2.5-6.742,0-9.242L21.849,134  H249.6c3.535,0,6.4-2.466,6.4-6s-2.865-6-6.4-6H21.849l78.676-78.881c1.25-1.25,1.875-2.993,1.875-4.629s-0.625-3.326-1.875-4.576  C98.024,31.413,93.974,31.361,91.474,33.861z" />
                  </svg>
                </a>
              </div>
              <h1 className="mb-only">
                {
                  lastStep ?
                    'Last Step: Adjust Servings'
                    :
                    'Assign to Plan'
                }
              </h1>
              <p className="after-hdng mb-only">
                {
                  lastStep ?
                    `${'Set the amount of servings for each meal. We’ve automatically added'} ${localStorage.getItem('default_serving_size')} ${'servings for each meal based on your account settings.'}`
                    :
                    'Select the day(s) you want to eat each recipe to match your schedule this week. Feel free to assign a recipe to multiple days on the calendar… after all, who doesn’t love leftovers? Happy planning!'
                }
              </p>
            </div>

          </div>
        </header>
        {
          loader &&
          <div className="loading" style={{top:'12%'}}>
            <Loader />
            <div className="modal-backdrop fade in" />
          </div>
        }
        <section className='select-meal-cntnt choose-recipe-cntnt'>
          <div className="container">
            {
              editCookDay &&
              <div>
                <EditCookDay
                  cancelCookDay={()=>this.editCookDay()}
                  saveCookDay={(event, data, cookDate)=>this.saveCookDay(event, data, cookDate)}
                  indexValue={indexValue}
                  dayDates={dayDates}
                />
                <div className="modal-backdrop fade in" />
              </div>
            }
            <div className="top-back-row-comon-hdr mb-hide">
              <a onClick={()=>this.pressBack()}>
                <svg enableBackground="new 0 0 256 256" height="256px" id="Layer_1" version="1.1" viewBox="0 0 256 256" width="256px" xmlSpace="preserve" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink">
                  <path d="M91.474,33.861l-89.6,89.6c-2.5,2.5-2.5,6.551,0,9.051l89.6,89.6c2.5,2.5,6.551,2.5,9.051,0s2.5-6.742,0-9.242L21.849,134  H249.6c3.535,0,6.4-2.466,6.4-6s-2.865-6-6.4-6H21.849l78.676-78.881c1.25-1.25,1.875-2.993,1.875-4.629s-0.625-3.326-1.875-4.576  C98.024,31.413,93.974,31.361,91.474,33.861z" />
                </svg>
                <span className="mb-hide" style={{display:'inherit'}}>Back</span>
              </a>
            </div>
            <div className="choose-recipe-cntnt-box cook-recipe">
              <h1 className="mb-hide">
                {
                  lastStep ?
                    'Last Step: Adjust Servings'
                    :
                    'Assign to Plan'
                }
              </h1>
              <p className="after-hdng mb-hide">
                {
                  lastStep ?
                    `${'Set the amount of servings for each meal. We’ve automatically added'} ${localStorage.getItem('default_serving_size')} ${'servings for each meal based on your account settings.'}`
                    :
                    'Select the day(s) you want to eat each recipe to match your schedule this week. Feel free to assign a recipe to multiple days on the calendar… after all, who doesn’t love leftovers? Happy planning!'
                }
              </p>
            </div>
            <div className={`dishes-row-box select-cook ${slideClass}`}>
              <div className="">
                <div className="cal-days-cntnt dishes-row cooking-page">
                  {
                    recipeArr.length > 0 && !lastStep ?
                    // this.recipeData('test')
                      <RecipeData
                        length='test'
                        recipeArr={recipeArr}
                        lastStep={lastStep}
                        cookingDate={cookingDate}
                        cookingDays={(index)=>this.cookingDays(index)}
                        selectCookDay={(indexV, data, cookingDays)=>this.selectCookDay(indexV, data, cookingDays)}
                        editCookDay={(index)=>this.editCookDay(index)}
                        cookArr={cookArr}
                        setServingSize={(index, cookDate,getDate ,getServingSize,recipeId)=>this.setServingSize(index, cookDate,getDate ,getServingSize,recipeId)}
                        location="cook"
                        viewRecipe={()=>this.viewRecipe()}
                      />
                      :
                      this.sortRecipeData()
                  }

                </div>               
              </div>
            </div>
                
            <CookBottom
              totalCooking={cookArr}
              count={count}
              recipeArr={recipeArr}
              onPressNext={()=>this.finalStep()}
              lastStep={lastStep}
              estimateServings={totalLength}
              totalServing={totalServing}
              cookDayArr={cookDayArr}
              dayDates={dayDates}
              error={error}
              cookingDate={cookingDate}
            />
          </div>
        </section>
      </div>
    );
  }
}

export default SelectCookDays;

