import React, { Component } from 'react'
import moment from 'moment'
import { browserName } from 'react-device-detect'

class EditCookDay extends Component {
  constructor(props) {
    super(props)
    this.state={
      dayDates: [],
      indexValue: props.indexValue
    }
  }

  componentDidMount() {
    this.setWeek()
  }

  setWeek() {
    let cookDayText = 'Other Cook Day'
    if(this.props.location && this.props.location === 'dashboard') {
      cookDayText = 'Move meal to a different day'
    }
    const dayDates = this.props.dayDates
    this.setState({ dayDates, cookDayText });

  }

  selectCookDay(cookDay, cookDate) {
    this.setState({ cookDay, cookDate })
  }

  render(){
    const { dayDates, cookDay, indexValue, cookDate, cookDayText } = this.state
    const listClass = browserName.indexOf('Edge') > -1 ? 'ie-design' : ''
    return(
      <section className="dashboard-main">
        <div className="container">
          <div id="schedule-popup" className="modal fade in" role="dialog" style={{ display: 'block', overflowY:'hidden' }}>
            <div className="modal-dialog">
              <div className="modal-content edt-cook-popup" style={{background: 'transparent', boxShadow: 'none'}}>
                <div className="select-meal-cntnt-box other-copy">

                  <h2 className="mb-hide">{cookDayText}: </h2>

                  <p className="after-hdng color-change"> Select which day you want to eat this recipe.</p>

                  <div className="week-food-days left-space no-space">

                    <ul>
                      {
                        dayDates.length > 0 &&
                      dayDates.map((days) =>{
                        const dayDate = `${days.Year}-${days.Month}-${days.date}`;
                        const weekDays = moment(dayDate).format('ddd')
                        return(
                          <li key={weekDays} className={listClass}>
                            <input type="radio" onChange={()=>this.selectCookDay(weekDays, dayDate)} name='cooking' />
                            <span className="checkmark1 mb-hide alignt" tabIndex="0" role="button">
                              {weekDays}
                            </span>
                           
                          </li>      
                        )
                      } )
                      }
                    </ul>
                  </div>
                  <div className="select-meal-btns edit-cook">
                    <button className="next-btn pop-up-button-white "> 
                      <span aria-label="Next" onClick={()=>this.props.saveCookDay(indexValue, cookDay, cookDate)}> Save </span>                  </button>
                    <button className="next-btn pop-up-button "> 
                      <span aria-label="Next" onClick={()=>this.props.cancelCookDay()}> Cancel </span>
                    </button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        
      </section>
    )
  }
}
export default EditCookDay