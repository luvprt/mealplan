import React from 'react'
import _ from 'lodash'

// import moment from 'moment'

// cook page bottom part cookDays design
// const LastStep = (props) => {
//   const { lastStep, cookDayArr, dayDates, estimateServings } = props
//   let daycount = 0
//   let cookCount = 0
//   const length = Object.keys(cookDayArr).length
//   return(
//     <div style={{ float: 'left', width:'100%'}}>
//       {
//         dayDates.length > 0 && !lastStep &&
//           dayDates.map((days) => {
//             const dayDate = `${days.Year}-${days.Month}-${days.date}`;
//             let dayClass = ''
                
//             const cookDay = moment(dayDate).format('ddd')
//             if(daycount > 1 && window.innerWidth < 768 ) {
//               dayClass = 'mb-hide'
//             } else {
//               dayClass = ''
//             }
//             if(cookDayArr[cookDay] && dayClass !== 'mb-hide') {
//               daycount += 1
//               return (
//                 <span 
//                   className={`set-cookday ${dayClass}`} 
//                   key={dayDate}
//                 >
//                   {cookDayArr[cookDay]} {cookDay}
//                   <span>.&nbsp;</span>
//                 </span>
//               )
//             }
//             if(dayClass === 'mb-hide' && cookDayArr[cookDay] && cookCount === 0) {
//               cookCount +=1
//               return (
//                 <div className='set-cookday' key={dayDate}>+{length - 2} More</div>
//               )
//             }
//             return null
//           })
//       } 
//       {
//         lastStep &&
//         <span className='set-cookday'>Estimated Need: {estimateServings}  </span>
//       }
//     </div>
//   )
// }

// cook page bottom part

const CookBottom = (props) => {
  const {cookingDate, totalCooking,  recipeArr, lastStep, error } = props
  // const disable = totalCooking.length === recipeArr.length
  const disable = _.uniqBy(totalCooking,'key').length === recipeArr.length
  // const setCookDays = count > 0 ? `${count} ` : 'No'
  const activeClass = (disable || lastStep) ? '-green' : ''
  const btnTxt = lastStep ? 'Finalize' : 'Next'
  let totalServing = 0
  // const unassignedMeal = recipeArr.length - totalCooking.length 
  // const meal = recipeArr.length - totalCooking.length > 1 ? 'meals' : 'meal'
  const unassignedMeal = recipeArr.length - _.uniqBy(totalCooking,'key').length 
  const meal = recipeArr.length - _.uniqBy(totalCooking,'key').length > 1 ? 'meals' : 'meal'
  // if(lastStep) {
  //   recipeArr.map((item) =>{
  //     totalServing += parseInt(item.servingSize,0)
  //     return null;
  //   })
  // }
  if(lastStep) {
    cookingDate.map((item) => {
      item.recipe.map((getRecipe) =>{
        totalServing += parseInt(getRecipe.serving_size,0)
        return null;
      })
      return null;
    })
  }
  const servingText = totalServing > 1 ? 'Servings' : 'Serving'
  return(
    <div className="choose-recipe-btns select-meal-btns selected bottom-space">
      <div className="container">
        <div className="recipe-selected-status main-cook-set">
          <p className={error ? 'mb-hide cook-days error-msg':"mb-hide cook-days"} > 
            {/* {error ?
              `Error: ${recipeArr.length - totalCooking.length} ${meal} without a cook day`
              :
              lastStep ?
                `Servings: ${totalServing} `
                :
                `${setCookDays} Cook Days Set `
            } */}
            {error ?
              `Error: ${recipeArr.length - _.uniqBy(totalCooking,'key').length} ${meal} Unassigned`
              // `Error: ${recipeArr.length - totalCooking.length} ${meal} Unassigned`
              :
              lastStep ?
                `${servingText}: ${totalServing} `
                :
                unassignedMeal > 0 ?
                  `${unassignedMeal} ${meal} Unassigned `
                  :
                  <span className="assignedMeal"> All Meals Assigned! </span>
            }
          </p>
          {/* <LastStep {...props}/> */}
        </div>
        <button
          className={`next-btn outer-boder button-small-device${activeClass}`} 
          onClick={()=>props.onPressNext()}
        >
          <span aria-label="Next">{btnTxt}</span>
        </button>
      </div>
    </div>
  )
}

export default CookBottom