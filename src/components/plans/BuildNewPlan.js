import React from 'react'

const BuildNewPlan = (props) => (
  <div>
    <div className="heade_title ">
      <span>
        <h4 className="gray"> {window.innerWidth > 768 ? 'No Cook Days Set' : 'Create a New Plan'} </h4>
      </span>              
    </div>
    <div className="create-plan-popup-box">
      <div className="card-bg text-center">
        <h4> New Week, New Plan! </h4>
        <p> Let's get you prepped and ready to conquer another week with a healthy meal plan. </p>
        <button type="button" className="btn btn-success create plan" onClick={()=>props.createPlan(props.dayDates)}> 
          Create Plan 
        </button>
      </div>
    </div>
  </div>
)

export default BuildNewPlan