import React, {Component} from 'react'
import AddFeedbackPopup from './AddFeedbackPopup'

export const RatingPopup = (props) => {
  const { showContent } = props
  const showRating  = showContent === 'rating'
  const topHeading = showContent === 'rating' ? 'Way to go Ninja !' : 'Thank you for the feedback!'
  const btnContent = showRating ? 'Next' : `Let's Do It!`
  const ratingSubmitClass = showRating ? '' : `sbmt-rtng`
  return (
    <div className="rtng-feedbck">
      <div className="row">
        <div className="col-md-2">
          <img src="/images/step1.svg" alt="ninja" />
        </div>
        <div className="col-md-10">
          <h4 className="mb-hide">{topHeading} </h4>
          {showRating ? <h4 className="mb-only"> Way to go
            <span> Ninja ! </span></h4>
            :
            <h4 className="thnk-msg-hdng mb-only">{topHeading} </h4>
          }
          {showRating && <p className="mb-hide"> Your first plan is built and you’re ready to rock this week! </p>}
        </div>
      </div>
      <div className="row mb-only stp1-subhdng">
        <div className="col-md-12">
          {showRating && <p className="mb-only"> Your first plan is built and you’re ready to rock this week! </p>}
        </div>


      </div>
      <div className="row rating-row">
        <div className="col-md-12">
          {showRating ? 
            <p> How would you rate your first planning experience? </p>
            :
            <p className="thnk-msg-subhdng"> Your plan is ready! From your weekly plan you can: </p>
          }
          {showRating ?
            <span>
              { 
                props.ratingArr.map((item, index)=> {
                  let ratingClass = "fa fa-star-o"
                  if(props.ratingIndex>=index || props.setRatingIndex >=index) {
                    ratingClass = "fa fa-star"
                  }
                  return( 
                    <i className={ratingClass} aria-hidden="true" key={item} onMouseOver={()=>props.hoverRating(index)}  onTouchMove={()=>props.hoverRating(index)} onMouseOut={()=>props.hideRating()} onClick={()=>props.setRating(index)}/>
                  )
                }) 
              }
            </span>
            :
            <ul className="thnk-msg">
              <li>	View, Edit & Print Grocery List </li>
              <li>	Edit This Week’s Plan </li>
              <li>	Switch Weeks to Create a New Plan for a Future Week</li>
            </ul>
          }
        </div>
      </div>
      <div className="row rating-nxt-btn">
        <button type="button" className={`btn btn-success create plan ${ratingSubmitClass}`} onClick={()=>props.onPressNext()}> 
          {btnContent}
        </button>
      </div>
    </div>
  )
}
class FeedbackPopup extends Component { 
  constructor(props) {
    super(props)
    this.state = {
      ratingIndex: -1,
      setRatingIndex: -1,
      addFeedbackScreen: false,
      showContent: 'rating',
      inputs:{},
      error: false
    }
  }
  
  onPressNext() {
    const { setRatingIndex, showContent, inputs } = this.state
    if(setRatingIndex > -1 && showContent === 'rating') {
      inputs.feedback = setRatingIndex+1
      this.props.submitFeedback(inputs)
      this.setState({ addFeedbackScreen: true, inputs})
    } else if(showContent === 'message'){
      this.props.removeFeedbackPopup()
    }
  }

  setRating(setRatingIndex) {
    this.setState({ setRatingIndex })
  }
  
  hoverRating(ratingIndex) {
    this.setState({ ratingIndex })
  }

  hideRating() {
    this.setState({ ratingIndex: -1})
  }

  submitFeedback(value) {
    const { inputs } = this.state
    let{ error } = this.state
    if(value === 'skip') {
      inputs.comment = 'skip'
      error = false
    }else if(!inputs.comment){
      error = true
    }
    if(!error) {
      this.props.updateFeedback(inputs)
      this.setState({ addFeedbackScreen: false, showContent: 'message'})
    }
    this.setState({ error: true })
  }
  
  // eslint-disable-next-line
  updateValue(event) {
    const { inputs } = this.state
    inputs.comment = event.target.value
    this.setState({ inputs, error: false})
  }

  render(){
    const ratingArr = [1,2,3,4,5];
    const { addFeedbackScreen } = this.state
    const feedbackclass = addFeedbackScreen   ? "feedback-popup" : "" 
    return(
      <div className="rating-popup">
        <div className="create-plan-popup-box">
          <div className={`card-bg ${feedbackclass}`}>
            {!addFeedbackScreen ?
              <RatingPopup 
                hideRating={(index)=>this.hideRating(index)}
                hoverRating={(index)=>this.hoverRating(index)}
                setRating={(index)=>this.setRating(index)}
                onPressNext={(index)=>this.onPressNext(index)}
                {...this.state}
                ratingArr={ratingArr}
              />
              :
              <AddFeedbackPopup 
                onSubmit={(index)=>this.submitFeedback(index)}
                updateValue={(event)=>this.updateValue(event)}
                {...this.state}
              />
            }
            
          </div>
        </div>
      </div>
    )
  }
}

export default FeedbackPopup