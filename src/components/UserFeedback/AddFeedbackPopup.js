import React from 'react'

const AddFeedbackPopup = (props) => (
  <div>
    <div className="add-feedback">
      <p>Why did you give that rating? </p>
      <span> What did you like / dislike about the planning experience? </span>
      <div className="fdbck-txtarea">
        <textarea 
          className="textarea" 
          placeholder= "Please explain here..." 
          onChange={(event)=>props.updateValue(event)}
          // eslint-disable-next-line
          autoFocus
        />
        { props.error ?
          <span className="error-msg">Please enter the comment</span> : null
        }
      </div>
      <div className="nxt-stp">
        <button type="button" className="skip-feedback" onClick={()=>props.onSubmit('skip')}> 
        Skip
        </button>
        <button type="button" className="btn btn-success create plan" onClick={()=>props.onSubmit('next')}> 
        Next
        </button>
      </div>
    </div>
  </div>
)

export default AddFeedbackPopup