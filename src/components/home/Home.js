/*
Component Name: Home component
Included components: Header
*/

import React from 'react';
import Header from '../Header';

const Home = () => (
  <div className="wrapper transition-item">
    <Header/>
    <section className="homepage-cntnt">
      <div className="container">
        <h2>The Faster & Easier Way To Eat Healthy</h2>
        <p>Custom Meal Planning Software</p>
        <p>Coming Soon</p>
        <p>For Busy People Who Want To Eat Healthier In Less Time</p>
        <div className="home-banner-btn">
          <a href="">Get Early Access</a>
        </div>
      </div>
    </section>
    <div className="home-social">
		 <a href="">
        <img src="/images/social-side-icn.png" alt="" />
      </a>
		 </div>
    <section className="homepage-cntnt-inner">
      <div className="container">
        <a className="contact-home-icn">
          <i className="fa fa-envelope" />
        </a>
        <p>Meal Planning Software That Saves Time And Reduces Food Wast</p>
      </div>
    </section>
  </div>
)
  


export default Home;
