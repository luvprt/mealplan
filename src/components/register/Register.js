import React, { Component } from 'react';
import RoutingConstants from 'lib/RoutingConstants';
import { getGuestData, updateGuestData, removeTag, post } from 'lib/api';
import moment from 'moment';
import Basic from './Basic';
import MealPlan from './MealPlan';


class Register extends Component {
  constructor(props) {
    super(props);
    console.log(props.location)
    this.state = {
      currentStep: props.location.state ? props.location.state.currentStep : 'basic',
    };
  }

  componentWillReceiveProps(props){
    if(props.location.state && props.location.state.currentStep === 'mealPlan'){
      this.setState({ currentStep: props.location.state.currentStep });
    }
  }

  async onClickFinish(data) {
    const guestData = sessionStorage.getItem('guestAccount') ?
      JSON.parse(sessionStorage.getItem('guestAccount')) : null;
    if (guestData) {
      const guestEmail = guestData.basic.email;
      const inputData = {};
      inputData.email = guestEmail;
      const responseJson = await getGuestData(inputData);
      if (responseJson && responseJson.result_code === 1) {
        inputData.id = responseJson.id;
        inputData.tags = '';
        inputData['p[6]'] = 6;
        inputData['field[%PEOPLE_COOKING_FOR%, 0]'] = data.PEOPLE_COOKING_FOR;
        // inputData['field[%COOKING_STYLE%, 0]'] = guestData.mealPlan.COOKING_STYLE;
        inputData['field[%DIET_TYPE%, 0]'] = data.DIET_TYPE;
        inputData['field[%FREE_TRIAL_SIGN_UP_DATE%, 0]'] = moment();
        updateGuestData(inputData).then(() => {
          const inputs = {};
          inputs.email = guestEmail;
          inputs.tags = 'Incomplete Trial Sign Up';
          removeTag(inputs).then((responseJson2) => {
            if (responseJson2 && responseJson2.result_code === 1);
          });
        });
        if (guestEmail && guestEmail != null) {
          const url = RoutingConstants.mealPlanStyles;
          post(url,data).then((response) =>{
            // eslint-disable-next-line
            console.log(response);

            if(data.DIET_TYPE.length > 0) {
              window.analytics.track('New Signup Completed', {
                specialDiet: 'Yes',
                numberOfPeople: data.PEOPLE_COOKING_FOR,
                dietType: data.DIET_TYPE,
                email: guestEmail
              });
            } else {
              window.analytics.track('New Signup Completed', {
                specialDiet: 'No',
                numberOfPeople: data.PEOPLE_COOKING_FOR,
                email: guestEmail
              });
            }
            this.props.history.push('/onboarding',{servingSize: data.PEOPLE_COOKING_FOR});
            
          }).catch(err=>{
            // eslint-disable-next-line
            console.log(err);
          })
        } else {
          this.props.history.push('/login');
        }
      }
    }
  }

  onClickNext(currentState, nextState, data) {
    let guestAccount = {};
    const guestAccountData = sessionStorage.getItem('guestAccount');
    if (guestAccountData && guestAccountData != null) {
      guestAccount = JSON.parse(guestAccountData);
    }
    guestAccount[`${currentState}`] = data;
    sessionStorage.setItem('guestAccount', JSON.stringify(guestAccount));
    this.setState({ currentStep: nextState });
  }

  renderContent() {
    switch (this.state.currentStep) {
    case 'basic':
      return (
        <Basic onClickNext={data => this.onClickNext('basic', 'mealPlan', data)} {...this.props} />
      );

    case 'mealPlan':
      return (
        <MealPlan
          onClickNext={data => this.onClickFinish(data)}
          onClickBack={() => { this.setState({ currentStep: 'basic' }); }}
          {...this.props}
        />
      );
    default:
    }
    return this.state.currentStep;
  }
  render() {
    const { history } = this.props;

    return (
      <div className="wrapper" style={{ transition: 'ease-in 0.5s'}}>
        <header>
          <div className="container">
            <img src="/images/logo.png" alt="" onClick={() => { history.push('/'); }} />
          </div>
        </header>
        {this.renderContent()}

        <section className="copyright">
          <div className="container">
            <p>© Copyright - Meal Plan Ninja, Inc. & Niche Navigator, LLC 2018</p>
          </div>
        </section>
      </div>
    );
  }
}

export default Register;
