/*
Component Name: Basic
Description: First step of registeration.
*/

import React, { Component } from 'react';
import OwlCarousel from 'react-owl-carousel';
import 'owl.carousel/dist/assets/owl.carousel.css';
// import 'owl.carousel/dist/assets/owl.theme.default.css';

export default class Onboarding extends Component {
  constructor(props) {
    super(props);
    this.backBtn = React.createRef();
    this.nextBtn = React.createRef();
    const servingSize = this.props.location.state && this.props.location.state.servingSize !== undefined? this.props.location.state.servingSize : 2;
    const startPosition = this.props.location.state && this.props.location.state.startPosition !== undefined ? this.props.location.state.startPosition : 0;
    let navText = ['Skip', 'Next']
    let prev = 0
    if(startPosition > 0 ){
      navText = ['Back', 'Lets Do It']
      prev = 1
    }
    this.state = {
      prev,
      servingSize,
      sliderOptions: {
        margin: 10,
        padding:5,
        nav:true,
        center: true,
        mouseDrag: false,
        rewind: true,
        startPosition,
        navText,
        responsive:{
          0: {
            items: 1,
          },
          600: {
            items: 1,
          },
          1000: {
            items: 1,
          },
        },
      },
    };
  }

  componentDidMount(){
    if(!localStorage.getItem('token')) {
      this.props.history.push('/')
    }
  }


  changeSlider(e){
    let { prev } = this.state
    const { servingSize } = this.state
    const email = localStorage.getItem('email')
    if (e.property.name === 'position') {

      const sliderOptions = Object.assign({}, this.state.sliderOptions);
      sliderOptions.startPosition = e.property.value;
      if(prev === 1 && e.property.value === 0 && e.item.index === 4) {
        window.analytics.track('Onboarding Tour Complete', { email});
        // this.props.history.push('/selectMeals', {servingSize, email })
        this.props.history.push('/chooseRecipe', {servingSize, email })
      }
      if (sliderOptions.startPosition === 4) {
        sliderOptions.navText = ['Back','Lets Do It'];
      }else if (sliderOptions.startPosition === 0) {
        sliderOptions.navText = ['Skip','Next'];
      }else {
        sliderOptions.navText = ['Back','Next'];
      }
      if((e.property.value === 4 && e.item.index === 0) || (e.property.value === 4 && e.item.index === 3)) {
        prev = 1
      } else {
        prev = 0
      }
      if(prev === 1) {
        window.analytics.track('Pressed Skip Onboarding',{email});
      }
      this.setState({sliderOptions, prev});
    }

  }

  render() {
    const { sliderOptions } = this.state;
    return (
      <div className="wrapper">
        <header>
          <div className="container">
            <a href="/"><img src="images/logo.png" alt="" /></a>
          </div>
        </header>
        <section className="onboarding-cntnt">
          <div className="container">
            <div className="onboarding-cntnt-box">
              <div className="onboarding-cntnt-lft-hdng">
  		<h2 className="text-center">Welcome Ninja!</h2>
		   </div>
		   <div className="onboarding-cntnt-main">
                <OwlCarousel
                  className="owl-theme"
                  {...sliderOptions}
                  onChange={(e)=>this.changeSlider(e)}
                >
                  <div className="item">
                    <h2 className="mb-hide">Let’s Give You A Quick Intro</h2>
                    <h2 className="mb-only">Welcome Ninja</h2>
                    <div className="hdng-img-box"><img src="images/step1.svg" alt="" /></div>
                    <p className="big-font-para">Meal planning and prep is easier and healthier with Meal Plan Ninja!</p>
                  </div>
                  <div className="item">
            				<div className="img-box-nw"><img src="images/step2.svg" alt="" /></div>
            				<h4>1. Meal Plan on Your Schedule</h4>
            				<p>Hint: Check your calendar as you plan, it helps to make sure you have enough food for the week</p>
            			</div>
                  <div className="item">
                    <div className="img-box-nw"><img src="images/step3.svg" alt="" /></div>
                    <h4>2. Make a Plan in 5 Mins or Less!</h4>
                    <p>Create a new menu, or select meals from a previous week. Our carefully selected recipes are always healthy, simple & fast!</p>
                  </div>
                  <div className="item">
              			<div className="img-box-nw"><img src="images/step4.svg" alt="" /></div>
              			<h4>3. Grocery Lists Made Easy</h4>
              			<p>Your grocery list is created automatically, based on your menu and number of servings.</p>
              		</div>
                  <div className="item">
            				<div className="img-box-nw"><img src="images/step5.svg" alt="" /></div>
            				<h4>Smile! Meal Prepping  means you’re ready for the week!</h4>
            				<p>Open your fridge, tupperware is stacked and ready to go. You’re a Meal Plan Ninja!</p>
            			</div>
                </OwlCarousel>

		          </div>
            </div>
          </div>
        </section>
      </div>
    );
  }
}
