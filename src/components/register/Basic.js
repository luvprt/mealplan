/* eslint-disable no-underscore-dangle */
/*
Component Name: Basic
Description: First step of registeration.
*/

import React, { Component } from 'react';
import RoutingConstants from 'lib/RoutingConstants';
import  {Link}  from 'react-router-dom'
import { validateInputs } from 'lib/services';
import { registerGuestUser } from 'lib/api';
import ReactHtmlParser from 'react-html-parser';
import history from '../../history';
import SocialLoginButton from '../login/SocialLoginButton'
import FacebookLogo from '../../assets/images/facebook-signup.png';
import GoogleLogo from '../../assets/images/google-signup.png';

export default class Basic extends Component {
  constructor(props) {
    super(props);
    this.state = {
      inputs: {},
      errorMessage: {},
      isLoading: false,
      accept: false,
    };
  }

  onClickNext() {
    const { email, password,
    } = this.state.inputs;
    const { errorMessage, inputs, accept } = this.state;
    window.analytics.track('Create Account', {
      first_name: inputs.first_name,
      email,
      id: email
    }, {
      integrations: {
        'All': true,
        'Facebook Pixel': false,
      }
    });
    const dateStr = `${this.state.year}-${this.state.month}-${this.state.day}`;
    if (!validateInputs('alphabetics', inputs.first_name)) {
      errorMessage.first_name = 'Enter your first name';
    } else {
      errorMessage.first_name = false;
    } if (!validateInputs('email', email)) {
      errorMessage.email = 'Please enter a valid email address';
    } else {
      errorMessage.email = false;
    } if (!validateInputs('password', password)) {
      errorMessage.password = 'Create a secure password. It must contain 8+ characters with 1 number or special character.';
    } else {
      errorMessage.password = false;
    } if(!accept) {
      errorMessage.terms_service_consent = 'In order to sign up with Meal Plan Ninja, you must accept the terms of service and privacy policy.';
    } else {
      errorMessage.terms_service_consent = false;
    }

    if (!errorMessage.first_name &&
        !errorMessage.email &&
        !errorMessage.password &&
        !errorMessage.terms_service_consent
    ) {
      this.setState({ isLoading: true }, () => {
        const Inputs = this.state.inputs;

        const url = RoutingConstants.register;
        Inputs.date_of_birth = dateStr;

        registerGuestUser(url, Inputs).then((responseJson) => {
          if (responseJson.result) {
            sessionStorage.removeItem('guestAccount');
            localStorage.removeItem('lastName');
            localStorage.setItem('token', responseJson.result.token);
            localStorage.setItem('firstName', responseJson.result.first_name);
            localStorage.setItem('email', responseJson.result.email);
            localStorage.setItem('analytics', 'User New SignUp did');
            localStorage.removeItem('isTourCompleted');
            window.analytics.identify(Inputs.email, {
              first_name: Inputs.first_name,
              email: Inputs.email,
              id: Inputs.email,
            });
          }
          this.props.onClickNext(Inputs);
        }).catch((err) => {
          if (err.error && err.error.email[0] === 'The email has already been taken.') {
            errorMessage.email =
              ` <div>
              This email already exists. Please go to the
              <a href=/login>
                login
              </a>
              page.
              </div>`;
            errorMessage.email = ReactHtmlParser(errorMessage.email);
            window.analytics.track('Error Create Account', {
              email: Inputs.email,
              error: 'User Already Exist',
            });
          }
          this.setState({ isLoading: false, errorMessage });
        });
      });
    } else {
      this.setState({ errorMessage });

      // track error
      window.analytics.track('Error Create Account', {
        error: errorMessage,
      });
    }
  }

  updateInputs(ref, text) {
    const {inputs, errorMessage} = this.state;
    let { accept } = this.state;
    errorMessage[`${ref}`] = ''
    inputs[`${ref}`] = text.trim();
    if( ref === "terms_service_consent" ) {
      accept = !accept
      inputs[`${ref}`] = accept;
    }
    this.setState({ inputs, accept });
  }


  handleSocialLogin(user){
    const userProfile = user._profile;
    userProfile.loginType = user._provider;    
    // email, firstName ,id, lastName , name , profilePicURL
    if (userProfile.name && userProfile.email) {
      const url = RoutingConstants.socialLogin;

      registerGuestUser(url, userProfile).then(async (response) => {
        if (response.result) {
          sessionStorage.removeItem('guestAccount');
          localStorage.removeItem('lastName');         
          localStorage.setItem('analytics', 'User New SignUp did');
          localStorage.removeItem('isTourCompleted');          
          localStorage.setItem('firstName', response.result.first_name);
          localStorage.setItem('token', response.result.token);
          if(response.result.last_name) {
            localStorage.setItem('lastName', response.result.last_name);
          }
          localStorage.setItem('email', response.result.email);
          if (!response.result.remainder) {
            localStorage.setItem('test', '');
          }
          localStorage.setItem('type', 'login');
          localStorage.removeItem('role');
          window.analytics.identify(response.result.email, {
            first_name: response.first_name,
            last_name: response.last_name,
            email:response.result.email,
          },{
            integrations: {
              'All': true,
              'Facebook Pixel': false,
            }
          });

          setTimeout(()=>{
            if(response.result.alreadyUser === true){           
              history.push('/dashboard', {
                guestEmail: response.result.email, type: '/login', reminder: response.result.remainder
              });
            }else{
              // stored the guest account data in seesion strorage similar to the Direct signup          
              sessionStorage.setItem('guestAccount', JSON.stringify({basic:{email:response.result.email}}));          
              history.push('/register',{currentStep:'mealPlan'});
            }
          },500);                  
          
        }
      }).catch(() => {
        this.setState({errorMessage: 'Something went wrong on server.'})
      });     
    } else {
      this.setState({errorMessage:'Something went wrong in Create Account!!'})

      // track error
      window.analytics.track('Error Create Account', {
        error: 'Something went wrong',
      });
    }
  }
   
  handleSocialLoginFailure(err){
    if(err){
      this.setState({errorMessage:'Something went wrong!!'})
    }
  }

  render() {
    const { errorMessage } = this.state;
    return (
      <section className="main-cntnt slidetest" style={{right: '-300px'}}>
        <div className="container">
          <div className="main-cntnt-box">
            <h2 className="mb-hide">Ready to Become A Ninja?</h2>
            <h2 className="mb-only">Ready to Be A Ninja?</h2>
            <p className="hdng-para">
            Healthy, simple, fast and easy. That’s how we like our meal plans!
Get ready for total control over your health and food.
            </p>
            <div className="main-cntnt-inner">
              <div className="col-xs-12 col-md-6 intro-sectn">
                <div className="timeline">
                  <div className='timeline-row active'>
                    <div className="timeline-content">
                      <span />
                      <p>The Basics</p>
                    </div>
                  </div>
                  <div className='timeline-row'>
                    <div className="timeline-content">
                      <span />
                      <p>Meal Plan Style</p>
                    </div>
                  </div>
                  <div className='timeline-row last-row-timeline'>
                    <div className="timeline-content">
                      <span />
                      <p>Build Your First Plan</p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-xs-12 col-md-6 login-sectn">
                <div>
                  <h4 className="mb-hide">Let{'\''}s Start with Some Basics</h4>
                  <div className="row">
  		              <h4 className="mb-only text-left new-step-mb">Step 1: The Basics</h4>
  		            </div>
                  {errorMessage.error ? <span className="error-msg" style={{ position: 'static' }}>{errorMessage.error}</span> : null}
                  <div className="row">
                    <SocialLoginButton
                      provider='facebook'
                      appId='1306170349549515'
                      onLoginSuccess={(e) => this.handleSocialLogin(e)}
                      onLoginFailure={(e) =>this.handleSocialLoginFailure(e)}
                    >
                      <img src={FacebookLogo} alt="Login with Faceebook"/>
                    </SocialLoginButton>
                    <SocialLoginButton
                      provider='google'
                      appId='726295967537-7q1n60a4cr9q22gcs5ttnmu1cp3epdbp.apps.googleusercontent.com'
                      onLoginSuccess={(e) => this.handleSocialLogin(e)}
                      onLoginFailure={(e) =>this.handleSocialLoginFailure(e)}
                    >
                      <img src={GoogleLogo} alt="Login with Google"/> 
                    </SocialLoginButton>
                  </div>
                  <div className="row">
                    <h5>&nbsp;</h5>
                  </div>
                  <div className="row">
                    <div className="col-md-12 form-row email-label">
                      <label htmlFor="first_name" className={errorMessage.first_name ? 'error-label' : null}>Name:</label>
                      <input type="text" onChange={e => this.updateInputs('first_name', e.target.value)}  />
                      {errorMessage.first_name ? <span className="error-msg">{errorMessage.first_name}</span> : null}
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-12 form-row email-label">
                      <label htmlFor="email" className={errorMessage.email ? 'error-label' : null}>Email:</label>
                      <input type="email" onChange={e => this.updateInputs('email', e.target.value)}  />
                      {errorMessage.email ? <span className="error-msg">{errorMessage.email}</span> : null}
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-12 form-row pass-row">
                      <label htmlFor="password" className={errorMessage.password ? 'error-label' : null}><span className="mb-hide">Create</span> Password:</label>
                      <span className="hint-msg mb-only">8+ characters & 1 number or symbol.</span>
                      <input
                        className="pwdInput"
                        type="password"
                        onChange={e => this.updateInputs('password', e.target.value)}
                      />
                      {errorMessage.password ?
                        <span className="error-msg">{errorMessage.password}</span>
                        :
                        <span className="hint-msg mb-hide">8+ characters with 1 number or symbol</span>
                      }
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-12 form-row pass-row">

                      <label className={errorMessage.terms_service_consent ? 'container1 terms-condition error-label' : 'container1 terms-condition'}>

                        <input
                          type="checkbox"
                          name="terms_service_consent"
                          onChange={e => this.updateInputs('terms_service_consent', e.target.value)}
                          style={{width:'auto'}}
                          tabIndex="-1"
                        />
                        <span className="checkmark1" tabIndex="0" role="button"/>


                      </label>
                      <span className="terms-condition" style={{display:'block'}}>
            By creating an account, you agree to our
                        <a href="https://mealplanninja.com/terms-of-service/" target='_blank' rel="noopener noreferrer">Terms of Service</a>
              and
                        <a href="https://mealplanninja.com/privacy-policy/" target='_blank' rel="noopener noreferrer">Privacy Policy</a>
                      </span>

                      {errorMessage.terms_service_consent &&
              <span className="error-msg">{errorMessage.terms_service_consent}</span>
                      }
                    </div>
                  </div>
                  <div className="row">
                    <div className="form-row text-right col-md-12 next-btn-box">
                      {this.state.isLoading ?
                        <button type="button" className="next-btn" disabled>Loading...</button> :
                        <button type="button" className="next-btn" onClick={() => this.onClickNext()}>Next</button>
                      }
                    </div>
                  </div>
                  <div className="row">
                    <p className="dont-have-ac-p signup-link"> Already have an account?&nbsp;<Link to="/login">Login</Link></p>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

    );
  }
}
