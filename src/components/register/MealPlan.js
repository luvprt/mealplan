import React, { Component } from 'react';
import Loader from 'lib/loader'

export default class MealPlan extends Component {
  constructor(props) {
    super(props);
    this.state = {
      PEOPLE_COOKING_FOR: 2,
      DIET_TYPE: [],
      SPECIFIC_DIET: false,
      errorMessage: {},
      otherDiet: false,
      userName: null,
      loadData: false
    };
  }

  componentDidMount() {
    this.loadData();
  }

  onClickNext() {
    const { PEOPLE_COOKING_FOR, DIET_TYPE, SPECIFIC_DIET, otherDiet } = this.state;
    const errorMessage = this.state.errorMessage;
    this.setState({ loadData: true})
    if (PEOPLE_COOKING_FOR && SPECIFIC_DIET) {
      if (SPECIFIC_DIET === 'No') {
        this.props.onClickNext({ PEOPLE_COOKING_FOR, DIET_TYPE: [] });
      }else if (DIET_TYPE.length > 0) {
        if (DIET_TYPE.includes('others')) {
          if (otherDiet === '' || !otherDiet) {
            errorMessage.DIET_TYPE = 'Please Choose diet type';
            this.setState({errorMessage, loadData: false})
          }else {
            const index = DIET_TYPE.indexOf('others')
            if(index > -1) {
              DIET_TYPE[index] = otherDiet
            }
            this.props.onClickNext({ PEOPLE_COOKING_FOR, DIET_TYPE});
          }
        }else {
          this.props.onClickNext({ PEOPLE_COOKING_FOR, DIET_TYPE });
        }
      }else {
        errorMessage.DIET_TYPE = 'Please Choose diet type';
        this.setState({errorMessage, loadData: false})
      }


    } else {
      if (!PEOPLE_COOKING_FOR) {
        errorMessage.PEOPLE_COOKING_FOR = 'Please Select people cooking for value from dropdown';
      } else {
        errorMessage.PEOPLE_COOKING_FOR = false;
      }
      if (!SPECIFIC_DIET) {
        errorMessage.SPECIFIC_DIET = 'Please Select specific diet';
      } else {
        if (DIET_TYPE.length === 0) {
          errorMessage.DIET_TYPE = 'Please Choose diet type';
        } else {
          errorMessage.DIET_TYPE = false;
        }
        errorMessage.SPECIFIC_DIET = false;
      }
      this.setState({ errorMessage, loadData: false });
    }
  }

  updateList(itemKey, itemVal){
    const { DIET_TYPE } = this.state
    const index = DIET_TYPE.indexOf(itemVal)
    if(index === -1) {
      DIET_TYPE.push(itemVal)
    } else {
      DIET_TYPE.splice(index,1)
    }
    this.setState({DIET_TYPE});
  }

  async loadData() {
    const userName = await localStorage.getItem('firstName');
    this.setState({userName});
    const data = await sessionStorage.getItem('guestAccount') ?
      JSON.parse(sessionStorage.getItem('guestAccount')) : null;
    if (data && data.mealPlan) {
      this.setState(data.mealPlan);
    }
  }

  render() {
    const { errorMessage, SPECIFIC_DIET, DIET_TYPE, userName, loadData } = this.state;
    return (
      <section className="main-cntnt slidetest" style={{right: '-300px'}}>
        <div className="container">
          <div className="main-cntnt-box">
            {
              loadData &&
              <div className="loading">
                <Loader />
                <div className="modal-backdrop fade in" />
              </div>
            }
            <h2 className="mb-hide">Great! Nice to Meet You {userName}</h2>
            <h2 className="mb-only">Great! Nice to meet you {userName}!</h2>
            <p className="hdng-para">We just need to know a couple things to get you started.</p>
            <div className="main-cntnt-inner">
              <div className="col-xs-12 col-md-6 intro-sectn">
                <div className="timeline">
                  <div className='timeline-row'>
                    <div className="timeline-content">
                      <span />
                      <p>The Basics</p>
                    </div>
                  </div>
                  <div className='timeline-row active'>
                    <div className="timeline-content">
                      <span />
                      <p>Meal Plan Style</p>
                    </div>
                  </div>
                  <div className='timeline-row last-row-timeline'>
                    <div className="timeline-content">
                      <span />
                      <p>Build Your First Plan</p>
                    </div>
                  </div>
                </div>
              </div>
              <div className="col-xs-12 col-md-6 login-sectn">
                <div>
                  <div className="row">
                    <h4 className="mb-only text-left new-step-mb">Step 2: Meal Plan Preferences</h4>
                  </div>
                  <div className="row">
                    <div className="col-md-12 form-row select-row-small mealplan-top-row">
                      <label  htmlFor="cooking_for" className={errorMessage.PEOPLE_COOKING_FOR ? 'container1 error-label' : 'container1'}>
            How many people do you plan for on a weekly basis?
                      </label>
                      <div className="btn-group bootstrap-select custm-selct mealplan-btn">

                        <select className="selectpicker show-tick form-control" onChange={(e) => { this.setState({ PEOPLE_COOKING_FOR: e.target.value }); }} value={this.state.PEOPLE_COOKING_FOR}>
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                          <option value="5">5</option>
                        </select>
                      </div>
                    </div>
                  </div>
                  <div className="row">
                    <div className="col-md-12 form-row">
                      <p
                        className={errorMessage.SPECIFIC_DIET ?
                          'inner-check-row-hdng error-label' : 'nner-check-row-hdng'}
                      >
                        Are you following a specific diet?
                      </p>
                      <div className="check-row">
                        <label className="container1 rbutton">
                          <input type="radio" name="best-describe" onChange={() => { this.setState({ SPECIFIC_DIET: 'Yes' }); }}/>
                          <span className="checkmark" /><b>Yes</b> (select your diet(s) below)
                        </label>
                      </div>
                      <div className="check-row">
                        <label className="container1 rbutton">
                          <input type="radio" name="best-describe" onChange={() => { this.setState({ SPECIFIC_DIET: 'No', DIET_TYPE: [], otherDiet: false }); }}/>
                          <span className="checkmark" /><b>No</b>
                        </label>
                      </div>
                    </div>
                  </div>
                  {SPECIFIC_DIET && SPECIFIC_DIET === 'Yes' ?
                    <div className="row">
                      <div className="divider1">
                      </div>
                      <div className="row">
                        <div className="text-left col-md-12 mb-nunito">
                          <p><span className="red-font">HEADS UP!</span> Right now, we have low carb recipes with meats & veggies.</p>
                          <p>In the future, we{`'ll`} offer meal plans for specific diets.</p>
                        </div>
                      </div>
                      <div className="divider2">
                      </div>
                      <div className="row">
                        <div className="col-md-12 form-row select-row-small label-row-full mb-boot-box-padd mb-nunito">
                          <label htmlFor="taste" className={errorMessage.DIET_TYPE ?
                            'inner-check-row-hdng error-label' : 'nner-check-row-hdng'}>
                          Let us know what type of diet you follow ans want us to add:
                          </label>
                          <div className="row select-checks-box">

                            <div className="col-md-12">
                              <div className="col-md-4 ">
                                <label className="container1">
                                  <input
                                    // checked={(DIET_TYPE === 'Low-carb')}
                                    type="checkbox"
                                    onChange={() => this.updateList('DIET_TYPE', 'Low-carb')}
                                  />
                                  <span className="checkmark1" />Low-carb
                                </label>
                              </div>
                              <div className="col-md-4 ">
                                <label className="container1">
                                  <input
                                    // checked={(DIET_TYPE === 'Ketogenic')}
                                    type="checkbox"
                                    onChange={() => this.updateList('DIET_TYPE', 'Ketogenic')}
                                  />
                                  <span className="checkmark1" />Ketogenic
                                </label>
                              </div>
                              <div className="col-md-4 ">
                                <label className="container1">
                                  <input
                                    // checked={(DIET_TYPE === 'Whole30')}
                                    type="checkbox"
                                    onChange={() => this.updateList('DIET_TYPE', 'Whole30')}
                                  />
                                  <span className="checkmark1" />Whole30
                                </label>
                              </div>
                              <div className="col-md-4 ">
                                <label className="container1">
                                  <input
                                    // checked={(DIET_TYPE === 'Paleo')}
                                    type="checkbox"
                                    onChange={() => this.updateList('DIET_TYPE', 'Paleo')}
                                  />
                                  <span className="checkmark1" />Paleo
                                </label>
                              </div>
                              <div className="col-md-4 ">
                                <label className="container1">
                                  <input
                                    // checked={(DIET_TYPE === 'Vegan')}
                                    type="checkbox"
                                    onChange={() => this.updateList('DIET_TYPE', 'Vegan')}
                                  />
                                  <span className="checkmark1" />Vegan
                                </label>
                              </div>
                              <div className="col-md-4 ">
                                <label className="container1">
                                  <input
                                    // checked={(DIET_TYPE === 'Vegetarian')}
                                    type="checkbox"
                                    onChange={() => this.updateList('DIET_TYPE', 'Vegetarian')}
                                  />
                                  <span className="checkmark1" />Vegetarian
                                </label>
                              </div>
                              <div className="col-md-4 ">
                                <label className="container1">
                                  <input
                                    // checked={(DIET_TYPE === 'Pescatarian')}
                                    type="checkbox"
                                    onChange={() => this.updateList('DIET_TYPE', 'Pescatarian')}
                                  />
                                  <span className="checkmark1" />Pescatarian
                                </label>
                              </div>
                              <div className="col-md-8 checkbox-wid-input">
                                <label className="container1">
                                  <input
                                    // checked={(DIET_TYPE === 'others')}
                                    type="checkbox"
                                    onChange={() => this.updateList('DIET_TYPE','others')}
                                  />
                                  <span className="checkmark1" />
                                   Others
                                </label>
                                {DIET_TYPE.includes('others') &&
                                <div>
                                  <input type="text" onChange={(e) => {this.setState({otherDiet: e.target.value.trim()})}}/>
                                  {errorMessage.DIET_TYPE ?
                                    <span className="error-label">Please fill other type of diet</span> :
                                    null
                                  }
                                </div>
                                }
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                    :null}
                  <div className="row">
                    <div className="form-row text-right cnfrm-btn-box col-md-12">
                      <button className="next-btn" onClick={() => this.onClickNext()}>Confirm</button>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>

    );
  }
}
