
const style = {
  design:
  `<style>
  .bordr-top-hdng {
      text-decoration:none;
      list-style:none;  
      padding-bottom:6px;
      font-size: 24px;
      font-weight: normal;
    }
    .grocery-inner-list li {
      float: left;
      width: 100%;
      font-size: 16px;
      padding: 3px;
  }
  .grocery-inner-list {
    padding: 0 0 0 20px;
    margin: 0 0 0 0;
  }
  .col-md-6 {
    display:inline;
    float:left;
    width:50%;
  }
  .grocery-list-icn{ display:none; }
  .multiple-units{ border: 0px!important;}
.listStyle {
    font-size: 14px !important;
    padding-left: 18px !important;
}
.listStyle{
  list-style:none;
  // list-style-position: inside;
}
.listStyle span {
    font-size: 39px;
    line-height: 0;
    margin: 0 5px 0 0;
    position: relative;
    top: -2px;
    visibility: visible;
}
.bullet-style { 
    visibility: hidden;
}
  </style>`,
};

export default style;
