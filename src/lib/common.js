import history from '../history'

export function onPressCancelBtn(props, dataProps){
  const {  cookingDates, cookArr, dayDates } = props
  const recipeArr = JSON.parse(localStorage.getItem('editRecipes'))
  sessionStorage.removeItem('recipeArr')
  const editWeek = dataProps && dataProps.editWeek && dataProps.editWeek[0]
  const inputs = dataProps && dataProps.inputs && dataProps.inputs
  const previousCookingDate = dataProps && dataProps.previousCookingDate && dataProps.previousCookingDate
  const defaultServingSize = dataProps && dataProps.defaultServingSize && dataProps.defaultServingSize
  sessionStorage.removeItem('filter')
  history.push('/dashboard',{ 
    recipeArr, cookingDate: cookingDates, lastStep: true, cookArr, editRecipe: true,
    servingArr: editWeek.servingArr, deleteRecipeIndex: editWeek.deleteRecipeIndex,
    cookDayArr: editWeek.cookDayArr, inputs, action:'cancel', dayDates,
    previousCookingDate,
    defaultServingSize, slideClass: 'slidelefttrue'
  })
  return null;
}

export function onPressSaveBtn(props, dataProps){
  const {  cookingDates, cookArr, recipeArr, action, dayDates} = props
  if(recipeArr.length > 0){
    const recipeIndex = dataProps && dataProps.index
    let recipes = dataProps && dataProps.recipes
    const inputs = dataProps && dataProps.inputs && dataProps.inputs
    const previousCookingDate = dataProps && dataProps.previousCookingDate && dataProps.previousCookingDate
    let cookingDate ;
    let servingSize ;
    let previousData;
    if(action === 'swap') {
      const recpe = JSON.parse(localStorage.getItem('editRecipes'))[recipeIndex]
      cookingDate = recpe.cooking_date
      servingSize = recpe.servingSize
      previousData = recpe
      const id = recpe.items.recipe.id
      recipeArr[0].items.recipe.recipe_id = recipeArr[0].items.recipe.id
      recipeArr[0].items.recipe.id = id
      recipes[recipeIndex]=recipeArr[0]
      recipes[recipeIndex].cooking_date = cookingDate
      recipes[recipeIndex].servingSize = servingSize
    } else if(action === 'add') {
      recipes = recipes.concat(recipeArr)
    }
    const editWeek = dataProps && dataProps.editWeek && dataProps.editWeek[0]
    const defaultServingSize = dataProps && dataProps.defaultServingSize && dataProps.defaultServingSize
    sessionStorage.removeItem('filter')
    history.push('/dashboard',{ 
      recipeArr: recipes, previousData, cookingDate: cookingDates, lastStep: true, cookArr, editRecipe: true,
      servingArr: editWeek.servingArr, deleteRecipeIndex: editWeek.deleteRecipeIndex, previousCookingDate,
      cookDayArr: editWeek.cookDayArr, recipeIndex, inputs, action, dayDates, defaultServingSize
    })

  } else {
    onPressCancelBtn(props, dataProps);
  }
}