
const MobileDesign = {
  design:
  `.next-back-btns { display: none; }
  .dishes-row { overflow-x: auto; -webkit-overflow-scrolling: touch; -ms-overflow-style: none;  -webkit-transform: tranzlateZ(0);   margin-bottom: -25px;
    padding-bottom: 25px;}
  .dishes-row-box {overflow:hidden;}
  ::-webkit-scrollbar { display: none; fill: #fff; background:#fff!important; color: #fff; -webkit-appearance: none;}
  .choose-recipe-btns.select-meal-btns.selected { display:block}
  .dishes-row .meal-post:hover{ transform: none;}
  @media(max-width: 767px) and (min-width: 320px) {
    .dishes-row .meal-post {
      width: 140px!important;
    }
  }
  `,
};

export default MobileDesign;