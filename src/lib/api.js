/*
common logics
*/

const api = {};
module.exports = api;


api.fetch = (method, url, params, headers) => {
  const options = {
    method,
    headers,
  };
  if (url.match(/.*api.php.*/) && method === 'POST') {
    options.body = params;
  } else if (url.match(/.*api.php.*/) && method === 'GET');
  else {
    options.body = JSON.stringify(params);
  }
  return fetch(url, options).then(response => (
    response.json()
  ))
    .catch((error) => {
    // console.log('API error:', method, url, params, error);
      const erresponse = { error: 501, description: error };
      throw (erresponse);
    }).then((responseJson) => {
      if (!responseJson.error) {
        return responseJson;
      }
      throw (responseJson);
    });
};


api.post = (url, params) => {
  const token = localStorage.getItem('token');
  const headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'Access-Control-Allow-Origin': '*',
  };
  if (token) {
    headers.Authorization = `bearer ${token}`;
  }
  return api.fetch('POST', url, params, headers);
};

api.put = (url, params) => {
  const token = localStorage.getItem('token');
  const headers = {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'Access-Control-Allow-Origin': '*',
  };
  if (token) {
    headers.Authorization = `bearer ${token}`;
  }
  return api.fetch('PUT', url, params, headers);
};

api.get = (url, params) => {
  const token = localStorage.getItem('token');
  const headers = {
    'Content-Type': 'application/json',
    Accept: 'application/json',
    'Access-Control-Allow-Origin': '*',
  };
  if (localStorage.getItem('token')) {
    headers.Authorization = `bearer ${token}`;
  }
  return api.fetch('GET', url, params, headers);
};

api.delet = (url, params) => {
  const token = localStorage.getItem('token');
  const headers = {
    'Content-Type': 'application/json',
    Accept: 'application/json',
    'Access-Control-Allow-Origin': '*',
  };
  if (localStorage.getItem('token')) {
    headers.Authorization = `bearer ${token}`;
  }
  return api.fetch('DELETE', url, params, headers);
};

api.registerGuestUser = (url, params) => {
  const data = params;

  data['p[4]'] = 4;
  data['p[5]'] = 5;
  data.tags = 'Incomplete Trial Sign Up';

  api.campaignPost('POST', 'contact_add', data);
  return api.post(url, params);
};

api.updateGuestData = data => (
  api.campaignPost('POST', 'contact_edit', data)
);

api.getGuestData = data => (
  api.campaignPost('GET', 'contact_view_email', data)
);

api.removeTag = data => (
  api.campaignPost('POST', 'contact_tag_remove', data)
);

api.campaignPost = (method, path, data) => {
  const apiKey = 'a9a19389c60f4748f0b7e6d94f7bd9570e048bbe278bf83399084a79e95cb71ba1fe65e4';
  data.api_key = apiKey;
  data.api_output = 'json';
  data.api_action = path;
  const proxyurl = 'https://cors-anywhere.herokuapp.com/';
  const searchParams = Object.keys(data).map(key => (
    `${encodeURIComponent(key)}=${encodeURIComponent(data[key])}`
  )).join('&');
  let hostURL = `https://mealplanninja.api-us1.com/admin/api.php?${searchParams}`;
  hostURL = proxyurl + hostURL;
  const urlheaders = {
    'Content-Type': 'application/x-www-form-urlencoded',
    mode: 'no-cors',
    'Access-Control-Allow-Origin': '*',
  };

  return api.fetch(method, hostURL, searchParams, urlheaders);
};
