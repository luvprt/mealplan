
const baseURL = 'https://meal-plan-ninja-api.herokuapp.com/api/';
// const baseURL = 'http://127.0.0.1:8000/api/';
// const baseURL = 'https://meal-plan-ninja-staging.herokuapp.com/api/';

const RoutingConstants = {
  login: `${baseURL}login`,
  forgotPassword: `${baseURL}password/forgot`,
  resetPassword: `${baseURL}password/reset`,
  register: `${baseURL}register`,
  verifyToken: `${baseURL}verifyResetToken/`,
  addUserWeeklySchedule: `${baseURL}addWeeklySchedule`,
  getUserWeeklySchedule: `${baseURL}GetMealPlanSchedule`,
  editUserWeeklySchedule: `${baseURL}editWeeklySchedule`,
  mealPlan: `${baseURL}mealplans`,
  mealPlanStyles: `${baseURL}user_meal_plan_settings/add`,
  fetchProteinExclusion: `${baseURL}user_meal_plan_settings`,
  fetchMealData: `${baseURL}getRecipesData`,
  updateSideDish: `${baseURL}updateSideDish`,
  dragAndDrop: `${baseURL}dragDropRecipe`,
  deleteMeal: `${baseURL}deleteWeeklySchedule/`,
  getEquipmentList: `${baseURL}getEquipmentList`,
  getSwapRecipeList: `${baseURL}getRecipeList`,
  groceryList: `${baseURL}getGroceryData`,
  fetchProfileDetail: `${baseURL}getprofileDetails`,
  updateMealData: `${baseURL}updateProfile`,
  changePassword: `${baseURL}changePassword`,
  contactSupport: `${baseURL}sendSupportEmail`,
  getSubscriptionPlan: `${baseURL}getPlans`,
  getPlanById: `${baseURL}getPlanById`,
  subscribePlan: `${baseURL}subscribe`,
  updateCard: `${baseURL}updateCard`,
  userList: `${baseURL}user?`,
  key: 'pk_live_zxiX71V9BuLDv8NCZpMRjhgA',
  newRegister: `${baseURL}register2`,
  checkEmail:  `${baseURL}emailCheck`,
  swapAllRecipe: `${baseURL}swapRecipe`,
  changeGroceryItemStatus: `${baseURL}changeGroceryStatus`,
  changeCustomGroceryItemStatus: `${baseURL}changeCustomGroceryItemStatus`,
  addCustomGroceryItem: `${baseURL}customGrocery/add`,
  getRecipes: `${baseURL}recipies`,
  getFilterAttributes: `${baseURL}getFilterAttributes`,
  getFilterRecipe: `${baseURL}filterRecipies`,

  buildPlan: `${baseURL}v1/saveDateWithRecipe`,
  getWeeklySchedule: `${baseURL}v1/getWeeklyRecipe` ,
  editWeeklySchedule: `${baseURL}v1/updateCookingRecipe`,
  userFeedback: `${baseURL}feedback`,
  socialLogin: `${baseURL}socialLogin`,
};

export default RoutingConstants;
