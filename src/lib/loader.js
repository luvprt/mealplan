import React, { Component } from 'react';

export default class Loader extends Component {
  constructor(props) {
    super(props);
    this.state = {
      cIndex: 0,
      cXpos: 0,
    };
    this.continueAnimation = this.continueAnimation.bind(this);
  }

  componentDidMount() {
    this.startAnimation();
  }

  startAnimation() {
    const { cXpos, cIndex } = this.state;
    setTimeout(() => this.continueAnimation(cXpos, cIndex), 300);
  }

  continueAnimation(cXpos, cIndex) {
    const cTotalFrames = 8;
    const cFrameWidth = 64;
    cXpos += cFrameWidth;
    // increase the index frame of our animation
    cIndex += 1;
    // compare index and totalFrames
    if (cIndex >= cTotalFrames) {
      cXpos = 0;
      cIndex = 0;
    }
    if (document.getElementById('loaderImage')) {
      document.getElementById('loaderImage').style.backgroundPosition = `${cXpos}px 0`;
    }
    // this.setState({ cXpos, cIndex });
    setTimeout(() => this.continueAnimation(cXpos, cIndex), 300);
  }

  render() {
    return (
      <div className="loaderImage" id="loaderImage" />
    );
  }
}
