/*
common logics
*/
const moment = require('moment');

const services = {};
module.exports = services;


services.validateInputs = (type, inputText) => {
  switch (type) {
  case 'string': {
    const strings = /^[A-Za-z0-9'\-\s]+$/i;
    if (inputText && inputText.match(strings)) {
      return true;
    }
    return false;
  }
  case 'alphabetics':
    if (inputText) {
      const alphabetics = /^[a-zA-Z\s]+$/i;
      return alphabetics.test(inputText);
    }
    return false;
  
  case 'hypenAlphabetics':
    if (inputText) {
      const lastName = /^[`\-a-zA-Z\s]+$/i;
      return lastName.test(inputText);
    }
    return false;

  case 'date': {
    const datePattern =
      /^((19|20)?[0-9]{2}[- /.](0?[1-9]|1[012])[- /.](0?[1-9]|[12][0-9]|3[01]))*$/;

    if (inputText) {
      return datePattern.test(inputText);
    }
    return false;
  }

  case 'number': {
    const numbers = /^[0-9]+$/i;
    return numbers.test(inputText);
  }

  case 'email': {
    const emails = /^(([^<>()\]\\.,;:\s@"]+(\.[^<>()\]\\.,;:\s@"]+)*)|(".+"))@(([0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return emails.test(inputText);
  }
  case 'phone': {
    const phoneno = /^\(?([0-9]{3})\)?[-. ]?([0-9]{3})[-. ]?([0-9]{4})$/;
    if (inputText.match(phoneno)) {
      return true;
    }
    return false;
  }
  case 'password': {
    const passwordExpression = new RegExp('^(?=.{8,})((?=.*[0-9])|(?=.*[!@#%&]))');
    if (inputText && inputText.match(passwordExpression)) {
      return true;
    }
    return false;
  }
  default:
  }
  return type;
};

services.getCurrentDate = () => {
  const today = moment().format('YYYY-MM-DD');
  return today;
};

services.getTimeStamp = (value) => {
  let min = value % 60;
  let hour = 0;
  let time;

  if (value >= 60) {
    min = value % 60;
    hour = Math.floor(value / 60);
  }
  if (min === 0) {
    time = `${hour}h`;
  } else if (hour === 0) {
    time = `${min}m`;
  } else {
    time = `${hour}h ${min}m`;
  }
  return time;
};

// compare int value

services.compareRecipeTime = (first, second) => {
  let value = null;
  if (first > second) {
    value = first;
  } else {
    value = second;
  }
  return value;
};

// get time

services.getFormatTime = (value) => {
  if (value.indexOf('m') > -1) {
    value = value.split('m');
    value = `${value[0]} mins`;
  }
  return value;
};

// calculate passive time
services.differnceTimeStamp = (first, second) => {
  let value = null;
  let tvalue;
  let hvalue;
  let fIndex = first.indexOf('h');
  let sIndex = second.indexOf('h');
  if (fIndex > -1) {
    fIndex = first.slice(0, fIndex);
    fIndex *= 60;
    first = first.split('h');
    if (first[1]) {
      first = parseInt(first[1], 0);
    } else {
      first = 0;
    }
    tvalue = parseInt(fIndex, 0) + first;
  } else {
    fIndex = first.split('m');
    tvalue = fIndex[0];
  }
  if (sIndex > -1) {
    sIndex = second.slice(0, sIndex);
    sIndex *= 60;
    second = second.split('h');
    second = second[1].split('m');
    hvalue = parseInt(sIndex, 0) + parseInt(second[0], 0);
  } else {
    sIndex = second.split('m');
    hvalue = sIndex[0];
  }
  value = tvalue - hvalue;
  return value;
};
// To get the selected week end points

services.getSelectedWeekDates = (selectedWeek, weekStartEnd) => {
  let currentWeekDates = {};
  /* for current week selection */
  if (selectedWeek === 'currentWeek') {
    const weekStart = moment().startOf('week').format('MMMM DD YYYY');
    const weekEnd = moment().endOf('week').format('MMMM DD YYYY');
    currentWeekDates = { weekStart, weekEnd, selectedWeek };
  }
  /* for previous week selection */
  if (selectedWeek === 'previousWeek') {
    const backwardLimit = moment().startOf('week').subtract(2, 'weeks').format('MMMM DD YYYY');
    let weekStart = moment(weekStartEnd.weekStart);
    let weekEnd = moment(weekStartEnd.weekEnd);

    weekStart = moment(weekStart).subtract(1, 'weeks').format('MMMM DD YYYY');
    weekEnd = moment(weekEnd).subtract(1, 'weeks').format('MMMM DD YYYY');
    /*  to get the diffrence between two weeks */
    const startDate = moment(weekStart, 'DD.MM.YYYY');
    const endDate = moment(backwardLimit, 'DD.MM.YYYY');
  
    const switchLimit = startDate.diff(endDate, 'days');

    if (switchLimit < -1);


    currentWeekDates = { weekStart, weekEnd, selectedWeek };
  }


  /* for Next week selection */
  if (selectedWeek === 'nextWeek') {
    let weekStart = moment(weekStartEnd.weekStart);
    let weekEnd = moment(weekStartEnd.weekEnd);
    weekStart = moment(weekStart).add(1, 'weeks').format('MMMM DD YYYY');
    weekEnd = moment(weekEnd).add(1, 'weeks').format('MMMM DD YYYY');
    currentWeekDates = { weekStart, weekEnd, selectedWeek };
  }
  return currentWeekDates;
};

/* get the week days of selected week */
services.getWeekDays = (weekStartEnd) => {
  const weekStart = moment(weekStartEnd.weekStart, 'MMMM DD YYYY');
  const fromDate = moment(weekStart);
  const toDate = moment(weekStart).add(6, 'days');
  const results = services.enumerateDaysBetweenDates(fromDate, toDate);
  return results;
};
services.totalValue = (id, arr, t) => {
  let count = 1;
  if (t === 0) {
    count = 0;
  }
  for (const x in arr) {
    if (arr[x] === id) {
      count += 1;
    }
  }
  return count;   
};

services.totalCook = (arr) => {
  let count = 1
  let current;
  for(let i = 0; i < arr.length; i +=1) {
    current = arr[i].data
    // cookArr = []
    for(let j = i; j < arr.length; j+=1) {
    
      if(current !== arr[j].data) {
        current = arr[j].data
        count +=1
        i = j
      }                             
    }
  }
  return count;
};

services.totalCookCount = (arr) => {
  const counts = [];
  arr.forEach((x)=> { 
    counts[x.data] = (
      counts[x.data] || 0
    )+1;
  });
  return counts;
};
/* get week days by suppling starting and ending date */
services.enumerateDaysBetweenDates = (startDate, endDate) => {
  const now = startDate;
  const dates = [];

  while (now.isSameOrBefore(endDate)) {
    const temp = [];
    temp.day = now.format('ddd');
    temp.date = now.format('DD');
    temp.Month = now.format('MM');
    temp.Year = now.format('Y');
    temp.fullDay = now.format('dddd');
    temp.fullMonth = now.format('MMMM');
    temp.halfMonth = now.format('MMM');
    dates.push(temp);
    now.add(1, 'days');
  }

  return dates;
};

services.sortByKey = (key,desc) => (a,b) => {
  let nameA = a[key]
  if(nameA) {
    nameA = nameA.toUpperCase(); // ignore upper and lowercase
  } else {
    nameA = 'Z'
  }
  let nameB = b[key] // ignore upper and lowercase
  if(nameB) {
    nameB = nameB.toUpperCase(); // ignore upper and lowercase
  } else {
    nameB = 'Z'
  }
  if (desc === 'asc') {
    if (nameA < nameB) {
      return -1;
    }
    if (nameA > nameB) {
      return 1;
    }
  }else {
    if (nameA < nameB) {
      return 1;
    }
    if (nameA > nameB) {
      return -1;
    }
  }
  return 0;
}

services.formatTimeStamp = (totalTime) => {
  if(totalTime) {
    let index = totalTime.indexOf('h')
    let tTime=[];
    if(index > -1) {
      tTime = totalTime.split('h')
      tTime[0] = parseInt(tTime[0],0)
      tTime[0] *=60;
    }
    if(tTime.length > 1) {
      tTime[1] = tTime[1].split('m')[0]
      tTime[1] = parseInt(tTime[1],0)
      tTime[1] += tTime[0]
      return tTime[1]
    } 
    index = totalTime.indexOf('m')
    if(index > -1) {
      tTime = totalTime.split('m')[0]
      return parseInt(tTime,0)
    }
    return tTime[0]; 
  }
  return 0
}

