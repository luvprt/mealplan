/*
Component Name: App component
External Modules: react-router-page-transition,react-router-dom
Default screen: Home
*/

import 'font-awesome/css/font-awesome.min.css';
import 'intro.js/introjs.css';
import React from 'react';
import {Router, Switch, Route } from 'react-router-dom';
import Register from './components/register/Register';
import Onboarding from './components/register/Onboarding_slider';
import SelectMealDays from './components/plans/SelectMealDays';
import SelectCookDays from './components/plans/SelectCookDays';
import NewRegister from './components/newRegister/Register';
import Login from './components/login/Login';
import ForgetPassword from './components/login/ForgetPassword';
import ResetPassword from './components/login/ResetPassword';
import Dashboard from './components/dashboard/Dashboard';
import Profile from './components/Profile/Settings';
import RecipeDashboard from './components/recipeList/RecipeDashboard';
import SelectRecipe from './components/recipeList/SelectRecipe';
import AdminPage from './components/UserList/AdminPage';
import history from './history';
import ViewMeal from './components/dashboard/ViewMeal';
import PrintPlan from './components/printPlan/PrintPlan';

const App = () => {
  window.analytics.page();
  return(
    <Router history={history}>
      <Switch >
        <Route exact path="/" component={Login} />
        <Route exact path="/register" component={Register} />
        <Route exact path="/onboarding" component={Onboarding} />
        <Route exact path="/selectMeals" component={SelectMealDays} />

        <Route exact path="/register2" component={NewRegister} />
        <Route path="/login" component={Login} />
        <Route exact path="/checkUser" component={Login} />
        <Route path="/adminDashboard" component={AdminPage} />
        <Route path="/forgetPassword" component={ForgetPassword} />
        <Route path="/resetPassword/:id?" component={ResetPassword} />
        <Route path="/dashboard" key="dashboard" component={Dashboard} />
        <Route path="/groceryList" key="grocery" component={Dashboard} />
        <Route path="/printPlan" key="printPlan" component={PrintPlan} />
        <Route path="/recipeList" component={RecipeDashboard} />
        <Route path="/profileSettings" key="profileSettings" component={Profile} />
        <Route path="/support" key="support" component={Profile} />
        <Route path="/billingDetails" key="billingDetails" component={Profile} />
        <Route path="/viewRecipe" component={ViewMeal} />
        <Route path="/chooseRecipe" component={SelectRecipe} />
        <Route path="/setCookSchedule" component={SelectCookDays} />
      </Switch>
     
    </Router>
  )
};


export default App;
